<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

// Route::middleware('auth:api')->get('/user', function (Request $request) {
//     return $request->user();
// });

// Route::apiResource('market', 'API\MarketController');
// Route::apiResource('sector', 'API\SectorController');
// Route::apiResource('stand', 'API\StandController');
// Route::apiResource('driver', 'API\DriverController');
// Route::apiResource('gyre', 'API\GyreController');
// Route::apiResource('rate', 'API\RateController');
// Route::apiResource('agent', 'API\AgentController'); 

Route::post("uploadfile",'API\FileController@uploadfile');
Route::get("stand/account-status-by-month",'API\StandController@getAccountStatusByMonth');
Route::get("stand/titular/{titular_id}",'API\StandController@getStandsByClientID'); // lista el estado de cuenta de un puesto por año
Route::get("stand/{id}/stand-with-debts/{paymednt_status}",'API\StandController@getStandWithDebts');
Route::get("stand/{id}/bring-debts-to-date/{paymednt_status}",'API\StandController@getBringDebtsToDate');
Route::get("stand/list-stand-with-titular",'API\StandController@getListStandWithTitular');
Route::delete("transaction/detail/{id}",'API\TransactionController@removeDetail'); // Actualiza el estado del detalle de la trasacción
Route::get("transaction/details-by-concepts",'API\TransactionController@allDetailsByConcepts'); // Connsulta los pagos detalladas y se puede filtrar por concepto
Route::get("transaction/calculate-total-by-concept",'API\TransactionController@calculateTotalByConcept'); // Connsulta los pagos detalladas y se puede filtrar por concepto
Route::get("transaction/calculate-total-by-sector",'API\TransactionController@calculateTotalBySector'); // Suma los pagos por sector
Route::put("transaction/{id}/delete",'API\TransactionController@delete'); // Anula la transacción
Route::get("transaction/resumen-by-concept",'API\TransactionController@resumenByConcept'); // 
Route::get("transaction/group-by-month",'API\TransactionController@groupByMonth'); // 
Route::get("debt/{id}/transaction/",'API\DebtController@getTransaction'); // Obtiene l información de la transacción por debt_id
Route::get("debt/{type}",'API\DebtController@index'); // listas las deudas por tipo
Route::get("debt/{id}/account-status/{paymednt_status}",'API\DebtController@getAccountStatusByTitular'); // lista el estado de cuenta de una persona por año
Route::get("debt/for_coactive/client/{client_id}",'API\DebtController@getDebtsForCoactive'); // lista el estado de cuenta de una persona por año
Route::post("debt/multiple",'API\DebtController@multipleStore'); // guardado en bloque de deudas
Route::get('person/verify-duplicate-dni/{dni}','API\PersonController@verifyDuplicateDNI');
Route::get("market/{market_id}/generatedebts",'API\MarketController@generateDebts');
Route::get("market/account-status-by-year",'API\MarketController@allAccountStatusByYear'); // lista el estado de cuenta de los mercados por año
Route::get("market/account-status-by-month",'API\MarketController@getAccountStatusByMonth'); // lista el estado de cuenta de un mercado por mes
Route::get("market/account-status-by-stands",'API\MarketController@getAccountStatusByStands'); // lista el estado de cuenta de los puestos de un mercado por año
Route::get("market/name-years-of-debts",'API\MarketController@getNameYearsOfDebt'); // lista el estado de cuenta de los puestos de un mercado por año
Route::get('user/{id}/validate-code/{code}', 'API\UserController@validateCode');
Route::get('user/{user_id}/can_management_roles/', 'API\UserController@can_management_roles');
Route::get('user/{user_id}/permissions', 'API\UserController@permissions');
Route::post('user/{user_id}/permissions', 'API\UserController@savePermissions');
Route::get('user/menu', 'API\UserController@menu');
Route::get('user/commission-agent', 'API\UserController@allCommissionAgent');
Route::get('user/cashiers-and-commission-agent', 'API\UserController@cashiersAndCommissionAgent');
Route::get('person/debts', 'API\PersonController@getPeopleWithDebts');
Route::get('person/list-people-for-view-payments', 'API\PersonController@listPeopleForViewPayments');
Route::get('sector/by-market/{market_id}', 'API\SectorController@all_by_market');
Route::put("fractionary/{id}/activate-deactivate",'API\FractionaryController@activateDeactivate');
Route::get('fractionary/{fractionary_id}/schedule', 'API\FractionaryController@listSchedule');
Route::post('fractionary/forgive-debts', 'API\FractionaryController@forgiveDebts');
Route::get('gyre/by-market/{market_id}', 'API\GyreController@all_by_market');
Route::post('user/change-password', 'API\UserController@saveChangePasswordUser');
Route::post('notification/send-individual-mail', 'API\NotificationController@sendMail');

// print 

Route::get('transaction/print-payment-voucher', 'API\TransactionController@printPaymentVoucher');   
Route::get('user/role', 'API\UserController@list_roles');
Route::middleware('auth.api')->get('user/access', 'API\UserController@list_access');   

Route::get('izipay/createToken', 'API\IzipayController@createToken');   
Route::get('culqi/createOrder', 'API\CulqiController@createOrder');

Route::apiResources([
    'market' => 'API\MarketController',
    'sector' => 'API\SectorController',
    'stand' => 'API\StandController',
    'driver' => 'API\DriverController',
    'gyre' => 'API\GyreController',
    'rate' => 'API\RateController',
    'agent' => 'API\CobratorTicketController',
    'person' => 'API\PersonController',
    'user' => 'API\UserController',
    'responsability' => 'API\ResponsabilityController',
    'file' => 'API\FileController',
    'transaction' => 'API\TransactionController',
    'debt' => 'API\DebtController',
    'cashbox' => 'API\CashboxController',
    'fractionarytype' => 'API\FractionaryTypeController',
    'fractionary' => 'API\FractionaryController',
    'concept' => 'API\ConceptController',
    'setting' => 'API\SettingController',
    'uit' => 'API\UITController',
    'coactive'=>'API\CoactiveController',
    'notification'=>'API\NotificationController',
    'company'=>'API\CompanyController',
    'reversion'=>'API\ReversionController',
    'forgiveness'=>'API\ForgivenessController',
]);


