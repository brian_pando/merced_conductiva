<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () { return view('home'); })->middleware('auth');
Route::get('/h', function (Request $request) { dd( request()->server('HTTP_HOST') ); return view('home'); });

Auth::routes();
// Route::get('/', function () { return redirect('home'); });
//Route::get('/', 'HomeController@index')->name('home');

// ########################   Exports   ########################
Route::get('cashbox/export-pdf', 'API\CashboxController@exportPdf');
Route::get('cashbox/export-excel', 'API\CashboxController@exportExcel');
Route::get('transaction/export-pdf', 'API\TransactionController@exportPdf');
Route::get('transaction/export-excel', 'API\TransactionController@exportExcel');
Route::get('transaction/detailed-export-excel', 'API\TransactionController@detailedExportExcel');
Route::get('transaction/detailed-export-pdf', 'API\TransactionController@detailedExportPdf');
Route::post('transaction/listgroupbymonth-export-pdf', 'API\TransactionController@listGroupByMonthExportPdf');
Route::post('transaction/listCommission-export-excel', 'API\TransactionController@listCommissionExportExcel');
Route::post('transaction/listCommission-export-pdf', 'API\TransactionController@listCommissionExportPdf');
Route::post('transaction/listgroupbymonth-export-excel', 'API\TransactionController@listGroupByMonthExportExcel');
Route::get('transaction/detailed-payments-export-pdf', 'API\TransactionController@detailedPaymentsExportPdf');
Route::get('transaction/detailed-payments-export-excel', 'API\TransactionController@detailedPaymentsExportExcel');
Route::post('transaction/print-commission-collection-pdf', 'API\TransactionController@printCommissionCollection');
Route::post('market/status-account-export-pdf', 'API\MarketController@exportStatusAccountPdf');
Route::post('market/status-account-export-excel', 'API\MarketController@exportStatusAccountExcel');
Route::get('market/status-account-by-stands-export-pdf', 'API\MarketController@exportStatusAccountByStandsPdf');
Route::get('market/status-account-by-stands-export-excel', 'API\MarketController@exportStatusAccountByStandsExcel');
Route::post('market/select-row-market-export-pdf', 'API\MarketController@exportselectRowMarketPdf');
Route::post('market/select-row-market-export-excel', 'API\MarketController@exportselectRowMarketExcel');
Route::get('debt/export-pdf', 'API\DebtController@exportPdf');
Route::get('debt/export-excel', 'API\DebtController@exportExcel');
Route::post('fractionary/export-pdf', 'API\FractionaryController@exportPdf');
Route::post('fractionary/export-excel', 'API\FractionaryController@exportExcel');
Route::get('fractionary/{id}/export-pdf', 'API\FractionaryController@pdf');
Route::get('notification/{id}/pdf', 'API\NotificationController@pdf');
Route::post('notification/all-pdf', 'API\NotificationController@allPdf');
Route::post('stand/print-account-status-pdf', 'API\StandController@printAccountStatus');
Route::post('stand/print-debts-for-stand-pdf', 'API\StandController@printDebtsForStand');
Route::get('admin/change-password', 'API\UserController@changePasswordUser');
Route::post('market/status-account-by-month-export-excel', 'API\MarketController@exportStatusAccountByMonthExcel');
Route::post('market/status-account-by-month-export-pdf', 'API\MarketController@exportStatusAccountByMonthPdf');



Route::any('{all}', function () {return view('home');})->where(['all' => '.*']);
