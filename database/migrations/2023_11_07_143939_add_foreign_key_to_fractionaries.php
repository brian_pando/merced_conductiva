<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddForeignKeyToFractionaries extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('fractionaries', function (Blueprint $table) {
            $table->unsignedBigInteger('new_titular_id');
            $table->unsignedBigInteger('new_concept_id');
            $table->unsignedBigInteger('new_representative_id');
        });

        // Copiar los datos a las nuevas columnas
        DB::statement('UPDATE fractionaries SET new_titular_id = titular_id');
        DB::statement('UPDATE fractionaries SET new_concept_id = concept_id');
        DB::statement('UPDATE fractionaries SET new_representative_id = representative_id');
        // Eliminar las columnas originales
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
       
        Schema::table('fractionaries', function (Blueprint $table) {
            $table->dropColumn('new_titular_id');
            $table->dropColumn('new_concept_id');
            $table->dropColumn('new_representative_id');
        });
    }
}
