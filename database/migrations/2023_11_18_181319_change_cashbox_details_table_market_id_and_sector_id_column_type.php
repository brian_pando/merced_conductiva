<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class ChangeCashboxDetailsTableMarketIdAndSectorIdColumnType extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('cashbox_details', function (Blueprint $table) {
            $table->unsignedBigInteger('sector_id')->nullable()->change();
            $table->unsignedBigInteger('market_id')->nullable()->change();
            $table->foreign('sector_id')->references('id')->on('sectors');
            $table->foreign('market_id')->references('id')->on('markets');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('cashbox_details', function (Blueprint $table) {
            $table->dropForeign(['cashbox_details_sector_id_foreign','cashbox_details_market_id_foreign']);
            $table->integer('sector_id')->change();
            $table->integer('market_id')->change();
        });
    }
}
