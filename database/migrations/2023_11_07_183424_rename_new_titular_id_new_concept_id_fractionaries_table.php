<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class RenameNewTitularIdNewConceptIdFractionariesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('fractionaries', function (Blueprint $table) {
            $table->renameColumn('new_titular_id', 'titular_id');
            $table->renameColumn('new_concept_id', 'concept_id');
            $table->renameColumn('new_representative_id', 'representative_id');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        $table->renameColumn('titular_id', 'new_titular_id');
        $table->renameColumn('concept_id', 'new_concept_id');
        $table->renameColumn('representative_id', 'new_representative_id');
    }
}
