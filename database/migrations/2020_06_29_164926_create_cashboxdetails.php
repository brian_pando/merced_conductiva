<?php 
use Illuminate\Support\Facades\Schema;

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;
//Generated By PlantUML Command
class CreateCashboxdetails extends Migration{
	public function up(){ 
 		Schema::create('cashbox_details', function (Blueprint $table) { 
			$table->bigIncrements('id');
			$table->float('total');
			$table->string('code');
			$table->string('operation_code');
			$table->unsignedBigInteger('concept_id')->nullable();
			$table->string('concept_code')->nullable();
			$table->integer('sector_id')->nullable();
			$table->string('sector_code')->nullable();
			$table->integer('market_id')->nullable();
			$table->string('market_code')->nullable();
			$table->string('debt_type')->nullable();
			$table->foreign('concept_id')->references('id')->on('concepts')->onDelete("cascade");
			$table->unsignedBigInteger('cashbox_id')->nullable();
            $table->foreign('cashbox_id')->references('id')->on('cashboxes')->onDelete("cascade");
			$table->timestamps();
		});
 	} 
	public function down(){
 
	} 
}