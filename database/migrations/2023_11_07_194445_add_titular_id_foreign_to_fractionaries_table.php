<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddTitularIdForeignToFractionariesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('fractionaries', function (Blueprint $table) {
            $table->foreign('titular_id')->references('id')->on('people');
            $table->foreign('concept_id')->references('id')->on('concepts');
            $table->foreign('representative_id')->references('id')->on('people');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('fractionaries', function (Blueprint $table) {
            $table->dropForeign(['titular_id','concept_id','representative_id']);
        });
    }
}
