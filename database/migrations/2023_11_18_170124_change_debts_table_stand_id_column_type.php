<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class ChangeDebtsTableStandIdColumnType extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        DB::statement('update debts set stand_id = null where id in (SELECT id FROM debts WHERE stand_id NOT IN (SELECT id FROM stands));');

        Schema::table('debts', function (Blueprint $table) {
            $table->unsignedBigInteger('stand_id')->nullable()->change();
            $table->foreign('stand_id')->references('id')->on('stands');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('debts', function (Blueprint $table) {
            $table->dropForeign('debts_stand_id_foreign');
        });

        DB::statement('update debts set stand_id = 0 where id in (SELECT id FROM debts WHERE stand_id NOT IN (SELECT id FROM stands));');

        Schema::table('debts', function (Blueprint $table) {
            $table->integer('stand_id')->change();
        });
    }
}
