<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class ChangeFractionariesTableStandIdAndUserIdColumnType extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('fractionaries', function (Blueprint $table) {
            $table->unsignedBigInteger('stand_id')->nullable()->change();
            $table->integer('user_id')->nullable()->change();
        });

        DB::statement('update fractionaries set user_id = null where id in (SELECT id FROM fractionaries WHERE user_id NOT IN (SELECT id FROM users))');

        Schema::table('fractionaries', function (Blueprint $table) {
            $table->unsignedBigInteger('user_id')->nullable()->change();
            $table->foreign('stand_id')->references('id')->on('stands');
            $table->foreign('user_id')->references('id')->on('users');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('fractionaries', function (Blueprint $table) {
            $table->dropForeign('fractionaries_stand_id_foreign');
            $table->dropForeign('fractionaries_user_id_foreign');
            $table->integer('stand_id')->change();
            $table->integer('user_id')->change();
        });

        DB::statement('update fractionaries set user_id = 5 where id in (SELECT id FROM fractionaries WHERE user_id NOT IN (SELECT id FROM users))');
    }
}
