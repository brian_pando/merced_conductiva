<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class UpdateTransactionDetailsTableTransactionIdColumnType extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('transaction_details', function (Blueprint $table) {
            $table->integer('transaction_id')->nullable()->change();
        });
        DB::statement('update transaction_details set transaction_id = null where id in (SELECT id FROM transaction_details WHERE transaction_id NOT IN (SELECT id FROM transactions));');
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        DB::statement('update transaction_details set transaction_id = 0 where id in (SELECT id FROM transaction_details WHERE transaction_id NOT IN (SELECT id FROM transactions));');
        Schema::table('transaction_details', function (Blueprint $table) {
            $table->integer('transaction_id')->nullable(false)->change();
        });
    }
}
