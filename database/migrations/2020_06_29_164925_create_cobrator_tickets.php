<?php 
use Illuminate\Support\Facades\Schema;

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;
//Generated By PlantUML Command
class CreateCobratorTickets extends Migration{
	public function up(){ 
 		Schema::create('cobrator_tickets', function (Blueprint $table) { 
			$table->bigIncrements('id');
			$table->string('code');
			$table->string('file')->nullable();
			$table->date('contract_start')->nullable();
			$table->date('contract_end')->nullable();
			$table->unsignedBigInteger('responsable_id');
			$table->foreign('responsable_id')->references('id')->on('people')->onDelete("cascade");
			$table->integer('status')->default(1);
			$table->timestamps();
		});
 	} 
	public function down(){
 
	} 
}