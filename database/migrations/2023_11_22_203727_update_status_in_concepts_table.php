<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class UpdateStatusInConceptsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('concepts', function (Blueprint $table) {
              // Cambiar el valor de 'status' a 0 donde 'id' sea igual a 12
        DB::table('concepts')->where('id', 12)->update(['status' => 0]);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('concepts', function (Blueprint $table) {
           DB::table('concepts')->where('id', 12)->update(['status' => 1]);
        });
    }
}
