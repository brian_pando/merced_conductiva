<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class UpdateDebtsColFractionaryScheduleIdDebtsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        DB::statement('UPDATE debts set fractionary_schedule_id=null where id in (432769,432770,432771,432772,432773,432774,432775,432776,432777,432778,432779,432780)');
        Schema::table('debts', function (Blueprint $table) {
          
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        DB::statement('UPDATE debts set fractionary_schedule_id= 1970 where id =432769');
        DB::statement('UPDATE debts set fractionary_schedule_id= 1971 where id =432770');
        DB::statement('UPDATE debts set fractionary_schedule_id= 1972 where id =432771');
        DB::statement('UPDATE debts set fractionary_schedule_id= 1973 where id =432772');
        DB::statement('UPDATE debts set fractionary_schedule_id= 1974 where id =432773');
        DB::statement('UPDATE debts set fractionary_schedule_id= 1975 where id =432774');
        DB::statement('UPDATE debts set fractionary_schedule_id= 1976 where id =432775');
        DB::statement('UPDATE debts set fractionary_schedule_id= 1977 where id =432776');
        DB::statement('UPDATE debts set fractionary_schedule_id= 1978 where id =432777');
        DB::statement('UPDATE debts set fractionary_schedule_id= 1979 where id =432778');
        DB::statement('UPDATE debts set fractionary_schedule_id= 1980 where id =432779');
        DB::statement('UPDATE debts set fractionary_schedule_id= 1981 where id =432780');
        Schema::table('debts', function (Blueprint $table) {
            //
        });
    }
}
