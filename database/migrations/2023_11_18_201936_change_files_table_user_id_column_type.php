<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class ChangeFilesTableUserIdColumnType extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('files', function (Blueprint $table) {
            $table->integer('user_id')->nullable()->change();
        });

        DB::statement('update files set user_id = null where id in (SELECT id FROM files WHERE user_id NOT IN (SELECT id FROM users))');

        Schema::table('files', function (Blueprint $table) {
            $table->unsignedBigInteger('user_id')->nullable()->change();
            $table->foreign('user_id')->references('id')->on('users');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('files', function (Blueprint $table) {
            $table->dropForeign('files_user_id_foreign');
            $table->integer('user_id')->change();
        });

        DB::statement('update files set user_id = 5 where id in (SELECT id FROM files WHERE user_id NOT IN (SELECT id FROM users))');
    }
}
