<?php

use App\Models\Debt;
use App\Models\User;
use App\Models\Transaction;
use Faker\Generator as Faker;

/** @var \Illuminate\Database\Eloquent\Factory $factory */
$factory->define(Transaction::class, function (Faker $faker) {
    $transactiondetails = [];
    for ($i=0; $i < $faker->numberBetween($min = 1, $max = 10); $i++) { 
        $debt = Debt::query()->inRandomOrder()->first();
        array_push($transactiondetails,[
            'amount' => $i+1,
            'detail' => $debt->code,
            'total'  => $debt->total,
            'debt_id' => $debt->id,
        ]);
    }
    
    return [
        // 'code'                  => $faker->ean8,
        'date_at'               => $faker->date($format = 'Y-m-d h:m:s', $max = 'now'),
        'total'                 => array_reduce($transactiondetails, function($carry, $item){
            return $carry += $item['total'];
        },0),
        'client_id'             =>1,
        // 'transactiondetails'    => $transactiondetails,
        'user_id'               => User::query()->inRandomOrder()->first()->id,
    ];
});
