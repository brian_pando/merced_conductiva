<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Models\Person;
use Faker\Generator as Faker;

$factory->define(Person::class, function (Faker $faker) {
    return [
        'name'              => $faker->name($gender = 'male'|'female'),
        'lastname'              => $faker->lastName,
        'birth'              => $faker->date($format = 'Y-m-d h:m:s', $max = 'now'),
        'dni'              => $faker->randomNumber($nbDigits = 7, $strict = false),
        'phone1'          => $faker->e164PhoneNumber,
        'phone2'          => $faker->e164PhoneNumber,
        'address1'        => $faker->streetAddress,
        'address2'        => $faker->streetAddress,
        'user_id'         => null,
    ];
});
