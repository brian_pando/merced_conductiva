<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Models\Debt;
use Faker\Generator as Faker;

$factory->define(Debt::class, function (Faker $faker) {
    return [
        'date_at'                => $faker->date($format = 'Y-m-d h:m:s', $max = 'now'),
        'type'                   => 'normal' ,
        'ref_code'               => null,
        'code'                   => 'DEBT-MUNI',
        'description'            => null,
        'code_manual'            => null,
        'total'                  => $faker->numberBetween($min = 10, $max = 50), 
        'previous_total'         => null,
        'payment'                => 0,
        'observation'            => null,
        'is_last_schedule_debt'  => 0,
        'comment'                => null,
        'payment_date'           => null,
        'payment_status'         => 0,
        'stand_id'               => $faker->numberBetween($min = 1, $max = 5),
        'concept_id'             => $faker->numberBetween($min = 1, $max = 5),
        'previous_client_id'     => null,
        'client_id'              => $faker->numberBetween($min = 1, $max = 10),
        'user_id'                => 4,
        'fractionary_schedule_id'=> null,
        'fractionary_id'         => null,
        'coactive_id'            => null,
    ];
});
