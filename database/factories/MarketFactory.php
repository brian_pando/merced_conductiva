<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Models\Market;
use Faker\Generator as Faker;

$factory->define(Market::class, function (Faker $faker) {
    return [
        'code'              => $faker->ean8,
        'name'              => $faker->company,
        'type'              => $faker->lastName,
        'address'        => $faker->streetAddress,
        'photo'         => $faker->imageUrl($width = 640, $height = 480),
    ];
});
