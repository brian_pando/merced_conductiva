<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Models\Stand;
use Faker\Generator as Faker;

$factory->define(Stand::class, function (Faker $faker) {
    return [
        'code'                  => $faker->ean8,
        'location'              => $faker->streetAddress,
        'photo'                 => $faker->imageUrl($width = 640, $height = 480),
        'adjudication'          => null,
        'description'           => $faker->realText($maxNbChars = 200, $indexSize = 2),
        'name'                  => null,
        'status'                => 1,
        'rate_amount'           => $faker->numberBetween($min = 10, $max = 50),
        'rate_amount_secondary' => 0,
        'previous_titular_id'   => null,
        'titular_id'            => $faker->numberBetween($min = 1, $max = 5),
        'sector_id'             => $faker->numberBetween($min = 1, $max = 5),
        'market_id'             => $faker->numberBetween($min = 1, $max = 5),
        'gyre_id'               => $faker->numberBetween($min = 1, $max = 5),
        'rate_id'               => $faker->numberBetween($min = 1, $max = 5),
    ];
});
