<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Models\User;
use Faker\Generator as Faker;

$factory->define(User::class, function (Faker $faker) {
    return [
        'code'                  => $faker->ean8,
        'name'              => $faker->name($gender = 'male'|'female'),
        'username'          => $faker->firstName($gender = 'male'|'female'),
        'porcentage'        => $faker->randomDigit,
        'position'          => $faker->word,
        'role_id'           => $faker->numberBetween($min = 1, $max = 7),
        'photo'             => $faker->imageUrl($width = 640, $height = 480),
        'email'             => $faker->email,
        'email_verified_at' => $faker->email,
        'password'          => $faker->password,
        'status'            => 1,
        'company_id'        => 1,
        'remember_token'    => $faker->uuid,
    ];
});
