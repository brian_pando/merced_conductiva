<?php

use Illuminate\Database\Seeder;

class GyreSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('gyres')->insert(['name' => '-', 'code' => '-']);
        DB::table('gyres')->insert(['name' => 'Zapatos', 'code' => 'GYRE-01']);
        DB::table('gyres')->insert(['name' => 'Frutas', 'code' => 'GYRE-02']);
        DB::table('gyres')->insert(['name' => 'Verduras', 'code' => 'GYRE-03']);
        DB::table('gyres')->insert(['name' => 'Carnes', 'code' => 'GYRE-04']);
        DB::table('gyres')->insert(['name' => 'Comidas', 'code' => 'GYRE-05']);
        DB::table('gyres')->insert(['name' => 'Ropas', 'code' => 'GYRE-06']);
        DB::table('gyres')->insert(['name' => 'Maletas', 'code' => 'GYRE-07']);
        DB::table('gyres')->insert(['name' => 'Electronica', 'code' => 'GYRE-08']);
        DB::table('gyres')->insert(['name' => 'Costura', 'code' => 'GYRE-09']);
        DB::table('gyres')->insert(['name' => 'Abarrotes', 'code' => 'GYRE-10']);
        DB::table('gyres')->insert(['name' => 'Postres', 'code' => 'GYRE-11']);
        DB::table('gyres')->insert(['name' => 'Dulces', 'code' => 'GYRE-12']);
    }
}
