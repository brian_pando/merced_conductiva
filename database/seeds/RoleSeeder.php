<?php

use Illuminate\Database\Seeder;

class RoleSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('roles')->insert(['name' => 'su_admin']);
        DB::table('roles')->insert(['name' => 'ti_admin']);
        DB::table('roles')->insert(['name' => 'rt_admin']);
        DB::table('roles')->insert(['name' => 'ca_admin']);
        DB::table('roles')->insert(['name' => 'rt_auxil']);
        DB::table('roles')->insert(['name' => 'rt_cobra']);
        DB::table('roles')->insert(['name' => 'ca_cashb']);
    }
}
