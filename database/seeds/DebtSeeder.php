<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use App\Repository\DebtRepository;

class DebtSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $model = new \App\Models\Debt;
        $stand_model = new \App\Models\Stand;
        $debtRepository = new DebtRepository($model, $stand_model);
        $csvFiles = array();

        array_push($csvFiles,public_path().'/csv/deudas_kioskos_merced_1998_2020.csv'); // No se agrego la lista kioskos, no esta ordenado su excel
		array_push($csvFiles,public_path().'/csv/deudas_model_cosmos_merced_2008_2020.csv');
        array_push($csvFiles,public_path().'/csv/deudas_model_vivan_merced_2000_2020.csv');
        array_push($csvFiles,public_path().'/csv/deudas_model_nuevo_merced_1998_2020.csv'); 

        array_push($csvFiles,public_path().'/csv/deudas_model_antiguo_merced_1998_2020.csv');
        array_push($csvFiles,public_path().'/csv/deudas_model_ipd_merced_2008_2020.csv'); 
        array_push($csvFiles,public_path().'/csv/deudas_sisa_sisa_2005_2020.csv');
        array_push($csvFiles,public_path().'/csv/deudas_sisa_callao_2005_2020.csv');
       
        array_push($csvFiles,public_path().'/csv/deudas_tupac_merced_2003_2020.csv');
        
        $data = array();
        foreach($csvFiles as $i => $item){
            if(!file_exists($item) || !is_readable($item)) return FALSE;   
            $header = null;
            if (($handle = fopen($item, 'r')) !== FALSE){
                while (($row = fgetcsv($handle, 1000, $delimiter=',')) !== FALSE){
                    $row = array_map(function($item){return trim($item);},$row);
                    if(!$header) $header = $row;
                    else {$data[] = array_combine($header,  $row);}
                    //echo json_encode( $data);
                    /* echo json_encode( $data); */
                }
                fclose($handle);
            }
        }
        // recorremos el csv
        foreach($data as $i => $item){
            // obtenemos el puesto para asignar las deudas
            $stand = \App\Models\Stand::
                leftjoin('markets','markets.id','=','stands.market_id')
                ->leftjoin('sectors','sectors.id','=','stands.sector_id')
                ->where(['stands.code'=>trim($item['codigo_puesto']),'markets.code'=>trim($item['codigo_mercado']),'sectors.code'=>trim($item['codigo_sector'])])
                ->selectRaw('stands.*')
                ->with('market')
                ->with('sector')
                ->first();

            if((array)$stand)  {
                // ubicado el puesto recorremos todas las deudas por meses
                foreach ($item as $clave => $valor) {
					$GTM = new \DateTimeZone('America/Lima');
                    $payload_debt = [
                        'type' => trim($item['tipo']),
                        'concept_id' => $item['codigo_mercado'] == 'sisa'  ?  \App\Util::$concepts['SIS_ID']: \App\Util::$concepts['ME_ID'],  // tupac, model -> Merced Conductiva  sisa -> Sisa
                        'stand_id' => $stand->id,
                        'client_id' => $stand->titular_id
                    ];
                    $concept = DB::table('concepts')->find($payload_debt['concept_id']);
                    switch($clave) {
                        case 'enero':
                            $payload_debt['date_at'] = "{$item['year']}-01-01";
                        	$payload_debt['total'] = $this->add_total(trim($item['tarifa']),$payload_debt['concept_id'],cal_days_in_month(CAL_GREGORIAN, 1, $item['year']));
                            $payload_debt['payment'] = $item['enero'];
                            $payload_debt['code'] = $debtRepository->newCodeMonthly($stand,(array)$concept,new \DateTime($payload_debt['date_at']));
                            $payload_debt['payment_status'] = $this->add_payment_status($item['tarifa'],$item['enero'],$payload_debt['concept_id'],cal_days_in_month(CAL_GREGORIAN, 1, $item['year']));
                            \App\Models\Debt::create($payload_debt);
                            break;
                        case 'febrero':
                            $payload_debt['date_at'] = "{$item['year']}-02-01";
                        	$payload_debt['total'] = $this->add_total(trim($item['tarifa']),$payload_debt['concept_id'],cal_days_in_month(CAL_GREGORIAN, 2, $item['year']));
                            $payload_debt['payment'] = $item['febrero'];
                            $payload_debt['code'] = $debtRepository->newCodeMonthly($stand,(array)$concept,new \DateTime($payload_debt['date_at']));
                            $payload_debt['payment_status'] = $this->add_payment_status($item['tarifa'],$item['febrero'],$payload_debt['concept_id'],cal_days_in_month(CAL_GREGORIAN, 2, $item['year']));
                            \App\Models\Debt::create($payload_debt);
                            break;
                        case 'marzo':
                            $payload_debt['date_at'] = "{$item['year']}-03-01";
                        	$payload_debt['total'] = $this->add_total(trim($item['tarifa']),$payload_debt['concept_id'],cal_days_in_month(CAL_GREGORIAN, 3, $item['year']));
                            $payload_debt['payment'] = $item['marzo'];
                            $payload_debt['code'] = $debtRepository->newCodeMonthly($stand,(array)$concept,new \DateTime($payload_debt['date_at']));
                            $payload_debt['payment_status'] = $this->add_payment_status($item['tarifa'],$item['marzo'],$payload_debt['concept_id'],cal_days_in_month(CAL_GREGORIAN, 3, $item['year']));
                            \App\Models\Debt::create($payload_debt);
                            break;
                        case 'abril':
                            $payload_debt['date_at'] = "{$item['year']}-04-01";
                        	$payload_debt['total'] = $this->add_total(trim($item['tarifa']),$payload_debt['concept_id'],cal_days_in_month(CAL_GREGORIAN, 4, $item['year']));
                            $payload_debt['payment'] = $item['abril'];
                            $payload_debt['code'] = $debtRepository->newCodeMonthly($stand,(array)$concept,new \DateTime($payload_debt['date_at']));
                            $payload_debt['payment_status'] = $this->add_payment_status($item['tarifa'],$item['abril'],$payload_debt['concept_id'],cal_days_in_month(CAL_GREGORIAN, 4, $item['year']));
                            \App\Models\Debt::create($payload_debt);
                            break;
                        case 'mayo':
                            $payload_debt['date_at'] = "{$item['year']}-05-01";
                        	$payload_debt['total'] = $this->add_total(trim($item['tarifa']),$payload_debt['concept_id'],cal_days_in_month(CAL_GREGORIAN, 5, $item['year']));
                            $payload_debt['payment'] = $item['mayo'];
                            $payload_debt['code'] = $debtRepository->newCodeMonthly($stand,(array)$concept,new \DateTime($payload_debt['date_at']));
                            $payload_debt['payment_status'] = $this->add_payment_status($item['tarifa'],$item['mayo'],$payload_debt['concept_id'],cal_days_in_month(CAL_GREGORIAN, 5, $item['year']));
                            \App\Models\Debt::create($payload_debt);
                            break;
                        case 'junio':
                            $payload_debt['date_at'] = "{$item['year']}-06-01";
                        	$payload_debt['total'] = $this->add_total(trim($item['tarifa']),$payload_debt['concept_id'],cal_days_in_month(CAL_GREGORIAN, 6, $item['year']));
                            $payload_debt['payment'] = $item['junio'];
                            $payload_debt['code'] = $debtRepository->newCodeMonthly($stand,(array)$concept,new \DateTime($payload_debt['date_at']));
                            $payload_debt['payment_status'] = $this->add_payment_status($item['tarifa'],$item['junio'],$payload_debt['concept_id'],cal_days_in_month(CAL_GREGORIAN, 6, $item['year']));
                            \App\Models\Debt::create($payload_debt);
                            break;
                        case 'julio':
                            $payload_debt['date_at'] = "{$item['year']}-07-01";
                        	$payload_debt['total'] = $this->add_total(trim($item['tarifa']),$payload_debt['concept_id'],cal_days_in_month(CAL_GREGORIAN, 7, $item['year']));
                            $payload_debt['payment'] = $item['julio'];
                           	$payload_debt['code'] = $debtRepository->newCodeMonthly($stand,(array)$concept,new \DateTime($payload_debt['date_at']));
                            $payload_debt['payment_status'] = $this->add_payment_status($item['tarifa'],$item['julio'],$payload_debt['concept_id'],cal_days_in_month(CAL_GREGORIAN, 7, $item['year']));
                            \App\Models\Debt::create($payload_debt);
                            break;
                        case 'agosto':
                            $payload_debt['date_at'] = "{$item['year']}-08-01";
                        	$payload_debt['total'] = $this->add_total(trim($item['tarifa']),$payload_debt['concept_id'],cal_days_in_month(CAL_GREGORIAN, 8, $item['year']));
                            $payload_debt['payment'] = $item['agosto'];
							$payload_debt['code'] = $debtRepository->newCodeMonthly($stand,(array)$concept,new \DateTime($payload_debt['date_at']));
                            $payload_debt['payment_status'] = $this->add_payment_status($item['tarifa'],$item['agosto'],$payload_debt['concept_id'],cal_days_in_month(CAL_GREGORIAN, 8, $item['year']));
                            \App\Models\Debt::create($payload_debt);
                            break;
                        case 'setiembre':
                            $payload_debt['date_at'] = "{$item['year']}-09-01";
                        	$payload_debt['total'] = $this->add_total(trim($item['tarifa']),$payload_debt['concept_id'],cal_days_in_month(CAL_GREGORIAN, 9, $item['year']));
                            $payload_debt['payment'] = $item['setiembre'];
							$payload_debt['code'] = $debtRepository->newCodeMonthly($stand,(array)$concept,new \DateTime($payload_debt['date_at']));
                            $payload_debt['payment_status'] = $this->add_payment_status($item['tarifa'],$item['setiembre'],$payload_debt['concept_id'],cal_days_in_month(CAL_GREGORIAN, 9, $item['year']));
                            \App\Models\Debt::create($payload_debt);
                            break;
                        case 'octubre':
                            $payload_debt['date_at'] = "{$item['year']}-10-01";
                        	$payload_debt['total'] = $this->add_total(trim($item['tarifa']),$payload_debt['concept_id'],cal_days_in_month(CAL_GREGORIAN, 10, $item['year']));
                            $payload_debt['payment'] = $item['octubre'];
                            $payload_debt['code'] = $debtRepository->newCodeMonthly($stand,(array)$concept,new \DateTime($payload_debt['date_at']));
                            $payload_debt['payment_status'] = $this->add_payment_status($item['tarifa'],$item['octubre'],$payload_debt['concept_id'],cal_days_in_month(CAL_GREGORIAN, 10, $item['year']));
                            \App\Models\Debt::create($payload_debt);
                            break;
                        case 'noviembre':
                            $payload_debt['date_at'] = "{$item['year']}-11-01";
                        	$payload_debt['total'] = $this->add_total(trim($item['tarifa']),$payload_debt['concept_id'],cal_days_in_month(CAL_GREGORIAN, 11, $item['year']));
                            $payload_debt['payment'] = $item['noviembre'];
                            $payload_debt['code'] = $debtRepository->newCodeMonthly($stand,(array)$concept,new \DateTime($payload_debt['date_at']));
                            $payload_debt['payment_status'] = $this->add_payment_status($item['tarifa'],$item['noviembre'],$payload_debt['concept_id'],cal_days_in_month(CAL_GREGORIAN, 11, $item['year']));
                            \App\Models\Debt::create($payload_debt);
                            break;
                        case 'diciembre':
                            $payload_debt['date_at'] = "{$item['year']}-12-01";
                        	$payload_debt['total'] = $this->add_total(trim($item['tarifa']),$payload_debt['concept_id'],cal_days_in_month(CAL_GREGORIAN, 12, $item['year']));
                            $payload_debt['payment'] = $item['diciembre'];
                            $payload_debt['code'] = $debtRepository->newCodeMonthly($stand,(array)$concept,new \DateTime($payload_debt['date_at']));
                            $payload_debt['payment_status'] = $this->add_payment_status($item['tarifa'],$item['diciembre'],$payload_debt['concept_id'],cal_days_in_month(CAL_GREGORIAN, 12, $item['year']));
                            \App\Models\Debt::create($payload_debt);
                            break;
                    }
                }
            }
                
                // $payload_debt = [
                //     'date_at' => $item['year'],
                // ];
                // $stand = \App\Models\Debt::create($payload_debt);
        }

        
        // DB::table('debts')->insert(['date_at' => '2019-01-01 10:10:10','code' =>"MEC Pto: E-01|VIVAN|COSMO|2019.ENE",'type' => 'normal', 'total' => 20,'payment'=>0,'payment_date'=>'','client_id'=>2,'stand_id'=>1,'concept_id'=>1]);
        // DB::table('debts')->insert(['date_at' => '2019-02-01 10:10:10','code' =>"MEC Pto: E-01|VIVAN|COSMO|2019.FEB",'type' => 'normal', 'total' => 20,'payment'=>0,'payment_date'=>'','client_id'=>2,'stand_id'=>1,'concept_id'=>1]);

        // DB::table('debts')->insert(['date_at' => '2019-12-01 10:10:10','code' =>"MEC Pto: E-01|VIVAN|COSMO|2019.DIC",'type' => 'normal', 'total' => 20,'payment'=>0,'payment_date'=>'','client_id'=>2,'stand_id'=>1,'concept_id'=>1]);
        // DB::table('debts')->insert(['date_at' => '2019-11-01 10:10:10','code' =>"MEC Pto: E-01|VIVAN|COSMO|2019.NOV",'type' => 'normal', 'total' => 20,'payment'=>0,'payment_date'=>'','client_id'=>2,'stand_id'=>1,'concept_id'=>1]);
        // DB::table('debts')->insert(['date_at' => '2019-10-01 10:10:10','code' =>"MEC Pto: E-01|VIVAN|COSMO|2019.OCT",'type' => 'normal', 'total' => 20,'payment'=>0,'payment_date'=>'','client_id'=>2,'stand_id'=>1,'concept_id'=>1]);
        // DB::table('debts')->insert(['date_at' => '2018-10-01 10:10:10','code' =>"MEC Pto: E-01|VIVAN|COSMO|2018.OCT",'type' => 'normal', 'total' => 20,'payment'=>0,'payment_date'=>'','client_id'=>2,'stand_id'=>1,'concept_id'=>1]);

        // DB::table('debts')->insert(['date_at' => '2019-12-01 10:10:10','code' =>"MEC Pto: E-05|FRUT|COSMO|2019.DIC",'type' => 'normal', 'total' => 25,'payment'=>0,'payment_date'=>'','client_id'=>5,'stand_id'=>7,'concept_id'=>1]);
        // DB::table('debts')->insert(['date_at' => '2019-11-01 10:10:10','code' =>"MEC Pto: E-05|FRUT|COSMO|2019.NOV",'type' => 'normal', 'total' => 25,'payment'=>0,'payment_date'=>'','client_id'=>5,'stand_id'=>7,'concept_id'=>1]);
    }

    private function add_payment_status($rate, $month, $concept_id,$number_days_month) {
        $result = 0;
/*         if($concept_id == \App\Util::$concepts['ME_ID'] ) $result = (floatval($rate) - floatval($month) ) == 0 ? 1 : 0; */
/*         elseif($concept_id == \App\Util::$concepts['SIS_ID']) $result = floatval($month) >  0 ? 1 : 0; */
		if($concept_id ==  \App\Util::$concepts['SIS_ID'])  $result = ($number_days_month*floatval($rate) - floatval($month)) == 0 || (floatval($month) > $number_days_month*floatval($rate)) ? 1 : 0;
		else $result = (floatval($rate) - floatval($month)) == 0 || (floatval($month) > floatval($rate)) ? 1 : 0;

        return $result;
	}

	private function add_total($month_total, $concept_id, $number_days_month) {
		$result = "";	
		if($concept_id ==  \App\Util::$concepts['SIS_ID'])  $result = floatval($month_total)*$number_days_month;
		else $result = $month_total;

		return $result;
	}
}
