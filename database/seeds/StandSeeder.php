<?php

use Illuminate\Database\Seeder;

class StandSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        // // Merced
        // DB::table('stands')->insert(['code'=>'E-01','location'=>'Jr. Gomez Davila Principal','rate_amount'=>20,'titular_id'=>2,'sector_id'=>2,'market_id'=>1,'gyre_id'=>1,'rate_id'=>4]);
        // DB::table('stands')->insert(['code'=>'E-02','location'=>'Jr. Fredy Aliaga','rate_amount'=>10,'titular_id'=>2,'sector_id'=>3,'market_id'=>1,'gyre_id'=>5,'rate_id'=>8]);
        // // SISA 
        // DB::table('stands')->insert(['code'=>'SISA-001','location'=>'plaza Alameda','rate_amount'=>20,'titular_id'=>2,'sector_id'=>8,'market_id'=>4,'gyre_id'=>10,'rate_id'=>null]);
        // // QUIOSCOS
        // DB::table('stands')->insert(['code'=>'QUIO-001','location'=>'Av. Raymondy Cdr. 3','rate_amount'=>5,'rate_amount_secondary'=>10,'titular_id'=>2,'sector_id'=>8,'market_id'=>5,'gyre_id'=>10,'rate_id'=>null]);
        // // Merced
        // DB::table('stands')->insert(['code'=>'E-03','location'=>'Jr. Jose Prato 05','rate_amount'=>15,'titular_id'=>3,'sector_id'=>2,'market_id'=>1,'gyre_id'=>5,'rate_id'=>4]);
        // DB::table('stands')->insert(['code'=>'E-04','location'=>'Jr. Jose Prato 06','rate_amount'=>20,'titular_id'=>4,'sector_id'=>2,'market_id'=>1,'gyre_id'=>4,'rate_id'=>1]);
        // DB::table('stands')->insert(['code'=>'E-05','location'=>'Jr. Jose Prato 07','rate_amount'=>25,'titular_id'=>5,'sector_id'=>3,'market_id'=>1,'gyre_id'=>2,'rate_id'=>2]);
        // DB::table('stands')->insert(['code'=>'E-06','location'=>'Jr. Jose Prato 08 ','rate_amount'=>15,'titular_id'=>6,'sector_id'=>2,'market_id'=>1,'gyre_id'=>8,'rate_id'=>8]);
        // DB::table('stands')->insert(['code'=>'E-07','location'=>'Jr. Jose Prato 09','rate_amount'=>10,'titular_id'=>6,'sector_id'=>2,'market_id'=>1,'gyre_id'=>9,'rate_id'=>5]);
        // DB::table('stands')->insert(['code'=>'E-08','location'=>'Jr. Jose Prato 10','rate_amount'=>15,'titular_id'=>8,'sector_id'=>3,'market_id'=>1,'gyre_id'=>1,'rate_id'=>2]);
        // DB::table('stands')->insert(['code'=>'E-09','location'=>'Jr. Jose Prato 11','rate_amount'=>10,'titular_id'=>9,'sector_id'=>1,'market_id'=>1,'gyre_id'=>8,'rate_id'=>3]);
        // DB::table('stands')->insert(['code'=>'E-10','location'=>'Jr. Jose Prato 12','rate_amount'=>15,'titular_id'=>10,'sector_id'=>2,'market_id'=>1,'gyre_id'=>5,'rate_id'=>1]);
        // DB::table('stands')->insert(['code'=>'E-11','location'=>'Jr. Jose Prato 13','rate_amount'=>25,'titular_id'=>11,'sector_id'=>4,'market_id'=>1,'gyre_id'=>4,'rate_id'=>1]);
        // DB::table('stands')->insert(['code'=>'E-12','location'=>'Jr. Jose Prato 14','rate_amount'=>25,'titular_id'=>12,'sector_id'=>4,'market_id'=>1,'gyre_id'=>4,'rate_id'=>1]);
        // DB::table('stands')->insert(['code'=>'E-13','location'=>'Jr. Jose Prato 15','rate_amount'=>45,'titular_id'=>13,'sector_id'=>4,'market_id'=>1,'gyre_id'=>4,'rate_id'=>1]);
        // DB::table('stands')->insert(['code'=>'E-14','location'=>'Jr. Alameda N° 01','rate_amount'=>45,'titular_id'=>14,'sector_id'=>4,'market_id'=>1,'gyre_id'=>4,'rate_id'=>1]);
        // DB::table('stands')->insert(['code'=>'E-15','location'=>'Jr. Alameda N° 02','rate_amount'=>45,'titular_id'=>15,'sector_id'=>4,'market_id'=>1,'gyre_id'=>4,'rate_id'=>1]);
        // DB::table('stands')->insert(['code'=>'E-16','location'=>'Jr. Alameda N° 03','rate_amount'=>45,'titular_id'=>15,'sector_id'=>4,'market_id'=>1,'gyre_id'=>4,'rate_id'=>1]);
        // DB::table('stands')->insert(['code'=>'E-17','location'=>'Jr. Alameda N° 04','rate_amount'=>45,'titular_id'=>16,'sector_id'=>4,'market_id'=>1,'gyre_id'=>4,'rate_id'=>1]);
        // DB::table('stands')->insert(['code'=>'E-18','location'=>'Jr. Alameda N° 05','rate_amount'=>45,'titular_id'=>17,'sector_id'=>4,'market_id'=>1,'gyre_id'=>4,'rate_id'=>1]);
        // DB::table('stands')->insert(['code'=>'E-19','location'=>'Jr. Alameda N° 06','rate_amount'=>45,'titular_id'=>18,'sector_id'=>4,'market_id'=>1,'gyre_id'=>4,'rate_id'=>1]);
        // DB::table('stands')->insert(['code'=>'E-20','location'=>'Jr. Alameda N° 07','rate_amount'=>45,'titular_id'=>19,'sector_id'=>4,'market_id'=>1,'gyre_id'=>4,'rate_id'=>1]);
        // DB::table('stands')->insert(['code'=>'E-21','location'=>'Jr. Alameda N° 08','rate_amount'=>45,'titular_id'=>20,'sector_id'=>4,'market_id'=>1,'gyre_id'=>4,'rate_id'=>1]);
        // DB::table('stands')->insert(['code'=>'E-22','location'=>'Jr. Alameda N° 09','rate_amount'=>45,'titular_id'=>20,'sector_id'=>4,'market_id'=>1,'gyre_id'=>4,'rate_id'=>1]);
        // DB::table('stands')->insert(['code'=>'E-23','location'=>'Jr. Alameda N° 10','rate_amount'=>45,'titular_id'=>22,'sector_id'=>4,'market_id'=>1,'gyre_id'=>4,'rate_id'=>1]);
        // DB::table('stands')->insert(['code'=>'E-24','location'=>'Jr. Alameda N° 11','rate_amount'=>45,'titular_id'=>23,'sector_id'=>4,'market_id'=>1,'gyre_id'=>4,'rate_id'=>1]);
        // DB::table('stands')->insert(['code'=>'E-25','location'=>'Jr. Alameda N° 12','rate_amount'=>45,'titular_id'=>24,'sector_id'=>4,'market_id'=>1,'gyre_id'=>4,'rate_id'=>1]);
        // DB::table('stands')->insert(['code'=>'E-26','location'=>'Jr. Alameda N° 13','rate_amount'=>45,'titular_id'=>25,'sector_id'=>4,'market_id'=>1,'gyre_id'=>4,'rate_id'=>1]);
        // DB::table('stands')->insert(['code'=>'E-28','location'=>'Jr. Alameda N° 14','rate_amount'=>45,'titular_id'=>26,'sector_id'=>4,'market_id'=>1,'gyre_id'=>4,'rate_id'=>1]);
        // DB::table('stands')->insert(['code'=>'E-29','location'=>'Jr. Casintillo N° 01','rate_amount'=>45,'titular_id'=>26,'sector_id'=>4,'market_id'=>1,'gyre_id'=>4,'rate_id'=>1]);
        // DB::table('stands')->insert(['code'=>'E-30','location'=>'Jr. Casintillo N° 02','rate_amount'=>45,'titular_id'=>26,'sector_id'=>4,'market_id'=>1,'gyre_id'=>4,'rate_id'=>1]);
        // DB::table('stands')->insert(['code'=>'E-31','location'=>'Jr. Casintillo N° 03','rate_amount'=>45,'titular_id'=>29,'sector_id'=>4,'market_id'=>1,'gyre_id'=>4,'rate_id'=>1]);
        // DB::table('stands')->insert(['code'=>'E-32','location'=>'Jr. Casintillo N° 04','rate_amount'=>45,'titular_id'=>30,'sector_id'=>4,'market_id'=>1,'gyre_id'=>4,'rate_id'=>1]);
        // DB::table('stands')->insert(['code'=>'E-33','location'=>'Jr. Casintillo N° 05','rate_amount'=>45,'titular_id'=>31,'sector_id'=>4,'market_id'=>2,'gyre_id'=>4,'rate_id'=>1]);
        // DB::table('stands')->insert(['code'=>'E-34','location'=>'Jr. Casintillo N° 06','rate_amount'=>45,'titular_id'=>33,'sector_id'=>4,'market_id'=>2,'gyre_id'=>4,'rate_id'=>1]);
        // DB::table('stands')->insert(['code'=>'E-35','location'=>'Jr. Casintillo N° 07','rate_amount'=>45,'titular_id'=>35,'sector_id'=>4,'market_id'=>2,'gyre_id'=>4,'rate_id'=>1]);
        // DB::table('stands')->insert(['code'=>'E-36','location'=>'Jr. Casintillo N° 08','rate_amount'=>45,'titular_id'=>36,'sector_id'=>4,'market_id'=>2,'gyre_id'=>4,'rate_id'=>1]);
        // DB::table('stands')->insert(['code'=>'E-37','location'=>'Jr. Casintillo N° 09','rate_amount'=>45,'titular_id'=>37,'sector_id'=>4,'market_id'=>2,'gyre_id'=>4,'rate_id'=>1]);
        // DB::table('stands')->insert(['code'=>'E-38','location'=>'Jr. Casintillo N° 10','rate_amount'=>45,'titular_id'=>38,'sector_id'=>4,'market_id'=>2,'gyre_id'=>4,'rate_id'=>1]);
        // DB::table('stands')->insert(['code'=>'E-39','location'=>'Jr. Casintillo N° 11','rate_amount'=>45,'titular_id'=>38,'sector_id'=>4,'market_id'=>2,'gyre_id'=>4,'rate_id'=>1]);
        // DB::table('stands')->insert(['code'=>'E-40','location'=>'Jr. Casintillo N° 12','rate_amount'=>45,'titular_id'=>40,'sector_id'=>4,'market_id'=>2,'gyre_id'=>4,'rate_id'=>1]);

        // // ##################################   LOCAL SEEDER   ##########################
        // DB::table('stands')->insert(['name'=>'Licoreria Leon','location'=>'Jr. Gomez cdr.3 Costado UDH','titular_id'=>75,'market_id'=>6]);
    }
}
