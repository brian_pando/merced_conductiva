<?php

use Illuminate\Database\Seeder;

class CompanySeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('companies')->insert(['code' => 'MPLP','ruc'=>'20200042744', 'name' => 'MUNICIPALIDAD PROVINCIAL LEONCIO PRADO','address'=>'AV. ALAMEDA PERU 525', 'phone'=>'','city'=>'Tingo Maria']);
    }
}
