<?php

use Illuminate\Database\Seeder;

class SectorSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('sectors')->insert(['name' => '-', 'code' => '-']);
        DB::table('sectors')->insert(['name' => 'Cosmos', 'code' => 'COSMOS']); // ropas
        DB::table('sectors')->insert(['name' => 'Vivanderia', 'code' => 'VIVAN']);
        DB::table('sectors')->insert(['name' => 'Mercado Nuevo', 'code' => 'NUEVO']);
        DB::table('sectors')->insert(['name' => 'Mercado Antiguo', 'code' => 'ANTIGUO']);
        DB::table('sectors')->insert(['name' => 'AREAS VERDES', 'code' => 'AREAS VERDES']);
        DB::table('sectors')->insert(['name' => 'BELLA DURMIENTE', 'code' => 'BELLA DURMIENTE']);
        DB::table('sectors')->insert(['name' => 'LETRA  " I "', 'code' => 'LETRA  " I "']);
        DB::table('sectors')->insert(['name' => 'MERCADILLO EL COLONO-IPD', 'code' => 'MERCADILLO EL COLONO-IPD']);
        DB::table('sectors')->insert(['name' => 'LAMAS', 'code' => 'LAMAS']);
        DB::table('sectors')->insert(['name' => 'NOCHE', 'code' => 'NOCHE']);
        DB::table('sectors')->insert(['name' => 'PAMPAS', 'code' => 'PAMPAS']);
        DB::table('sectors')->insert(['name' => 'PANADEROS', 'code' => 'PANADEROS']);
        DB::table('sectors')->insert(['name' => 'PARQUE RAMON CASTILLA', 'code' => 'PARQUE RAMON CASTILLA']);
        DB::table('sectors')->insert(['name' => 'SEGUNDO PISO', 'code' => 'SEGUNDO PISO']);
        DB::table('sectors')->insert(['name' => 'VEREDA INTERMEDIA', 'code' => 'VEREDA INTERMEDIA']);
        DB::table('sectors')->insert(['name' => 'JR. CALLAO CDR. 01-MAL. LIMA', 'code' => 'JR. CALLAO']);
        DB::table('sectors')->insert(['name' => 'FRUTAS', 'code' => 'FRUTAS']);
    }
}
