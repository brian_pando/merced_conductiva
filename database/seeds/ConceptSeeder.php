<?php

use Illuminate\Database\Seeder;

class ConceptSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('concepts')->insert(['code' => 'MEC','number'=>'31', 'name' => 'Merced Conductiva','classificator'=>'1.3.29.15.9']);
        DB::table('concepts')->insert(['code' => 'SIS','number'=>'32', 'name' => 'SISA','classificator'=>'2.5.30.46.10']);
        DB::table('concepts')->insert(['code' => 'RRS','number'=>'33', 'name' => 'Recojo de Residuos Solidos','classificator'=>'1.5.31.19.8']);
        DB::table('concepts')->insert(['code' => 'MTD','number'=>'34', 'name' => 'Multa Directa','classificator'=>'1.4.19.34.7']);
        DB::table('concepts')->insert(['code' => 'PIT','number'=>'35', 'name' => 'Papeleta Infraccion Transito','classificator'=>'1.5.41.25.3']);
        DB::table('concepts')->insert(['code' => 'TER','number'=>'36', 'name' => 'Venta de Terreno','classificator'=>'1.2.41.12.1']);
        DB::table('concepts')->insert(['code' => 'MAU','number'=>'37', 'name' => 'Mausoleo','classificator'=>'2.6.22.32.9']);
        DB::table('concepts')->insert(['code' => 'PNL','number'=>'38', 'name' => 'Panel Publicitario','classificator'=>'2.9.31.12.2']);
        DB::table('concepts')->insert(['code' => 'NCH','number'=>'39', 'name' => 'Cementario','classificator'=>'1.9.34.70.4']);
        DB::table('concepts')->insert(['code' => 'BAP','number'=>'40', 'name' => 'Baja policia','classificator'=>'1.8.60.70.5']);
    }
}
