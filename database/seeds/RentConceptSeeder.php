<?php

use Illuminate\Database\Seeder;

class RentConceptSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('concepts')->insert(['code' => 'ARR','number'=>'41', 'name' => 'Arrendamiento','classificator'=>'1.3.29.15.9']);
     }
}
