<?php

use Illuminate\Database\Seeder;

class UserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        
        DB::table('users')->insert(['code'=>'123','username'=>'su',   'name' => 'Juan Pablo','email'=>'jhon0.admin@gmail.com','email_verified_at'=>'2020-08-18 10:15_25','password'=>Hash::make('password'),'remember_token'=>Hash::make(microtime()),'role_id'=>1,'porcentage'=>10,'people_id'=>2]);//ti_admin
        DB::table('users')->insert(['code'=>'123','username'=>'informatica',   'name' => 'Andres Salas','email'=>'jhon1.admin@gmail.com','email_verified_at'=>'2020-08-18 10:15_25','password'=>Hash::make('password'),'remember_token'=>Hash::make(microtime()),'role_id'=>2,'porcentage'=> 15,'people_id'=>3]);//ti_admin
        DB::table('users')->insert(['code'=>'123','username'=>'gerente_renta','name' => 'Junco','email'=>'junco.admin@gmail.com','email_verified_at'=>'2020-08-18 10:15_25','password'=>Hash::make('password'),'remember_token'=>Hash::make(microtime()),'role_id'=>3,'porcentage'=> 20,'people_id'=>4]);//rt_admin
        DB::table('users')->insert(['code'=>'123','username'=>'gerente_caja',  'name' => 'Pablo Cotrina','email'=>'jhon3.admin@gmail.com','email_verified_at'=>'2020-08-18 10:15_25','password'=>Hash::make('password'),'remember_token'=>Hash::make(microtime()),'role_id'=>4,'porcentage'=> 30,'people_id'=>5]);//ca_admin
	    DB::table('users')->insert(['code'=>'123','username'=>'auxiliar_renta','name' => 'Ruby Dominguez','email'=>'jhon5.admin@gmail.com','email_verified_at'=>'2020-08-18 10:15_25','password'=>Hash::make('password'),'remember_token'=>Hash::make(microtime()),'role_id'=>5,'porcentage'=>20,'people_id'=>6]);//rt_auxil
        DB::table('users')->insert(['code'=>'123','username'=>'comisionista1',  'name' => 'Diana Ibarrarte','email'=>'diana.admin@gmail.com','email_verified_at'=>'2020-08-18 10:15_25','password'=>Hash::make('password'),'remember_token'=>Hash::make(microtime()),'role_id'=>6,'porcentage'=>20,'people_id'=>7]);//rt_cobra
        DB::table('users')->insert(['code'=>'123','username'=>'comisionista2',  'name' => 'Juan Palmas','email'=>'juan.admin@gmail.com','email_verified_at'=>'2020-08-18 10:15_25','password'=>Hash::make('password'),'remember_token'=>Hash::make(microtime()),'role_id'=>6,'porcentage'=>25,'people_id'=>8]);//rt_cobra
        DB::table('users')->insert(['code'=>'123','username'=>'comisionista3',  'name' => 'Sevas Bastian','email'=>'sevas.admin@gmail.com','email_verified_at'=>'2020-08-18 10:15_25','password'=>Hash::make('password'),'remember_token'=>Hash::make(microtime()),'role_id'=>6,'porcentage'=>30,'people_id'=>9]);//rt_cobra
        DB::table('users')->insert(['code'=>'123','username'=>'cajero', 'name' => 'Box','email'=>'jhon8.admin@gmail.com','email_verified_at'=>'2020-08-18 10:15_25','password'=>Hash::make('password'),'remember_token'=>Hash::make(microtime()),'role_id'=>7,'porcentage'=>15,'people_id'=>10]);//ca_cashb
        DB::table('users')->insert(['code'=>'123','username'=>'tmp_comisionista', 'name' => 'Karla','email'=>'tmp_cajero@gmail.com','email_verified_at'=>'2020-08-18 10:15_25','password'=>Hash::make('password'),'remember_token'=>Hash::make(microtime()),'role_id'=>6,'porcentage'=>15,'people_id'=>11]);//ca_cashb
       
    }
}
