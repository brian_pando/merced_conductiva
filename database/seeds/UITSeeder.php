<?php

use Illuminate\Database\Seeder;

class UITSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('u_i_t_s')->insert(['amount' => '4200','year'=>'2020']);
    }
}
