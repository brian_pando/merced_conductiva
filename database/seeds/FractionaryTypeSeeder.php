<?php

use Illuminate\Database\Seeder;

class FractionaryTypeSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('fractionary_types')->insert(['code'=>'MEC','name'=>'Merced Conductiva','fields'=>'stand_id']);
        // DB::table('fractionary_types')->insert(['code'=>'SISA','name'=>'SISA','fields'=>'stand_id']);
        DB::table('fractionary_types')->insert(['code'=>'RRS','name'=>'Recojo de Residuos Solidos','fields'=>'stand_id']);
        DB::table('fractionary_types')->insert(['code'=>'MTD','name'=>'Multa Directa','fields'=>'stand_id,apply_date']);
        DB::table('fractionary_types')->insert(['code'=>'PIT','name'=>'Papeleta de Transito','fields'=>'apply_date']);
        DB::table('fractionary_types')->insert(['code'=>'TER','name'=>'Venta de Terreno','fields'=>'']);
        DB::table('fractionary_types')->insert(['code'=>'MAU','name'=>'Mausoleo','fields'=>'']);
        DB::table('fractionary_types')->insert(['code'=>'PNL','name'=>'Panel Publicitario','fields'=>'']);
        DB::table('fractionary_types')->insert(['code'=>'NCH','name'=>'Nicho','fields'=>'']);
    }
}
