<?php

use Illuminate\Database\Seeder;

class SadConceptSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('concepts')->insert(['code' => 'SAD','number'=>'42', 'name' => 'Sancion Administrativa','classificator'=>'1.3.29.15.9']);
     }
}
