<?php

use Illuminate\Database\Seeder;

class PeopleSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        // create user dafault Muni
        \App\Models\Person::create(['name'=>'Libre','dni'=>0000000]);    
		\App\Models\Person::create(['name'=>'Juan Pablo','dni'=>2334567,'user_id'=>1]);    
		\App\Models\Person::create(['name'=>'Andres Salas','dni'=>2334534,'user_id'=>2]);    
		\App\Models\Person::create(['name'=>'María Dina','dni'=>2334234,'user_id'=>3]);    
		\App\Models\Person::create(['name'=>'Pablo Cotrina','dni'=>2323467,'user_id'=>4]);    
		\App\Models\Person::create(['name'=>'Ruby Dominguez','dni'=>4534567,'user_id'=>5]);    
		\App\Models\Person::create(['name'=>'Diana Ibarrarte','dni'=>5634567,'user_id'=>6]);    
		\App\Models\Person::create(['name'=>'Juan Palmas','dni'=>2336767,'user_id'=>7]);    
		\App\Models\Person::create(['name'=>'Sevas Bastian','dni'=>1334567,'user_id'=>8]);    
		\App\Models\Person::create(['name'=>'Dina Paucar','dni'=>4534567,'user_id'=>9]);    
		\App\Models\Person::create(['name'=>'Karla','dni'=>45345234,'user_id'=>10]);    

        
        $csvFiles = array();
        array_push($csvFiles,public_path().'/csv/ciudadanos_model_cosmos_merced.csv');
        array_push($csvFiles,public_path().'/csv/ciudadanos_model_vivan_merced.csv');
        array_push($csvFiles,public_path().'/csv/ciudadanos_model_nuevo_merced.csv');
        array_push($csvFiles,public_path().'/csv/ciudadanos_model_antiguo_merced.csv');
      
      
        array_push($csvFiles,public_path().'/csv/ciudadanos_model_ipd_merced.csv');
        array_push($csvFiles,public_path().'/csv/ciudadanos_sisa_sisa_merced.csv');
        array_push($csvFiles,public_path().'/csv/ciudadanos_sisa_callao_merced.csv');
        array_push($csvFiles,public_path().'/csv/ciudadanos_tupac_merced.csv');
        array_push($csvFiles,public_path().'/csv/ciudadanos_kiosko_merced.csv');
        
        $data = array();
        foreach($csvFiles as $i => $item){
            if(!file_exists($item) || !is_readable($item)) return FALSE;    
            $header = NULL;
            if (($handle = fopen($item, 'r')) !== FALSE){
                while (($row = fgetcsv($handle, 1000, $delimiter=',')) !== FALSE){
                    $row = array_map(function($item){return trim($item);},$row);
                    if(!$header) $header = $row;
                    else {$data[] = array_combine($header,  $row);}
                    // echo json_encode( $data);
                }
                fclose($handle);
            }
        }
        
        // $csvFile = public_path().'/csv/ciudadanos_model_cosmos_merced.csv';
        // if(!file_exists($csvFile) || !is_readable($csvFile)) return FALSE;
        // $header = NULL;
        // $data = array();

        // if (($handle = fopen($csvFile, 'r')) !== FALSE){
        //     while (($row = fgetcsv($handle, 1000, $delimiter=',')) !== FALSE){
        //         if(!$header) $header = $row;
        //         else {$data[] = array_combine($header,  $row);}
        //     }
        //     fclose($handle);
        // }

        foreach($data as $i => $item){
			$item['nombre'] = preg_replace("/ +/"," ", $item['nombre']);
            $person = [];
            $person_id = $this->existsPerson($item['nombre']);
            
            if(!$person_id) {// cuando se repite la misma persona de acuerdo a la secuencias
                $payload_person = [
                    'name'=>$item['nombre'],
                    'dni'=>trim($item['dni']),
                ];
                $person=\App\Models\Person::create($payload_person);    
            }
            
            $payload_stand = [
                'code'=>$item['codigo_puesto'],
                'rate_amount'=>$item['monto_tarifa'],
                'titular_id'=>$person_id ? $person_id->id : $person->id,
                'market_id'=>1,
            ];
            
            switch($item['mercado']){
                case 'model':
                    $payload_stand['market_id'] = \App\Util::$markets['MODEL_ID'];
                    break;
                case 'tupac':
                    $payload_stand['market_id'] = \App\Util::$markets['TUPC_ID'];
                    break;
                case 'kiosko':
                    $payload_stand['market_id'] = \App\Util::$markets['KIOS_ID'];
                    break;
                case 'sisa':
                    $payload_stand['market_id'] = \App\Util::$markets['SISA_ID'];
                    break;
                case 'ipd':
                    $payload_stand['market_id'] = \App\Util::$markets['IPD_ID'];
                    break;
                default:
                    $payload_stand['market_id'] = -1;
                    break;
            }

            switch(trim($item['sector'])){
                case 'cosmos':
                    $payload_stand['sector_id'] = \App\Util::$sectors['COSMO_ID'];
                    break;
                case 'vivan':
                    $payload_stand['sector_id'] = \App\Util::$sectors['VIVAN_ID'];
                    break;
                case 'nuevo':
                    $payload_stand['sector_id'] = \App\Util::$sectors['NUEVO_ID'];
                    break;
                case 'antiguo':
                    $payload_stand['sector_id'] = \App\Util::$sectors['ANTIGUO_ID'];
                    break;
                case 'AREAS VERDES':
                    $payload_stand['sector_id'] = \App\Util::$sectors['AREA_VERDE_ID'];
                    break;
                case 'BELLA DURMIENTE':
                    $payload_stand['sector_id'] = \App\Util::$sectors['BELLA_DURMI_ID'];
                    break;
                case 'LETRA  " I "':
                    $payload_stand['sector_id'] = \App\Util::$sectors['LETRA_ID'];
                    break;
                case 'MERCADILLO EL COLONO-IPD':
                    $payload_stand['sector_id'] = \App\Util::$sectors['MERCADILLO_ID'];
                    break;
                case 'LAMAS':
                    $payload_stand['sector_id'] = \App\Util::$sectors['LAMAS_ID'];
                    break;
                case 'NOCHE':
                    $payload_stand['sector_id'] = \App\Util::$sectors['NOCHE_ID'];
                    break;
                case 'PAMPAS':
                    $payload_stand['sector_id'] = \App\Util::$sectors['PAMPA_ID'];
                    break;
                case 'PANADEROS':
                    $payload_stand['sector_id'] = \App\Util::$sectors['PANADE_ID'];
                    break;
                case 'PARQUE RAMON CASTILLA':
                    $payload_stand['sector_id'] = \App\Util::$sectors['PARQUE_ID'];
                    break;
                case 'SEGUNDO PISO':
                    $payload_stand['sector_id'] = \App\Util::$sectors['SEGUND_ID'];
                    break;
                case 'VEREDA INTERMEDIA':
                    $payload_stand['sector_id'] = \App\Util::$sectors['VEREDA_ID'];
                    break;
				case 'JR. CALLAO CDR. 01-MAL. LIMA':
					$payload_stand['sector_id'] = \App\Util::$sectors['JR_CALLAO_ID']; 
                    break;
                case 'frutas':
                    $payload_stand['sector_id'] = \App\Util::$sectors['IPD']; 
                    break;
                default:
                    $payload_stand['sector_id'] = \App\Util::$sectors['NONE_ID'];
                    break;
            }

            switch($item['giro']){
                case 'carnes':
                    $payload_stand['gyre_id'] = \App\Util::$gyres['CARNE_ID'];
                    break;
                default:
                    $payload_stand['gyre_id'] = \App\Util::$gyres['NONE_ID'];
                    break;
            }

            $stand = \App\Models\Stand::create($payload_stand);
        }

        //el primer cliente siempre debe ser la institucion, para poder revertir los puestos a nombre de esta persona.
        // DB::table('people')->insert(['name' => 'MUNICIPALIDAD','lastname'=>'MUNICIPALIDAD','dni'=>'0000000','birth'=>'2020-05-15','phone1'=>'--','phone2'=>'--','address1'=>'--', 'address2'=>'--']);

    }

    private function existsPerson($name_person) {
        return \App\Models\Person::where('name',$name_person)->first('id');
    }
}
