<?php

use Illuminate\Database\Seeder;

class SettingSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('settings')->insert(['category' => 'notification','name'=>'signature_name', 'value' =>'Juan Perez','description'=>'']);
        DB::table('settings')->insert(['category' => 'notification','name'=>'signature_role', 'value' =>'G. Recaudacion Tributaria','description'=>'']);
        DB::table('settings')->insert(['category' => 'notification','name'=>'signature_image', 'value' =>'','description'=>'']);
        DB::table('settings')->insert(['category' => 'notification','name'=>'header', 'value' =>'','description'=>'']);
        DB::table('settings')->insert(['category' => 'fractionary','name'=>'interest', 'value' =>'0.0088','description'=>'']);
        DB::table('settings')->insert(['category' => 'fractionary','name'=>'nota', 'value' =>'Este es el texto base legal de fraccionamiento','description'=>'Texto que se muestra en el footer al momento de imprimir un fraccionamiento']);
        DB::table('settings')->insert(['category' => 'notification','name'=>'nota', 'value' =>'Este es el texto base legal de notificación','description'=>'Texto que se muestra en el footer al momento de imprimir un notificacion']);
        DB::table('settings')->insert(['category' => 'notification_fractionary','name'=>'signature_image', 'value' =>'','description'=>'']);
    }
}
