<?php

use Illuminate\Database\Seeder;

class RateSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('rates')->insert(['market_id' => 1, 'gyre_id' => 1,'amount'=>20]);
        DB::table('rates')->insert(['market_id' => 2, 'gyre_id' => 2,'amount'=>15]);
        DB::table('rates')->insert(['market_id' => 4, 'gyre_id' => 4,'amount'=>10]);
        DB::table('rates')->insert(['market_id' => 3, 'gyre_id' => 9,'amount'=>25]);
        DB::table('rates')->insert(['market_id' => 4, 'gyre_id' => 8,'amount'=>10]);
        DB::table('rates')->insert(['market_id' => 1, 'gyre_id' => 3,'amount'=>5]);
        DB::table('rates')->insert(['market_id' => 2, 'gyre_id' => 7,'amount'=>25]);
        DB::table('rates')->insert(['market_id' => 3, 'gyre_id' => 6,'amount'=>15]);
        DB::table('rates')->insert(['market_id' => 3, 'gyre_id' => 5,'amount'=>10]);
        DB::table('rates')->insert(['market_id' => 3, 'gyre_id' => 2,'amount'=>25]);
        DB::table('rates')->insert(['market_id' => 2, 'gyre_id' => 5,'amount'=>10]);
        DB::table('rates')->insert(['market_id' => 3, 'gyre_id' => 3,'amount'=>15]);
    }
}
