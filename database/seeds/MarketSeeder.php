<?php

use Illuminate\Database\Seeder;

class MarketSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        // OJO: valor del campo type se usan en las vistas stands y local para hacer comparaciones
        DB::table('markets')->insert(['name' => 'Mercado Modelo', 'type' => 'merced','code' => 'MODEL', 'address' => 'Jr. Alameda Cdr. 2']);
        DB::table('markets')->insert(['name' => 'Mercado Tupac Amaru', 'type' => 'merced','code' => 'TUPAC', 'address' => 'addres tupac amaru']);
        DB::table('markets')->insert(['name' => 'SISA', 'type' => 'merced','code' => 'SISA','is_debt_daily' => 1, 'address' => 'address sisa']);
        DB::table('markets')->insert(['name' => 'Kioskos', 'type' => 'merced','code' => 'KIOSKO', 'address' => 'address kioskos']);
        DB::table('markets')->insert(['name' => 'Locales', 'type' => 'local','code' => 'LOC', 'address' => '-']);
        DB::table('markets')->insert(['name' => 'ipd', 'type' => 'merced','code' => 'IPD', 'address' => '-']);
    }
}
