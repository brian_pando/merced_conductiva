<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
     /*   $this->call(
            []
        );*/
        $this->call([
            CompanySeeder::class, 
            SettingSeeder::class, 
            UITSeeder::class, 
            RoleSeeder::class, 
            MarketSeeder::class, 
            GyreSeeder::class, 
            SectorSeeder::class, 
            UserSeeder::class,
            PeopleSeeder::class, 
            RateSeeder::class, 
            StandSeeder::class, 
            ConceptSeeder::class, 
            DebtSeeder::class,
            //RentConceptSeeder::class,
            //FractionaryTypeSeeder::class,
            //AdministrativeFractionarySeeder::class,
            //FooterKioskoNotificationSeeder::class,
            
           // FooterNotificationFractionarySeeder::class,//cerar base legal para norificacion de fraccionamiento
        ]); 

        //Acreditation Formats
        // $format = factory(App\Models\Format::class)->create();

    }
}


