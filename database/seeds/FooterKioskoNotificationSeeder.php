<?php

use Illuminate\Database\Seeder;

class FooterKioskoNotificationSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('settings')->insert(['category' => 'notification','name'=>'nota_kiosko', 'value' =>'Este es el texto base legal de notificación kiosko','description'=>'Texto que se muestra en el footer al momento de imprimir un notificacion Kiosko']);
    }
}
