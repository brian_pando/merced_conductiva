<?php

use Illuminate\Database\Seeder;

class FooterNotificationFractionarySeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
         DB::table('settings')->insert(['category' => 'notification Fractionary','name'=>'notification_fractionary', 'value' =>'Este es el texto base legal de notificación de tipo fraccionamiento ','description'=>'Texto que se ilustra en el footer al momento de imprimir un notificacion se tipo fraccionamiento','created_at'=>NOW()]);
    }
}
