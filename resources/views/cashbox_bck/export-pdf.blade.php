@php
    $title_colspan = 13;
    $style_th = "border: 0px solid #404040; background:#404040; color:#ffffff; text-align:center; font-size:9px;";
@endphp
<table>
    <thead>
        <tr>
            <th colspan="{{$title_colspan}}" style="border: 0 solid #ffffff; font-size:10px;" >{{$cashboxes['company']->name}}</th>
        </tr>
        <tr>
            <th colspan="{{$title_colspan}}" style="border: 0px solid #ffffff; font-size:10px;">{{$cashboxes['company']->city}} - RUC. {{$cashboxes['company']->ruc}}</th>
        </tr>
        <tr>
            <th colspan="{{$title_colspan}}" style="border: 0px solid #ffffff; font-size:10px;"><b>REPORTE DE CIERRES DE CAJA SGRT</b></th>
        </tr>
        <tr>
            <th colspan="{{$title_colspan}}" style="border: 0px solid #ffffff; font-size:10px;">Generado e{{date_default_timezone_set("America/Lima")}} {{date("d/m/Y H:i:s", time())}}</th>
        </tr>
        <tr>
            <th colspan="{{$title_colspan}}" style="border: 0px solid #ffffff; font-size:10px;">Por: {{$cashboxes['user_logged_in']->name}} - {{$cashboxes['user_logged_in']->username}}</th>
        </tr>
        <tr> 
            <th style="{{$style_th}}">Apertura</th>
            <th style="{{$style_th}}">Cierre</th>
           @foreach($cashboxes['concepts'] as $key=>$concepts)
                <th style="{{$style_th}}">{{$concepts->code}}</th>
           @endforeach
            <th style="{{$style_th}}">Total</th>
        </tr>
    </thead>
    <tbody>

    @foreach($cashboxes['sum_concepts_total'] as $key=>$cashbox)
        <tr>
            @foreach($cashbox as $cashbox_details)
                <td style="border: 0px solid #ffffff; text-align:right; font-size:8px;">{{$cashbox_details}}</td>
            @endforeach
        </tr>
    @endforeach
    <tr>
        <td colspan="13" style="border: 0px solid #ffffff; text-align:right; font-size:8px;">{{$cashboxes['sum_total']}}</td>
    </tr>
    </tbody>
</table>