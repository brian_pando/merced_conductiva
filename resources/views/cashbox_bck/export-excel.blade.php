@php
    $title_colspan = 12;
    $style_th = "background:#404040; color:#ffffff; text-align:center;";
@endphp
<table>
    <thead>
        <tr>
            <th colspan="{{$title_colspan}}">{{$cashboxes['company']->name}}</th>
        </tr>
        <tr>
            <th colspan="{{$title_colspan}}">{{$cashboxes['company']->city}} - RUC. {{$cashboxes['company']->ruc}}</th>
        </tr>
        <tr>
            <th colspan="{{$title_colspan}}"><b>REPORTE DE CIERRES DE CAJA SGRT</b></th>
        </tr>
        <tr>
            <th colspan="{{$title_colspan}}">Generado e{{date_default_timezone_set("America/Lima")}} {{date("d/m/Y H:i:s", time())}}</th>
        </tr>
        <tr>
            <th colspan="{{$title_colspan}}">Por: {{$cashboxes['user_logged_in']->name}} - {{$cashboxes['user_logged_in']->username}}</th>
        </tr>
        <tr>
            <th style="{{$style_th}}">Apertura</th>
            <th style="{{$style_th}}">Cierre</th>
            @foreach($cashboxes['concepts'] as $key=>$concepts)
                <th style="{{$style_th}}">{{$concepts->code}}</th>
            @endforeach
            <th style="{{$style_th}}">Total</th>
        </tr>
    </thead>
    <tbody>
        @foreach($cashboxes['sum_concepts_total'] as $key=>$cashbox)
        <tr>
            @foreach($cashbox as $cashbox_details)
                <td style="text-align:right;">{{$cashbox_details}}</td>
            @endforeach
        </tr>
        @endforeach
        <tr>
            <td colspan="13" style="text-align:right;">{{$cashboxes['sum_total']}}</td>
        </tr>
    </tbody>
</table>