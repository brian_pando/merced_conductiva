
@php
    $colum = count($market['marketStatusAccount']['column']);
    $fontSize = $colum == 12 ? '8px' : '6px'; //12 medio año 8 año completo
@endphp

<table>
  <tr>
    <th style="border: 0px solid #fff; font-size: 8px;" colspan="{{ count($market['marketStatusAccount']['column'][0]) }}">
      {{ $market['marketStatusAccount']['company']['name'] }}
    </th>
  </tr>
  <tr>
    <th style="border: 0px solid #fff; font-size: 8px;" colspan="{{ count($market['marketStatusAccount']['column'][0]) }}">
      {{ $market['marketStatusAccount']['company']['city'] }} - RUC. {{ $market['marketStatusAccount']['company']['ruc'] }}
    </th>
  </tr>
  <tr>
    <th style="border: 0px solid #fff; font-size: 8px;" colspan="{{ count($market['marketStatusAccount']['column'][0]) }}">
      <b>REPORTES DE {{ $market['marketStatusAccount']['payment_status'] }} POR MESES: </b>
    </th>
  </tr>
  <tr>
    <th style="border: 0px solid #fff; font-size: 8px;" colspan="{{ count($market['marketStatusAccount']['column'][0]) }}">
      Generado el {{ date_default_timezone_set("America/Lima") }} {{ date("d/m/Y H:i:s", time()) }}
    </th>
  </tr>
  <tr>
    <th style="border: 0px solid #fff; font-size: 8px;" colspan="{{ count($market['marketStatusAccount']['column'][0]) }}">
      Por: {{ $market['user_logged_in']['name'] }} - {{ $market['user_logged_in']['username'] }}
    </th>
  </tr>
</table>



<table style="width:100%">
  
  <tr>
    @foreach($market['marketStatusAccount']['column'] as $key=>$column)
        <th style="border: 0px solid #404040; background:#404040; text-align: center; color:white;font-size: {{ $fontSize }};word-wrap: break-word;">{{ $column[0] }}</th> 
    @endforeach
  </tr>
  <!-- ... filas de datos de la segunda tabla ... -->
  @foreach($market['marketStatusAccount']['data'] as $key=>$market_data)
    <tr>
        @foreach($market['marketStatusAccount']['column'] as $key=>$column)
           <td style="text-align: left;  word-wrap: break-word; font-size: {{ $fontSize }};">{{ isset($market_data[$column[1]]) ? $market_data[$column[1]] : '' }} </td>     
        @endforeach
    </tr>
  @endforeach
</table>