<table>
  <tr>
    <th colspan="{{count($market['marketStatusAccount']['column'])}}">{{$market['marketStatusAccount']['company']['name']}}</th>
  </tr>
  <tr>
    <th colspan="{{count($market['marketStatusAccount']['column'])}}">{{$market['marketStatusAccount']['company']['city']}} - RUC. {{$market['marketStatusAccount']['company']['ruc']}}</th>
  </tr>
  <tr>
    <th colspan="{{count($market['marketStatusAccount']['column'])}}"><b>REPORTES DE {{$market['marketStatusAccount']['name_list_debtorpaid']}} POR AÑOS: {{strtoupper($market['marketStatusAccount']['search_names'])}}</b></th>
  </tr>
  <tr> 
    <th colspan="{{count($market['marketStatusAccount']['column'])}}">Generado e{{date_default_timezone_set("America/Lima")}} {{date("d/m/Y H:i:s", time())}}</th>
  </tr>
  <tr> 
    <th colspan="{{count($market['marketStatusAccount']['column'])}}">Por: {{$market['user_logged_in']['name']}} - {{$market['user_logged_in']['username']}}</th>
  </tr>
  <tr>
    @foreach($market['marketStatusAccount']['column'] as $key=>$column)
        <th style="background:#404040; color:#ffffff; text-align:center;">{{$column[0]}}</th> 
    @endforeach
  </tr>
    {{$travel=count($market['marketStatusAccount']['data'])}}
    @foreach($market['marketStatusAccount']['data'] as $key=>$market_data)
        {{$index=$key}}
        <tr>
        @foreach($market['marketStatusAccount']['column'] as $key=>$column)
            @if(($travel-1)!=$index)
                @if ( $column[1] === 'name')
                    <td style="text-align: left;">{{$market_data[$column[1]]}}</td> 
                @elseif ($column[1] === 'stand_code' || $column[1] === 'sector_name' || $column[1] === 'sectorname' || $column[1] === 'gyrename')
                    <td style="text-align: left;">{{ isset($market_data[$column[1]]) ? $market_data[$column[1]] : '' }}</td>
                @else
                    <td style="text-align:right;">{{number_format($market_data[$column[1]],2,'.',' ')}}</td> 
                @endif
            @else
                @if ( $column[1] === 'name')
                    <td style="text-align: left; font-weight: bold;">{{strtoupper($market_data[$column[1]])}}</td> 
                @elseif ($column[1] === 'stand_code' || $column[1] === 'sector_name' || $column[1] === 'sectorname' || $column[1] === 'gyrename')
                    <td style="text-align: left; font-weight: bold;">{{ isset($market_data[$column[1]]) ? $market_data[$column[1]] : ''}}</td>
                @else
                    <td style="text-align:right; font-weight: bold;">{{number_format($market_data[$column[1]],2,'.',' ')}}</td> 
                @endif
            @endif    
        @endforeach
        </tr>
    @endforeach
</table>