<table>
  <tr>
    <th style="border: 0px solid #fff; font-size: 8px;" colspan="{{count($market['marketStatusAccount']['column'][0])}}">{{$market['marketStatusAccount']['company']['name']}}</th>
  </tr>
  <tr>
    <th style="border: 0px solid #fff; font-size: 8px;" colspan="{{count($market['marketStatusAccount']['column'][0])}}">{{$market['marketStatusAccount']['company']['city']}} - RUC. {{$market['marketStatusAccount']['company']['ruc']}}</th>
  </tr>
  <tr>
    <th style="border: 0px solid #fff; font-size: 8px;" colspan="{{count($market['marketStatusAccount']['column'][0])}}"><b>REPORTES DE {{$market['marketStatusAccount']['name_list_debtorpaid']}} POR AÑOS: {{strtoupper($market['marketStatusAccount']['search_names'])}}</b></th>
  </tr>
  <tr> 
    <th style="border: 0px solid #fff; font-size: 8px;" colspan="{{count($market['marketStatusAccount']['column'][0])}}">Generado e{{date_default_timezone_set("America/Lima")}} {{date("d/m/Y H:i:s", time())}}</th>
  </tr>
  <tr> 
    <th style="border: 0px solid #fff; font-size: 8px;" colspan="{{count($market['marketStatusAccount']['column'][0])}}">Por: {{$market['user_logged_in']['name']}} - {{$market['user_logged_in']['username']}}</th>
  </tr>
</table>
@foreach($market['marketStatusAccount']['column'] as $key=>$columns)
<table style="width:100%">
  <tr>
    @foreach($columns as $key=>$column)
        <th style="border: 0px solid #404040; background:#404040; text-align: center; color:#fff; font-size: 10px;">{{$column[0]}}</th> 
    @endforeach
  </tr>
    {{$travel=count($market['marketStatusAccount']['data'])}}
    @foreach($market['marketStatusAccount']['data'] as $key=>$market_data)
        {{$index=$key}}
        <tr>
        @foreach($columns as $key=>$column)
            @if(($travel-2)==$index)
                @if ( $column[1] === 'name')
                    <td style="border: 1px solid black; border-left: 1px solid #ffffff; border-right: 1px solid #ffffff; text-align: left; font-size: 8px;">{{$market_data[$column[1]]}}</td> 
                @elseif ($column[1] === 'stand_code' || $column[1] === 'sector_name' || $column[1] === 'sectorname' || $column[1] === 'gyrename')
                    <td style="border: 1px solid black; border-left: 1px solid #ffffff; border-right: 1px solid #ffffff; text-align: left; font-size: 8px;">{{ isset($market_data[$column[1]]) ? $market_data[$column[1]] : '' }}</td>
                @else
                    <td style="border: 1px solid black; border-left: 1px solid #ffffff; border-right: 1px solid #ffffff; text-align:right; font-size: 8px;">{{number_format($market_data[$column[1]],2,'.',' ')}}</td> 
                @endif
            @elseif(($travel-1)==$index)
                @if ( $column[1] === 'name')
                    <td style="border: 0px solid #fff; text-align: left; font-weight: bold; font-size: 8px;">{{strtoupper($market_data[$column[1]])}}</td> 
                @elseif ($column[1] === 'stand_code' || $column[1] === 'sector_name' || $column[1] === 'sectorname' || $column[1] === 'gyrename')
                    <td style="border: 0px solid #fff; text-align: left; font-weight: bold; font-size: 8px;">{{ isset($market_data[$column[1]]) ? $market_data[$column[1]] : ''}}</td>
                @else
                    <td style="border: 0px solid #fff; text-align:right; font-weight: bold; font-size: 8px;">{{number_format($market_data[$column[1]],2,'.',' ')}}</td> 
                @endif
            @else
                @if ( $column[1] === 'name')
                    <td style="border: 0px solid #fff; text-align: left; font-size: 8px;">{{strtoupper($market_data[$column[1]])}}</td> 
                @elseif ($column[1] === 'stand_code' || $column[1] === 'sector_name' || $column[1] === 'sectorname' || $column[1] === 'gyrename')
                    <td style="border: 0px solid #fff; text-align: left; font-size: 8px;">{{ isset($market_data[$column[1]]) ? $market_data[$column[1]] : ''}}</td>
                @else
                    <td style="border: 0px solid #fff; text-align:right; font-size: 8px;">{{number_format($market_data[$column[1]],2,'.',' ')}}</td> 
                @endif
                
            @endif    
        @endforeach
        </tr>
    @endforeach
</table>
@endforeach