<table>
  <tr>
    <th colspan="{{count($market['marketSelectRow']['column'])}}">{{$market['marketSelectRow']['company']['name']}}</th>
  </tr>
  <tr>
    <th colspan="{{count($market['marketSelectRow']['column'])}}">{{$market['marketSelectRow']['company']['city']}} - RUC. {{$market['marketSelectRow']['company']['ruc']}}</th>
  </tr>
  <tr>
    <th colspan="{{count($market['marketSelectRow']['column'])}}"><b>REPORTES DE {{$market['marketSelectRow']['deuda_or_pagado']}} POR MESES: {{strtoupper($market['marketSelectRow']['search_names'])}}</b></th>
  </tr>
  <tr> 
    <th colspan="{{count($market['marketSelectRow']['column'])}}">Generado e{{date_default_timezone_set("America/Lima")}} {{date("d/m/Y H:i:s", time())}}</th>
  </tr>
  <tr> 
    <th colspan="{{count($market['marketSelectRow']['column'])}}">Por: {{$market['user_logged_in']['name']}} - {{$market['user_logged_in']['username']}}</th>
  </tr>
  @foreach($market['marketSelectRow']['data'] as $key=>$market_data)
    <tr>
        <th style="text-align: center; font-weight: bold;" colspan="{{count($market['marketSelectRow']['column'])}}">{{$market['marketSelectRow']['data'][$key]['market']['name']}}</th>
    </tr>
    <tr>
    @foreach($market['marketSelectRow']['column'] as $key=>$column)
        <th style="background:#404040; text-align: center; color:#ffffff;">{{$column[0]}}</th> 
    @endforeach
    </tr>
    @foreach($market_data['debts'] as $key=>$data_debts)
      <tr>
        @foreach($market['marketSelectRow']['column'] as $key=>$column)
          @if($column[1]=='year')
            <td style="text-align: left;">{{$data_debts[$column[1]]}}</td>
          @elseif($column[1]!='total')
            <td style="text-align: right;">{{$data_debts[$column[1]]['total']}}</td>
          @else
            <td style="text-align: right;">{{$data_debts[$column[1]]}}</td>
          @endif
        @endforeach
      </tr>
    @endforeach 
  @endforeach
</table>