@php
    $colspan = count($market['result']);
@endphp
<table style="text-align: left;">
    <thead>
        <tr>
            <th style="border: 0px solid #ffffff; font-size:9px;" colspan="{{$colspan+5}}">{{$market['company']['name']}}</th>
        </tr>
        <tr>
            <th style="border: 0px solid #ffffff; font-size:9px;" colspan="{{$colspan+5}}">{{$market['company']['city']}} - RUC. {{$market['company']['ruc']}}</th>
        </tr>
        <tr>
            <th style="border: 0px solid #ffffff; font-size:9px;" colspan="{{$colspan+5}}"><b>REPORTES DE {{$market['search_names']}}</b></th>
        </tr>
        <tr>
            <th style="border: 0px solid #ffffff; font-size:9px;" colspan="{{$colspan+5}}">Generado e{{date_default_timezone_set("America/Lima")}} {{date("d/m/Y H:i:s", time())}}</th>
        </tr>
        <tr>
            <th style="border: 0px solid #ffffff; font-size:9px;" colspan="{{$colspan+5}}">Por: {{$market['user_logged']['name']}} - {{$market['user_logged']['username']}}</th>
        </tr>
        
        <tr>
            <th style="border: 0px solid #404040; background:#404040; color:#ffffff; text-align:left; font-size:7px;">Puesto</th>
            <th style="border: 0px solid #404040; background:#404040; color:#ffffff; text-align:center; font-size:7px;">Sector</th>
            <th style="border: 0px solid #404040; background:#404040; color:#ffffff; text-align:center; font-size:7px;">DNI</th>
            <th style="border: 0px solid #404040; background:#404040; color:#ffffff; text-align:center; font-size:7px;">Titular</th>
            <th style="border: 0px solid #404040; background:#404040; color:#ffffff; text-align:center; font-size:7px;">Tasa</th>
            @foreach($market['result'] as $item)
                <th style="border: 0px solid #404040; background:#404040; color:#ffffff; text-align:center; font-size:7px;">{{$item->year}}</th> 
            @endforeach
        </tr>
        
    </thead>
    <tbody> 
        @foreach($market['datos'] as $item_data)
            <tr>
                @foreach($item_data as $item)
                    <td style="border: 0px solid #ffffff; font-size:7px;">{{$item}}</td>
                @endforeach
            </tr>
        @endforeach
        <tr>
            <td style="border: 0px solid #ffffff; font-size:7px; text-align:center;" colspan="5"><b>Total</b></td>
            @foreach($market['result'] as $item)
                <td style="border: 0px solid #ffffff; font-size:7px; text-align:right;">{{$item->total}}</td>
            @endforeach
        </tr>
    </tbody>
</table>