<table>
  <tr>
    <th colspan="{{count($market['marketStatusAccount']['column'])}}">{{$market['marketStatusAccount']['company']['name']}}</th>
  </tr>
  <tr>
    <th colspan="{{count($market['marketStatusAccount']['column'])}}">{{$market['marketStatusAccount']['company']['city']}} - RUC. {{$market['marketStatusAccount']['company']['ruc']}}</th>
  </tr>
  <tr>
    <th colspan="{{count($market['marketStatusAccount']['column'])}}"><b>REPORTES DE {{$market['marketStatusAccount']['payment_status']}} POR MESES: </b></th>
  </tr>
  <tr> 
    <th colspan="{{count($market['marketStatusAccount']['column'])}}">Generado e{{date_default_timezone_set("America/Lima")}} {{date("d/m/Y H:i:s", time())}}</th>
  </tr>
  <tr> 
    <th colspan="{{count($market['marketStatusAccount']['column'])}}">Por: {{$market['user_logged_in']['name']}} - {{$market['user_logged_in']['username']}}</th>
  </tr>
  <tr>
    @foreach($market['marketStatusAccount']['column'] as $key=>$column)
        <th style="background:#404040; color:#ffffff; text-align:center;">{{$column[0]}}</th> 
    @endforeach
  </tr>
    {{$travel=count($market['marketStatusAccount']['data'])}}

    @foreach($market['marketStatusAccount']['data'] as $key=>$market_data)
        {{$index=$key}}
        <tr>
        @foreach($market['marketStatusAccount']['column'] as $key=>$column)
            <td style="text-align: left;">{{ isset($market_data[$column[1]]) ? $market_data[$column[1]] : '' }}</td>  
        @endforeach
        </tr>
    @endforeach
</table>