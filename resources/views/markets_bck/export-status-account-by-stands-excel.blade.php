@php
    $colspan = count($market['result']);
@endphp
<table>
    <thead>
        <tr>
            <th colspan="{{$colspan+5}}">{{$market['company']['name']}}</th>
        </tr>
        <tr>
            <th colspan="{{$colspan+5}}">{{$market['company']['city']}} - RUC. {{$market['company']['ruc']}}</th>
        </tr>
        <tr>
            <th colspan="{{$colspan+5}}"><b>REPORTES DE {{$market['search_names']}}</b></th>
        </tr>
        <tr>
            <th colspan="{{$colspan+5}}">Generado e{{date_default_timezone_set("America/Lima")}} {{date("d/m/Y H:i:s", time())}}</th>
        </tr>
        <tr>
            <th colspan="{{$colspan+5}}">Por: {{$market['user_logged']['name']}} - {{$market['user_logged']['username']}}</th>
        </tr>
        
        <tr>
            <th style="background:#404040; color:#ffffff; text-align:center;">Nro</th>
            <th style="background:#404040; color:#ffffff; text-align:center;">Sector</th>
            <th style="background:#404040; color:#ffffff; text-align:center;">DNI</th>
            <th style="background:#404040; color:#ffffff; text-align:center;">Titular</th>
            <th style="background:#404040; color:#ffffff; text-align:center;">Tasa</th>
            @foreach($market['result'] as $item)
                <th style="background:#404040; color:#ffffff; text-align:center;">{{$item->year}}</th> 
            @endforeach
        </tr>
        
    </thead>
    <tbody> 
        @foreach($market['datos'] as $item_data)
            <tr>
                @foreach($item_data as $item)
                    <td>{{$item}}</td>
                @endforeach
            </tr>
        @endforeach
        <tr>
            <td style="text-align:center;" colspan="5"><b>Total</b></td>
            @foreach($market['result'] as $item)
                <td style="text-align:right;">{{$item->total}}</td>
            @endforeach
        </tr>
    </tbody>
</table>