@php
    $title_colspan = 7;
    $count = 1;
    $font_size_titles = 'font-size:9px; border: 0 solid #ffffff;';
    $style_th = "border: 0px solid #404040; background:#404040; text-align: center; color:#fff; font-size:8px;";
    $style_td_with_border_all = "border: 0px solid #ffffff; font-size:6px;";
@endphp
<table>
  <tr>
    <th style="{{$font_size_titles}}" colspan="{{$title_colspan}}">{{$debts_direct_fine['company']['name']}}</th>
  </tr>
  <tr>
    <th style="{{$font_size_titles}}" colspan="{{$title_colspan}}">{{$debts_direct_fine['company']['city']}} - RUC. {{$debts_direct_fine['company']['ruc']}}</th>
  </tr>
  <tr>
    <th style="{{$font_size_titles}}" colspan="{{$title_colspan}}"><b>REPORTES DE MULTAS DIRECTAS</b></th>
  </tr>
  <tr> 
    <th style="{{$font_size_titles}}" colspan="{{$title_colspan}}">Generado e{{date_default_timezone_set("America/Lima")}} {{date("d/m/Y H:i:s", time())}}</th>
  </tr>
  <tr> 
    <th style="{{$font_size_titles}}" colspan="{{$title_colspan}}">Por: {{$debts_direct_fine['user_logged_in']['name']}} - {{$debts_direct_fine['user_logged_in']['username']}}</th>
  </tr>
  <tr>
    
    <th style="{{$style_th}}">Cod. Multa</th>
    <th style="{{$style_th}}">Fecha</th> 
    <th style="{{$style_th}}">DNI</th>
    <th style="{{$style_th}}">Titular</th>
    <th style="{{$style_th}}">Vigencia</th>
    <th style="{{$style_th}}">Estado</th>
    <th style="{{$style_th}}">Monto</th>
  </tr>
    {{$travel=count($debts_direct_fine['listDirectFine']['data'])}}
    @foreach($debts_direct_fine['listDirectFine']['data'] as $key=>$direct_fine_data)   
        <tr>
          <td style="{{$style_td_with_border_all}}">{{$direct_fine_data['code']}}</td>
          <td style="{{$style_td_with_border_all}}">{{date_format(date_create($direct_fine_data['date_at']),"d/m/Y")}}</td>
          <td style="{{$style_td_with_border_all}}">{{$direct_fine_data['titular_dni']}}</td>
          <td style="{{$style_td_with_border_all}}">{{$direct_fine_data['titular_name']}} {{$direct_fine_data['titular_lastname']}}</td>
          <td style="{{$style_td_with_border_all}} text-align:rigth;">{{$direct_fine_data['payment_status']==1 ? 'Pagado' : 'Deuda'}}</td>
          <td style="{{$style_td_with_border_all}} text-align:rigth;"> {{ $direct_fine_data['status']==1 ? 'Activo' : 'Anulado' }}</td>
          <td style="{{$style_td_with_border_all}} text-align:rigth;">{{$direct_fine_data['total']}}</td>
        </tr>
    @endforeach
</table>