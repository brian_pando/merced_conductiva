<table>
  <tr>
    <th colspan="7">{{$debts_direct_fine['company']['name']}}</th>
  </tr>
  <tr>
    <th colspan="7">{{$debts_direct_fine['company']['city']}} - RUC. {{$debts_direct_fine['company']['ruc']}}</th>
  </tr>
  <tr>
    <th colspan="7"><b>REPORTES DE MULTAS DIRECTAS</b></th>
  </tr>
  <tr> 
    <th colspan="7">Generado e{{date_default_timezone_set("America/Lima")}} {{date("d/m/Y H:i:s", time())}}</th>
  </tr>
  <tr> 
    <th colspan="7">Del: {{$debts_direct_fine['report_date']}}</th>
  </tr>
  <tr> 
    <th colspan="7">Por: {{$debts_direct_fine['user_logged']['name']}} - {{$debts_direct_fine['user_logged']['username']}}</th>
  </tr>
  <tr>
    <th style="background:#404040; color:#ffffff; text-align:center;">Cod. Multa</th>
    <th style="background:#404040; color:#ffffff; text-align:center;">Fecha</th> 
    <th style="background:#404040; color:#ffffff; text-align:center;">DNI</th>
    <th style="background:#404040; color:#ffffff; text-align:center;">Titular</th>
    <th style="background:#404040; color:#ffffff; text-align:center;">Vigencia</th>
    <th style="background:#404040; color:#ffffff; text-align:center;">Estado</th>
    <th style="background:#404040; color:#ffffff; text-align:center;">Monto</th>
  </tr>

    @foreach($debts_direct_fine['list_direct_fine'] as $key=>$direct_fine_data)   
        <tr>
          <td>{{$direct_fine_data['code']}}</td>
          <td>{{date_format(date_create($direct_fine_data['date_at']),"d/m/Y")}}</td>
          <td style="text-align:left;">{{$direct_fine_data['titular_dni']}}</td>
          <td>{{$direct_fine_data['titular_name']}} {{$direct_fine_data['titular_lastname']}}</td>
          <td>{{$direct_fine_data['payment_status']==1 ? 'Pagado' : 'Deuda'}}</td>
          <td> {{ $direct_fine_data['status']==1 ? 'Activo' : 'Anulado' }}</td>
          <td style="text-align:rigth;">{{$direct_fine_data['total']}}</td>
        </tr>
    @endforeach
</table>