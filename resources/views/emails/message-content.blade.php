

<h4> Sr(a). {{ $data['name'] }} {{ $data['lastname'] }}</h4>


    <p>Le hacemos llegar la notificación: {{ $data['code'] }} acerca de una deuda pendiente de pago de: {{ number_format($data['totalDebt'], 2, ',', '.') }}.</p>

    <p>Para conocer los detalles, apérsonese a las oficinas del area de recaudacion tributaria de la Municipalidad Provincial de Leoncio Prado</p>

    <p>Gracias por su atención.</p>

    <p>Atentamente</p>
    <p>Municipalidad Provincial de Leoncio Prado</p>