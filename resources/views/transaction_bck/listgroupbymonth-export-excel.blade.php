<table>
    <thead>
        <tr>
            <th colspan="15">{{$transactions['company']['name']}}</th>
        </tr>
        <tr>
            <th colspan="15">{{$transactions['company']['city']}} - RUC. {{$transactions['company']['ruc']}}</th>
        </tr>
        <tr>
            <th colspan="15"><b>REPORTE DE COMISIONISTAS POR RECAUDACIONES MENSUALES - {{$transactions['commissionersList']['year']}}</b></th>
        </tr>
        <tr>
            <th colspan="15">Generado e{{date_default_timezone_set("America/Lima")}} {{date("d/m/Y H:i:s", time())}}</th>
        </tr>
        <tr>
            <th colspan="15">Por: {{$transactions['user_logged_in']['name']}} - {{$transactions['user_logged_in']['username']}}</th>
        </tr>
        <tr>
            @foreach($transactions['commissionersList']['column'] as $key=>$column)
                <th style="background:#404040; color:#ffffff; text-align:center;">{{$column[0]}}</th>
            @endforeach
        </tr> 
    </thead>
    <tbody>
        @foreach($transactions['commissionersList']['data'] as $key_t=>$market_data)
        <tr>
            @foreach($transactions['commissionersList']['column'] as $key=>$column)
                @if((count($transactions['commissionersList']['data'])-1)!=$key_t)
                    @if($column[1]==='comisionista')
                        <td>{{isset($market_data['name']) ? $market_data['name'] : ''}}</td>
                    @elseif($column[1]==='position')
                        <td>{{isset($market_data['position']) ? $market_data['position'] : ''}}</td>
                    @elseif($column[1]==='ene')
                        <td>{{isset($market_data[1]) ? $market_data[1] : ''}}</td>
                    @elseif($column[1]==='feb')
                        <td>{{isset($market_data[2]) ? $market_data[2] : ''}}</td>
                    @elseif($column[1]==='mar')
                        <td>{{isset($market_data[3]) ? $market_data[3] : ''}}</td>
                    @elseif($column[1]==='abr')
                        <td>{{isset($market_data[4]) ? $market_data[4] : ''}}</td>
                    @elseif($column[1]==='may')
                        <td>{{isset($market_data[5]) ? $market_data[5] : ''}}</td>
                    @elseif($column[1]==='jun')
                        <td>{{isset($market_data[6]) ? $market_data[6] : ''}}</td>
                    @elseif($column[1]==='jul')
                        <td>{{isset($market_data[7]) ? $market_data[7] : ''}}</td>
                    @elseif($column[1]==='ago')
                        <td>{{isset($market_data[8]) ? $market_data[8] : ''}}</td>
                    @elseif($column[1]==='set')
                        <td>{{isset($market_data[9]) ? $market_data[9] : ''}}</td>
                    @elseif($column[1]==='oct')
                        <td>{{isset($market_data[10]) ? $market_data[10] : ''}}</td>
                    @elseif($column[1]==='nov')
                        <td>{{isset($market_data[11]) ? $market_data[11] : ''}}</td>
                    @elseif($column[1]==='dic')
                        <td>{{isset($market_data[12]) ? $market_data[12] : ''}}</td>
                    @elseif($column[1]==='total')
                        <td>{{isset($market_data['total']) ? $market_data['total'] : ''}}</td>
                    @endif
                @else
                    @if($column[1]==='comisionista')
                        <td style="font-weight: bold;">{{isset($market_data['name']) ? $market_data['name'] : ''}}</td>
                    @elseif($column[1]==='position')
                        <td style="font-weight: bold;">{{isset($market_data['position']) ? $market_data['position'] : ''}}</td>
                    @elseif($column[1]==='ene')
                        <td style="font-weight: bold;">{{isset($market_data[1]) ? $market_data[1] : ''}}</td>
                    @elseif($column[1]==='feb')
                        <td style="font-weight: bold;">{{isset($market_data[2]) ? $market_data[2] : ''}}</td>
                    @elseif($column[1]==='mar')
                        <td style="font-weight: bold;">{{isset($market_data[3]) ? $market_data[3] : ''}}</td>
                    @elseif($column[1]==='abr')
                        <td style="font-weight: bold;">{{isset($market_data[4]) ? $market_data[4] : ''}}</td>
                    @elseif($column[1]==='may')
                        <td style="font-weight: bold;">{{isset($market_data[5]) ? $market_data[5] : ''}}</td>
                    @elseif($column[1]==='jun')
                        <td style="font-weight: bold;">{{isset($market_data[6]) ? $market_data[6] : ''}}</td>
                    @elseif($column[1]==='jul')
                        <td style="font-weight: bold;">{{isset($market_data[7]) ? $market_data[7] : ''}}</td>
                    @elseif($column[1]==='ago')
                        <td style="font-weight: bold;">{{isset($market_data[8]) ? $market_data[8] : ''}}</td>
                    @elseif($column[1]==='set')
                        <td style="font-weight: bold;">{{isset($market_data[9]) ? $market_data[9] : ''}}</td>
                    @elseif($column[1]==='oct')
                        <td style="font-weight: bold;">{{isset($market_data[10]) ? $market_data[10] : ''}}</td>
                    @elseif($column[1]==='nov')
                        <td style="font-weight: bold;">{{isset($market_data[11]) ? $market_data[11] : ''}}</td>
                    @elseif($column[1]==='dic')
                        <td style="font-weight: bold;">{{isset($market_data[12]) ? $market_data[12] : ''}}</td>
                    @elseif($column[1]==='total')
                        <td style="font-weight: bold;">{{isset($market_data['total']) ? $market_data['total'] : ''}}</td>
                    @endif
                @endif
            @endforeach
        </tr>
        @endforeach
    </tbody>  
</table>