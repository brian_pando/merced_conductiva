@php
    $title_colspan = 9;
    $count = 1;
@endphp
<!-- NOTA: Cualquier cambio aqui, tambien hacerlo en el detailed-export-pdf.blade -->
<table>
    <thead>
        <tr>
            <th colspan="{{$title_colspan}}" >{{$transactions['company']->name}}</th>
        </tr>
        <tr>
            <th colspan="{{$title_colspan}}">{{$transactions['company']->city}} - RUC. {{$transactions['company']->ruc}}</th>
        </tr>
        <tr>
            <th colspan="{{$title_colspan}}" ><b>REPORTE DE PAGOS DETALLADO EN CAJA SGRT</b></th>
        </tr>
        <tr>
            <th colspan="{{$title_colspan}}" >Generado e{{date_default_timezone_set("America/Lima")}} {{date("d/m/Y H:i:s", time())}}</th>
        </tr>
        <tr>
            <th colspan="{{$title_colspan}}" >Del: {{$transactions['report_date']}} </th>
        </tr>
        <tr>
            <th colspan="{{$title_colspan}}" >Generado por: {{$transactions['user_logged_in']->name}} - {{$transactions['user_logged_in']->username}}</th>
        </tr>
        <tr>
            <th style="background:#404040; color:#ffffff; text-align:center;"> Correlativo</th>
            <th style="background:#404040; color:#ffffff; text-align:center;"> Fecha</th>
            <th style="background:#404040; color:#ffffff; text-align:center;"> Cliente</th>
            <th style="background:#404040; color:#ffffff; text-align:center;"> DNI</th>
            <th style="background:#404040; color:#ffffff; text-align:center;"> Detalle</th>
            <th style="background:#404040; color:#ffffff; text-align:center;"> Codi. Clasifi.</th>
            <th style="background:#404040; color:#ffffff; text-align:center;"> Monto</th>
            <th style="background:#404040; color:#ffffff; text-align:center;"> Monto E.</th>
            <th style="background:#404040; color:#ffffff; text-align:center;"> Estado</th>
        </tr>
    </thead>
    <tbody>
    @foreach($transactions['data'] as $key_t=>$transaction):
        @foreach($transaction['transactiondetails'] as $key=>$detail):
            <tr>
                <td style="text-align:center;">{{$transaction['code']}}{{$transaction['status'] == 1 ? '' : '(E)'}}</td>
                <td style="text-align:center;">{{date_format(date_create($transaction['date_at']),"d/m/Y")}}</td>
                <td style="text-align:left;">{{strtoupper($transaction['client']->name)}} {{strtoupper($transaction['client']->lastname)}}</td>
                <td style="text-align:left;">{{$transaction['client']->dni}}</td>
                <td style="text-align:left;">{{$detail['detail']}}</td>
                <td style="text-align:center;">{{$detail['classifier_ref']}}</td>
                @if($detail['status']==1)
                    <td style="text-align:right;">{{$detail['total']}}</td>
                    <td style="text-align:right;"></td>
                @endif
                @if($detail['status']==0)
                    <td style="text-align:right;"></td>
                    <td style="text-align:right;">{{$detail['total']}}</td>
                @endif
                <td style="text-align:center;">{{$transaction['status'] == 1 ? 'Pagado' : 'Extornado'}}</td>
            </tr>
        @endforeach
    @endforeach
            <tr>
                <td style="text-align:center; font-weight: bold;" colspan="6">TOTAL</td>
                <td style="text-align:right; font-weight: bold;">{{$transactions['transactions_total']}}</td>
                <td style="text-align:right; font-weight: bold;">{{$transactions['sum_total_extornado']}}</td> 
                <td></td>
            </tr>
    </tbody>
</table>