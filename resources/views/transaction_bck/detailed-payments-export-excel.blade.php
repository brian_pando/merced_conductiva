@php
    $title_colspan = 8;
    $style_th = "text-align:center; border: 1px solid #404040; 
                background:#404040; color: #ffffff;";
@endphp

<table>
    <thead>
        <tr>
            <th colspan="{{$title_colspan}}">{{$transactions['company']->name}}</th>
        </tr>
        <tr>
            <th colspan="{{$title_colspan}}">{{$transactions['company']->city}} - RUC. {{$transactions['company']->ruc}}</th>
        </tr>
        <tr>
            <th colspan="{{$title_colspan}}"><b>{{$transactions['title']}}: {{strtoupper($transactions['search_names'])}}</b></th>
        </tr>
        <tr>
            <th colspan="{{$title_colspan}}">Generado e{{date_default_timezone_set("America/Lima")}} {{date("d/m/Y H:i:s", time())}}</th>
        </tr>
        <tr>
            <th colspan="{{$title_colspan}}">Del: {{$transactions['report_date']}}</th>
        </tr>
        <tr>
            <th colspan="{{$title_colspan}}">Generado por: {{$transactions['user_logged']->name}} - {{$transactions['user_logged']->username}}</th>
        </tr>
        <tr>
            <th style="{{$style_th}}">Correlativo</th>
            <th style="{{$style_th}}">Fecha</th>
            <th style="{{$style_th}}">Responsable</th>
            <th style="{{$style_th}}">Cliente</th>
            <th style="{{$style_th}}">DNI</th>
            <th style="{{$style_th}}">Detalle</th>
            <th style="{{$style_th}}">Monto</th>
            <th style="{{$style_th}}">Estado</th>
        </tr>
    </thead>
    <tbody>
    @foreach($transactions['data'] as $key=>$transaction)
        <tr>
            <td>{{$transaction['code']}}</td>
            <td style="text-align:center;">{{date_format(date_create($transaction['date_at']),"d/m/Y")}}</td>
            <td style="text-align:left;">{{$transaction['user_name']}}-{{$transaction['user_username']}}</td>
            <td style="text-align:left;">{{strtoupper($transaction['client_name'])}} {{strtoupper($transaction['client_lastname'])}}</td>
            <td style="text-align:left;">{{$transaction['client_dni']}}</td>
            <td style="text-align:left;">{{$transaction['detail']}}</td>
            <td style="text-align:right;">{{$transaction['total']}}</td>
            <td style="text-align:right;">{{$transaction['status']==0 ? 'E' : 'P'}}</td>
        </tr>
    @endforeach
    <tr>
        <td style="text-align:center; font-weight: bold;" colspan="6">TOTAL</td>
        <td style="text-align:rigth; font-weight: bold;">{{$transactions['sum_total']}}</td>
        <td></td> 
    </tr>
    </tbody>
</table>