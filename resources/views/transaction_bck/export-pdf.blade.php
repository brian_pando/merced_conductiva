@php
    $title_colspan = 9;
    $font_size_titles = 'font-size:8px; border: 0px solid #ffffff;';
    $style_th = "text-align:center; border: 1px solid #404040; font-size:8px; 
                background:#404040; color: #ffffff;";
    $style_td_with_border_right_left = "border-left: 0 solid #fff;border-right: 0 solid #fff; font-size:6px;";
    $style_td_with_border_all = "border: 0 solid #fff; font-size:6px;";
@endphp
<table>

    <thead>
        <tr>
            <th colspan="7" style="{{$font_size_titles}}">{{$transactions['company']->name}}</th>
        </tr>
        <tr>
            <th colspan="7" style="{{$font_size_titles}}">{{$transactions['company']->city}} - RUC. {{$transactions['company']->ruc}}</th>
        </tr>
        <tr>
            <th colspan="7" style="{{$font_size_titles}}"><b>REPORTE DE PAGOS EN CAJA SGRT</b></th>
        </tr>
        <tr> 
            <th colspan="7" style="{{$font_size_titles}}">Generado e{{date_default_timezone_set("America/Lima")}} {{date("d/m/Y H:i:s", time())}}</th>
        </tr>
        <tr>
            <th colspan="7" style="{{$font_size_titles}}">Del: {{$transactions['report_date']}}</th>
        </tr>
        <tr>
            <th colspan="7" style="{{$font_size_titles}}">Generado por: {{$transactions['user_logged_in']->name}} - {{$transactions['user_logged_in']->username}}</th>
        </tr>
        <tr>
            <th style="{{$style_th}}">Correlativo</th>
            <th style="{{$style_th}}">Fecha</th>
            <th style="{{$style_th}}">DNI</th>
            <th style="{{$style_th}}">Cliente</th>
            <th style="{{$style_th}}">Obs. E.</th>
            <th style="{{$style_th}}">Monto</th>
            <th style="{{$style_th}}">Monto E.</th>
        </tr>
    </thead>
    <tbody>
    {{$travel=count($transactions['data'])}}
    @foreach($transactions['data'] as $key=>$transaction)
        @if(($travel-1)==$key)
        <tr>
            <td style="{{$style_td_with_border_right_left}} text-align:left;">{{$transaction['code']}}{{$transaction['status'] == 1 ? '' : '(E)'}}</td>
            <td style="{{$style_td_with_border_right_left}} text-align: center;">{{date_format(date_create($transaction['date_at']),"d/m/Y")}}</td>
            <td style="{{$style_td_with_border_right_left}} text-align: center;">{{$transaction['client']->dni}}</td>
            <td style="{{$style_td_with_border_right_left}} text-align: left;">{{$transaction['client']->name}} {{$transaction['client']->lastname}}</td>
            <td style="{{$style_td_with_border_right_left}} text-align: left;">{{$transaction['message_delete']}}</td>
            @if($transaction['status']==1)
                <td style="{{$style_td_with_border_right_left}} text-align: right;">{{$transaction['total']}}</td>
                <td style="{{$style_td_with_border_right_left}} text-align: right;"></td>
            @endif
            @if($transaction['status']==0)
                <td style="{{$style_td_with_border_right_left}} text-align: right;"></td>
                <td style="{{$style_td_with_border_right_left}} text-align: right;">{{$transaction['total']}}</td>
            @endif
            
        </tr>
        @else
        <tr>
            <td style="{{$style_td_with_border_all}} text-align:left;">{{$transaction['code']}}{{$transaction['status'] == 1 ? '' : '(E)'}}</td>
            <td style="{{$style_td_with_border_all}} text-align: center;">{{date_format(date_create($transaction['date_at']),"d/m/Y")}}</td>
            <td style="{{$style_td_with_border_all}} text-align: center;">{{$transaction['client']->dni}}</td>
            <td style="{{$style_td_with_border_all}} text-align: left;">{{$transaction['client']->name}} {{$transaction['client']->lastname}}</td>
            <td style="{{$style_td_with_border_all}} text-align: left;">{{$transaction['message_delete']}}</td>
            @if($transaction['status']==1)
                <td style="{{$style_td_with_border_all}} text-align: right;">{{$transaction['total']}}</td>
                <td style="{{$style_td_with_border_all}} text-align: right;"></td>
            @endif
            @if($transaction['status']==0)
                <td style="{{$style_td_with_border_all}} text-align: right;"></td>
                <td style="{{$style_td_with_border_all}} text-align: right;">{{$transaction['total']}}</td>
            @endif
        </tr>
        @endif
    @endforeach
        <tr>
            <td style="border: 0px solid #fff; text-align: center; font-size:6px; font-weight: bold;" colspan="5">TOTAL</td>
            <td style="border: 0px solid #fff; text-align: right; font-size:6px; font-weight: bold;">{{$transactions['transactions_total']}}</td>
            <td style="border: 0px solid #fff; text-align: right; font-size:6px; font-weight: bold;">{{$transactions['sum_total_extornado']}}</td>
        </tr>
        <tr><td colspan="7" style="text-align:center; border: 0px solid #fff; font-size:6px; height:40px;">&nbsp;</td></tr>
        <tr>
            <td colspan="4" style="text-align:center; border: 0px solid #fff; font-size:6px;">_ _ _ _ _ _ _ _ _ _ _ _ _ _ _  _</td>
            <td colspan="3" style="text-align:center; border: 0px solid #fff; font-size:6px;">_ _ _ _ _ _ _ _ _ _ _ _ _ _ _  _</td>
        </tr>
        <tr>
            <td colspan="4" style="text-align:center; border: 0px solid #fff; font-size:6px;">Jefe de Tesoreria</td>
            <td colspan="3" style="text-align:center; border: 0px solid #fff; font-size:6px;">BV Jefe cajero</td>
        </tr>
    </tbody>
</table>