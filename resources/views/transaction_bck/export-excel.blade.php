<table>
    <caption style="text-align:center"></caption>
    <thead>
        <tr>
            <th colspan="7">{{$transactions['company']->name}}</th>
        </tr>
        <tr>
            <th colspan="7">{{$transactions['company']->city}} - RUC. {{$transactions['company']->ruc}}</th>
        </tr>
        <tr>
            <th colspan="7"><b>REPORTE DE PAGOS EN CAJA SGRT</b></th>
        </tr>
        <tr> 
            <th colspan="7">Generado e{{date_default_timezone_set("America/Lima")}} {{date("d/m/Y H:i:s", time())}}</th>
        </tr>
        <tr>
            <th colspan="7">Del: {{$transactions['report_date']}}</th>
        </tr>
        <tr>
            <th colspan="7">Generado por: {{$transactions['user_logged_in']->name}} - {{$transactions['user_logged_in']->username}}</th>
        </tr>
        <tr>
            <th style="text-align:center; background:#404040; color: #ffffff;">Correlativo</th>
            <th style="text-align:center; background:#404040; color: #ffffff;">Fecha</th>
            <th style="text-align:center; background:#404040; color: #ffffff;">DNI</th>
            <th style="text-align:center; background:#404040; color: #ffffff;">Cliente</th>
            <th style="text-align:center; background:#404040; color: #ffffff;">Obs. E.</th>
            <th style="text-align:center; background:#404040; color: #ffffff;">Monto</th>
            <th style="text-align:center; background:#404040; color: #ffffff;">Monto E.</th>
        </tr>
    </thead>
    <tbody>
    @foreach($transactions['data'] as $key=>$transaction)
        <tr>
            <td style="text-align: left;">{{$transaction['code']}}{{$transaction['status'] == 1 ? '' : '(E)'}}</td>
            <td style="text-align: left;">{{date_format(date_create($transaction['date_at']),"d/m/Y")}}</td>
            <td style="text-align: right;">{{$transaction['client']->dni}}</td>
            <td style="text-align: left;">{{$transaction['client']->name}} {{$transaction['client']->lastname}}</td>
            <td style="text-align: left;">{{$transaction['message_delete']}}</td>
            @if($transaction['status']==1)
                <td style="text-align: right;">{{$transaction['total']}}</td>
                <td style="text-align: right;"></td>
            @endif
            @if($transaction['status']==0)
                <td style="text-align: right;"></td>
                <td style="text-align: right;">{{$transaction['total']}}</td>
            @endif
        </tr>

    @endforeach
        <tr>
            <td style="text-align: center; font-weight: bold;" colspan="5">TOTAL</td>
            <td style="text-align: right; font-weight: bold;">{{$transactions['transactions_total']}}</td>
            <td style="text-align: right; font-weight: bold;">{{$transactions['sum_total_extornado']}}</td>
        </tr>       
    </tbody>
</table>