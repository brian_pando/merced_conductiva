@php
$title_colspan = 8;
$font_size_titles = 'font-size:8px; border: 0px solid #ffffff;';
$style_th = "border: 1px solid #404040; text-align:center;
background:#404040; color: #ffffff; font-size:6px;";
$style_td_with_border_right_left = "border-left: 0 solid #fff;border-right: 0 solid #fff; font-size:5px;";
$style_td_with_border_all = "border: 0 solid #fff; font-size:5px;";
@endphp

<table>
    <thead>
        <tr>
            <th colspan="{{$title_colspan}}" style="{{$font_size_titles}}" >{{$transactions['company']->name}}</th>
        </tr>
        <tr>
            <th style="{{$font_size_titles}} " colspan="{{$title_colspan}}">{{$transactions['company']->city}} - RUC. {{$transactions['company']->ruc}}</th>
        </tr>
        <tr>
            <th colspan="{{$title_colspan}}" style="{{$font_size_titles}}" ><b>REPORTE DE RECAUDACIÓN POR COMISIONISTA :{{ date("d/m/Y", strtotime($transactions['date_init'])) }} - {{ date("d/m/Y", strtotime($transactions['date_end'])) }} </b></th>
        </tr>
        <tr>
            <th colspan="{{$title_colspan}}" style="{{$font_size_titles}}" ><b>Comisionista: {{strtoupper($transactions['search_names'])}}</b></th>
        </tr>
        <tr>
            <th colspan="{{$title_colspan}}" style="{{$font_size_titles}}" >Generado por: {{$transactions['user_logged']->name}} - {{$transactions['user_logged']->username}}</th>
        </tr>
        <tr>
            <th colspan="{{$title_colspan}}" style="{{$font_size_titles}}" >Fecha Operacón  {{date("d/m/Y H:i:s", time())}}</th>
        </tr>
        <tr>
            <th style="{{$style_th}}">Correlativo</th>
            <th style="{{$style_th}}">Fecha</th>
            <th style="{{$style_th}}">Responsable</th>
            <th style="{{$style_th}}">Cliente</th>
            <th style="{{$style_th}}">DNI</th>
            <th style="{{$style_th}}">Detalle</th>
            <th style="{{$style_th}}">Estado</th>
            <th style="{{$style_th}}">Monto</th>
        </tr>
    </thead>
    <tbody>
       {{$travel=count($transactions['data'])}}
       @foreach($transactions['data'] as $key=>$transaction)
       <tr>
           <td style="{{$style_td_with_border_all}} text-align:center;">{{$transaction['code']}}</td>
           <td style="{{$style_td_with_border_all}} text-align:center;">{{date_format(date_create($transaction['date_at']),"d/m/Y")}}</td>
           <td style="{{$style_td_with_border_all}} text-align:left;">{{$transaction['user_name']}}</td>
           <td style="{{$style_td_with_border_all}} text-align:left;">{{strtoupper($transaction['client_name'])}}</td>
           <td style="{{$style_td_with_border_all}} text-align:center;">{{$transaction['client_dni']}}</td>
           <td style="{{$style_td_with_border_all}} text-align:left;">{{$transaction['detail']}}</td>
           <td style="{{$style_td_with_border_all}} text-align:center;">{{$transaction['status']}}</td>
           <td style="{{$style_td_with_border_all}} text-align:center;">{{$transaction['total']}}</td>
       </tr>
       @endforeach
       <tr>
        <td style="border: 0 solid #fff; font-size:6px; text-align:center; font-weight: bold;" colspan="7">TOTAL</td>
        <td style="border: 0 solid #fff; font-size:6px; text-align:rigth; font-weight: bold;">{{$transactions['sum_total']}}</td>
       
    </tr>

   </tbody>
</table>