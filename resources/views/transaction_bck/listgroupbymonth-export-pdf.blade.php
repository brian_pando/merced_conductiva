@php
$style_td_with_border_right_left = "border: 1px solid black; border-left: 1px solid #ffffff; border-right: 1px solid #ffffff; font-size:8px; text-align:right;";
$style_td_with_border_all = "border: 0 solid #fff;vertical-align: top;font-size:8px; text-align:right;";
@endphp
<table>
    <thead>
        <tr>
            <th colspan="15" style="border: 0px solid #ffffff; text-align:center; font-size:9px;">{{$transactions['company']['name']}}</th>
        </tr>
        <tr>
            <th colspan="15" style="border: 0px solid #ffffff; text-align:center; font-size:9px;">{{$transactions['company']['city']}} - RUC. {{$transactions['company']['ruc']}}</th>
        </tr>
        <tr>
            <th colspan="15" style="border: 0px solid #ffffff; text-align:center; font-size:9px;"><b>REPORTE DE COMISIONISTAS POR RECAUDACIONES MENSUALES - {{$transactions['commissionersList']['year']}}</b></th>
        </tr>
        <tr> 
            <th colspan="15" style="border: 0px solid #ffffff; text-align:center; font-size:9px;">Generado e{{date_default_timezone_set("America/Lima")}} {{date("d/m/Y H:i:s", time())}}</th>
        </tr>
        <tr>
            <th colspan="15" style="border: 0px solid #ffffff; text-align:center; font-size:9px;">Por: {{$transactions['user_logged_in']->name}} - {{$transactions['user_logged_in']->username}}</th>
        </tr>
        <tr>
            @foreach($transactions['commissionersList']['column'] as $key=>$column)
                <th style="text-align:center; background:#fafafa; border: 1px solid #404040; 
            background:#404040; color: #ffffff; font-size:8px;">{{$column[0]}}</th>
            @endforeach
        </tr>
    </thead>
    <tbody>
    @foreach($transactions['commissionersList']['data'] as $key_t=>$market_data)
        <tr>
            @foreach($transactions['commissionersList']['column'] as $key_d=>$column)
                @if(count($transactions['commissionersList']['data'])-2==$key_t)
                    @if($column[1]==='comisionista')
                        <td style="{{$style_td_with_border_right_left}} text-align: left;">{{isset($market_data['name']) ? $market_data['name'] : ''}}</td>
                    @elseif($column[1]==='position')
                        <td style="{{$style_td_with_border_right_left}}">{{isset($market_data['position']) ? $market_data['position'] : ''}}</td>
                    @elseif($column[1]==='ene')
                        <td style="{{$style_td_with_border_right_left}}">{{isset($market_data[1]) ? $market_data[1] : ''}}</td>
                    @elseif($column[1]==='feb')
                        <td style="{{$style_td_with_border_right_left}}">{{isset($market_data[2]) ? $market_data[2] : ''}}</td>
                    @elseif($column[1]==='mar')
                        <td style="{{$style_td_with_border_right_left}}">{{isset($market_data[3]) ? $market_data[3] : ''}}</td>
                    @elseif($column[1]==='abr')
                        <td style="{{$style_td_with_border_right_left}}">{{isset($market_data[4]) ? $market_data[4] : ''}}</td>
                    @elseif($column[1]==='may')
                        <td style="{{$style_td_with_border_right_left}}">{{isset($market_data[5]) ? $market_data[5] : ''}}</td>
                    @elseif($column[1]==='jun')
                        <td style="{{$style_td_with_border_right_left}}">{{isset($market_data[6]) ? $market_data[6] : ''}}</td>
                    @elseif($column[1]==='jul')
                        <td style="{{$style_td_with_border_right_left}}">{{isset($market_data[7]) ? $market_data[7] : ''}}</td>
                    @elseif($column[1]==='ago')
                        <td style="{{$style_td_with_border_right_left}}">{{isset($market_data[8]) ? $market_data[8] : ''}}</td>
                    @elseif($column[1]==='set')
                        <td style="{{$style_td_with_border_right_left}}">{{isset($market_data[9]) ? $market_data[9] : ''}}</td>
                    @elseif($column[1]==='oct')
                        <td style="{{$style_td_with_border_right_left}}">{{isset($market_data[10]) ? $market_data[10] : ''}}</td>
                    @elseif($column[1]==='nov')
                        <td style="{{$style_td_with_border_right_left}}">{{isset($market_data[11]) ? $market_data[11] : ''}}</td>
                    @elseif($column[1]==='dic')
                        <td style="{{$style_td_with_border_right_left}}">{{isset($market_data[12]) ? $market_data[12] : ''}}</td>
                    @elseif($column[1]==='total')
                        <td style="{{$style_td_with_border_right_left}}">{{isset($market_data['total']) ? $market_data['total'] : ''}}</td>
                    @endif
                @elseif(count($transactions['commissionersList']['data'])-1==$key_t)  
                    @if($column[1]==='comisionista')
                        <td style="{{$style_td_with_border_all}} text-align: left; font-weight: bold;">{{isset($market_data['name']) ? $market_data['name'] : ''}}</td>
                    @elseif($column[1]==='position')
                        <td style="{{$style_td_with_border_all}} font-weight: bold;">{{isset($market_data['position']) ? $market_data['position'] : ''}}</td>
                    @elseif($column[1]==='ene')
                        <td style="{{$style_td_with_border_all}} font-weight: bold;">{{isset($market_data[1]) ? $market_data[1] : ''}}</td>
                    @elseif($column[1]==='feb')
                        <td style="{{$style_td_with_border_all}} font-weight: bold;">{{isset($market_data[2]) ? $market_data[2] : ''}}</td>
                    @elseif($column[1]==='mar')
                        <td style="{{$style_td_with_border_all}} font-weight: bold;">{{isset($market_data[3]) ? $market_data[3] : ''}}</td>
                    @elseif($column[1]==='abr')
                        <td style="{{$style_td_with_border_all}} font-weight: bold;">{{isset($market_data[4]) ? $market_data[4] : ''}}</td>
                    @elseif($column[1]==='may')
                        <td style="{{$style_td_with_border_all}} font-weight: bold;">{{isset($market_data[5]) ? $market_data[5] : ''}}</td>
                    @elseif($column[1]==='jun')
                        <td style="{{$style_td_with_border_all}} font-weight: bold;">{{isset($market_data[6]) ? $market_data[6] : ''}}</td>
                    @elseif($column[1]==='jul')
                        <td style="{{$style_td_with_border_all}} font-weight: bold;">{{isset($market_data[7]) ? $market_data[7] : ''}}</td>
                    @elseif($column[1]==='ago')
                        <td style="{{$style_td_with_border_all}} font-weight: bold;">{{isset($market_data[8]) ? $market_data[8] : ''}}</td>
                    @elseif($column[1]==='set')
                        <td style="{{$style_td_with_border_all}} font-weight: bold;">{{isset($market_data[9]) ? $market_data[9] : ''}}</td>
                    @elseif($column[1]==='oct')
                        <td style="{{$style_td_with_border_all}} font-weight: bold;">{{isset($market_data[10]) ? $market_data[10] : ''}}</td>
                    @elseif($column[1]==='nov')
                        <td style="{{$style_td_with_border_all}} font-weight: bold;">{{isset($market_data[11]) ? $market_data[11] : ''}}</td>
                    @elseif($column[1]==='dic')
                        <td style="{{$style_td_with_border_all}} font-weight: bold;">{{isset($market_data[12]) ? $market_data[12] : ''}}</td>
                    @elseif($column[1]==='total')
                        <td style="{{$style_td_with_border_all}} font-weight: bold;">{{isset($market_data['total']) ? $market_data['total'] : ''}}</td>
                    @endif
                @else
                    @if($column[1]==='comisionista')
                        <td style="{{$style_td_with_border_all}} text-align: left;">{{isset($market_data['name']) ? $market_data['name'] : ''}}</td>
                    @elseif($column[1]==='position')
                        <td style="{{$style_td_with_border_all}}">{{isset($market_data['position']) ? $market_data['position'] : ''}}</td>
                    @elseif($column[1]==='ene')
                        <td style="{{$style_td_with_border_all}}">{{isset($market_data[1]) ? $market_data[1] : ''}}</td>
                    @elseif($column[1]==='feb')
                        <td style="{{$style_td_with_border_all}}">{{isset($market_data[2]) ? $market_data[2] : ''}}</td>
                    @elseif($column[1]==='mar')
                        <td style="{{$style_td_with_border_all}}">{{isset($market_data[3]) ? $market_data[3] : ''}}</td>
                    @elseif($column[1]==='abr')
                        <td style="{{$style_td_with_border_all}}">{{isset($market_data[4]) ? $market_data[4] : ''}}</td>
                    @elseif($column[1]==='may')
                        <td style="{{$style_td_with_border_all}}">{{isset($market_data[5]) ? $market_data[5] : ''}}</td>
                    @elseif($column[1]==='jun')
                        <td style="{{$style_td_with_border_all}}">{{isset($market_data[6]) ? $market_data[6] : ''}}</td>
                    @elseif($column[1]==='jul')
                        <td style="{{$style_td_with_border_all}}">{{isset($market_data[7]) ? $market_data[7] : ''}}</td>
                    @elseif($column[1]==='ago')
                        <td style="{{$style_td_with_border_all}}">{{isset($market_data[8]) ? $market_data[8] : ''}}</td>
                    @elseif($column[1]==='set')
                        <td style="{{$style_td_with_border_all}}">{{isset($market_data[9]) ? $market_data[9] : ''}}</td>
                    @elseif($column[1]==='oct')
                        <td style="{{$style_td_with_border_all}}">{{isset($market_data[10]) ? $market_data[10] : ''}}</td>
                    @elseif($column[1]==='nov')
                        <td style="{{$style_td_with_border_all}}">{{isset($market_data[11]) ? $market_data[11] : ''}}</td>
                    @elseif($column[1]==='dic')
                        <td style="{{$style_td_with_border_all}}">{{isset($market_data[12]) ? $market_data[12] : ''}}</td>
                    @elseif($column[1]==='total')
                        <td style="{{$style_td_with_border_all}}">{{isset($market_data['total']) ? $market_data['total'] : ''}}</td>
                    @endif
                @endif
            @endforeach
        </tr>
    @endforeach
    </tbody>
</table>
