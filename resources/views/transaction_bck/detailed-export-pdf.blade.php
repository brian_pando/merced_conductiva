@php
    $title_colspan = 7;
    $count = 1;
    $font_size_titles = 'font-size:9px;';
    $style_th = "border: none; background:#404040; color:#fff; text-align:center; font-size:8px";
    $style_td_with_border_right_left = "text-align:center;border-left: 0 solid #fff;border-right: 0 solid #fff; font-size:6px;";
    $style_td_with_border_all = "text-align:center;border: 0 solid #fff; font-size:6px;";
@endphp

<table>
    <thead>
        <tr>
            <th colspan="{{$title_colspan}}" style="{{$font_size_titles}} border: 0 solid #fff;" >{{$transactions['company']->name}}</th>
        </tr>
        <tr>
            <th style="{{$font_size_titles}} border: 0px solid #fff;" colspan="{{$title_colspan}}">{{$transactions['company']->city}} - RUC. {{$transactions['company']->ruc}}</th>
        </tr>
        <tr>
            <th colspan="{{$title_colspan}}" style="{{$font_size_titles}} border: 0 solid #fff;" ><b>REPORTE DE PAGOS DETALLADO EN CAJA SGRT</b></th>
        </tr>
        <tr>
            <th colspan="{{$title_colspan}}" style="{{$font_size_titles}} border: 0 solid #fff;" >Generado e{{date_default_timezone_set("America/Lima")}} {{date("d/m/Y H:i:s", time())}}</th>
        </tr>
        <tr>
            <th colspan="{{$title_colspan}}" style="{{$font_size_titles}} border: 0 solid #fff;" >Del: {{$transactions['report_date']}}</th>
        </tr>
        <tr>
            <th colspan="{{$title_colspan}}" style="{{$font_size_titles}} border: 0 solid #fff;" >Generado por: {{$transactions['user_logged_in']->name}} - {{$transactions['user_logged_in']->username}}</th>
        </tr>
        <tr>
            <th style="{{$style_th}}">Correlativo</th>
            <th style="{{$style_th}}">DNI</th>
            <th style="{{$style_th}}">Cliente</th>
            <th style="{{$style_th}}">Detalle</th>
            <th style="{{$style_th}}">Clasifi.</th>
            <th style="{{$style_th}}">Monto</th>
            <th style="{{$style_th}}">Monto E.</th>
        </tr>
    </thead>
    <tbody>
    
    @foreach($transactions['data'] as $key_t=>$transaction):
        @foreach($transaction['transactiondetails'] as $key_d=>$detail):
            @if ((count($transactions['data']) - 1 == $key_t) && (count($transaction['transactiondetails']) -1 == $key_d  ))
                <tr>
                    <td style="{{$style_td_with_border_right_left}}">{{$transaction['code']}}{{$transaction['status'] == 1 ? '' : '(E)'}}</td>
                    <td style="{{$style_td_with_border_right_left}}">{{ $transaction['client']->dni }}</td>
                    <td style="{{$style_td_with_border_right_left}} text-align:left;">{{strtoupper($transaction['client']->name)}} {{strtoupper($transaction['client']->lastname)}} </td>
                    <td style="{{$style_td_with_border_right_left}} text-align:left;">{{$detail['detail']}}</td>
                    <td style="{{$style_td_with_border_right_left}}">{{$detail['classifier_ref']}}</td>
                    @if($detail['status']==1)
                        <td style="{{$style_td_with_border_right_left}} text-align:right;">{{$detail['total']}}</td>
                        <td style="{{$style_td_with_border_right_left}} text-align:right;"></td>
                    @endif
                    @if($detail['status']==0)
                        <td style="{{$style_td_with_border_right_left}} text-align:right;"></td>
                        <td style="{{$style_td_with_border_right_left}} text-align:right;">{{$detail['total']}}</td>
                    @endif
                </tr>
            @else
                <tr>
                    <td style="{{$style_td_with_border_all}}">{{$transaction['code']}}{{$transaction['status'] == 1 ? '' : '(E)'}}</td>
                    <td style="{{$style_td_with_border_all}}">{{$transaction['client']->dni}}</td>
                    <td style="{{$style_td_with_border_all}} text-align:left;">{{$transaction['client']->name}} {{$transaction['client']->lastname}}</td>
                    <td style="{{$style_td_with_border_all}} text-align:left;">{{$detail['detail']}}</td>
                    <td style="{{$style_td_with_border_all}}">{{$detail['classifier_ref']}}</td>
                    @if($detail['status']==1)
                        <td style="{{$style_td_with_border_all}} text-align:rigth;">{{$detail['total']}}</td>
                        <td style="{{$style_td_with_border_all}} text-align:rigth;"></td>
                    @endif
                    @if($detail['status']==0)
                        <td style="{{$style_td_with_border_all}} text-align:rigth;"></td>
                        <td style="{{$style_td_with_border_all}} text-align:rigth;">{{$detail['total']}}</td>
                    @endif
                </tr>
            @endif
        @endforeach
    @endforeach
        <tr>
            <td style="border: 0 solid #fff; font-size:6px; text-align:center; font-weight: bold;" colspan="5">TOTAL</td>
            <td style="border: 0 solid #fff; font-size:6px; text-align:rigth; font-weight: bold;">{{$transactions['transactions_total']}}</td>
            <td style="border: 0 solid #fff; font-size:6px; text-align:rigth; font-weight: bold;">{{$transactions['sum_total_extornado']}}</td>
        </tr>
        <tr><td colspan="7" style="text-align:center; border: 0px solid #fff; font-size:6px; height:40px;">&nbsp;</td></tr>
        <tr>
            <td colspan="4" style="text-align:center; border: 0px solid #fff; font-size:6px;">_ _ _ _ _ _ _ _ _ _ _ _ _ _ _  _</td>
            <td colspan="3" style="text-align:center; border: 0px solid #fff; font-size:6px;">_ _ _ _ _ _ _ _ _ _ _ _ _ _ _  _</td>
        </tr>
        <tr>
            <td colspan="4" style="text-align:center; border: 0px solid #fff; font-size:6px;">Jefe de Tesoreria</td>
            <td colspan="3" style="text-align:center; border: 0px solid #fff; font-size:6px;">BV Jefe cajero</td>
        </tr>
    </tbody>
</table>