/**
 * First we will load all of this project's JavaScript dependencies which
 * includes Vue and other libraries. It is a great starting point when
 * building robust, powerful web applications using Vue and Laravel.
 */

require('./bootstrap');
import Antd from 'ant-design-vue'
import VueRouter from 'vue-router'
import 'ant-design-vue/dist/antd.css';
window.Vue = require('vue');
Vue.use(Antd);
Vue.use(VueRouter);

Vue.mixin({
    methods: {
        asset(path) {
            var base_path = window._asset || '';
            var route = '';
            if (base_path.includes('localhost')) route = base_path + path;
            else route = base_path + '/' + path;
            return route;
        }
    }
});
/**
 * The following block of code may be used to automatically register your
 * Vue components. It will recursively scan this directory for the Vue
 * components and automatically register them with their "basename".
 *
 * Eg. ./components/ExampleComponent.vue -> <example-component></example-component>
 */

// const files = require.context('./', true, /\.vue$/i)
// files.keys().map(key => Vue.component(key.split('/').pop().split('.')[0], files(key).default))

Vue.component('menu-component', require('./components/MenuComponent.vue').default);
Vue.component('file-component', require('./components/FileComponent.vue').default);
Vue.component('print-transaction-component', require('./components/PrintTransactionComponent.vue').default);
Vue.component('print-fractionarie-component', require('./components/PrintFractionarieComponent.vue').default);
Vue.component('print-notification-component', require('./components/PrintNotificationComponent.vue').default);
Vue.component('print-commission-agent-liquidation-component', require('./components/PrintCommissionAgentLiquidationComponent.vue').default);
Vue.component('picture-component', require('./components/PictureComponent.vue').default);
Vue.component('select-people-component', require('./components/SelectPeopleComponent.vue').default);
Vue.component('select-stand-people-component', require('./components/SelectStandPeopleComponent.vue').default);
Vue.component('select-gyre-component', require('./components/SelectGyreComponent.vue').default);
Vue.component('select-sector-component', require('./components/SelectSectorComponent.vue').default);
Vue.component('select-market-component', require('./components/SelectMarketComponent.vue').default);
Vue.component('select-stand-component', require('./components/SelectStandComponent.vue').default);
Vue.component('debt-list-component', require('./components/cashbox/DebtListComponent.vue').default);
Vue.component('debt-list-by-stands-component', require('./components/cashbox/DebtListByStandsComponent.vue').default);
Vue.component('select-concept-component', require('./components/SelectConceptComponent.vue').default);
Vue.component('select-fractionary-type-component', require('./components/SelectFractionaryTypeComponent.vue').default);
Vue.component('select-sector-multiple-component', require('./components/SelectMultipleSectorComponent.vue').default);
Vue.component('select-gyre-multiple-component', require('./components/SelectMultipleGyreComponent.vue').default);
 //Vue.component('users-change-password-component', require('./components/admin/UsersCangePassword.vue').default);



const CollectionComponent = Vue.component('collection-component', require('./components/commission_agent/CollectionComponent.vue').default);
const MarketDebComponent = Vue.component('market-debt-component', require('./components/debts/MarketDebtComponent.vue').default);
const IndividualDebtComponent = Vue.component('drivers-component', require('./components/debts/IndividualDebtComponent.vue').default);
const CoactivesComponent = Vue.component('coactives-component', require('./components/debts/CoactivesComponent.vue').default);
const DirectFineComponent = Vue.component('direct-debt-component', require('./components/debts/DirectFineComponent.vue').default);
const PrescriptionComponent = Vue.component('prescription-component', require('./components/debts/PrescriptionComponent.vue').default);
const StandsComponent = Vue.component('stands-component', require('./components/admin/StandsComponent.vue').default);
const LocalsComponent = Vue.component('locals-component', require('./components/admin/LocalsComponent.vue').default);
const GyresComponent = Vue.component('gyres-component', require('./components/admin/GyresComponent.vue').default);
const SectorsComponent = Vue.component('sectors-component', require('./components/admin/SectorsComponent.vue').default);
const MarketsComponent = Vue.component('markets-component', require('./components/admin/MarketsComponent.vue').default);
const RatesComponent = Vue.component('rates-component', require('./components/admin/RatesComponent.vue').default);
const PersonsComponent = Vue.component('persons-component', require('./components/admin/PersonsComponent.vue').default);
//const AgentsComponent = Vue.component('agents-component', require('./components/admin/AgentsComponent.vue').default);
const NotificationsComponent = Vue.component('notifications-component', require('./components/notifications/NotificationsComponent.vue').default);
const CommissionAgentLiquidationComponent = Vue.component('commission-agent-liquidation-component', require('./components/liquidation/CommissionAgentLiquidationComponent.vue').default);
const MonthLiquidationComponent = Vue.component('month-liquidation-component', require('./components/liquidation/MonthLiquidationComponent.vue').default);
const CashboxComponent = Vue.component('cashbox-component', require('./components/cashbox/CashboxComponent.vue').default);
const AnalyticalReportComponent = Vue.component('analytical-report-component', require('./components/liquidation/AnalyticalReportComponent.vue').default);
const TransactionComponent = Vue.component('payment-component', require('./components/cashbox/TransactionComponent.vue').default);
const DetailedTransactionComponent = Vue.component('detailed-transaction-component', require('./components/report/DetailedTransactionComponent.vue').default);
const FractionaryPayComponent = Vue.component('fractionary-pay-component', require('./components/report/FractionaryPayComponent.vue').default);
const ExtortedPaymentsComponent = Vue.component('extorted-payments', require('./components/report/ExtortedPaymentsComponent.vue').default);////
const FractionariesComponent = Vue.component('fractionaries-component', require('./components/fractionary/FractionariesComponent.vue').default);
const FractionaryTypesComponent = Vue.component('fractionary-types-component', require('./components/fractionary/FractionaryTypesComponent.vue').default);
const UsersComponent = Vue.component('users-component', require('./components/admin/UsersComponent.vue').default);
const UITComponent = Vue.component('uit-component', require('./components/admin/UITComponent.vue').default);
const ConceptComponent = Vue.component('concept-component', require('./components/admin/ConceptComponent.vue').default);
const SettingComponent = Vue.component('setting-component', require('./components/admin/SettingComponent.vue').default);
const ForgivenessComponent = Vue.component('forgiveness-component', require('./components/debts/ForgivenessComponent.vue').default);
const ForgivenessGenericComponent = Vue.component('forgiveness-generic-component', require('./components/debts/ForgivenessGenericComponent.vue').default);
const ReversionComponent = Vue.component('reversion-component', require('./components/admin/ReversionComponent.vue').default);
const DebtsMarketMeatsComponent = Vue.component('debts-market-meats-component', require('./components/admin/DebtsMarketMeatsComponent.vue').default);
const UserChangePasswordComponent = Vue.component('users-change-password-component', require('./components/admin/UsersCangePassword.vue').default);

const routes = [
    { path: '/collection', name: 'collection', component: CollectionComponent },
    { path: '/debts/individual-debt', name: 'individual', component: IndividualDebtComponent },
    { path: '/debts/market-debt', name: 'market', component: MarketDebComponent },
    { path: '/debts/coactives', name: 'coactives', component: CoactivesComponent },
    { path: '/debts/forgiveness', name: 'forgiveness', component: ForgivenessComponent },
    { path: '/debts/forgiveness-generic', name: 'forgiveness_generic', component: ForgivenessGenericComponent },
    { path: '/debts/prescription', name: 'prescription', component: PrescriptionComponent },
    { path: '/admin/reversion', name: 'reversion', component: ReversionComponent },
    { path: '/admin/debts-market-meats', name: 'debts_market_meats', component: DebtsMarketMeatsComponent },
    { path: '/direct-fine', name: 'direct-debts', component: DirectFineComponent },
    { path: '/liquidations/commission-agent-liquidation', name: 'commission_agent', component: CommissionAgentLiquidationComponent },
    { path: '/liquidations/month-liquidation', name: 'month_liquidation', component: MonthLiquidationComponent },
    { path: '/liquidations/analytical-report', name: 'analytical_report', component: AnalyticalReportComponent },
    //{ path: '/notifications',name:'notifications', component: NotificationsComponent },
    { path: '/notifications/:type', name: 'frac_notifications', component: NotificationsComponent, props: true },
    { path: '/fractionaries', name: 'fractionaries', component: FractionariesComponent },
    { path: '/cashbox/opening-closing', name: 'opening_closing', component: CashboxComponent },
    { path: '/cashbox/transaction', name: 'transaction', component: TransactionComponent },
    { path: '/admin/markets', name: 'markets', component: MarketsComponent },
    { path: '/admin/sectors', name: 'sectors', component: SectorsComponent },
    { path: '/admin/stands', name: 'stands', component: StandsComponent },
    { path: '/admin/locals', name: 'locals', component: LocalsComponent },
    { path: '/admin/gyres', name: 'gyres', component: GyresComponent },
    { path: '/admin/rates', name: 'rates', component: RatesComponent },
    //{ path: '/admin/agents',name:'agents', component: AgentsComponent },
    { path: '/admin/persons', name: 'persons', component: PersonsComponent },
    { path: '/admin/users', name: 'users', component: UsersComponent },
    { path: '/admin/uits', name: 'uit', component: UITComponent },
    { path: '/admin/concepts', name: 'concept', component: ConceptComponent },
    { path: '/admin/setting', name: 'setting', component: SettingComponent },
    { path: '/admin/fractionary-types', name: 'fractionary-types', component: FractionaryTypesComponent },
    { path: '/report/detailed-transaction', name: 'detailed-transaction', component: DetailedTransactionComponent },
    { path: '/report/fractionary-pay', name: 'fraction-transaction', component: FractionaryPayComponent },
    { path: '/report/extorted-payments', name: 'extorted-payments', component: ExtortedPaymentsComponent },
    { path: '/admin/change-password', name: 'change-password', component: UserChangePasswordComponent },
];

const router = new VueRouter({
    mode: 'history',
    routes // short for `routes: routes`
})

/**
 * Next, we will create a fresh Vue application instance and attach it to
 * the page. Then, you may begin adding components to this application
 * or customize the JavaScript scaffolding to fit your unique needs.
 */

const app = new Vue({
    el: '#app',
    router,
    data: {
        app_url: "<?php echo app_url(); ?>",
        user_role: "<?php echo Auth::user()->role; ?>",
    }
});
/* Cuando usas el menu en el lateral */
$(document).ready(function () {
    $('#sidebarCollapse').on('click', function () {
        $('#sidebar').toggleClass('active');
    });
});
