


  var paymentOptions =  {};


  export default {

    init:()=>{
        console.log("iniciando culqi", Culqi);
        Culqi.publicKey = 'pk_test_dc9afec6dded235f';

        Culqi.init();

        Culqi.options({
            installments: false,
            paymentMethods: {
                tarjeta: true,
                bancaMovil: true,
                agente: true,
                billetera: true,
                cuotealo: true,
                yape: true
            },
            style: {
                logo: 'http://localhost:4200/logo.png',
                bannerColor: '', // hexadecimal
                buttonBackground: '', // hexadecimal
                menuColor: '', // hexadecimal
                linksColor: '', // hexadecimal
                buttonText: '', // texto que tomará el botón
                buttonTextColor: '', // hexadecimal
                priceColor: 'red' // hexadecimal
            }
        });
    },

    start:(order_id, currency, amount)=>{
        console.log("orderid",order_id);
        Culqi.settings({
            title:"DoncaPlatform",
            currency: currency,
            amount: amount, // Este parámetro es requerido para realizar pagos yape
            order: order_id// 'ord_live_0CjjdWhFpEAZlxlz' // Este parámetro es requerido para realizar pagos con pagoEfectivo, billeteras y Cuotéalo
        });
    },

    getPaymentOptions:()=>{
        Culqi.validationPaymentMethods();
        paymentOptions = Culqi.paymentOptionsAvailable;
        console.log("paymentOptions",paymentOptions);
    },

    processCheckout:()=>{
        Culqi.open();
    },

    processPayment:(paymentOption)=>{
        //Culqi.open(); //abre el modal del mismo Culqi.
        // Crea el objeto Token con Culqi JS
        if ( paymentOption=='card' && paymentOptions.token.available) {
            paymentOptions.token.generate();
        }
        // Crea el objeto Token con Culqi JS
        if (paymentOption=='yape' && paymentOptions.yape.available) {
            console.log("yape activo");
            paymentOptions.yape.generate();
        }
        // Crea cip
        if (paymentOption=='cip' && paymentOptions.cip.available) {
            paymentOptions.cip.generate();
        }
        // Crea el link de cuotéalo
        if (paymentOption=='coutalo' && paymentOptions.cuotealo.available) {
            paymentOptions.cuotealo.generate();
        }
    }
  }