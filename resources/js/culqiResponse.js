import server from "../server";
import EventManager from "./EventManager";

export const culqiEvent = async () => {

	try {
		if (Culqi.token) {

			let userToken = localStorage.getItem("token");
			// Objeto Token creado exitosamente!
			const token = Culqi.token.id;
			console.log("Se ha creado un Token: ", token);
			let { amount, currency, order } = Culqi.getSettings;
			let email = Culqi.token.email;
			let payload = { userToken, token, email, amount, currency, order }
			EventManager.publish("mainLoading", { show: true })
			Culqi.close()
			let data = await server.saveCheckoutCharge(payload);

			location.reload();
		} else if (Culqi.order) {
			// Objeto order creado exitosamente!
			const order = Culqi.order;
			console.log("Se ha creado el objeto Order: ", order);
			if (order.paymentCode) {
				console.log("Se ha creado el cip: ", order.paymentCode);
			}
			if (order.qr) {
				console.log("Se ha generado el qr: ", order.qr);
			}
			if (order.cuotealo) {
				console.log("Se ha creado el link cuotéalo: ", order.cuotealo);
			}
		} else {
			// Mostramos JSON de objeto error en consola
			console.log("Error : ", Culqi.error);

		}
	} catch (error) {
		alert("Hubo error al procesar Pago. Recargue la pagina.");
	}


}