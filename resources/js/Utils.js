module.exports = {
    checkEditMode: () => {
        let editAllowed = false;
        let menu = sessionStorage.getItem("menu");
        if (menu) menu = JSON.parse(menu);
        let currentMenu = menu.find(e => e.link == location.pathname.toLowerCase());
        if (currentMenu && currentMenu.mode == 'W') editAllowed = true;
        return editAllowed;
    },
    getAccessMarkets: () => {
        let markets = null;
        let menu = sessionStorage.getItem("menu");
        if (menu) menu = JSON.parse(menu);
        let currentMenu = menu.find(e => e.link == location.pathname);
        if (currentMenu) markets = currentMenu.markets;
        return markets ? JSON.parse(markets) : null;
    }
}