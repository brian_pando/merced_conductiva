import backend from "./clients/ClientAxios";
const resource = `${layout.APP_URL}/api/market`;
const resource_export = `${layout.APP_URL}/market`;

const validate_account_status_by_month_filter = (params) => {
    return `market_id=${params.market_id ? params.market_id : ''}&
        ${validate_account_status_filter(params)}`;
};

const validate_account_status_filter = (params) => {
    return `sector_id=${params.sector_id ? params.sector_id : ''}&
		market_id=${params.market_id ? params.market_id : ''}&
		stands_id=${params.stands_id ? params.stands_id : ''}&
        concept_id=${params.concept_id ? params.concept_id : ''}&
        market_id=${params.market_id ? params.market_id : ''}&
        type=${params.type ? params.type : ''}&
        year=${params.year ? params.year : ''}&
        date_init=${params.date_init ? params.date_init : ''}&
        date_end=${params.date_end ? params.date_end : ''}&
        page=${params.page ? params.page : ''}&
        results=${params.results ? params.results : ''}&
        sortField=${params.sortField ? params.sortField : ''}&
        sortOrder=${params.sortOrder ? params.sortOrder : ''}&
        search_names=${params.search_names ? params.search_names : ''}&
        sectors=${params.sectors ? params.sectors : ''}&
        gyres=${params.gyres ? params.gyres : ''}&
        payment_status=${params.payment_status ? params.payment_status : ''}&
        columns=${params.groupedBy || ''}`;
}

export default {
    list(params) {
        return backend.get(`${resource}`, { params })
    },
    save(payload) {
        if ('id' in payload) return backend.put(`${resource}/${payload.id}`, payload)
        return backend.post(`${resource}`, payload)
    },
    get(id) {
        return backend.post(`${resource}/${id}`)
    },
    allAccountStatusByYear(params) {
        return backend.get(`${resource}/account-status-by-year?${validate_account_status_filter(params)}`);
    },
    getAccountStatusByMonth(params) {
        return backend.get(`${resource}/account-status-by-month?${validate_account_status_by_month_filter(params)}`);
    },
    getNameYearsOfDebt(params) {
        return backend.get(`${resource}/name-years-of-debts?${validate_account_status_filter(params)}`);
    },
    getAccountStatusByStands(params) {
        return backend.get(`${resource}/account-status-by-stands?${validate_account_status_filter(params)}`);
    },
    generateDebts(params) {
        return backend.get(`${resource}/${params.market_id}/generatedebts`, { params });
    },
    statusAccountExportPdf(payload) {
        return backend.post(`${resource_export}/status-account-export-pdf`, payload);
    },
    statusAccountExportExcel(payload) {
        return backend.post(`${resource_export}/status-account-export-excel`, payload);
    },
    statusAccountByStandsExportPdf(payload) {
        return `${resource_export}/status-account-by-stands-export-pdf?${validate_account_status_by_month_filter(payload)}`;
    },
    statusAccountByStandsExportExcel(payload) {
        return `${resource_export}/status-account-by-stands-export-excel?${validate_account_status_by_month_filter(payload)}`;
    },
    //ejemplo de meses
    statusAccountByMonthExcel(payload) {
        return backend.post(`${resource_export}/status-account-by-month-export-excel`, payload);
    },
    statusAccountByMonthPdf(payload) {
        return backend.post(`${resource_export}/status-account-by-month-export-pdf`, payload);
    },
    selectRowMarketExportPdf(payload) {
        return backend.post(`${resource_export}/select-row-market-export-pdf`, payload);
    },
    selectRowMarketExportExcel(payload) {
        return backend.post(`${resource_export}/select-row-market-export-excel`, payload);
    },
}
