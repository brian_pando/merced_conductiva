import backend from "./clients/ClientAxios";
const resource = `${layout.APP_URL}/api/transaction`;
const resource_export = `${layout.APP_URL}/transaction`;
const export_filter = (params) => {
    return `search=${params.search ? params.search : ''}&
            user_id=${params.user_id ? params.user_id : ''}&
            user_logeado_id=${params.user_logeado_id ? params.user_logeado_id : ''}&
            date_init=${params.date_init ? params.date_init : ''}&
            date_end=${params.date_end ? params.date_end : ''}&
            market_id=${params.market_id ? params.market_id : ''}&
            concept_id=${params.concept_id ? params.concept_id : ''}&
            sector_id=${params.sector_id ? params.sector_id : ''}&
            type=${params.type ? params.type : ''}&
            status=${params.status ? params.status : ''}&
            search_names=${params.search_names ? params.search_names : ''}`;
};

export default {
    list(params){
        return backend.get(`${resource}`,{params})
    },
    listGroupByMonth(params){
        return backend.get(`${resource}/group-by-month`,{params})
    },
    save(payload){
        if('id' in payload)  return backend.put(`${resource}/${payload.id}`,payload)
        return backend.post(`${resource}`,payload)
    },
    delete(payload){
        return backend.put(`${resource}/${payload.id}/delete`,payload)
    },
    get(id){
        return backend.post(`${resource}/${id}`)
    },
    updateDetail(payload){
        return backend.put(`${resource}/detail/${payload.id}`,payload)
    },
    removeDetail(payload){
        return backend.delete(`${resource}/detail/${payload.id}`)
    },
    resumenByConcept(params){
        return backend.get(`${resource}/resumen-by-concept`,{params})
    },
    printPaymentVoucher(params){
        return backend.post(`${resource}/print-payment-voucher`,{params})
    },
    getUrlExportPdf(params){
        return `${resource_export}/export-pdf?${export_filter(params)}`;
    },
    getUrlDetailedExportPdf(params){
        return `${resource_export}/detailed-export-pdf?${export_filter(params)}`;
    },
    getUrlExportExcel(params){
        return `${resource_export}/export-excel?${export_filter(params)}`;
    },
    getUrlDetailedExportExcel(params){
        return `${resource_export}/detailed-export-excel?${export_filter(params)}`;
    },
    getListGroupByMonthExportPdf(payload){
        return backend.post(`${resource_export}/listgroupbymonth-export-pdf`,payload);
    },
    getListGroupByMonthExportExcel(payload){
        return backend.post(`${resource_export}/listgroupbymonth-export-excel`,payload);
    },
    getListCommissionExportExcel(payload){
        return backend.post(`${resource_export}/listCommission-export-excel`,payload);
    },
    getListCommissionExportPdf(payload){
        return backend.post(`${resource_export}/listCommission-export-pdf`,payload);
    },
    getUrlDetailedPaymentsExportPdf(params){
        return `${resource_export}/detailed-payments-export-pdf?${export_filter(params)}`;
    },
    getUrlDetailedPaymentsExportExcel(params){
        return `${resource_export}/detailed-payments-export-excel?${export_filter(params)}`;
    },
    allDetailsByConcepts(params) {
        return backend.get(`${resource}/details-by-concepts`,{params})
    },
    calculateTotalByConcept(params) {
        return backend.get(`${resource}/calculate-total-by-concept`,{params})
    },
    calculateTotalBySector(params) {
        return backend.get(`${resource}/calculate-total-by-sector`,{params})
    },
    printCommissionCollection(list_commission){
        return backend.post(`${resource_export}/print-commission-collection-pdf`,list_commission);
    }
}
