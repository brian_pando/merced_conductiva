import backend from "./clients/ClientAxios";
const resource = `${layout.APP_URL}/api/user`;
export default {
    list(params){
        return backend.get(`${resource}`,{params});
    },
    allCommissionAgent(params){
        return backend.get(`${resource}/commission-agent`,{params});
    },
    cashiersAndCommissionAgent(params={}){
        return backend.get(`${resource}/cashiers-and-commission-agent`,{params});
    },
    listMenu(){
        return backend.get(`${resource}/menu`);
    },
    listRoles(params={}){
        return backend.get(`${resource}/role`,{params});
    },
    listAccess(params={}){
        return backend.get(`${resource}/access`,{params});
    },
    listPermissions(params){
        return backend.get(`${resource}/${params.user_id}/permissions`);
    },
    savePermissions(payload){
        return backend.post(`${resource}/${payload.user_id}/permissions`, payload);
    },
    save(payload){
        if('id' in payload)  return backend.put(`${resource}/${payload.id}`,payload)
        return backend.post(`${resource}`,payload)
    },
    delete(id){
        return backend.delete(`${resource}/${id}`)
    },
    get(id){
        return backend.get(`${resource}/${id}`)
    },
    validateCode(params) {
        return backend.get(`${resource}/${params.id}/validate-code/${params.code}`)
    },
    can_managment_roles(user_id){
        return backend.get(`${resource}/${user_id}/can_management_roles`);
    },
     saveChangePassword(payload){
        return backend.post(`${resource}/change-password`, payload);
    },
}