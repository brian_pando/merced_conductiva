import backend from "./clients/ClientAxios";
const resource = `${layout.APP_URL}/api/person`;
export default {
    list(params){
        return backend.get(`${resource}`,{params})
    },
    save(payload){
        if('id' in payload)  return backend.put(`${resource}/${payload.id}`,payload)
        return backend.post(`${resource}`,payload)
    },
    get(id){
        return backend.post(`${resource}/${id}`)
    },
    verifyDuplicateDNI(dni){
        return backend.get(`${resource}/verify-duplicate-dni/${dni}`);
    },
    getPeopleWithDebts(params){
        return backend.get(`${resource}/debts`,{params});
   },
   listPeopleForViewPayments(params){
        return backend.get(`${resource}/list-people-for-view-payments`,{params});
   },
}