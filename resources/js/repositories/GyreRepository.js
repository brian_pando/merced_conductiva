import backend from "./clients/ClientAxios";
const resource = `${layout.APP_URL}/api/gyre`;
export default {
    list(params){
        return backend.get(`${resource}`,{params})
    },
    save(payload){
        if('id' in payload)  return backend.put(`${resource}/${payload.id}`,payload)
        return backend.post(`${resource}`,payload)
    },
    get(id){
        return backend.post(`${resource}/${id}`)
    },
    list_by_market(params){
        return backend.get(`${resource}/by-market/${params.market_id}`);
    },
}