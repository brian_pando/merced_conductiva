import backend from "./clients/ClientAxios";
const resource = `${layout.APP_URL}/api/sector`;
export default {
    list(params){
        return backend.get(`${resource}`,{params})
    },
    show(id){
        return backend.get(`${resource}/${id}`)
    },
    save(payload){
        if('id' in payload)  return backend.put(`${resource}/${payload.id}`,payload)
        return backend.post(`${resource}`,payload)
    },
    list_by_market(params){
        return backend.get(`${resource}/by-market/${params.market_id}`);
    },
    get(id){
        return backend.post(`${resource}/${id}`)
    }
}