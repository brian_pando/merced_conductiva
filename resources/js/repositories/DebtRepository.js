import backend from "./clients/ClientAxios";
const resource = `${layout.APP_URL}/api/debt`;
const resource_export = `${layout.APP_URL}/debt`;
const export_filter = (params) => {
    return `search=${params.search ? params.search : ''}&
            status=${params.status ? params.status : ''}&
            date_init=${params.date_init ? params.date_init : ''}&
            date_end=${params.date_end ? params.date_end : ''}&
            payment_status=${params.payment_status ? params.payment_status : ''}&
            concept_id=${params.concept_id ? params.concept_id : ''}&
            type=${params.type ? params.type : ''}`;
};
export default {
    list(params,type){
        return backend.get(`${resource}/${type}`,{params})
    },
    save(payload){
        if('id' in payload)  return backend.put(`${resource}/${payload.id}`,payload)
        return backend.post(`${resource}`,payload)
    },
    get(id){
        return backend.post(`${resource}/${id}`)
    },
    // getDebtsForCoactive(params){
    //     return backend.get(`${resource}/for_coactive/client/${person_id}`,{params});
    // },
    getTransaction(id){
        return backend.get(`${resource}/${id}/transaction`)
    },
    getAccountStatusByTitular(person_id,payment_status=0,params={}){
        return backend.get(`${resource}/${person_id}/account-status/${payment_status}`,{params});
    },
    getExportPdf(params){
        return `${resource_export}/export-pdf?${export_filter(params)}`;
    },
    getExportExcel(params){
        return `${resource_export}/export-excel?${export_filter(params)}`;
    },
    saveMultiple(payload) {
        return backend.post(`${resource}/multiple`,payload);
    }
}