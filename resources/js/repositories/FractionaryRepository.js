import backend from "./clients/ClientAxios";
const resource = `${layout.APP_URL}/api/fractionary`;
const resource_export = `${layout.APP_URL}/fractionary`;
import moment from "moment";
moment.locale('es');

export default {
    list(params) {
        return backend.get(`${resource}`, { params });
    },
    save(payload) {
        if ('id' in payload) return backend.put(`${resource}/${payload.id}`, payload);
        return backend.post(`${resource}`, payload);
    },
    /*remove(id){
        return backend.delete(`${resource}/${id}`);
    },*/
    activateAndDeactivateFractionaries(id) {
        return backend.put(`${resource}/${id}/activate-deactivate`, id)
    },
    get(id) {
        return backend.post(`${resource}/${id}`);
    },
    litSchedule(params) {
        return backend.get(`${resource}/${params.fractionary_id}/schedule`, {});
    },
    gen_schedule(data) {
        const { date_start, quotes, fractionary_amount, interest } = data;
        const amortization = (fractionary_amount / quotes).toFixed(2) * 1;
        const init_date = moment(date_start);

        let schedule = [{ date_at: init_date, pending_amount: fractionary_amount, amort: 0 }];
        let numberInitDate = init_date.format('DD');
        let dateReprogrammed;
        for (var i = 1; i <= parseInt(quotes); i++) {
            const date_at = moment(schedule[i - 1].date_at).add(1, 'M');
            if (date_at.format('DD') < parseInt(numberInitDate)) {
                dateReprogrammed = date_at.endOf('month').format('YYYY-MM-DD HH:mm:ss');
            } else {
                dateReprogrammed = date_at.format('YYYY-MM-DD HH:mm:ss');
            }
            const pending = (schedule[i - 1].pending_amount - schedule[i - 1].amort).toFixed(2) * 1;
            const calc_interest = (pending * interest).toFixed(2) * 1;
            const total = amortization + calc_interest;
            const item = { date_at: dateReprogrammed, pending_amount: pending, interest: calc_interest, amort: amortization, total: total, status: 1 };
            schedule.push(item);
        }
        schedule = schedule.slice(1);
        return schedule;
    },
    calc_initial_amount(data, uit) {
        let initial_amount = data.debt * 0.25;
        if (data.debt > uit) initial_amount = data.debt * 0.15;
        return initial_amount;
    },
    forgiveDebts(fractionaries_data) {
        return backend.post(`${resource}/forgive-debts`, fractionaries_data);
    },
    getExportPdf(payload) {
        return backend.post(`${resource_export}/export-pdf`, payload);
    },
    getExportExcel(payload) {
        return backend.post(`${resource_export}/export-excel`, payload);
    },
}