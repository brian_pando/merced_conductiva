import backend from "./clients/ClientAxios";
const resource = `${layout.APP_URL}/api/cashbox`;
const resource_export = `${layout.APP_URL}/cashbox`;
const export_filter = (params) => {
    return `search=${params.search ? params.search : ''}&
            user_id=${params.user_id ? params.user_id : ''}&
            date_init=${params.date_init ? params.date_init : ''}&
            date_end=${params.date_end ? params.date_end : ''}`;
};
export default {
    list(params){ 
        return backend.get(`${resource}`,{params})
    },
    save(payload){
        if('id' in payload)  return backend.put(`${resource}/${payload.id}`,payload)
        return backend.post(`${resource}`,payload)
    },
    delete(payload){
        return backend.put(`${resource}/${payload.id}/delete`,payload)
    },
    get(id){
        return backend.post(`${resource}/${id}`)
    },
    getUrlExportPdf(params){ 
        return `${resource_export}/export-pdf?${export_filter(params)}`;
    },
    getUrlExportExcel(params){
        return `${resource_export}/export-excel?${export_filter(params)}`;
    },
}