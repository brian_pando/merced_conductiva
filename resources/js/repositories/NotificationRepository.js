import backend from "./clients/ClientAxios";
const resource = `${layout.APP_URL}/api/notification`;
const resource_export = `${layout.APP_URL}/notification`;
import moment from "moment";
moment.locale('es');

export default {
    list(params){
        return backend.get(`${resource}`,{params});
    },
    save(payload){
        if('id' in payload)  return backend.put(`${resource}/${payload.id}`,payload);
        return backend.post(`${resource}`,payload);
    },
    
    get(id){
        return backend.post(`${resource}/${id}`);
    },
    saveMassive1x1(payload){
        return backend.post(`${resource}`,payload);
    },
    allPdf(notifications_info) {
       // return backend.post(`${resource}/all-pdf`,notifications_id);  
        return backend.post(`${resource_export}/all-pdf`,notifications_info);
    },
     sendMail(payload) { 
     console.log(payload); 
        return backend.post(`${resource}/send-individual-mail`,payload);
    },
}