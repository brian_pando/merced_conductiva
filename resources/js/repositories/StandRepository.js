import backend from "./clients/ClientAxios";
const resource = `${layout.APP_URL}/api/stand`;
const resource_export = `${layout.APP_URL}/stand`;

const validate_account_status_filter = (params) => {
    return `stand_id=${params.stand_id ? params.stand_id : ''}&
        concept_id=${params.concept_id ? params.concept_id : ''}&
        market_id=${params.market_id ? params.market_id : ''}&
        sector_id=${params.sector_id ? params.sector_id : ''}&
        page=${params.page ? params.page : ''}&
        type=${params.type ? params.type : ''}&
        date_init=${params.date_init ? params.date_init : ''}&
        date_end=${params.date_end ? params.date_end : ''}&
        standsAll=${params.standsAll ? params.standsAll : ''}&
        individual_view_payments=${params.individual_view_payments ? params.individual_view_payments : ''}&
        payment_status=${params.payment_status ? params.payment_status : ''}&
        ${'nopaginate' in params ? 'noPaginate='+params.nopaginate : ''}`;
}

export default {
    list(params) {
        return backend.get(`${resource}`, { params })
    },
    getStandsByClientID(params) {
        return backend.get(`${resource}/titular/${params.titular_id}`, { params })
    },
    save(payload) {
        if ('id' in payload) return backend.put(`${resource}/${payload.id}`, payload)
        return backend.post(`${resource}`, payload)
    },
    get(id) {
        return backend.post(`${resource}/${id}`)
    },
    getStandWithDebts(person_id, payment_status = 0, params = {}) {
        return backend.get(`${resource}/${person_id}/stand-with-debts/${payment_status}`, { params });
    },
    getBringDebtsToDate(person_id, payment_status = 0, params = {}) {
        return backend.get(`${resource}/${person_id}/bring-debts-to-date/${payment_status}`, { params });
    },
    getAccountStatusByMonth(params) {
        return backend.get(`${resource}/account-status-by-month?${validate_account_status_filter(params)}`);
    },
    getListStandWithTitular(params) {
        return backend.get(`${resource}/list-stand-with-titular?${validate_account_status_filter(params)}`);
    },
    dataStatusAccounFormatInTable(data) {
        let debtsMonthxYear = [];

        data.forEach(element => {
            let indexYear = debtsMonthxYear.findIndex(item => item.year == element.year); // Ubicamos el indice del año
            if (indexYear > -1) {
                // existe año registrado					
                debtsMonthxYear[indexYear][element.month] = {
                    total: Number.parseFloat(element.total).toFixed(2),
                    debt_id: element.id,
                    fractionary_id: element.fractionary_id,
                    coactive_id: element.coactive_id,
                    previous_total: element.previous_total,
                    payment: element.payment,
                    payment_status: element.payment_status,
                    comment: element.comment,
                    reversion_id: element.reversion_id,
                };
            } else {
                // no existe año registrado. registrar!!
                debtsMonthxYear.push({
                    year: element.year,
                    1: { total: '', debt_id: '', fractionary_id: '', coactive_id: '', previous_total: '', payment: '', payment_status: '', comment: '' },
                    2: { total: '', debt_id: '', fractionary_id: '', coactive_id: '', previous_total: '', payment: '', payment_status: '', comment: '' },
                    3: { total: '', debt_id: '', fractionary_id: '', coactive_id: '', previous_total: '', payment: '', payment_status: '', comment: '' },
                    4: { total: '', debt_id: '', fractionary_id: '', coactive_id: '', previous_total: '', payment: '', payment_status: '', comment: '' },
                    5: { total: '', debt_id: '', fractionary_id: '', coactive_id: '', previous_total: '', payment: '', payment_status: '', comment: '' },
                    6: { total: '', debt_id: '', fractionary_id: '', coactive_id: '', previous_total: '', payment: '', payment_status: '', comment: '' },
                    7: { total: '', debt_id: '', fractionary_id: '', coactive_id: '', previous_total: '', payment: '', payment_status: '', comment: '' },
                    8: { total: '', debt_id: '', fractionary_id: '', coactive_id: '', previous_total: '', payment: '', payment_status: '', comment: '' },
                    9: { total: '', debt_id: '', fractionary_id: '', coactive_id: '', previous_total: '', payment: '', payment_status: '', comment: '' },
                    10: { total: '', debt_id: '', fractionary_id: '', coactive_id: '', previous_total: '', payment: '', payment_status: '', comment: '' },
                    11: { total: '', debt_id: '', fractionary_id: '', coactive_id: '', previous_total: '', payment: '', payment_status: '', comment: '' },
                    12: { total: '', debt_id: '', fractionary_id: '', coactive_id: '', previous_total: '', payment: '', payment_status: '', comment: '' },
                    total: '',
                });
                debtsMonthxYear[debtsMonthxYear.length - 1][element.month] = {
                    total: Number.parseFloat(element.total).toFixed(2),
                    debt_id: element.id,
                    fractionary_id: element.fractionary_id,
                    coactive_id: element.coactive_id,
                    previous_total: element.previous_total,
                    payment: element.payment,
                    payment_status: element.payment_status,
                    comment: element.comment,
                    reversion_id: element.reversion_id,
                };
            }
        });
        return debtsMonthxYear;
    },
    sumTotalDebtxYear(data) {
        // Calculo Total por Año
        data.forEach(item => {
            let accum = 0;
            for (let i = 1; i <= 12; i++) {
                accum += (
                    parseFloat(item[i].total) &&
                        (!item[i].fractionary_id && !item[i].coactive_id && !item[i].reversion_id)
                        && !(/fraccio|coactiv|condonad|anulad|cancelad|exonera|exoner/i.test(item[i].comment)) ? parseFloat(item[i].total) : 0);
            }
            item.total = Number.parseFloat(accum).toFixed(2);
        });
        return data;
    },
    sumTotalDebt(data) {
        // Calculo Total General
        return data.reduce((accum, item) => accum + parseFloat(item.total), 0);
    },
    printAccountStatus(list_debts) {
        return backend.post(`${resource_export}/print-account-status-pdf`, list_debts);
    },
    printDebtsForStand(list_debts_the_stand) {
        return backend.post(`${resource_export}/print-debts-for-stand-pdf`, list_debts_the_stand);
    }

}