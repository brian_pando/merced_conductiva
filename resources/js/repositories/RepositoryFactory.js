import MarketRepository from './MarketRepository'
import SectorRepository from './SectorRepository'
import StandRepository from './StandRepository'
import DriverRepository from './DriverRepository'
import GyreRepository from './GyreRepository'
import RateRepository from './RateRepository'
import AgentRepository from './AgentRepository'
import PersonRepository from './PersonRepository'
import MemberRepository from './MemberRepository'
import FileRepository from './FileRepository'
import TransactionRepository from './TransactionRepository'
import CashboxRepository from './CashboxRepository'
import DebtRepository from './DebtRepository'
import UserRepository from './UserRepository'
import FractionaryRepository from './FractionaryRepository'
import FractionaryTypeRepository from './FractionaryTypeRepository'
import ConceptRepository from './ConceptRepository'
import UITRepository from './UITRepository'
import CompanyRepository from './CompanyRepository'
import SettingRepository from './SettingRepository'
import CoactiveRepository from './CoactiveRepository'
import NotificationRepository from './NotificationRepository'
import ForgivenessRepository from './ForgivenessRepository'
import ReversionRepository from './ReversionRepository'
import ResponsabilityRepository from './ResponsabilityRepository'
import IzipayRepository from './IzipayRepository'
import CulqiRepository from './CulqiRepository'

const repositories ={
    market: MarketRepository,
    responsability: ResponsabilityRepository,
    sector: SectorRepository,
    stand:  StandRepository,
    driver: DriverRepository,
    gyre:   GyreRepository,
    rate: RateRepository,
    agent: AgentRepository,
    person: PersonRepository,
    member: MemberRepository,
    file: FileRepository,
    transaction: TransactionRepository,
    cashbox: CashboxRepository,
    debt: DebtRepository,
    user: UserRepository,
    fractionarytype:FractionaryTypeRepository,
    fractionary:FractionaryRepository,
    concept:ConceptRepository,
    uit:UITRepository,
    company:CompanyRepository,
    setting:SettingRepository,
    coactive:CoactiveRepository,
    notification:NotificationRepository,
    forgiveness:ForgivenessRepository,
    reversion:ReversionRepository,
    izipay:IzipayRepository,
    culqi:CulqiRepository,
    //mas repos
}

export default {
    get: name => repositories[name]
}
