<?php
namespace App;
use Illuminate\Support\Facades\Storage;

class Util{

    public static $concepts=[
        "--"=>0,
        "ME_ID"=>1,
        "SIS_ID"=>2,
        "RRS_ID"=>3,
        "MTD_ID"=>4,
        "PIT_ID"=>5,
        "TER_ID"=>6,
        "MAU_ID"=>7,
        "PNL_ID"=>8,
        "NCH_ID"=>9,
        "DDM_ID"=>10,
        //Nuevos 2023//
         "SA"=>11,
         "REN"=>12,//ya está de baja staus = 0
         "ARR"=>13


    ];
    public static $markets=[
        "MODEL_ID" => 1,
        "TUPC_ID" => 2,
        "SISA_ID" => 3,
        "KIOS_ID" => 4,
        "LOCAL_ID" => 5,
        "IPD_ID" => 6,
    ];

    public static $sectors=[
        "NONE_ID" => 1,
        "COSMO_ID" => 2,
        "VIVAN_ID" => 3,
        "NUEVO_ID" => 4,
        "ANTIGUO_ID" => 5,
        "AREA_VERDE_ID" => 6,
        "BELLA_DURMI_ID" => 7,
        "LETRA_ID" => 8,
        "MERCADILLO_ID" => 9,
        "LAMAS_ID" => 10,
        "NOCHE_ID" => 11,
        "PAMPA_ID" => 12,
        "PANADE_ID" => 13,
        "PARQUE_ID" => 14,
        "SEGUND_ID" => 15,
        "VEREDA_ID" => 16,
        "JR_CALLAO_ID" => 17,
        "IPD" =>18,
    ];

    public static $gyres=[
        "NONE_ID" => 1,
    ];
    public static $types_fractionaries=[
        "FRACTIONARY_TYPE_MEC"=>1,
        "FRACTIONARY_TYPE_RRC"=>2,
        "FRACTIONARY_TYPE_MTD"=>3,
        "FRACTIONARY_TYPE_PIT"=>4,
        "FRACTIONARY_TYPE_TER"=>5,
        "FRACTIONARY_TYPE_NCH"=>8,
        "FRACTIONARY_TYPE_PNL"=>7,
        "FRACTIONARY_TYPE_MAU"=>6,
    ];
    public static $RT_AUXIL=5;
    public static $RT_COBRA=6;
    public static $CA_CASHB=7;

    public static $MUNI_PERSON_ID=1;
    public static $ROL_PERSON_ID=3;
    public static $COMPANY_ID=1;
    public static $debts_wait_for_notify=1;
    public static $FRACTIONARY_TYPE_PIT=4;
    public static $FRACTIONARY_TYPE_TER=5;
    public static $FRACTIONARY_TYPE_NCH=8;
    public static $FRACTIONARY_TYPE_MAU=6;
    public static $FRACTIONARY_TYPE_SAD=9;
	public static $FORGIVENE_GENERIC='forgivene_generic'; 
	public static $DEBT_TYPE_NORMAL='normal'; 

    public static function check_mandatory_fields($data, $mandatory_params){
        foreach($mandatory_params as $v){ 
            if(  !array_key_exists($v,$data) ) throw new \Exception("$v is mandatory"); 
        }
        return true;
    }
    public static function namePhoto($name){   
        if(Storage::disk('local')->exists("public/".$name.".jpg")){
            return $name."x";
        }else if(Storage::disk('local')->exists("public/".$name."x".".jpg")){
            return $name;
        }else{
            return $name;
        }
    }
    public static function storageImage($name, $base64_content){
        $base64_str = substr($base64_content, strpos($base64_content, ",")+1);
        $image = base64_decode($base64_str);
       //se hace esto cambio de nombre porque en la vista no renderiza con el mismo nombre ya que vue no detecta cambio
        if(Storage::disk('local')->exists("public/".$name.".jpg")){//verifica si existe el archivo
            Storage::delete("public/".$name.".jpg");//elimina la foto existente
            Storage::put("public/".$name.".jpg",$image);
            return Storage::url("./public/".$name.".jpg");
        }else if(Storage::disk('local')->exists("public/".$name.".jpg")){
            Storage::delete("public/".$name.".jpg");//elimina la foto existente
            Storage::put("public/".$name.".jpg",$image);
            return Storage::url("./public/".$name.".jpg");
        }else{
            Storage::put("public/".$name.".jpg",$image);
            return Storage::url("./public/".$name.".jpg");
        }
    }
    public static function storageFile($name, $file){
        Storage::put("public/files/".$name,$file);
        return Storage::url("./public/files/".$name);
    }

    public static function verifyFile($params){
        if(array_key_exists("file",$params)&& !empty($params['file'])){
            $file = $params['file'];
        }else{
            $file="";
        }
        return $file;
    }

    public static function verifyPhoto($params){
        if(array_key_exists("photo",$params)&& ! empty($params['photo'])){
            $photo = $params['photo'];
        }else{
            $photo="";
        }
        return $photo;
    }

    public static function is_base64image($image_content){
        return !empty($image_content) && preg_match('/data:image\/[a-zA-Z]*;base64,/',$image_content);
    }

    public static function separate_files_into($data) {
        $files=null;
        if(array_key_exists("files",$data)){
            $files=$data['files'];
            unset($data['files']);
        }
        return $files;
    }

    public static function loadImageInBase64($img_path) {
        $opciones_ssl=array(
            "ssl"=>array(
            "verify_peer"=>false,
            "verify_peer_name"=>false,
            ),
        );
        $extencion = pathinfo($img_path, PATHINFO_EXTENSION);
        $data_ = file_get_contents($img_path, false, stream_context_create($opciones_ssl));
        $img_base_64 = base64_encode($data_);
        $path_img = 'data:image/' . $extencion . ';base64,' . $img_base_64;
        return $path_img;
    }
}
