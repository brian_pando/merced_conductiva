<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;
use Illuminate\Support\Facades\Schema;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->bind('App\Repository\UserRepositoryI','App\Repository\UserRepository');
        $this->app->bind('App\Repository\CompanyRepositoryI','App\Repository\CompanyRepository');
        $this->app->bind('App\Repository\MarketRepositoryI','App\Repository\MarketRepository');
        $this->app->bind('App\Repository\SectorRepositoryI','App\Repository\SectorRepository');
        $this->app->bind('App\Repository\StandRepositoryI','App\Repository\StandRepository');
        $this->app->bind('App\Repository\DriverRepositoryI','App\Repository\DriverRepository');
        $this->app->bind('App\Repository\GyreRepositoryI','App\Repository\GyreRepository');
        $this->app->bind('App\Repository\RateRepositoryI','App\Repository\RateRepository');
        $this->app->bind('App\Repository\CobratorTicketRepositoryI','App\Repository\CobratorTicketRepository');
        $this->app->bind('App\Repository\PersonRepositoryI','App\Repository\PersonRepository');
        $this->app->bind('App\Repository\FileRepositoryI','App\Repository\FileRepository');
        $this->app->bind('App\Repository\DebtRepositoryI','App\Repository\DebtRepository');
        $this->app->bind('App\Repository\TransactionRepositoryI','App\Repository\TransactionRepository');
        $this->app->bind('App\Repository\CashboxRepositoryI','App\Repository\CashboxRepository');
        $this->app->bind('App\Repository\FractionarytypeRepositoryI','App\Repository\FractionarytypeRepository');
        $this->app->bind('App\Repository\FractionaryRepositoryI','App\Repository\FractionaryRepository');
        $this->app->bind('App\Repository\ConceptRepositoryI','App\Repository\ConceptRepository');
        $this->app->bind('App\Repository\UITRepositoryI','App\Repository\UITRepository');
        $this->app->bind('App\Repository\SettingRepositoryI','App\Repository\SettingRepository');
        $this->app->bind('App\Repository\CoactiveRepositoryI','App\Repository\CoactiveRepository');
        $this->app->bind('App\Repository\PublicSserviceRepositoryI','App\Repository\PublicServiceRepository');
        $this->app->bind('App\Repository\NotificationRepositoryI','App\Repository\NotificationRepository');
        $this->app->bind('App\Repository\ResponsabilityRepositoryI','App\Repository\ResponsabilityRepository');
        $this->app->bind('App\Repository\ForgivenessRepositoryI','App\Repository\ForgivenessRepository');
        $this->app->bind('App\Repository\ReversionRepositoryI','App\Repository\ReversionRepository');
    }

    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        \Carbon\Carbon::setLocale("app.locale");
        Schema::defaultStringLength(191);
    }
}
