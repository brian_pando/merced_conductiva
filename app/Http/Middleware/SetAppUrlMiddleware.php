<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Http\Request;

class SetAppUrlMiddleware
{
    public function handle(Request $request, Closure $next)
    {
        // Get the request host
        $host = $request->getHost();
        // Logic to set APP_URL based on the host
        if ($host === 'merced.munitingomaria.gob.pe') {
            config(['app.url' => ($request->secure()?'https://':'http://').$host ]);
        }else if($host=='190.234.242.186'){
            config(['app.url' => 'http://190.234.242.186']);
        }else {
            config(['app.url' => env('LAN_APP_URL')]);
        }

        return $next($request);
    }
}
