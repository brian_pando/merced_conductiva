<?php

namespace App\Http\Controllers\API;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Repository\MemberRepositoryI;

class MemberController extends Controller
{
    private $repository;
    public function __construct(MemberRepositoryI $repository){
       $this->repository = $repository;
    }

    public function index(Request $request){
        $params = [];
        if($request->query('name')) $params['name']=$request->query('name');
        $l = $this->repository->all($params);
        return response()->json($l);
    }

    public function store(Request $request){
        $data=$request->all();
        $o = $this->repository->save($data);
        return response()->json($o);
    }

  
    public function show($id){
        return $this->repository->get($id);
    }

    public function update(Request $request, $id){
        $data=$request->all();
        $data['id'] = $id;
        $o = $this->repository->save($data);
        return response()->json($o);
        
    }

    public function destroy($id){
        //
    }

}
