<?php

namespace App\Http\Controllers\API;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Repository\SectorRepositoryI;

class SectorController extends Controller
{
    private $repository;
    public function __construct(SectorRepositoryI $repository){
       $this->repository = $repository;
    }

    public function index(Request $request){
        $params = [];
        if($request->query('search')) $params['search']=$request->query('search');
        if($request->query('market_id')) $params['market_id']=$request->query('market_id');
        if($request->query('not_paginate')) $params['not_paginate']=$request->query('not_paginate');
        if(is_numeric($request->query('id'))) $params['id']=$request->query('id');

        $l = $this->repository->all($params);
        return response()->json($l);
    }

    public function all_by_market(Request $request, $market_id){
        $params = compact("market_id");
        $l = $this->repository->all_by_market($params);
        return response()->json($l);
    }



    public function store(Request $request){
        $data=$request->all();
        $o = $this->repository->save($data);
        return $o;
    }

    public function show($sector){
        return $this->repository->find($sector);
    }

    public function update(Request $request, $id){
        $data=$request->all();
        $data['id'] = $id;
        $o = $this->repository->save($data);
        return $o;
    }

    public function destroy($id){
        //
    }
}
