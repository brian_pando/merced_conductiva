<?php

namespace App\Http\Controllers\API;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Repository\RateRepositoryI;

class RateController extends Controller
{
    private $repository;
    public function __construct(RateRepositoryI $repository){
       $this->repository = $repository;
    }

    public function index(Request $request){
        $params = [];
        if($request->query('search')) $params['search']=$request->query('search');
        $params['xpage']= $request->query('xpage',10);
        $l = $this->repository->all($params);
        return response()->json($l);
    }

    public function store(Request $request){
        $data=$request->all();
        $o = $this->repository->save($data);
        return $o;
    }

  
    public function show($id){
        return $this->repository->get($id);
    }

    public function update(Request $request, $id){
        $data=$request->all();
        $data['id'] = $id;
        $o = $this->repository->save($data);
        return $o;
    }

    public function destroy($id){
        //
    }
}
