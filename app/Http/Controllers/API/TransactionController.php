<?php

namespace App\Http\Controllers\API;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\AuthenticatesUsers;  
use App\Repository\TransactionRepositoryI;
use App\Repository\CompanyRepositoryI;
use Maatwebsite\Excel\Facades\Excel;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\Log;

class TransactionController extends Controller
{
    private $repository;
    private $repository_company;
    public function __construct(TransactionRepositoryI $repository,CompanyRepositoryI $repository_company){
       $this->repository = $repository;
       $this->repository_company=$repository_company;
    }

    public function index(Request $request){
        $params = $this->filters($request);
        $l = $this->repository->all($params);
        $total = $this->repository->CalculateTotalAll($params)->total;
        return response()->json(['data'=>$l,'transactions_total'=>$total]);
    }
    public function groupByMonth(Request $request){
        $year = $request->query("year");
        $l = $this->repository->allGroupByMonth(compact("year"));
        return response()->json($l);
    }
    public function listGroupByMonthExportPdf(Request $request){
        $commissionersList = $request->all();
        $u=\App\Models\User::find( \Auth::user()->id );//user logeado
        $company=$this->repository_company->find(\App\Util::$COMPANY_ID); //datos de la muni
        $name_file_excel = 'reporte_commisionistas_recaudaciones_mensuales'.'.pdf';
        Excel::store(new \App\Exports\TransactionListGroupByMonthPdfExport(['company'=>$company,'user_logged_in'=>$u,'commissionersList'=>$commissionersList]),'public/files/'.$name_file_excel, 'local', \Maatwebsite\Excel\Excel::DOMPDF);
        return Storage::url("/public/files/".$name_file_excel);
    }
    public function listCommissionExportExcel(Request $request){
        $params = $request->all();
        $dataToPrint = [];
        $usuar=\App\Models\User::find( \Auth::user()->id );//user logeado
        $commisionariesTransactions = $this->repository->allDetailsByConcepts($params);
        $data =  isset($params['no_paginate']) ? $commisionariesTransactions->toArray() :  $commisionariesTransactions->toArray()['data'];
        $company=$this->repository_company->find(\App\Util::$COMPANY_ID); 
        $balancetotal =0;
        $dataToPrint = array_map(function($item) { 
           
            return array(
                "correlativo" => $item['code'],
                "fecha" => $item['date_at'],
                "responsable" => $item['user']['name'],
                "cliente" => $item['client']['name'] . " " . $item['client']['lastname'],
                "dni" => $item['client']['dni'],
                "detalle" => $item['detail'],
                "estado" => $item['status']==1?"Pagado":"Deuda",
                "total" => $item['total']
            );
        }, $data);
        array_unshift($dataToPrint, ["correlativo" => "correlativo",
            "fecha" => "fecha",
            "responsable" => "Responsable",
            "cliente" => "Cliente",
            "dni" => "Dni",
            "detalle" => "Detalle",
            "estado" => "Estado",
            "total" => "Total"]);

            $search_names = isset($params["user_id"]) ? $commisionariesTransactions[0]["user"]["name"] : "";
            array_unshift($dataToPrint, ["user" => "Generado por: ".$usuar['name'].'-'.$usuar['username'] ]);
            array_unshift($dataToPrint, ["date" => "fecha de operación: ".date("d/m/Y H:i:s") ]);
            array_unshift($dataToPrint, ["commission" =>"Comisionista -". $search_names]);
            array_unshift($dataToPrint, ["info" => "REPORTE DE COMISIONISTAS POR RECAUDACIONES ENTRE " . 
                                         date("d/m/Y", strtotime($params["date_end"])) . ' - ' . 
                                         date("d/m/Y", strtotime($params["date_init"]))
                                        ]);
            array_unshift($dataToPrint, ["city" => $company['city'].'-'.$company['ruc']]);
            array_unshift($dataToPrint, ["company" => $company['name']]);

          $balancetotal  = array_reduce($data, function ($previous, $current) {
               return $previous + $current['total'];
           },0);

        Excel::store(new \App\Exports\CommissionAgenTransaction($dataToPrint,$balancetotal), 'public/files/reporte_commisionistas_recaudacion_diarios.xlsx', 'local', \Maatwebsite\Excel\Excel::XLSX);
        return Storage::url("/public/files/reporte_commisionistas_recaudacion_diarios.xlsx");
    }
    
    public function listCommissionExportPdf(Request $request){
       $params = $request->all();
       $dataToPrint = [];
        $usuar=\App\Models\User::find( \Auth::user()->id );//user logeado
        $commisionariesTransactions = $this->repository->allDetailsByConcepts($params);
        $data =  isset($params['no_paginate']) ? $commisionariesTransactions->toArray() :  $commisionariesTransactions->toArray()['data'];
        $company=$this->repository_company->find(\App\Util::$COMPANY_ID); 
        $balancetotal =0;
        $dataToPrint = array_map(function($item) { 
           
            return array(
                "code" => $item['code'],
                "date_at" => $item['date_at'],
                "user_name" => $item['user']['name'],
                "client_name" => $item['client']['name'] . " " . $item['client']['lastname'],
                "client_dni" => $item['client']['dni'],
                "detail" => $item['detail'],
                "status" => $item['status']==1?"Pagado":"Deuda",
                "total" => $item['total']
            );
        }, $data);

        $balancetotal  = array_reduce($data, function ($previous, $current) {
               return $previous + $current['total'];
           },0);
         
         $name_file_pdf="commisionistas_recaudacion_diarios.pdf";
         if(isset($params["user_id"])){
            $search_names = $commisionariesTransactions[0]["user"]["name"];
        }else{
             $search_names = "";
        }

        
        Excel::store(new \App\Exports\CommissionAgenTransactionPdfExport(['company'=>$company,'user_logged'=>$usuar,'data'=>$dataToPrint,'date_end'=>$params["date_end"],'date_init'=>$params["date_init"],'sum_total'=>$balancetotal,'search_names'=>$search_names,'title'=>'']),'public/files/'.$name_file_pdf, 'local', \Maatwebsite\Excel\Excel::DOMPDF);
        return Storage::url("/public/files/".$name_file_pdf);

       
     }

    public function listGroupByMonthExportExcel(Request $request) {
        $commissionersList = $request->all();
        $u=\App\Models\User::find( \Auth::user()->id );//user logeado
        $company=$this->repository_company->find(\App\Util::$COMPANY_ID); //datos de la muni
        $name_file_excel = 'reporte_commisionistas_recaudaciones_mensuales'.'.xlsx';
        Excel::store(new \App\Exports\TransactionListGroupByMonthExcelExport(['company'=>$company,'user_logged_in'=>$u,'commissionersList'=>$commissionersList]),'public/files/'.$name_file_excel, 'local', \Maatwebsite\Excel\Excel::XLSX);
        return Storage::url("/public/files/".$name_file_excel);
    }
    public function detailedPaymentsExportPdf(Request $request){  
        $search_names=$request['search_names'];
        $params = $this->filters($request);
        $params['no_paginate'] = true;
        $report_date = '-';
        if(isset($params['date_init']) and isset($params['date_end'])) {
            $date_init = date_format(date_create($params['date_init']),"d/m/Y");
            $date_end = date_format(date_create($params['date_end']),"d/m/Y");
            $report_date = $date_init ." - ". $date_end;
        }
        if($request['type']){ $name_file_excel = 'reporte_listado_pagos_detallados_fraccionados'.'.pdf';
            $title='LISTADO DE PAGOS DE FRACCIONAMIENTO';
        }else if($params['status']=='0'){$name_file_excel = 'reporte_listado_pagos_detallados'.'.pdf';
            $title='LISTADO DE PAGOS EXTORNADOS';
        }else{ $name_file_excel = 'reporte_listado_pagos_detallados'.'.pdf';
            $title='LISTADO DE PAGOS DETALLADO';}
 
        if($request->query('user_logeado_id')) $params['user_logeado_id']=$request->query('user_logeado_id');
        $u=\App\Models\User::select('name','username','role_id')->where('id', \Auth::user()->id)->first();//user logeado
        $detailed_payments = $this->repository->cashReceiptsReport($params);
        $sum_total = $this->repository->calculateTotalAll($params)->total;
        $company=$this->repository_company->find(\App\Util::$COMPANY_ID); //datos de la muni
        return (new \App\Exports\TransactionDetailedPaymentsPdfExport(['company'=>$company,'user_logged'=>$u,'data'=>$detailed_payments,'report_date'=>$report_date,'sum_total'=>$sum_total,'search_names'=>$search_names,'title'=>$title]))->download($name_file_excel, \Maatwebsite\Excel\Excel::DOMPDF);
    }
    public function detailedPaymentsExportExcel(Request $request){
        $search_names=$request['search_names'];
        $params = $this->filters($request);
        $params['no_paginate'] = true;
        $report_date = '-';
        if(isset($params['date_init']) and isset($params['date_end'])) {
            $date_init = date_format(date_create($params['date_init']),"d/m/Y");
            $date_end = date_format(date_create($params['date_end']),"d/m/Y");
            $report_date = $date_init ." - ". $date_end;
        }
        if($request['type']){ 
            $name_file_excel = 'reporte_listado_pagos_detallados_fraccionados'.'.xlsx';
            $title='LISTADO DE PAGOS DE FRACCIONAMIENTO';
        }else if($params['status']=='0'){$name_file_excel = 'reporte_listado_pagos_detallados'.'.xlsx';
            $title='LISTADO DE PAGOS EXTORNADOS';
        }else{ $name_file_excel = 'reporte_listado_pagos_detallados'.'.xlsx';
            $title='LISTADO DE PAGOS DETALLADO';
        }

        if($request->query('user_logeado_id')) $params['user_logeado_id']=$request->query('user_logeado_id');
        $u=\App\Models\User::select('name','username','role_id')->where('id', \Auth::user()->id)->first();//user logeado
        $detailed_payments = $this->repository->cashReceiptsReport($params);
        $sum_total = $this->repository->calculateTotalAll($params)->total;

        $company=$this->repository_company->find(\App\Util::$COMPANY_ID); //datos de la muni
        return (new \App\Exports\TransactionDetailedPaymentsExcelExport(['company'=>$company,'user_logged'=>$u,'data'=>$detailed_payments,'report_date'=>$report_date,'sum_total'=>$sum_total,'search_names'=>$search_names,'title'=>$title]))->download($name_file_excel, \Maatwebsite\Excel\Excel::XLSX);
    }

    public function allDetailsByConcepts(Request $request)
    {
        $params = $this->filters($request);
        if($request->query('user_logeado_id')) $params['user_logeado_id']=$request->query('user_logeado_id');
        $l = $this->repository->allDetailsByConcepts($params);
        $total = $this->repository->calculateTotalAll($params)->total;
        return response()->json(['data'=>$l,'transactions_total'=>$total]);
    }
    
    public function calculateTotalByConcept(Request $request) {
        $params = $this->filters($request);
        $l = $this->repository->calculateTotalByConcept($params);
        return response()->json($l);
    }

    public function calculateTotalBySector(Request $request) {
        $params = $this->filters($request);
        $l = $this->repository->calculateTotalBySector($params);
        return response()->json($l);
    }

    public function exportPdf(Request $request) {
        $params = $this->filters($request);
        $params['no_paginate'] = true;
        $report_date = '-';
        if(isset($params['date_init']) and isset($params['date_end'])) {
            $date_init = date_format(date_create($params['date_init']),"d/m/Y");
            $date_end = date_format(date_create($params['date_end']),"d/m/Y");
            $report_date = $date_init ." - ". $date_end;
        }
        $transactions = $this->repository->all($params);
        $transactions_total = $this->repository->CalculateTotalAll($params)->total;
        $sum_total_extornado=0;
        foreach($transactions as $total_extornado){
            if($total_extornado->status==0){
                $sum_total_extornado=$sum_total_extornado+$total_extornado->total;
            }
        }
        $u=\App\Models\User::find( \Auth::user()->id );//user logeado
        $company=$this->repository_company->find(\App\Util::$COMPANY_ID); //datos de la muni
        return (new \App\Exports\TransactionPdfExport(['company'=>$company,'user_logged_in'=>$u,'data'=>$transactions,'report_date'=>$report_date,'transactions_total'=>$transactions_total,'sum_total_extornado'=>$sum_total_extornado]))->download('reporte_de_pagos.pdf', \Maatwebsite\Excel\Excel::DOMPDF);
    }

    public function exportExcel(Request $request) {
        $params = $this->filters($request);
        $params['no_paginate'] = true;
        $report_date = '-';
        if(isset($params['date_init']) and isset($params['date_end'])) {
            $date_init = date_format(date_create($params['date_init']),"d/m/Y");
            $date_end = date_format(date_create($params['date_end']),"d/m/Y");
            $report_date = $date_init ." - ". $date_end;
        }
        $transactions = $this->repository->all($params);
        $transactions_total = $this->repository->CalculateTotalAll($params)->total;
        $sum_total_extornado=0;
        foreach($transactions as $total_extornado){
            if($total_extornado->status==0){
                $sum_total_extornado=$sum_total_extornado+$total_extornado->total;
            }
        }
        $u=\App\Models\User::find( \Auth::user()->id );
        $company=$this->repository_company->find(\App\Util::$COMPANY_ID); //datos de la muni
        return (new \App\Exports\TransactionExcelExport(['company'=>$company,'user_logged_in'=>$u,'data'=>$transactions,'report_date'=>$report_date,'transactions_total'=>$transactions_total,'sum_total_extornado'=>$sum_total_extornado]))->download('reporte_de_pagos.xlsx', \Maatwebsite\Excel\Excel::XLSX);
    }

    public function detailedExportPdf(Request $request) {
        $params = $this->filters($request);
        $params['no_paginate'] = true;
        $report_date = '-'; 
        if(isset($params['date_init']) and isset($params['date_end'])) {
            $date_init = date_format(date_create($params['date_init']),"d/m/Y");
            $date_end = date_format(date_create($params['date_end']),"d/m/Y");
            $report_date = $date_init ." - ". $date_end;
        }
        //dd($request);
        $transactions = $this->repository->all($params);
        $transactions_total = $this->repository->CalculateTotalAll($params)->total;
        $sum_total_extornado=0;
        foreach($transactions as $total_extornado){
            if($total_extornado->status==0){
                $sum_total_extornado=$sum_total_extornado+$total_extornado->total;
            }
        }
        $u=\App\Models\User::find( \Auth::user()->id ); //el user logeado
        $company=$this->repository_company->find(\App\Util::$COMPANY_ID); //datos de la muni
        return (new \App\Exports\TransactionDetailedPdfExport(['company'=>$company,'user_logged_in'=>$u,'data'=>$transactions,'report_date'=>$report_date,'transactions_total'=>$transactions_total,'sum_total_extornado'=>$sum_total_extornado]))->download('reporte_de_pagos_detallado.pdf', \Maatwebsite\Excel\Excel::DOMPDF);
        // ->download('reporte_cajas.xlsx', \Maatwebsite\Excel\Excel::XLSX);
    }

    public function detailedExportExcel(Request $request) {
        $params = $this->filters($request);
        $params['no_paginate'] = true;
        $transactions = $this->repository->all($params);
        $transactions_total = $this->repository->CalculateTotalAll($params)->total;
        $u=\App\Models\User::find( \Auth::user()->id );
        $company=$this->repository_company->find(\App\Util::$COMPANY_ID); //datos de la muni
        $report_date = '-';
        if(isset($params['date_init']) and isset($params['date_end'])) {
            $date_init = date_format(date_create($params['date_init']),"d/m/Y");
            $date_end = date_format(date_create($params['date_end']),"d/m/Y");
            $report_date = $date_init ." - ". $date_end;
        }
        $sum_total_extornado=0;
        foreach($transactions as $total_extornado){
            if($total_extornado->status==0){
                $sum_total_extornado=$sum_total_extornado+$total_extornado->total;
            }
        }
        return (new \App\Exports\TransactionDetailedExcelExport(['company'=>$company,'user_logged_in'=>$u,'data'=>$transactions,'report_date'=>$report_date,'transactions_total'=>$transactions_total,'sum_total_extornado'=>$sum_total_extornado]))->download('reporte_de_pagos_detallado.xlsx', \Maatwebsite\Excel\Excel::XLSX);
        // ->download('reporte_cajas.xlsx', \Maatwebsite\Excel\Excel::XLSX);
    }

    private function filters(Request $request) {
        $params = [];
        if($request->query('search')) $params['search']=$request->query('search');
        if($request->query('concept_id')) $params['concept_id']=$request->query('concept_id');
        if($request->query('user_id')) $params['user_id']=$request->query('user_id');
        if($request->query('type')) $params['type']=$request->query('type');
        if($request->query('sector_id')) $params['sector_id']=$request->query('sector_id');
        if($request->query('sectors_id')) $params['sectors_id']=$request->query('sectors_id');
        if($request->query('market_id')) $params['market_id']=$request->query('market_id');
        if($request->query('date_init')) $params['date_init']=$request->query('date_init');
        if($request->query('date_end')) $params['date_end']=$request->query('date_end');
        if($request->query('no_paginate')) $params['no_paginate']=$request->query('no_paginate');
        if($request->query('status')==0) $params['status']=$request->query('status');
        return $params;
    }

    public function resumenByConcept(Request $request){
        $params = [];
        if($request->query('date_init')) $params['date_init']=$request->query('date_init');
        $params['date_end']=$request->query('date_end'); // se require el valor de date_end en null o con valor
        $params['transactions.user_id']=$request->query('user_id');
        $l = $this->repository->resumenByConcept($params);
        return response()->json($l);
    }

    public function store(Request $request){
        $data=$request->all();
        $o = $this->repository->save($data);
        return $o;
    }

    public function delete(Request $request,$id){
        $data=$request->all();
        $o = $this->repository->delete($data);
        return $o;
    }

  
    public function show($id){
        return $this->repository->get($id);
    }

    public function update(Request $request, $id){
        $data=$request->all();
        $data['id'] = $id;//llego
        $o = $this->repository->save($data);
        return $o;
    }

    public function removeDetail($id) {
        $o = $this->repository->removeDetail($id);
        return $o;   
    }

    public function printPaymentVoucher(Request $request) {
        // $transaction = $request->all();
        $pdf = app('dompdf.wrapper');
        // ['transaction'=>(object)$transaction]
        return \PDF::loadView('voucher')->stream('archivo.pdf');
    }
    public function printCommissionCollection(Request $request){
        $company=$this->repository_company->find(\App\Util::$COMPANY_ID); //datos de la muni
        $u=\App\Models\User::find( \Auth::user()->id );//usuario de inicio de sesion
        $data=[
            'totalByConcepts'=>$request['totalByConcepts'],      
            'company'=>$company,
            'username'=>$request['username'],
            'date_init'=>$request['date_init'],
            'date_end'=>$request['date_end'],
            'user'=>$u
        ];
        $name_file_pdf =  'reporte_recaudacion_comisionista.pdf';
        \PDF::setOptions(['isPhpEnabled'=>true])->loadView('commission.printCommissionCollection', $data)->setPaper('A4', 'portrait')->save(storage_path('../public/storage/public/files/') . $name_file_pdf );
        return Storage::url("/public/files/".$name_file_pdf);
    }
}
