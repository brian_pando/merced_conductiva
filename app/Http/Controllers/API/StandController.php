<?php

namespace App\Http\Controllers\API;

use App\Util;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Repository\StandRepositoryI;
use App\Repository\DebtRepositoryI;
use App\Repository\FileRepositoryI;
use Illuminate\Support\Facades\Storage;
use App\Repository\CompanyRepositoryI;

class StandController extends Controller
{
    private $repository;
    private $fileRepository;
    private $debtrepository;
    private $companyRepository;
    public function __construct(StandRepositoryI $repository, DebtRepositoryI $debtrepository, CompanyRepositoryI $companyRepository, FileRepositoryI $fileRepository)
    {
        $this->repository = $repository;
        $this->fileRepository = $fileRepository;
        $this->debtrepository = $debtrepository;
        $this->companyRepository = $companyRepository;
    }

    public function index(Request $request)
    {
        $params = [];
        if ($request->query('typeStatusAccount')) $params['typeStatusAccount'] = $request->query('typeStatusAccount'); // pagado o deuda
        if ($request->query('search')) $params['search'] = $request->query('search');
        if ($request->query('no_paginate')) $params['no_paginate'] = $request->query('no_paginate');
        if ($request->query('type')) $params['markets.type'] = $request->query('type');
        if ($request->query('market_id')) $params['market_id'] = $request->query('market_id');
        if ($request->query('standsAll')) $params['standsAll'] = $request->query('standsAll');
        if ($request->query('number_paginate')) $params['number_paginate'] = $request->query('number_paginate');
        if ($request->query('sector_id')) $params['sector_id'] = $request->query('sector_id');
        if ($request->query('gyre_id')) $params['gyre_id'] = $request->query('gyre_id');
        $l = $this->repository->all($params);
        return response()->json($l);
    }

    public function getStandsByClientID(Request $request)
    {
        $params = [];
        if ($request->query('titular_id')) $params['stands.titular_id'] = $request->query('titular_id');
        if (is_numeric($request->query('no_pagination'))) $params['no_pagination'] = $request->query('no_pagination');
        $l = $this->repository->getStandsByClientID($params);
        return response()->json($l);
    }

    public function store(Request $request)
    {
        $data = $request->all();
        $files = \App\Util::separate_files_into($data);
        $o = $this->repository->save($data);
        if (!is_null($files)) {
            if ($o->market_id == \App\Util::$markets['LOCAL_ID']) $this->fileRepository->check_and_save($files, ['type' => 'local', 'id' => $o->id]);
            else $this->fileRepository->check_and_save($files, ['type' => 'stand', 'id' => $o->id]);
        }
        return $o;
    }

    public function getAllStandsByMarket(Request $request)
    {
        $params = [];
        /*         return $this->repository->getAllStandsByMarket($params); */
    }

    public function show($id)
    {
        return $this->repository->get($id);
    }

    public function update(Request $request, $id)
    {
        $data = $request->all();
        $data['id'] = $id;
        $files = \App\Util::separate_files_into($data);
        $o = $this->repository->save($data);
        if (!is_null($files)) {
            if ($o->market_id == \App\Util::$markets['LOCAL_ID']) $this->fileRepository->check_and_save($files, ['type' => 'local', 'id' => $o->id]);
            else $this->fileRepository->check_and_save($files, ['type' => 'stand', 'id' => $o->id]);
        }
        return $o;
    }

    public function destroy($id)
    {
        //
    }

    public function getAccountStatusByMonth(Request $request)
    {

        $params = [];
        if ($request->query('stand_id'))                   $params['debts.stand_id'] = $request->query('stand_id');
        if (is_numeric($request->query('payment_status'))) $params['debts.payment_status'] = $request->query('payment_status');
        if ($request->query('type'))                       $params['debts.type'] = $request->query('type');
        if ($request->query("concept_id"))                 $params['debts.concept_id'] = $request->query("concept_id");
        if ($request->query('date_init')) $params['date_init'] = $request->query('date_init');
        if ($request->query('date_end')) $params['date_end'] = $request->query('date_end');
        return $this->debtrepository->getStandByMonth($params);
    }
    public function getStandWithDebts($id, $payment_status, Request $request)
    {
        $params = [];
        //$params=["debts.payment_status"=>$payment_status,"debts.client_id"=>$id];
        $params = ["payment_status" => $payment_status];
        if (is_numeric($request->query('stand_id'))) $params['stand_id'] = $request->query('stand_id');
        //if(is_numeric($request->query('individual_view_payments'))) $params['individual_view_payments']=$request->query('individual_view_payments');
        return $this->repository->getStandWithDebts($params);
    }
    public function getBringDebtsToDate($id, $payment_status, Request $request)
    {
        $params = [];
        $params = ["payment_status" => $payment_status];
        if (is_numeric($request->query('stand_id'))) $params['stand_id'] = $request->query('stand_id');
        return $this->repository->getBringDebtsToDate($params);
    }
    public function printAccountStatus(Request $request)
    {
        $company = $this->companyRepository->find(\App\Util::$COMPANY_ID); //datos de la muni
        $username = \App\Models\User::find(\Auth::user()->id); //user logeado
        $data = [
            'list_debts_by_year' => $request['list_debts_by_year'],
            'total_sum_of_debts' => $request['total_sum_of_debts'],
            'text_debt_or_payment' => $request['text_debt_or_payment'],
            'is_text_debts_direct_fine' => $request['is_text_debts_direct_fine'],
            'date_impresion' => $request['date_impresion'],
            'titular' => $request['titular'],
            'company' => $company,
            'username' => $username->username,
            "header_logo" => Util::loadImageInBase64(public_path("storage/public/setting_4.jpg")),
        ];
        $name_file_pdf =  'estado_cuenta.pdf';
        \PDF::setOptions(['isPhpEnabled' => true])->loadView('stand.printAccountStatus', $data)->setPaper('A5', 'landscape')->save(storage_path('../public/storage/public/files/') . $name_file_pdf);
        return Storage::url("/public/files/" . $name_file_pdf);
    }
    public function printDebtsForStand(Request $request)
    {
        $username = \App\Models\User::find(\Auth::user()->id); //user logeado
        $list_the_stand = $request['list_the_stand'];
        $params['type'] = 'normal';
        $params['payment_status'] = 0;
        if ($request['concept_id'])  $params['concept_id'] = $request['concept_id'];
        foreach ($list_the_stand as $index => $stand) {
            $params['stand_id'] = $stand['id'];
            $debts_the_stands_by_month[$index] = $this->debtrepository->getStandByMonth($params);
        }
        $data = [
            'market' => $request['market'],
            'sector' => $request['sector'],
            'list_the_stand' => $list_the_stand,
            'debts_the_stands_by_month' => $debts_the_stands_by_month,
            'username' => $username->username,
            "header_logo" => Util::loadImageInBase64(public_path("storage/public/setting_4.jpg")),
        ];
        $name_file_pdf =  'deudas_por_puesto.pdf';
        \PDF::loadView('stand.printDebtsForStand', $data)->setPaper('A4', 'portrait')->save(storage_path('../public/storage/public/files/') . $name_file_pdf);
        return Storage::url("/public/files/" . $name_file_pdf);
    }
    public function getListStandWithTitular(Request $request)
    {
        $params = [];
        $params['stands.market_id'] = $request['market_id'];
        if ($request['sector_id']) $params['stands.sector_id'] = $request['sector_id'];
        if ($request['noPaginate']) $params['noPaginate'] = $request['noPaginate'];
        $list_stand_and_titular = $this->repository->getListStandWithTitular($params);
        return response()->json($list_stand_and_titular);
    }
}
