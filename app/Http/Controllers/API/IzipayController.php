<?php

namespace App\Http\Controllers\API;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class IzipayController extends Controller
{
    private $iziPay;

    public function __construct(){
        /*$this->iziPay=[
            "stage"=>"https://testapi-pw.izipay.pe",
            "prod"=>"https://api-pw.izipay.pe",
            "requestSource"=>"MPLP",
            "merchantCode"=>"82362328",
            "publicKey"=>"82362328:testpublickey_4iVS9N5F9PnexKoSzCPiTbYQoPO13rxuhEtSe8sOQpQ10"
        ];*/

        $this->iziPay=[
            "url"=>"https://api.micuentaweb.pe/api-payment/V4/Charge/CreatePayment",
            "user"=>"82362328",
            "password"=>"testpassword_wsds7X9EYqxKQ3D0hjMbiMiwIQkEKsT3OsKNSiYJTRAdL",
        ];
    }

     public function createToken(Request $request){
        $number = date("ymdHis");
        $orderNumber = "OR-$number";
        $transactionId =  "$number";
        $payload=[
            'orderId'=>$orderNumber,
            'currency'=> "PEN",
            'amount'=>$request->query("amount")*1,
            'customer'=>["email"=>"sample@gmail.com"]
        ];
        $client = new \GuzzleHttp\Client(['auth' => [$this->iziPay['user'], $this->iziPay['password']]]);
        $response = $client->post($this->iziPay['url'], [
            'headers'=>['transactionId'=>$transactionId],
            'json'=> $payload
        ]);
        //dd($response->getBody());
        return response()->json(json_decode((string) $response->getBody()));
    }
}
