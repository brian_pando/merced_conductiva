<?php

namespace App\Http\Controllers\API;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Repository\MarketRepositoryI;
use App\Repository\DebtRepositoryI;
use App\Repository\StandRepositoryI;
use App\Repository\CompanyRepositoryI;
use App\Repository\FileRepositoryI;
use Maatwebsite\Excel\Facades\Excel;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\Log;

class MarketController extends Controller
{
    private $repository;
    private $fileRepository;
    private $debtRepository;
    private $standRepository;
    private $repository_company;
    public function __construct(MarketRepositoryI $repository,FileRepositoryI $fileRepository, DebtRepositoryI $debtRepository, StandRepositoryI $standRepository, CompanyRepositoryI $repository_company){
       $this->repository = $repository;
       $this->fileRepository = $fileRepository;
       $this->debtRepository = $debtRepository;
       $this->standRepository = $standRepository;
       $this->repository_company=$repository_company;
    }

    public function index(Request $request){
        $params = [];
        if($request->query('name')) $params['name']=$request->query('name');
        if($request->query('search')) $params['search']=$request->query('search');
        $l = $this->repository->all($params);
        return response()->json($l);
    }

    public function store(Request $request){
        $data=$request->all();
        $files = \App\Util::separate_files_into($data);
        
        $o = $this->repository->save($data);
        if(! is_null($files)) $this->fileRepository->check_and_save($files,['type'=>'market','id'=>$o->id]);
        return $o;
    }

    public function show($id){
        //return $this->repository->get($id);
    }

    public function update(Request $request, $id){
        $data=$request->all();
        $data['id'] = $id;
        $files = \App\Util::separate_files_into($data);
       
        $o = $this->repository->save($data);
        if(! is_null($files)) $this->fileRepository->check_and_save($files,['type'=>'market','id'=>$o->id]);
        return $o;
    }

    public function destroy($id){
        //
    }

    public function generateDebts($market_id, Request $request) {
        $date_init = $request->query("date_init");
        $date_finish = $request->query("date_finish");
        $concept_id = $request->query("concept_id");
        $is_debt_daily = $request->query("is_debt_daily");

        $params=compact("market_id","concept_id",'date_init','date_finish','is_debt_daily');
        
        if($request->query("sector_id")) $params['sector_id']=$request->query("sector_id");
        if($request->query("stand_id")) $params['stand_id']=$request->query("stand_id");

        try {
            $updated_market = false;
            $params['type']='normal';
            $debts = $this->debtRepository->allByMarket($params);
            if(count($debts)>0){ throw new \Exception("Los puestos ya tiene deudas del concepto elegido en el rango $date_init al $date_finish "); }
            $l=$this->debtRepository->createDebtsInMarket($params);
            return $l;
        } catch (\Exception $e) {
            throw $e;
        }  
        
    }

    // public function generateDebts() {
    //     $year = date("Y");
    //     // listar mercados
    //     $markets = $this->repository->all([]);
    //     // Listar las pagos por mes de diciembre del año actual
    //     $updated_markets = [];
    //     foreach($markets as $market) { //para quitar a todos los mercados que ya estan actualizados
    //         $updated_market = false;
    //         $debts = $this->debtRepository->lastMonthDebtsMarket($market->id); 
    //         if(count($debts) > 0) {
    //             // si el mes y año del payment es de diciembre 2020 
    //             $debt = $debts[0];
    //             $date_at = date("m-yy",strtotime($debt->date_at));
    //             if( $date_at == "12-".$year ) {
    //                 $updated_market = true;
    //             }
    //         }
    //         $updated_markets[] = ["market_id"=>$market->id, "update"=>$updated_market];
    //     }
    //     $pending_market = array_filter($updated_markets, function($item){return !$item["update"];});
    //     $l=$this->debtRepository->createDebtInMarkets($pending_market, $year); 
    //     // si no tiene registros, se debe autog. los pagos en modo deudas. 
    //     return $l;
    // }

    public function allAccountStatusByYear(Request $request) {
        $params = [];
        $params = $this->validateAccountStatusFilters($request, $params);

        return $this->debtRepository->allMarketByYear($params);
    }

    public function getAccountStatusByMonth(Request $request) {
        $params = [];
        $params = $this->validateAccountStatusFilters($request, $params);
        if($request->query('year')) $params['year']=$request->query('year');
        return $this->debtRepository->getMarketByMonth($params);
    }
    
    public function getAccountStatusByStands(Request $request) {
        $params = [];
        $params = $this->validateAccountStatusFilters($request, $params);
        //dd($params);
        $result = [];
        $result['data'] = $this->debtRepository->allStandsByYear($params);
		$result['total'] = $this->debtRepository->totalStandsByYear($params);

		return response()->json($result);
    }

	public function getNameYearsOfDebt(Request $request){
		$params = [];	
		$params = $this->validateAccountStatusFilters($request, $params);

		return $this->debtRepository->getNameYearsOfDebt($params);
	}

	private function validateAccountStatusFilters($request, $params) {
        if($request->query('sector_id')) $params['stands.sector_id']=$request->query('sector_id');
        if($request->query('stands_id')) $params['stands_id']=$request->query('stands_id');
        if($request->query('market_id')) $params['stands.market_id']=$request->query('market_id');
        if($request->query("concept_id")) $params['debts.concept_id']=$request->query("concept_id");
        if($request->query('type')) $params['debts.type']=$request->query('type');
        if(is_numeric($request->query('payment_status'))) $params['debts.payment_status']=$request->query('payment_status');
        //if($request->query('year')) $params['year']=$request->query('year');
        if($request->query('date_init')) $params['date_init']=$request->query('date_init');
        if($request->query('date_end')) $params['date_end']=$request->query('date_end');
        if($request->query('sectors')) $params['sectors']=$request->query('sectors');
        if($request->query('gyres') || is_numeric($request->query('gyres'))) $params['gyres']=$request->query('gyres');
        return $params;
    }


    public function exportStatusAccountPdf(Request $request) {
        $marketStatusAccount = $request->all();
        $newColumns = [];
        $columns = $marketStatusAccount['column'];
        $columnsYear = array_filter($columns, function($column) use ($marketStatusAccount) {
            return $marketStatusAccount['groupedBy'] == "months" ? is_numeric(substr($column[1],-1)) : is_numeric($column[0]);
        });
        $columnsGroups = array_chunk($columnsYear,$marketStatusAccount['groupedBy'] == "months"?12:10);
        foreach ($columnsGroups as $key => $value) {
            $columnsToPrint = [];
            foreach ($columns as $columnKey => $column) {
                if (is_numeric($column[0]) || is_numeric(substr($column[1],-1))) break;
                array_push($columnsToPrint,$column);
            }
            $columnsToPrint = array_merge($columnsToPrint,$value);
            array_push($columnsToPrint,$columns[count($columns)-1]);

            array_push($newColumns,$columnsToPrint);
        }
        $marketStatusAccount['column'] = $newColumns;
        $name_file_pdf = 'reporte_mercado'.'.pdf';
        $u=\App\Models\User::find( \Auth::user()->id );//user logeado
        Excel::store(new \App\Exports\MarketStatusAccountPdfExport(['user_logged_in'=>$u,'marketStatusAccount'=>$marketStatusAccount]),'public/files/'.$name_file_pdf, 'local', \Maatwebsite\Excel\Excel::DOMPDF);
        return Storage::url("/public/files/".$name_file_pdf);
    }

    public function exportStatusAccountExcel(Request $request) {
        $marketStatusAccount = $request->all();
        $name_file_excel = 'reporte_mercado'.'.xlsx';
        $u=\App\Models\User::find( \Auth::user()->id );//user logeado     
        Excel::store(new \App\Exports\MarketStatusAccountExcelExport(['user_logged_in'=>$u,'marketStatusAccount'=>$marketStatusAccount]),'public/files/'.$name_file_excel, 'local', \Maatwebsite\Excel\Excel::XLSX);
        return Storage::url("/public/files/".$name_file_excel);
    }
     public function exportStatusAccountByMonthExcel(Request $request){
         $marketStatusAccount = $request->all();
         $name_file_excel = 'Reporte_padron_por_meses'.'.xlsx';
         $u=\App\Models\User::find( \Auth::user()->id );//user logeado
        $company=$this->repository_company->find(\App\Util::$COMPANY_ID); //datos de la muni
        $marketStatusAccount['company']=$company;

          Excel::store(new \App\Exports\MarketStatusAccountByMonthExcelExport(['user_logged_in'=>$u,'marketStatusAccount'=>$marketStatusAccount]),'public/files/'.$name_file_excel, 'local', \Maatwebsite\Excel\Excel::XLSX);
        return Storage::url("/public/files/".$name_file_excel);
        
    }
    public function exportStatusAccountByMonthPdf(Request $request){
      $marketStatusAccount = $request->all();
         $name_file_pdf = 'Reporte_padron_por_meses'.'.pdf';
         $u=\App\Models\User::find( \Auth::user()->id );//user logeado
        $company=$this->repository_company->find(\App\Util::$COMPANY_ID); //datos de la muni
        $marketStatusAccount['company']=$company; 
          
         // return  $marketStatusAccount;
         Excel::store(new \App\Exports\MarketStatusAccountByMonthPdfExport(['user_logged_in'=>$u,'marketStatusAccount'=>$marketStatusAccount]),'public/files/'.$name_file_pdf, 'local', \Maatwebsite\Excel\Excel::DOMPDF);
        return Storage::url("/public/files/".$name_file_pdf); 

    }

    public function exportStatusAccountByStandsPdf(Request $request) {
        $marketStatusAccountByStands = $request->all();
        $params = [];
        $params['no_paginate']=true;
        if(is_numeric($request->query('market_id'))) $params['stands.market_id']=$request->query('market_id');
        $l = $this->standRepository->all($params);
 
        unset($params['no_paginate']);
        $a='';
        for($i = 0; $i < count($l); ++$i) {
          if($i==0){
            $a=$a.''.$l[$i]->id;
          }else{
            $a=$a . ",".$l[$i]->id;
          }
        }
        if($request->query('date_init')) $params['date_init']=$request->query('date_init');
        if($request->query('date_end')) $params['date_end']=$request->query('date_end');
        if($request->query('type')) $params['debts.type']=$request->query('type');
        if(is_numeric($request->query('sector_id'))) $params['stands.sector_id']=$request->query('sector_id');
        if(is_numeric($request->query('concept_id'))) $params['debts.concept_id']=$request->query('concept_id');
        if(is_numeric($request->query('payment_status'))) $params['debts.payment_status']=$request->query('payment_status');
        $search_names=$request->query('search_names');
        unset($params['search_names']);

        $params['stands_id']=$a;
       
        $result['data'] = $this->debtRepository->allStandsByYear($params);
        $result['total'] = $this->debtRepository->totalStandsByYear($params);
          if (count($result['data']) === 0 || count($result['total']) === 0) return 'No hay datos disponibles';
        $is_fin_datos=1;
        $index_data=0;
        $stand_id_data=$result['data'][0]->stand_id;
        $j=0;
        $numero_items_data=count($result['data']);
        while($is_fin_datos==1){
            for ($i = 0; $i < sizeof($result['total']); $i++) {
                if($i==0){
                    $datos[$j]['stand_code']=$result['data'][$index_data]->stand_code;
                    $datos[$j]['sector_name']=$result['data'][$index_data]->sector_name;
                    $datos[$j]['titular_dni']=$result['data'][$index_data]->titular_dni;
                    $datos[$j]['titular_name']=$result['data'][$index_data]->titular_name.' '.$result['data'][$index_data]->titular_lastname;
                    $datos[$j]['rate_amount']=$result['data'][$index_data]->rate_amount;   
                }
                if($numero_items_data!=$index_data){
                    if($result['data'][$index_data]->year==$result['total'][$i]->year && 
                    $result['data'][$index_data]->stand_id==$stand_id_data){
                        $datos[$j][$i]=$result['data'][$index_data]->total;
                        $index_data++;
                    }else{
                        $datos[$j][$i]=0;
                    }    
                }else{
                    $datos[$j][$i]=0;
                }                 
            }
            $j++;
            if($index_data===$numero_items_data){
                $is_fin_datos++;
            }else{
                $stand_id_data=$result['data'][$index_data]->stand_id;
            }
        }
        $name_file_excel = 'reporte_mercado_por_puestos'.'.pdf';
        $u=\App\Models\User::find( \Auth::user()->id );//user logeado
        $company=$this->repository_company->find(\App\Util::$COMPANY_ID); //datos de la muni
        return (new \App\Exports\MarketStatusAccountByStandsPdfExport(['company'=>$company,'user_logged'=>$u,'result'=>$result['total'],'datos'=>$datos,'search_names'=>$search_names]))->download('reporte_mercado_por_puestos.pdf', \Maatwebsite\Excel\Excel::DOMPDF);
    }

    public function exportStatusAccountByStandsExcel(Request $request) {
        $marketStatusAccountByStands = $request->all();
        $params = [];
        $params['no_paginate']=true;
        if(is_numeric($request->query('market_id'))) $params['stands.market_id']=$request->query('market_id');
        $l = $this->standRepository->all($params);
 
        unset($params['no_paginate']);
        $a='';
        for($i = 0; $i < count($l); ++$i) {
          if($i==0){
            $a=$a.''.$l[$i]->id;
          }else{
            $a=$a . ",".$l[$i]->id;
          }
        }
        if($request->query('date_init')) $params['date_init']=$request->query('date_init');
        if($request->query('date_end')) $params['date_end']=$request->query('date_end');
        if($request->query('type')) $params['debts.type']=$request->query('type');    
        if(is_numeric($request->query('sector_id'))) $params['stands.sector_id']=$request->query('sector_id');
        if(is_numeric($request->query('concept_id'))) $params['debts.concept_id']=$request->query('concept_id');
        if(is_numeric($request->query('payment_status'))) $params['debts.payment_status']=$request->query('payment_status');
        $search_names=$request->query('search_names');
        unset($params['search_names']);

        $params['stands_id']=$a;

        $result['data'] = $this->debtRepository->allStandsByYear($params);
        $result['total'] = $this->debtRepository->totalStandsByYear($params);

        if (count($result['data']) === 0 || count($result['total']) === 0) return 'No hay datos disponibles';
        $is_fin_datos=1;
        $index_data=0;
        $stand_id_data=$result['data'][0]->stand_id;
        $j=0;
        $numero_items_data=count($result['data']);
        while($is_fin_datos==1){
            for ($i = 0; $i < sizeof($result['total']); $i++) {
                if($i==0){
                    $datos[$j]['stand_code']=$result['data'][$index_data]->stand_code;
                    $datos[$j]['sector_name']=$result['data'][$index_data]->sector_name;
                    $datos[$j]['titular_dni']=$result['data'][$index_data]->titular_dni;
                    $datos[$j]['titular_name']=$result['data'][$index_data]->titular_name.' '.$result['data'][$index_data]->titular_lastname;
                    $datos[$j]['rate_amount']=$result['data'][$index_data]->rate_amount;   
                }
                if($numero_items_data!=$index_data){
                    if($result['data'][$index_data]->year==$result['total'][$i]->year && 
                    $result['data'][$index_data]->stand_id==$stand_id_data){
                        $datos[$j][$i]=$result['data'][$index_data]->total;
                        $index_data++;
                    }else{
                        $datos[$j][$i]=0;
                    }    
                }else{
                    $datos[$j][$i]=0;
                }                 
            }
            $j++;
            if($index_data===$numero_items_data){
                $is_fin_datos++;
            }else{
                $stand_id_data=$result['data'][$index_data]->stand_id;
            }
        }


        $u=\App\Models\User::find( \Auth::user()->id );//user logeado
        $company=$this->repository_company->find(\App\Util::$COMPANY_ID); //datos de la muni
        return (new \App\Exports\MarketStatusAccountByStandsExcelExport(['company'=>$company,'user_logged'=>$u,'result'=>$result['total'],'datos'=>$datos,'search_names'=>$search_names]))->download('reporte_mercado_por_puestos.xlsx', \Maatwebsite\Excel\Excel::XLSX);
    }
    public function exportselectRowMarketPdf(Request $request) {
        $marketSelectRow = $request->all();
        $name_file_excel = 'reporte_mercado_fila_seleccionada'.'.pdf';
        $u=\App\Models\User::find( \Auth::user()->id );//user logeado
        Excel::store(new \App\Exports\MarketSelectRowPdfExport(['user_logged_in'=>$u,'marketSelectRow'=>$marketSelectRow]),'public/files/'.$name_file_excel, 'local', \Maatwebsite\Excel\Excel::DOMPDF);
        return Storage::url("/public/files/".$name_file_excel);
    }
    public function exportselectRowMarketExcel(Request $request) {
        $marketSelectRow = $request->all();
        $name_file_excel = 'reporte_mercado_fila_seleccionada'.'.xlsx';
        $u=\App\Models\User::find( \Auth::user()->id );//user logeado
        Excel::store(new \App\Exports\MarketSelectRowExcelExport(['user_logged_in'=>$u,'marketSelectRow'=>$marketSelectRow]),'public/files/'.$name_file_excel, 'local', \Maatwebsite\Excel\Excel::XLSX);
        return Storage::url("/public/files/".$name_file_excel);
    }
}
