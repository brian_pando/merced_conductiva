<?php

namespace App\Http\Controllers\API;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Repository\GyreRepositoryI;

class GyreController extends Controller
{
    private $repository;
    public function __construct(GyreRepositoryI $repository){
       $this->repository = $repository;
    }

    public function index(Request $request){
        $params = [];
        $params['xpage']=$request->query('xpage', 10);
        if($request->query('name')) $params['name']=$request->query('name');
        if($request->query('search')) $params['search']=$request->query('search');
        if(is_numeric($request->query('id'))) $params['id']=$request->query('id');
    
        $l = $this->repository->all($params);
        return response()->json($l);
    }

    public function store(Request $request){
        $data=$request->all();
        $o = $this->repository->save($data);
        return $o;
    }

  
    public function show($id){
        return $this->repository->get($id);
    }

    public function update(Request $request, $id){
        $data=$request->all();
        $data['id'] = $id;
        $o = $this->repository->save($data);
        return $o;
    }

    public function destroy($id){
        //
    }

    public function all_by_market($marketId) {
        $l = $this->repository->all_by_market(['market_id'=>$marketId]);
        return response()->json($l);
    }
}
