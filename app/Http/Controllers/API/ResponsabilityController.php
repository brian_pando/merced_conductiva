<?php

namespace App\Http\Controllers\API;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Repository\ResponsabilityRepositoryI;

class ResponsabilityController extends Controller
{
    private $repository;
    public function __construct(ResponsabilityRepositoryI $repository){
       $this->repository = $repository;
    }

    public function index(Request $request){
        $params = [];
        if($request->query('user_id')) $params['user_id']=$request->query('user_id');
        if($request->query('status')) $params['status']=$request->query('status');
        $l = $this->repository->all($params);
        return response()->json($l);
    }

    public function store(Request $request){
        $data=$request->all();
        $o = $this->repository->save($data);
        return response()->json($o);
    }

  
    public function show($id){
        return $this->repository->get($id);
    }

    public function update(Request $request, $id){
        $data=$request->all();
        return $o;
    }


}
