<?php

namespace App\Http\Controllers\API;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Repository\UserRepositoryI;
use App\Repository\PersonRepositoryI;
use Auth;

class UserController extends Controller
{
    private $repository;
    private $personrepository;
    public function __construct(UserRepositoryI $repository, PersonRepositoryI $personrepository){
       $this->repository = $repository;
       $this->personrepository = $personrepository;
       //$this->middleware('auth');
   }

   private $menu=[
        /*"collections"=>[
            ['key'=>'collections-list','name'=>'Cobranzas','link'=>'/collections','role'=>'su_admin']
        ],*/
        "liquidations"=>[
            ['key'=>'cobrator-report','name'=>'Reporte por Comisionista','link'=>'/liquidations/commission-agent-liquidation','role'=>'rt_cobra,rt_admin,rt_auxil'],
            ['key'=>'month-cobrator-report','name'=>'Reporte Recaudaciones Mensuales','link'=>'/liquidations/month-liquidation','role'=>'rt_cobra,rt_admin'],
            // ['key'=>'analytical-report','name'=>'Reporte Analitico','link'=>'/liquidations/month-liquidation','role'=>'rt_cobra'],
        ],
        "debts"=>[
            ['key'=>'individual','name'=>'Individuales','link'=>'/debts/individual-debt','role'=>'rt_admin,rt_auxil,rt_cobra'],
            ['key'=>'market','name'=>'Mercados','link'=>'/debts/market-debt','role'=>'rt_auxil,rt_admin,rt_cobra'],
            ['key'=>'coactives','name'=>'Coactivos','link'=>'/debts/coactives','role'=>'rt_auxil,rt_admin'],
            ['key'=>'forgiveness','name'=>"Condonación",'link'=>'/debts/forgiveness','role'=>'rt_admin,rt_auxil'],
            ['key'=>'forgiveness_generic','name'=>'Condonación General','link'=>'/debts/forgiveness-generic','role'=>'rt_admin,rt_auxil'],
            ['key'=>'prescription','name'=>'Prescripción','link'=>'/debts/prescription','role'=>'rt_admin,rt_auxil'],

        ],
        "notifications"=>[
            ['key'=>'notifications-list','name'=>'Notificaciones','link'=>'/notifications/normal','role'=>'rt_admin,rt_auxil'],
        ],
        "f_notifications"=>[
            ['key'=>'f-notifications-list','name'=>'F.Notificaciones','link'=>'/notifications/fra','role'=>'rt_admin,rt_auxil'],
        ],
        "fractionaries"=>[
            ['key'=>'fractionaries-list','name'=>'fractionaries','link'=>'/fractionaries','role'=>'rt_admin,rt_auxil']
        ],
        "directfine"=>[
            ['key'=>'directfine-list','name'=>'fractionaries','link'=>'/direct-fine','role'=>'rt_admin,rt_auxil']
        ],
        "cashbox"=>[
            ['key'=>'openclose','name'=>'Apertura y Cierre','link'=>'/cashbox/opening-closing','role'=>'ca_cashb,rt_cobra,ca_admin,rt_admin'],
            ['key'=>'payment','name'=>'Pagos','link'=>'/cashbox/transaction','role'=>'ca_cashb,ca_admin,rt_cobra']
        ],
        "reports"=>[
            ['key'=>'detailed_pay','name'=>'Pagos Detallados','link'=>'/report/detailed-transaction','role'=>'ca_cashb,ca_admin, rt_admin, rt_auxil,rt_cobra'],
            ['key'=>'fractionay_pay','name'=>'Pagos Fraccionamiento','link'=>'/report/fractionary-pay','role'=>'ca_cashb,ca_admin, rt_admin, rt_auxil,rt_cobra'],
            ['key'=>'extorted-payments','name'=>'Extornados','link'=>'/report/extorted-payments','role'=>'ca_cashb,ca_admin, rt_admin, rt_auxil,rt_cobra'],
        ],
        "setting"=>[
            ['key'=>'admin_market','name'=>'Mercados','link'=>'/admin/markets','role'=>'rt_admin,rt_auxil'],
            ['key'=>'ebts_market_meats','name'=>'Mercados de Carnes','link'=>'/admin/debts-market-meats','role'=>'rt_admin,rt_auxil'],
            ['key'=>'admin_sectors','name'=>'Sectores','link'=>'/admin/sectors','role'=>'rt_admin,rt_auxil'],
            ['key'=>'admin_gyres','name'=>'Giros de Negocio','link'=>'/admin/gyres','role'=>'rt_admin,rt_auxil'],
            ['key'=>'admin_rates','name'=>'Tasa','link'=>'/admin/rates','role'=>'rt_admin,rt_auxil'],
            ['key'=>'admin_persons','name'=>'Ciudadanos','link'=>'/admin/persons','role'=>'rt_admin, ti_admin,rt_auxil,rt_cobra'],
            // ['key'=>'admin_titulars','name'=>'Conductores','link'=>'/admin/titulars','role'=>'rt_admin'],
            // ['key'=>'admin_infractors','name'=>'Infractores','link'=>'/admin/infractors','role'=>'rt_admin'],
            ['key'=>'admin_stands','name'=>'Puestos','link'=>'/admin/stands','role'=>'rt_admin,rt_auxil'],
            ['key'=>'admin_reversion','name'=>'Reversion P.','link'=>'/admin/reversion','role'=>'rt_admin'],
            ['key'=>'admin_locals','name'=>'Locales','link'=>'/admin/locals','role'=>'rt_admin,rt_auxil,rt_cobra'],
            ['key'=>'admin_fractionary_types','name'=>'T. Fraccionamiento','link'=>'/admin/fractionary-types','role'=>'rt_admin, rt_auxil'],
        // ['key'=>'admin_cabrators','name'=>'Comisionistas','link'=>'/admin/agents','role'=>'rt_admin'],
            ['key'=>'admin_users','name'=>'Usuarios','link'=>'/admin/users','role'=>'ti_admin,rt_admin,ca_admin,rt_cobra,rt_auxil,rt_auxil'],
            ['key'=>'admin_uits','name'=>'UIT','link'=>'/admin/uits','role'=>'ti_admin'],
            ['key'=>'admin_conpcepts','name'=>'Conceptos','link'=>'/admin/concepts','role'=>'ti_admin,ca_cashb,ca_admin'],
            ['key'=>'admin_setting','name'=>'Configuraciones','link'=>'/admin/setting','role'=>'ti_admin,rt_auxil,rt_admin'],
            ['key'=>'user_change_password','name'=>'Cambia Contraseña','link'=>'/admin/change-password','role'=>'ti_admin,rt_auxil,rt_admin'],
        ] 
    ];

    public function menu(){
        return response()->json($this->menu);
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request){

        $params=[];

        $user = \App\Models\User::find($request->query("iam_id"));
        if($user['role_id']==\App\Util::$RT_AUXIL || $user['role_id']==\App\Util::$RT_COBRA || $user['role_id']==\App\Util::$CA_CASHB){
            $params['id']=$user['id'];
        }else{
            $params=['role_name'=>$this->roles_by_role($user->role->name)];
        }

        $params['search']= isset($request->search) ? $request->search :'';
        
        $l = $this->repository->all($params);
        return response()->json($l);
       
    }

    public function can_management_roles($user_id){
        $user = \App\Models\User::find($user_id);
        return response()->json( $this->roles_by_role($user->role->name) );
    }

    public function permissions($user_id){
        $data = $this->repository->listPermissions($user_id);
        return response()->json($data);
    }

    public function savePermissions(Request $request){
        $permissions=$request->all();
        $this->repository->savePermissions($permissions);
        return response()->json(['status'=>true]);
    }

    private function roles_by_role($role_name){
        switch( $role_name ){
            case 'ti_admin': $can_change=['ti_admin','rt_admin','rt_cobra','ca_cashb','ca_admin','rt_auxil']; break;
            case 'rt_admin': $can_change=['rt_admin','tr_aux','rt_auxil','rt_cobra']; break;
            case 'ca_admin': $can_change=['ca_admin','ca_cashb']; break;
            case 'su_admin': $can_change=['su_admin','ti_admin','rt_admin','rt_cobra','ca_cashb','ca_admin','rt_auxil']; break;
            //solo tienen acceso a su propio usuario
            case 'rt_cobra': $can_change=['rt_cobra']; break;
            case 'ca_cashb': $can_change=['ca_cashb']; break;
            case 'rt_auxil': $can_change=['rt_auxil']; break;
        }
        return $can_change;
    }

    public function allCommissionAgent(Request $request) {
        $params=[];
        if($request->query('roles_id')) $params['roles_id']=$request->query('roles_id');
        $l = $this->repository->allCommissionAgent($params);
        return response()->json($l);
    }
    public function cashiersAndCommissionAgent(Request $request){
        $params=[];
        $l = $this->repository->cashiersAndCommissionAgent($params);
        return response()->json($l);
    }
    public function list_roles(){
        $l = $this->repository->all_roles([]);
        return response()->json($l);
    }

    public function list_access(Request $request){
        $role_id = $request->query("role_id");
        $user_id = $request->query("user_id");
        /*$role = $this->repository->get_role($role_id);
        $role_name = $role->name;
        $permissions= $this->repository->listPermission();
        $final_menu=[];
        foreach($this->menu as $k => $submenu){
            foreach($submenu as $item){
                if( preg_match("/$role_name/",$item['role'])|| $role_name=='su_admin' ){
                    $final_menu[$k][]=$item;
                }
            }
        }*/
        $permissions = $this->repository->listPermissions($user_id)->toArray();
      
        //$menu_permissions = [];
        //if(count($permissions)>0) $menu_permissions= array_map(function($e){ return $e->menu; }, $permissions);
        $SU_ADMIN_ROLE = 1;
        $final_menu=[];
        foreach($this->menu as $k => $submenu){
            foreach($submenu as $item){
                if($role_id==$SU_ADMIN_ROLE && count($permissions)==0 ){ $permissions[]=(object)["menu"=>'.',"mode"=>'W']; }
                $perms = array_filter($permissions,function($e) use($item, $role_id, $SU_ADMIN_ROLE){ return $e->menu == $item['link'] || $role_id==$SU_ADMIN_ROLE ; });
                if(count($perms)>0){
                    $perms=array_values($perms);
                    $menu_ = $item;
                    $menu_['mode'] = $role_id==$SU_ADMIN_ROLE?'W': $perms[0]->mode;
                    if( property_exists($perms[0], 'markets') ) $menu_['markets'] = $perms[0]->markets;
                    $final_menu[$k][]=$menu_;
                }
               
                /*if( in_array($item['link'],$menu_permissions) || $role_id==$SU_ADMIN_ROLE){
                    
                    
                }*/
            }
        }
        
        return $final_menu;
    }

 
    public function store(Request $request){
        $people_id = $request['people_id'];
        $data=$request->all();
        $data['remember_token']=str_random(60);
        $o = $this->repository->save($data);
        $params_person['id']=$people_id;
        $params_person['user_id']=$o->id;
       $this->personrepository->save($params_person);
        return $o;
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id){
        return $this->repository->get($id);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $data=$request->all();
        $data['id'] = $id;
        //lo que se esta quitando no pertenece a los campos de user
        unset($data['user_name']);
        unset($data['user_lastname']);
        unset($data['user_dni']);
        $o = $this->repository->save($data);
        return $o;
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        return $this->repository->delete($id);
    }

    public function validateCode(Request $request,$id,$code) {
        $params=["id"=>$id,"code"=>$code];
        $result = $this->repository->validateCode($params);
        return response()->json($result);   
    }
    public function changePasswordUser(){
    
     $data = [
            'titulo' => 'Cambiar Contraseña',
            // Otros datos que desees pasar a la vista
        ];

     return view('user-change-password');
    }
    public function saveChangePasswordUser(Request $request){
        $params = [];
        if($request->input('id')) $params['id'] = $request->input('id');
        if($request->input('currentpassword')) $params['currentpassword'] = $request->input('currentpassword');
        if($request->input('newpassword')) $params['newpassword'] = $request->input('newpassword');
        $result = $this->repository->saveChangePassword($params);
        return response()->json($result); 
    }
}
