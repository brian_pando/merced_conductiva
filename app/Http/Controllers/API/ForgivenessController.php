<?php

namespace App\Http\Controllers\API;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Repository\ForgivenessRepositoryI;
use App\Repository\FileRepositoryI;

class ForgivenessController extends Controller
{
    private $repository;
    private $fileRepository;
    public function __construct(ForgivenessRepositoryI $repository,FileRepositoryI $fileRepository){
        $this->repository = $repository;
        $this->fileRepository = $fileRepository;
     }

     public function index(Request $request){
        $params = [];
        if($request->query('search')) $params['search']=$request->query('search');
        if($request->query('type')) $params['forgivenesses.type']=$request->query('type');
        // $params = $request->query();
        $l = $this->repository->all($params);
        return response()->json($l);
    }

    public function store(Request $request){
        $data=$request->all();
        $files = \App\Util::separate_files_into($data);
        $o = $this->repository->save($data);
        if(! is_null($files)) $this->fileRepository->check_and_save($files,['type'=>'forgiveness','id'=>$o->id]);
        return response()->json($o);
    }

  
    public function show($id){
        return $this->repository->get($id);
    }

    public function update(Request $request, $id){
        $data=$request->all();
        $data['id'] = $id;
        $files = \App\Util::separate_files_into($data);
        $o = $this->repository->save($data);
        if(! is_null($files)) $this->fileRepository->check_and_save($files,['type'=>'forgiveness','id'=>$o->id]);
        return response()->json($o);
        
    }

    public function destroy($id){
        return $this->repository->delete($id);
    }
}
