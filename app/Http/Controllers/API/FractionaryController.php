<?php

namespace App\Http\Controllers\API;
use App\Util;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Repository\FractionaryRepositoryI;
use App\Repository\SettingRepositoryI;
use App\Repository\CompanyRepositoryI;
use App\Repository\StandRepositoryI;
use App\Repository\FileRepositoryI;
use Maatwebsite\Excel\Facades\Excel;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\Log;

class FractionaryController extends Controller
{
    private $repository;
    private $settingRepository;
    private $repository_company;
    private $fileRepository;
    private $repository_stand;

    private $merced_sisa_kioskos_mandatory=['date_at','concept_id','titular_id','representative_id','stand_id','reference_id','reference_description','date_start','interest','quotes','debt','initial_voucher','initial_amount','fractionary_amount','user_id','fractionary_type_id'];
    private $others_mandatory=             ['date_at','concept_id','titular_id','representative_id','reference_description','date_start','interest','quotes','debt','initial_voucher','initial_amount','fractionary_amount','user_id','fractionary_type_id'];
        
    public function __construct(FractionaryRepositoryI $repository,SettingRepositoryI $settingRepository,CompanyRepositoryI $repository_company,FileRepositoryI $fileRepository,StandRepositoryI $repository_stand){
       $this->repository = $repository;
       $this->settingRepository= $settingRepository;
       $this->fileRepository = $fileRepository;
       $this->repository_company=$repository_company;
       $this->repository_stand=$repository_stand;
    }

    public function index(Request $request){
        //$params = $request->query;
        $params=[];
        if($request->query('search')) $params['search']=$request->query('search');
        if($request->query('expired_debts')) $params['expired_debts']=$request->query('expired_debts');
        if($request->query('coactive_debts')) $params['coactive_debts']=$request->query('coactive_debts');
        if(is_numeric($request->query('status'))) $params['status']=$request->query('status');
        if($request->query('fractionary_type_id')) $params['fractionary_type_id']=$request->query('fractionary_type_id');
        if($request->query('concept_id')) $params['concept_id']=$request->query('concept_id');
        if($request->query('date_init')) $params['date_init']=$request->query('date_init');
        if($request->query('date_end')) $params['date_end']=$request->query('date_end');

        $tiempo_inicio = microtime(true);

        $l = $this->repository->all_v2($params); 

        $tiempo_fin = microtime(true);
        $tiempo_ejecucion = $tiempo_fin - $tiempo_inicio;
         Log::info("TIEMPO:".  $tiempo_ejecucion );

        return response()->json($l);
    }

    public function store(Request $request){
        
        try {
            $data=$request->all();
            $data['user_id']=$data['user_id'];
            $files = \App\Util::separate_files_into($data);
            $mandatory = preg_match("/^[1|2|3]$/",$data['fractionary_type_id']) ?$this->merced_sisa_kioskos_mandatory:$this->others_mandatory;
            \App\Util::check_mandatory_fields($data,$mandatory);
           // dd("data",$data);
            $o = $this->repository->save($data);
            if(! is_null($files)) $this->fileRepository->check_and_save($files,['type'=>'fractionary','id'=>$o->id]);
            return $o;
        } catch (\Exception $e) {
           throw $e;
        }
        
    }

    public function show($id){
        return $this->repository->get($id);
    }

    public function update(Request $request, $id){
        $data=$request->all(); 
        $data['id'] = $id;
        $files = \App\Util::separate_files_into($data);
        $o = $this->repository->save($data);
        if(! is_null($files)) $this->fileRepository->check_and_save($files,['type'=>'fractionary','id'=>$o->id]);
        return $o;
    }

    public function activateDeactivate($id){
        $o=$this->repository->activateDeactivate($id);
        return $o;
    }

    public function getFractionaryScheduleByFractionary($id) {
        return $this->repository->getFractionaryScheduleByFractionary($id);
    }

    public function listSchedule($fractionary_id){
        return $this->repository->listSchedule($fractionary_id);
    }
    public function forgiveDebts(Request $request){
        $this->repository->forgiveDebts([$request['fractionaries_id'],$request['porcent']]);
        if(! empty($request['files'])){
            foreach ($request['fractionaries_id'] as $fractionary_id) {
                $this->fileRepository->check_and_save($request['files'],['type'=>'fractionary','id'=>$fractionary_id]);
            }
        }
    }
    public function exportPdf(Request $request){
        $params=[];
        if($request['search']) $params['search']=$request['search'];
        if($request['expired_debts']) $params['expired_debts']=$request['expired_debts'];
        if($request['coactive_debts']) $params['coactive_debts']=$request['coactive_debts'];
        if(is_numeric($request['status'])) $params['status']=$request['status'];
        if($request['fractionary_type_id']) $params['fractionary_type_id']=$request['fractionary_type_id'];
        if($request['concept_id']) $params['concept_id']=$request['concept_id'];
        if($request['date_init']) $params['date_init']=$request['date_init'];
        if($request['date_end']) $params['date_end']=$request['date_end'];
        $fractionary_type_id=$params['fractionary_type_id'];

        $params['export']='PDF';
        $params['no_paginate']= true;
        $listfractionaries = $this->repository->all_v2($params);


        $report_date = '-';
        if(isset($params['date_init']) and isset($params['date_end'])) {
            $date_init = date_format(date_create($params['date_init']),"d/m/Y");
            $date_end = date_format(date_create($params['date_end']),"d/m/Y");
            $report_date = $date_init ." - ". $date_end;
        }
        $name_file_excel = 'reporte_fraccionamiento.pdf';
        $u=\App\Models\User::find( \Auth::user()->id );//user logeado
        $company=$this->repository_company->find(\App\Util::$COMPANY_ID); //datos de la muni
        $types_fractionaries=\App\Util::$types_fractionaries;
        Excel::store(new \App\Exports\FractionaryPdfExport(['company'=>$company,'user_logged_in'=>$u,
        'listfractionaries'=>$listfractionaries,'types_fractionaries'=>$types_fractionaries
        ,'fractionary_type_id'=>$fractionary_type_id,'report_date'=>$report_date]),'public/files/'.$name_file_excel, 'local', \Maatwebsite\Excel\Excel::DOMPDF);
        return Storage::url("/public/files/".$name_file_excel);
    }
    public function exportExcel(Request $request){
        $params=[];
       if($request['search']) $params['search']=$request['search'];
       if($request['expired_debts']) $params['expired_debts']=$request['expired_debts'];
        if($request['coactive_debts']) $params['coactive_debts']=$request['coactive_debts'];
        if(is_numeric($request['status'])) $params['status']=$request['status'];
        if($request['fractionary_type_id']) $params['fractionary_type_id']=$request['fractionary_type_id'];
        if($request['concept_id']) $params['concept_id']=$request['concept_id'];
        if($request['date_init']) $params['date_init']=$request['date_init'];
        if($request['date_end']) $params['date_end']=$request['date_end'];
        $fractionary_type_id=$params['fractionary_type_id'];

        $params['export']='excel';
        $params['no_paginate']= true;

        $listfractionaries = $this->repository->all_v2($params); 
        $report_date = '-';
        if(isset($params['date_init']) and isset($params['date_end'])) {
            $date_init = date_format(date_create($params['date_init']),"d/m/Y");
            $date_end = date_format(date_create($params['date_end']),"d/m/Y");
            $report_date = $date_init ." - ". $date_end;
        }
        $name_file_excel = 'reporte_fraccionamiento.xlsx';
        $u=\App\Models\User::find( \Auth::user()->id );//user logeado
        $types_fractionaries=\App\Util::$types_fractionaries;
        $company=$this->repository_company->find(\App\Util::$COMPANY_ID); //datos de la muni
        
        Excel::store(new \App\Exports\FractionaryExcelExport(['company'=>$company,'user_logged_in'=>$u,
        'listfractionaries'=>$listfractionaries,'types_fractionaries'=>$types_fractionaries
        ,'fractionary_type_id'=>$fractionary_type_id,'report_date'=>$report_date]),'public/files/'.$name_file_excel, 'local', \Maatwebsite\Excel\Excel::XLSX);
        return Storage::url("/public/files/".$name_file_excel);
    }

    public function pdf(Request $request,$id) {
        $fractionary = $this->repository->all(['id'=>$id])[0];
        $stand_code=null;
        if($fractionary->stand_id){
            $stand=$this->repository_stand->get($fractionary->stand_id);
            $stand_code=$stand->code;
            if($stand->name){
                $stand_code=$stand->name;
            }
        }
        //dd($fractionary);
        $setting = $this->settingRepository->all([]);
        $sum_total=0; $sum_merced=0; $sum_multa=0;
        $schedule=$this->repository->schedule(['fractionary_id'=>$id]);
        $company=$this->repository_company->find(\App\Util::$COMPANY_ID); //datos de la muni
        $username=\App\Models\User::find( \Auth::user()->id );//user logeado
        //$type = $this->fractionaryTypeRepository->find($fractionary->fractionary_type_id);
        if($fractionary->fractionary_type_id==\App\Util::$FRACTIONARY_TYPE_PIT){
            $name_representante='INFRACTOR';
            $is_type_fra_transito=true;
        }else{
            $name_representante='CONTRATANTE';
            $is_type_fra_transito=false;}
            
        if($fractionary->fractionary_type_id==\App\Util::$FRACTIONARY_TYPE_TER || 
        $fractionary->fractionary_type_id==\App\Util::$FRACTIONARY_TYPE_NCH ||
        $fractionary->fractionary_type_id==\App\Util::$FRACTIONARY_TYPE_MAU|| 
        $fractionary->fractionary_type_id==\App\Util::$FRACTIONARY_TYPE_SAD ){
            $is_not_contratante=false;
        }else{
            $is_not_contratante=true;
        }

        $is_venta_terreno=false;
        if($fractionary->fractionary_type_id==\App\Util::$FRACTIONARY_TYPE_TER || $fractionary->fractionary_type_id==\App\Util::$FRACTIONARY_TYPE_NCH){
            $is_venta_terreno=$fractionary->fractionary_type_id==\App\Util::$FRACTIONARY_TYPE_TER ? "INFORME":"UBICACION";
        }else if($fractionary->fractionary_type_id==\App\Util::$FRACTIONARY_TYPE_SAD){
            $is_sancionadministrativa=true;
        }else{$is_venta_terreno='';}
        $data=[
           // "header_logo"=>str_replace('/./',"/",$setting[3]->value),
            "header_logo"=> Util::loadImageInBase64(public_path("storage/public/setting_4.jpg") ),
            'f'=>$fractionary,
           // 'apply_date_visible'=>false,//depende del TF
            //'stand_visible'=>false,//depende del TF
            'schedule'=>$schedule,
            'footer' => $setting[5]->value,
            'name_representante'=>$name_representante,
            //"header_photo"=>str_replace('/./',"/",$setting[7]->value),
            "header_photo"=> Util::loadImageInBase64(public_path("storage/public/setting_8.jpg")),
            "name_muni"=>$company->name,
            "username"=>$username->username,
            "is_not_contratante"=>$is_not_contratante,
            "ruc_muni"=>$company->ruc,
            "address_muni"=>$company->address,
            "stand_code"=>$stand_code,
            "is_venta_terreno"=>$is_venta_terreno,
            "is_sancionadministrativa"=>isset($is_sancionadministrativa),
            "is_type_fra_transito"=>$is_type_fra_transito,
        ];
      //  $url_header_logo=$setting[3]->name_photo;
       // $url_header_photo=$setting[7]->name_photo;
      //  $data['header_logo']= public_path("storage/public/$url_header_logo.jpg");
       // $data['header_photo']= public_path("storage/public/$url_header_photo.jpg");
        $pdf = \PDF::setOptions(['isPhpEnabled'=>true])->loadView('fractionary.document', $data);
       // return $pdf->download("fraccionamiento.pdf");   
       return $pdf->stream("fraccionamiento.pdf");
    }
}
/*
{
        "date_at": "2023-11-13T22:04:29.043Z",
        "fractionary_type_id": 4,
        "schedule": [],
        "files": [],
        "is_titular_signature": true,
        "is_representative_signature": false,
        "is_select_titular_id2": false,
        "interest": "0.0072",
        "titular_id": 4259,
        "representative_id": 4259,
        "reference_id": "",
        "reference_description": "",
        "debt": "8000",
        "initial_amount": 1200,
        "fractionary_amount": 6800,
        "quotes": "10",
        "total_interest": "269.28",
        "total": "7069.28",
        "titular2_id": "",
        "description2": "cccscs",
        "concept_id": 5,
        "description": "scscscsc",
        "description3": "soat",
        "date_start": "2023-11-12T22:05:42.015Z",
        "comment": "xxxxaaaaaasssss",
        "initial_voucher": "4632155",
        "user_id": "1"
}*/
