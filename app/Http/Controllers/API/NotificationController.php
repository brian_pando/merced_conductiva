<?php

namespace App\Http\Controllers\API;
use App\Util;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Repository\NotificationRepositoryI;
use App\Repository\SettingRepositoryI;
use App\Repository\CompanyRepositoryI;
use App\Repository\StandRepositoryI;
use Illuminate\Support\Facades\Storage;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Illuminate\Support\Facades\Log;

use App\Mail\NotificationFractionary;
use Illuminate\Support\Facades\Mail;

class NotificationController extends Controller
{
    private $repository;
    private $settingRepository;
    private $companyRepository;
    public function __construct(NotificationRepositoryI $repository, SettingRepositoryI $settingRepository, CompanyRepositoryI $companyRepository, StandRepositoryI $standRepository){
       $this->repository = $repository;
       $this->settingRepository= $settingRepository;
       $this->companyRepository= $companyRepository;
       $this->standRepository=$standRepository;
    }

    public function index(Request $request){
        $params = [];
        if($request->query('search')) $params['search']=$request->query('search');
        if($request->query('type')) $params['notifications.type']=$request->query('type');
        $l = $this->repository->all($params);
        return response()->json($l);
    }

    public function store(Request $request){
        $data=$request->all();
        $o = $this->repository->save($data);
        return $o;
    }

    public function show($sector){
        return $this->repository->find($sector);
    }

    public function update(Request $request, $id){
        $data=$request->all();
        $data['id'] = $id;
        $o = $this->repository->save($data);
        return $o;
    }

    public function destroy($id){
        //
    }

    public function pdf(Request $request,$id) {
        $notification = $this->repository->all(['notifications.id'=>$id])[0];
        $debts=$notification->debts;
        $setting = $this->settingRepository->all([]);
        $company=$this->companyRepository->find(\App\Util::$COMPANY_ID); //datos de la muni
       // $company=$this->companyRepository->find(1);
        $stand = $this->standRepository->find($notification->stand_id);
        $footer = $setting[6]->value;
        if($stand->market_id == \App\Util::$markets['KIOS_ID'] ) $footer = $setting[8]->value;

        $total = array_reduce($debts->toArray(),function($s,$e){ $s+=$e['total']; return $s;},0);
        $url_firma=$setting[2]->name_photo;
        $data=[
            'signature_name'=>$setting[0]->value,
            'signature_role'=>$setting[1]->value,
            'signature_image'=>Util::loadImageInBase64(public_path("storage/public/$url_firma.jpg")),
            "header_logo"=>str_replace('/./',"/",$setting[3]->value),
            'n'=>$notification,
            'debts'=>$debts,
            "header_photo"=>str_replace('/./',"/",$setting[7]->value),
            'company'=>$company,
            'total' =>$total,
            'stand'=>$stand,
            'footer'=> $footer,
        ];
        $url_header_logo=$setting[3]->name_photo;
        $url_header_photo=$setting[7]->name_photo;
        $data['header_logo']= Util::loadImageInBase64(public_path("storage/public/$url_header_logo.jpg"));
        $data['header_photo']= Util::loadImageInBase64(public_path("storage/public/$url_header_photo.jpg"));

        $factor=28.38;
        $customPaper = array(0,0,20*$factor,14*$factor); //10CM X 20CM 
        $pdf = \PDF::loadView('notification.document', $data)->setPaper($customPaper, 'landscape');
        return $pdf->download("notificacion-".$notification->code.".pdf");   
    }
    public function allPdf(Request $request) {
        //$ids=$request->all();
        $notifications = $this->repository->all(['not_paginate'=>true,'notifications_id'=>$request['notifications_id'],'notifications_stands_id'=>$request['notifications_stands_id']]);
        $notifications = $notifications->toArray();
        $setting = $this->settingRepository->all([]);
        $company=$this->companyRepository->find(\App\Util::$COMPANY_ID); //datos de la muni
        $username=\App\Models\User::find( \Auth::user()->id );//user logeado
       // $u=\App\Models\User::find( \Auth::user()->id );
        for($i=0;$i<count($notifications); $i++){
           
            $debts=$notifications[$i]['debts'];
            $years = array_map(function($e){ return date("Y", strtotime($e['date_at'])); }, $debts);
            $years = array_unique($years);
            sort($years);
            $data_debts=[];
            $totaldebt=0;
            $total_debt = 0;
            foreach($years as $year){
                $total=0;
                $row= ['y'=>$year,'m01'=>0,'m02'=>0,'m03'=>0,'m04'=>0,'m05'=>0,'m06'=>0,'m07'=>0,'m08'=>0,'m09'=>0,'m10'=>0,'m11'=>0,'m12'=>0,'t'=>0];
                $debts_in_year = array_filter($debts, function($e) use($year){ return $year==date("Y", strtotime($e['date_at'])); } );
              
                foreach($debts_in_year as $debt){
                    if($debt['stand_id']==$notifications[$i]['stand_id']){
                        $row['m'.date("m", strtotime($debt['date_at']))]+=number_format($debt['total'],2,'.','');
                        $total+=$debt['total'];
                    }
                }
                $row['t']=$total;
                $total_debt+=$total;
                $data_debts[]=$row;
            }
                
            $notifications[$i]['debts_by_year'] = $data_debts;
            $notifications[$i]['total_debt'] = $total_debt;
        }
        
        //dd($notifications);
        $footer = $setting[6]->value;
        
        //dd( $notifications[$i]['stand'] );
        //if( array_key_exists("stand",$notifications[$i]) && $notifications[$i]['stand']['market']['code'] =='KIOSKO') $footer=$setting[9]->value;

       //return $notifications;
        $data=[
            'signature_name'=>$setting[0]->value,
            'signature_role'=>$setting[1]->value,
            //'signature_image'=>public_path("storage/public/setting_3.jpg"),
            'signature_image'=>Util::loadImageInBase64(public_path("storage/public/setting_3.jpg") ),
            //"header_logo"=>str_replace('/./',"/",$setting[3]->value),
            "header_logo"=>Util::loadImageInBase64(public_path("storage/public/setting_4.jpg")),
            'notifications'=>$notifications,
            'debts'=>$data_debts,
            'company'=>$company,
            'username'=>$username->username,
            'footer' => $footer,
            'footer_kiosko'=> isset($setting[8]->value) ? $setting[8]->value : '',
            'footer_fractionary'=> isset($setting[9]->value) ? $setting[9]->value : '',
            //"header_photo"=>str_replace('/./',"/",$setting[7]->value),
            "header_photo"=>Util::loadImageInBase64(public_path("storage/public/setting_8.jpg")),
            "concepts"=>Util::$concepts,
        ];
       // return $data;
        //$url_header_logo=$setting[3]->name_photo;
       // $url_header_photo=$setting[7]->name_photo;
       // $data['header_logo']= public_path("storage/public/$url_header_logo.jpg");
        //$data['header_photo']= public_path("storage/public/$url_header_photo.jpg");
        //$factor=28.38;
       // $customPaper = array(0,0,20*$factor,14*$factor); //10CM X 20CM 
       //$customPaper = array(0,0,32*$factor,26*$factor); //10CM X 20CM
        $name_file_pdf =  'notificaciones.pdf';
        //$dompdf->setPaper(, 'landscape');
        // dd(storage_path('../public/storage/public/files'));
        //ORIENTATION_LANDSCAPE ORIENTATION_PORTRAIT
        //dd($notifications);
          
        if(sizeof($notifications)==1){
            \PDF::setOptions(['isPhpEnabled'=>true])->loadView('notification.multipleDocuments', $data)->setPaper('A4', 'portrait')->save(storage_path('../public/storage/public/files/') . $name_file_pdf );
        }else{
            \PDF::loadView('notification.multipleDocuments', $data)->setPaper('A4', 'portrait')->save(storage_path('../public/storage/public/files/') . $name_file_pdf );
        }
        
        //->stream('notificaciones.pdf');
        //date("d-m-Y")
        // ->download("notificaciones.pdf");   
        return Storage::url("/public/files/".$name_file_pdf);
    }


     public function sendMail(Request $request){
        $data=$request->all();
        // $to = 'danielchaveztorriel1234@gmail.com'; //destinatario
         $to = $data['email']; 
           
         try {
           Mail::to($to)->send(new NotificationFractionary($data));
            return response()->json(['message' => 'Email sent successfully','status'=>true]);
           } catch (\Exception $e) {
           return response()->json(['message' => 'Error sending email', 'status'=>false,'error' => $e->getMessage()], 500);
          }
        
    }
}

