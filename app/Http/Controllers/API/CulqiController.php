<?php

namespace App\Http\Controllers\API;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class CulqiController extends Controller
{
    private $culqi;

    public function __construct(){
        $this->culqi=[
            "culqiAPI"=>"https://api.culqi.com/v2",
            "privatekey"=>"sk_test_vcarXJNHVY6oYYfb",
        ];
    }

    public function createOrder(Request $request){

        try{
           
            $amount= $request->input("amount");
            $currency_code=$request->input("currency_code");
            $description=$request->input("description");
            $order_number=$request->input("order_number");
            $expiration_date=$request->input("expiration_date");
            $client_details=$request->input("client_details");
            $metadata = $request->input("metadata");
            $confirm=$request->input("confirm");

            $culqiURL=$this->culqi['culqiAPI']."/orders";

           
            $client = new \GuzzleHttp\Client();
            $response = $client->post($culqiURL, [
                'headers'=>['Authorization'=>"Bearer ".$this->culqi['privatekey']],
                'json'=> [
                    'amount' => $amount,
                    'currency_code' => $currency_code,
                    'description'=>$description,
                    'order_number'=>$order_number,
                    'expiration_date'=>$expiration_date,
                    'client_details'=>$client_details,
                    'metadata'=>$metadata,
                    "confirm"=>$confirm
                ]
            ]);

           

                $response = $response->getBody();

            if( !array_key_exists("id",$response) ) 
                    throw new \Exception( json_encode( $response));

            return response()->json($response);
        }catch(\Exception $e){
            return response()->json(['error'=>$e->getMessage()],500);
        }

        

        



    }



}
