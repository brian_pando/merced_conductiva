<?php

namespace App\Http\Controllers\API;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Repository\CobratorTicketRepositoryI;
use App\Repository\RateRepositoryI;
use App\Repository\FileRepositoryI;

class CobratorTicketController extends Controller
{
    private $repository;
    private $fileRepository;
    public function __construct(CobratorTicketRepositoryI $repository,FileRepositoryI $fileRepository){
       $this->repository = $repository;
       $this->fileRepository = $fileRepository;
    }

    public function index(Request $request){
        $params = [];
        if($request->query('search')) $params['search']=$request->query('search');
        if($request->query('name')) $params['name']=$request->query('name');
        //dd($params);
        $l = $this->repository->all($params);
        return response()->json($l);
    }

    public function store(Request $request){
        $data=$request->all();
        $files = \App\Util::separate_files_into($data);
        $o = $this->repository->save($data);
        if(! is_null($files)) $this->fileRepository->check_and_save($files,['type'=>'agent','id'=>$o->id]);
        return $o;
    }
  
    public function show($id){
        return $this->repository->get($id);
    }

    public function update(Request $request, $id){
        $data=$request->all();
        $data['id'] = $id;
        $files = \App\Util::separate_files_into($data);
        $o = $this->repository->save($data);
        if(! is_null($files)) $this->fileRepository->check_and_save($files,['type'=>'stand','id'=>$o->id]);
        return $o;
    }

    public function destroy($id){
        //
    }
}
