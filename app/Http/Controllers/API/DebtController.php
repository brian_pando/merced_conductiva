<?php

namespace App\Http\Controllers\API;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Repository\DebtRepositoryI;
use App\Repository\TransactionRepositoryI;
use App\Repository\CompanyRepositoryI;
use Maatwebsite\Excel\Facades\Excel;
use Illuminate\Support\Facades\Storage;

class DebtController extends Controller
{
    private $repository;
    private $transactionrepository;
    private $repository_company;
    public function __construct(DebtRepositoryI $repository,TransactionRepositoryI $transactionrepository,CompanyRepositoryI $repository_company){
       $this->repository = $repository;
       $this->transactionrepository = $transactionrepository;
       $this->repository_company=$repository_company;
    }

    public function index(Request $request,$type){
        $params = [];
		$params = $this->validateDebtFilters($request, $params);
        $params['no_paginate'] = false;
        if($type) $params['type']=$type;

        $l = $this->repository->all($params);
        return response()->json($l);
    }

    public function getTransaction($id){
        return $this->transactionrepository->getTransactionByDebtID($id);
    }

    public function store(Request $request){
        $data=$request->all();
        $o = $this->repository->save($data);
        return $o;
    }

  
    public function show($id){
        return $this->repository->get($id);
    }

    public function update(Request $request, $id){
        $data=$request->all();
        $data['id'] = $id;
        $o = $this->repository->save($data);
        return $o;
    }

    public function getAccountStatusByTitular($id,$payment_status, Request $request){
		$params = []; 
        $params=["debts.payment_status"=>$payment_status,"debts.client_id"=>$id];
        if(is_numeric($request->query('debts_for_the_cashier'))) $params['debts_for_the_cashier']=$request->query('debts_for_the_cashier');
		$params = $this->validateDebtFilters($request, $params);
        return $this->repository->getDebtTitular($params);
    }
    public function exportPdf(Request $request){
        $params = [];
		$params = $this->validateDebtFilters($request, $params);
        $params['no_paginate'] = true;
        $report_date = '-';
		$report_date = $this->formatReportDate($report_date,$params);

        $list_direct_fine = $this->repository->all($params);

        $u=\App\Models\User::find( \Auth::user()->id );//user logeado 
        $company=$this->repository_company->find(\App\Util::$COMPANY_ID); //datos de la muni
        return (new \App\Exports\DirectFinePdfExport(['company'=>$company,'user_logged'=>$u,'list_direct_fine'=>$list_direct_fine,'report_date'=>$report_date]))->download('reporte_multas_directas.pdf', \Maatwebsite\Excel\Excel::DOMPDF);
    }
    public function exportExcel(Request $request){
        $params = [];
		$params = $this->validateDebtFilters($request, $params);
        $params['no_paginate'] = true;
        $report_date = '-';
		$report_date = $this->formatReportDate($report_date,$params);

        $list_direct_fine = $this->repository->all($params);

        $u=\App\Models\User::find( \Auth::user()->id );//user logeado
        $company=$this->repository_company->find(\App\Util::$COMPANY_ID); //datos de la muni
        return (new \App\Exports\DirectFineExcelExport(['company'=>$company,'user_logged'=>$u,'list_direct_fine'=>$list_direct_fine,'report_date'=>$report_date]))->download('reporte_multas_directas.xlsx', \Maatwebsite\Excel\Excel::XLSX);
    }
	 private function validateDebtFilters($request, $params) {
        if($request->query('search')) $params['search']=$request->query('search');
        if($request->query('status')) $params['status']=$request->query('status');
        if($request->query('type')) $params['debts.type']=$request->query('type');
        if($request->query('concept_id')) $params['concept_id']=$request->query('concept_id');
        if(is_numeric($request->query('payment_status'))) $params['payment_status']=$request->query('payment_status');
        if($request->query('date_init')) $params['date_init']=$request->query('date_init');
        if($request->query("is_coactive")) $params["coactive"]="debts.coactive_id > 0";
        if($request->query("stand_id")) $params["debts.stand_id"]=$request->query("stand_id");
        if($request->query('date_end')) $params['date_end']=$request->query('date_end');
        if($request->query("no_paginate")) $params['no_paginate'] = true;
        if($request->query("concepts_id")) $params["concepts_id"]= explode(",",$request->query("concepts_id"));

        return $params;
    }

	private function formatReportDate($report_date,$params){
		if(isset($params['date_init']) and isset($params['date_end'])) {
            $date_init = date_format(date_create($params['date_init']),"d/m/Y");
            $date_end = date_format(date_create($params['date_end']),"d/m/Y");
            $report_date = $date_init ." - ". $date_end;
        }
		return $report_date;
	}

    public function destroy($id){
        //
    }

    public function multipleStore(Request $request) {
        $data=$request->all();
        $o = null;
        foreach ($data['debts'] as $key => $debt) {
            $o = $this->repository->save($debt);
        }
        return $o;
    }
}
