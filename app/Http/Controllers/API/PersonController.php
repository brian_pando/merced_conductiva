<?php

namespace App\Http\Controllers\API;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Repository\PersonRepositoryI;
use App\Repository\DebtRepositoryI;

class PersonController extends Controller
{
    private $repository;
    private $debtrepository;
    public function __construct(PersonRepositoryI $repository, DebtRepositoryI $debtrepository){
       $this->repository = $repository;
       $this->debtrepository = $debtrepository;
    }

    public function index(Request $request){
        $params = [];
        if($request->query('search')) $params['search']=$request->query('search');
        if($request->query('status')) $params['status']=$request->query('status');
        if(is_numeric($request->query('id'))) $params['id']=$request->query('id');
        // if($request->query('all')) $params['all']=$request->query('all');

        $l = $this->repository->all($params);
        return response()->json($l);
    }

    public function store(Request $request){
        $data=$request->all();
        $o = $this->repository->save($data);
        return $o;
    }

  
    public function show($id){
        return $this->repository->get($id);
    }

    public function update(Request $request, $id){
        $data=$request->all();
        $data['id'] = $id;
        $o = $this->repository->save($data);
        return $o;
    }

    public function destroy($id){
        //
    }
    
    public function verifyDuplicateDNI($dni){
        $params=["dni"=>$dni];
        return $this->repository->verifyDuplicateDNI($params);
    }

    public function getPeopleWithDebts(Request $request){
        $params = []; 
        if($request->query("market_id")) $params['markets.id'] =$request->query("market_id"); 
        if($request->query("sector_id")) $params['sectors.id'] =$request->query("sector_id"); 
        if($request->query("concept_id")) $params['debts.concept_id'] =$request->query("concept_id");
        if(is_numeric($request->query("payment_status"))) $params['debts.payment_status'] =$request->query("payment_status");
        if(is_numeric($request->query("titular_id"))) $params['people.id'] =$request->query("titular_id");
        if($request->query("type")) $params['debts.type']=$request->query("type"); 
        if($request->query("debts_count")) $params['debts_count'] =$request->query("debts_count"); 
        return $this->repository->getPeopleWithDebts($params);
    }
    
    public function listPeopleForViewPayments(Request $request){
        $params = [];
        if($request->query('search')) $params['search']=$request->query('search');
        if($request->query('status')) $params['people.status']=$request->query('status');
        if(is_numeric($request->query('id'))) $params['people.id']=$request->query('id');
        $l = $this->repository->listPeopleForViewPayments($params);
        return response()->json($l);
    }
}
