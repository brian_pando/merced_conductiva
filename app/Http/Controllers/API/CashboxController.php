<?php

namespace App\Http\Controllers\API;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Repository\CashboxRepositoryI;
use App\Repository\CompanyRepositoryI;
use App\Repository\ConceptRepositoryI;

class CashboxController extends Controller
{
    private $repository;
    private $repository_company;
    private $repository_concepts;
    public function __construct(CashboxRepositoryI $repository,CompanyRepositoryI $repository_company,ConceptRepositoryI $repository_concepts){
       $this->repository = $repository;
       $this->repository_company=$repository_company;
       $this->repository_concepts=$repository_concepts;
    }

    public function index(Request $request){
        $params = $this->filters($request);
        $l = $this->repository->all($params);
        return response()->json($l);
    }

    public function store(Request $request){
        $data=$request->all();
        $o = $this->repository->save($data);
        return $o;
    }

    public function show($id){
        return $this->repository->get($id);
    }

    public function update(Request $request, $id){
        $data=$request->all();
        $data['id'] = $id;
        $o = $this->repository->save($data);
        return $o;
    }

    public function exportPdf(Request $request) {
        $params = $this->filters($request);
        $params['no_paginate'] = true;
        $cashboxes = $this->repository->all($params);
        
        $params_concepts['no_paginate']=true;
        $concepts=$this->repository_concepts->all($params_concepts);
        $sum_total=0;
        $u=\App\Models\User::find( \Auth::user()->id );//user logeado
        $company=$this->repository_company->find(\App\Util::$COMPANY_ID); //datos de la muni
        
        foreach($cashboxes as $index=>$cashbox){
            $indice_row=0;
            $indice=0;
            $sum_concepts_total[$index][$indice_row]=date_format(date_create($cashbox['date_init']),"d/m/Y H:i:s");
            $indice_row++;
            $sum_concepts_total[$index][$indice_row]=date_format(date_create($cashbox['date_end']),"d/m/Y H:i:s");
            $indice_row++;
            foreach ( $concepts as $key => $value ){
                if(isset($cashbox['cashboxdetails'][$indice]['concept_id'])){
                    if($value['id']==$cashbox['cashboxdetails'][$indice]['concept_id']){
                        $sum_concepts_total[$index][$indice_row]=$cashbox['cashboxdetails'][$indice]['total'];
                        $indice++;
                        $indice_row++;
                    }
                    else{
                        $sum_concepts_total[$index][$indice_row]=0;
                        $indice_row++;
                    }
                }
                else{
                    $sum_concepts_total[$index][$indice_row]=0;
                    $indice_row++;
                }               
            }
            $sum_concepts_total[$index][$indice_row]= $cashbox['total'];   
            $sum_total=$sum_total+$cashbox['total'];
        }
        return (new \App\Exports\CashboxPdfExport(['company'=>$company,'user_logged_in'=>$u,'data'=>$cashboxes,
        'sum_concepts_total'=>$sum_concepts_total,'concepts'=>$concepts,
        'sum_total'=>$sum_total]))->download('reporte_cajas.pdf', \Maatwebsite\Excel\Excel::DOMPDF);
    }

    public function exportExcel(Request $request) {
        $params = $this->filters($request);
        $params['no_paginate'] = true;
        $cashboxes = $this->repository->all($params);

        $params_concepts['no_paginate']=true;
        $concepts=$this->repository_concepts->all($params_concepts);
        $sum_total=0;
        $u=\App\Models\User::find( \Auth::user()->id );//user logeado
        $company=$this->repository_company->find(\App\Util::$COMPANY_ID); //datos de la muni
        foreach($cashboxes as $index=>$cashbox){
            $indice_row=0;
            $indice=0;
            $sum_concepts_total[$index][$indice_row]=date_format(date_create($cashbox['date_init']),"d/m/Y H:i:s");
            $indice_row++;
            $sum_concepts_total[$index][$indice_row]=date_format(date_create($cashbox['date_end']),"d/m/Y H:i:s");
            $indice_row++;
            foreach ( $concepts as $key => $value ){
                if(isset($cashbox['cashboxdetails'][$indice]['concept_id'])){
                    if($value['id']==$cashbox['cashboxdetails'][$indice]['concept_id']){
                        $sum_concepts_total[$index][$indice_row]=$cashbox['cashboxdetails'][$indice]['total'];
                        $indice++;
                        $indice_row++;
                    }
                    else{
                        $sum_concepts_total[$index][$indice_row]=0;
                        $indice_row++;
                    }
                }
                else{
                    $sum_concepts_total[$index][$indice_row]=0;
                    $indice_row++;
                }               
            }
            $sum_concepts_total[$index][$indice_row]= $cashbox['total'];   
            $sum_total=$sum_total+$cashbox['total'];
        }
        return (new \App\Exports\CashboxExcelExport(['company'=>$company,'user_logged_in'=>$u,'data'=>$cashboxes,
        'sum_concepts_total'=>$sum_concepts_total,'concepts'=>$concepts,
        'sum_total'=>$sum_total]))->download('reporte_cajas.xlsx', \Maatwebsite\Excel\Excel::XLSX);
    }

    private function filters(Request $request) {
        $params = [];
        if($request->query('search')) $params['search']=$request->query('search');
        if($request->query('user_id')) $params['user_id']=$request->query('user_id');
        if($request->query('date_init')) $params['date_init']=$request->query('date_init');
        if($request->query('date_end')) $params['date_end']=$request->query('date_end');
        return $params;
    }
}
