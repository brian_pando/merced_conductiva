<?php 
namespace App\Models;

use Illuminate\Database\Eloquent\Model;
//Generated By PlantUML Command
class TransactionDetail extends Model{
    protected $fillable=["amount",'detail','total','debt_id','transaction_id','status'];
    public function transaction(){ return $this->belongsTo('App\Models\Transaction'); }
    public function debt(){ return $this->belongsTo('App\Models\Debt'); }
// public function paymentconcept(){ return $this->belongsTo('App\Models\PaymentConcept'); }
}