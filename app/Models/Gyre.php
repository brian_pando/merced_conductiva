<?php 
namespace App\Models;

use Illuminate\Database\Eloquent\Model;
//Generated By PlantUML Command
class Gyre extends Model{
    protected $fillable = ['code', 'name','status'];
    public function stands(){ return $this->hasMany('App\Models\Stand'); }
    public function rates(){ return $this->hasMany('App\Models\Rate'); }
}