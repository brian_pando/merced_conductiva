<?php 
namespace App\Models;

use Illuminate\Database\Eloquent\Model;
//Generated By PlantUML Command
class Debt extends Model{
    protected $fillable=["code",'date_at','concept_id','total','payment','stand','status','type','description','ref_code','observation','client_id','user_id','stand_id','payment_date','payment_status','fractionary_id','fractionary_schedule_id','previous_total','previous_client_id','is_last_schedule_debt','coactive_id','code_manual','comment'];
public function user(){ return $this->belongsTo('App\Models\User'); }
public function client(){ return $this->belongsTo('App\Models\Person','client_id'); }
public function schedule(){ return $this->belongsTo('App\Models\FractionarySchedule'); }
public function fractionary(){ return $this->belongsTo('App\Models\Fractionary'); }
public function notifications(){ return $this->belongsToMany('App\Models\Notification'); }
public function reversions(){ return $this->belongsToMany('App\Models\Reversion'); }
public function forgiveness(){ return $this->belongsToMany('App\Models\Forgiveness'); }

}