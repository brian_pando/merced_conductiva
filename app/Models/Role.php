<?php 
namespace App\Models;

use Illuminate\Database\Eloquent\Model;
//Generated By PlantUML Command
class Role extends Model{
    protected $guarded = ['id'];
public function user(){ return $this->hasOne('App\User'); }
}