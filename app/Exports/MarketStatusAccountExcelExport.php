<?php

namespace App\Exports;

use Illuminate\Contracts\View\View;
use Maatwebsite\Excel\Concerns\FromView;
use Maatwebsite\Excel\Concerns\WithTitle;
use Maatwebsite\Excel\Concerns\WithEvents;
use Maatwebsite\Excel\Concerns\RegistersEventListeners;
use Maatwebsite\Excel\Events\AfterSheet;
use Maatwebsite\Excel\Concerns\Exportable;
use Maatwebsite\Excel\Concerns\WithColumnWidths;
use Maatwebsite\Excel\Concerns\ShouldAutoSize;
use Maatwebsite\Excel\Concerns\WithStyles;
use App\GlobalClasses\PHPExcel_Worksheet_PageSetup;
use PhpOffice\PhpSpreadsheet\Worksheet\Worksheet;

class MarketStatusAccountExcelExport implements FromView,WithTitle,WithEvents,ShouldAutoSize,WithStyles
{
    use RegistersEventListeners;
    use Exportable;
    
    function __construct($market) {
        $this->market = $market;
    }
    public function styles(Worksheet $sheet){
        $numberRowsCompanyInformation = 5;
        $headerRow = 1;
        $position_row = count($this->market['marketStatusAccount']['data']) + $numberRowsCompanyInformation + $headerRow;
        //chr un metodo q retorna valores de codigo ASCII(a partir 65 el ABECEDARIO) obteniendo valor columna de excel
        $startASCIIUpperCase = 65;
        $totalColumn = count($this->market['marketStatusAccount']['column']);
        $charASCIIFinal = $totalColumn-1+$startASCIIUpperCase;
        $finalColumn = chr($charASCIIFinal);
        if ($charASCIIFinal > 90) { // 90 = Z
            $totalColumn = $charASCIIFinal - 90;
            $finalColumn = 'A'.chr($totalColumn - 1 + $startASCIIUpperCase);
        }
        $row_range_total="A".$position_row.":".$finalColumn.$position_row;
        return [
            $row_range_total => [
                'borders' => [
                    'top' => [
                        'borderStyle' => \PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THICK,
                        'color' => ['argb' => '00000000'],
                    ]
                ]
            ],
        ];
    }

    public function view(): View
    {   
        return view('markets_bck.export-status-account-excel',['title' => $this->title(),'market'=>$this->market]);
    }

    /**
     * @return string
     */
    public function title(): string
    {
        return 'Mercados';
    }
    public static function afterSheet(AfterSheet $event)
    {
        $event->sheet->getDelegate()->getPageSetup()
            ->setPaperSize(PHPExcel_Worksheet_PageSetup::PAPERSIZE_A4);
    }

}
