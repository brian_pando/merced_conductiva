<?php

namespace App\Exports;

use Illuminate\Contracts\View\View;
use Maatwebsite\Excel\Concerns\FromView;
use Maatwebsite\Excel\Concerns\WithTitle;
use Maatwebsite\Excel\Concerns\WithEvents;
use Maatwebsite\Excel\Concerns\RegistersEventListeners;
use Maatwebsite\Excel\Events\AfterSheet;
use Maatwebsite\Excel\Concerns\Exportable;
use Maatwebsite\Excel\Concerns\WithColumnWidths;
use PhpOffice\PhpSpreadsheet\Style\NumberFormat;
use Maatwebsite\Excel\Concerns\WithColumnFormatting;
use Maatwebsite\Excel\Concerns\ShouldAutoSize;
use App\GlobalClasses\PHPExcel_Worksheet_PageSetup;
use PhpOffice\PhpSpreadsheet\Worksheet\Worksheet;

class DirectFinePdfExport implements FromView,WithTitle,WithEvents, WithColumnFormatting,ShouldAutoSize
{
    use RegistersEventListeners;
    use Exportable;
    
    function __construct($debts_direct_fine) {
        $this->debts_direct_fine = $debts_direct_fine;
    }
    
    public function view(): View
    {   
        return view('debts_bck.export-direct-fine-pdf',['title' => $this->title(),'debts_direct_fine'=>$this->debts_direct_fine]);
    }

    /**
     * @return string
     */
    public function title(): string
    {
        return 'multadirecta';
    }
    public static function afterSheet(AfterSheet $event)
    {
        $event->sheet->getDelegate()->getPageSetup()
            ->setPaperSize(PHPExcel_Worksheet_PageSetup::PAPERSIZE_A4);
    }
    public function columnFormats(): array
    {
        return [
            'G' => NumberFormat::FORMAT_NUMBER_00,
        ];
    }

}
