<?php

namespace App\Exports;

use Illuminate\Contracts\View\View;
use Maatwebsite\Excel\Concerns\FromView;
use Maatwebsite\Excel\Concerns\WithTitle;
use Maatwebsite\Excel\Concerns\ShouldAutoSize;
use Maatwebsite\Excel\Concerns\WithEvents;
use Maatwebsite\Excel\Concerns\RegistersEventListeners;
use Maatwebsite\Excel\Events\AfterSheet;
use Maatwebsite\Excel\Concerns\Exportable;
use App\GlobalClasses\PHPExcel_Worksheet_PageSetup;
use Maatwebsite\Excel\Concerns\WithCustomStartCell;



class MarketStatusAccountByMonthPdfExport implements FromView,WithTitle,ShouldAutoSize,WithEvents
{
    use RegistersEventListeners;
    use Exportable;
    public $market;
    
    function __construct($market) {
        $this->market = $market;
    }
    
    public function view(): View
    {
        return view('markets_bck.export-status-account-by-month-pdf',['title' => $this->title(),'market'=>$this->market]);
    }

    /**
     * @return string
     */
    public function title(): string
    {
        return 'Mercados';
    }
    public function startCell(): string
    {
        return 'A1';
    }
   /*public static function afterSheet(AfterSheet $event)
    {
        $event->sheet->getDelegate()->getPageSetup()
            ->setOrientation(\PhpOffice\PhpSpreadsheet\Worksheet\PageSetup::ORIENTATION_LANDSCAPE)
            ->setPaperSize(\PhpOffice\PhpSpreadsheet\Worksheet\PageSetup::PAPERSIZE_A4);
    }*/
    public static function afterSheet(AfterSheet $event)
    {
        // Configuración del ancho de las columnas
        $columns = range('A', 'Z'); // Puedes ajustar la cantidad de columnas según tus necesidades
        foreach ($columns as $column) {
            $event->sheet->getDelegate()->getColumnDimension($column)->setWidth(5); // Ancho predeterminado de 10
        }

        // Ajusta el ancho específico de la columna 4
        $event->sheet->getDelegate()->getColumnDimension('D')->setWidth(5); // Ajusta el ancho según tus necesidades
        $event->sheet->getDelegate()->getStyle('D')->getAlignment()->setWrapText(true);
         

        // Configuración de la orientación de la página
        $event->sheet->getDelegate()->getPageSetup()
            ->setOrientation(\PhpOffice\PhpSpreadsheet\Worksheet\PageSetup::ORIENTATION_LANDSCAPE)
            ->setPaperSize(\PhpOffice\PhpSpreadsheet\Worksheet\PageSetup::PAPERSIZE_A4);
    }

}
