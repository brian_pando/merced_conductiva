<?php

namespace App\Exports;

use Illuminate\Contracts\View\View;
use Maatwebsite\Excel\Concerns\FromView;
use Maatwebsite\Excel\Concerns\WithTitle;
use Maatwebsite\Excel\Concerns\WithEvents;
use Maatwebsite\Excel\Concerns\RegistersEventListeners;
use Maatwebsite\Excel\Events\AfterSheet;
use Maatwebsite\Excel\Concerns\Exportable;
use Maatwebsite\Excel\Concerns\WithColumnWidths;
use PhpOffice\PhpSpreadsheet\Style\NumberFormat;
use Maatwebsite\Excel\Concerns\WithColumnFormatting;
//use Maatwebsite\Excel\Concerns\ShouldAutoSize;
use App\GlobalClasses\PHPExcel_Worksheet_PageSetup;
use PhpOffice\PhpSpreadsheet\Worksheet\Worksheet;

class TransactionDetailedPaymentsPdfExport implements FromView,WithTitle,WithEvents, WithColumnFormatting
{
    use RegistersEventListeners;
    use Exportable; 
    
    function __construct($transactions) {
        $this->transactions = $transactions;
    }

    
    public function view(): View
    {
        return view('transaction_bck.detailed-payments-export-pdf',['title' => $this->title(),'transactions'=>$this->transactions]);
    }
    
    /**
     * @return string
     */
    public function title(): string
    {
        return 'detailed payments';
    }

    /**
     * @param \Maatwebsite\Excel\Events\AfterSheet $event
     */
    public static function afterSheet(AfterSheet $event)
    {
        //$event->sheet->setBorder('A1', 'solid');
        $event->sheet->getDelegate()->getPageSetup()
            ->setPaperSize(PHPExcel_Worksheet_PageSetup::PAPERSIZE_A4);
    }

    /**
     * @return array
     */
    public function columnFormats(): array
    {
        return [
            'H' => NumberFormat::FORMAT_NUMBER_00,
        ];
    }
}
