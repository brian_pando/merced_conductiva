<?php

namespace App\Exports;

use Illuminate\Contracts\View\View;
use Maatwebsite\Excel\Concerns\FromView;
use Maatwebsite\Excel\Concerns\WithTitle;
use Maatwebsite\Excel\Concerns\WithEvents;
use Maatwebsite\Excel\Concerns\RegistersEventListeners;
use Maatwebsite\Excel\Events\AfterSheet;
use Maatwebsite\Excel\Concerns\Exportable;
use Maatwebsite\Excel\Concerns\WithColumnWidths;
use PhpOffice\PhpSpreadsheet\Style\NumberFormat;
use Maatwebsite\Excel\Concerns\WithColumnFormatting;
use Maatwebsite\Excel\Concerns\ShouldAutoSize;
use App\GlobalClasses\PHPExcel_Worksheet_PageSetup;
use PhpOffice\PhpSpreadsheet\Worksheet\Worksheet;

class FractionaryPdfExport implements FromView,WithTitle,WithEvents, WithColumnFormatting,ShouldAutoSize
{
    use RegistersEventListeners;
    use Exportable;
    
    function __construct($fractionary) {
        $this->fractionary = $fractionary;
    }
    
    public function view(): View
    {   
        return view('fractionary.export-fractionary-pdf',['title' => $this->title(),'fractionary'=>$this->fractionary]);
    }

    /**
     * @return string
     */
    public function title(): string
    {
        return 'fractionary';
    }
    public static function afterSheet(AfterSheet $event)
    {
        $event->sheet->getDelegate()->getPageSetup()
            ->setOrientation(PHPExcel_Worksheet_PageSetup::ORIENTATION_LANDSCAPE)
            ->setPaperSize(PHPExcel_Worksheet_PageSetup::PAPERSIZE_A4);
    }
    public function columnFormats(): array
    {
        return [
            //'G' => NumberFormat::FORMAT_NUMBER_00,
        ];
    }

}
