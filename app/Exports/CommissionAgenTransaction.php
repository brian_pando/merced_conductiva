<?php

namespace App\Exports;

use Maatwebsite\Excel\Concerns\FromArray;
use Maatwebsite\Excel\Concerns\WithStyles;
use PhpOffice\PhpSpreadsheet\Worksheet\Worksheet;
use Maatwebsite\Excel\Concerns\FromCollection;
use Maatwebsite\Excel\Concerns\WithBackgroundColor;
use PhpOffice\PhpSpreadsheet\Style\Fill;

class CommissionAgenTransaction implements FromArray,WithStyles
{
	protected $transactions;
    protected $total;

	public function __construct(array $transactions,$balancetotal)
	{
		$this->transactions = $transactions;
        $this->total=$balancetotal;
       // $this->total= $this->transactions[count($transactions)-1]['total_recaudado'];
        //unset($this->transactions[count($transactions)-1]);
	}

    /**
    * @return \Illuminate\Support\Collection
    */
    public function array(): array
    {    
       //$total= $this->transactions['total_recaudado'];
        return $this->transactions;
    }
    public function styles(Worksheet $sheet)
        {
          $sheet->getStyle('A1:G1')->getFont()->setBold(true);
           $sheet->getStyle('A4:G4')->getFont()->setBold(true);
          $sheet->getStyle('A7:H7')->applyFromArray([
            'font' => [
                'bold' => true,
                        'color' => ['rgb' => 'FFFFFF'], // Color blanco
                    ],
                    'fill' => [
                        'fillType' => \PhpOffice\PhpSpreadsheet\Style\Fill::FILL_SOLID,
                        'startColor' => ['rgb' => '404040'], // Color de fondo #404040
                    ],
                    'alignment' => [
                        'horizontal' => \PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER,
                    ],
                ]);
          $position_row=count($this->transactions)+1;

          $sheet->getStyle('A'.$position_row.':H'.$position_row)->applyFromArray([
            'fill' => [
                'fillType' => \PhpOffice\PhpSpreadsheet\Style\Fill::FILL_SOLID,
                'startColor' => ['rgb' => '255, 0, 0'],
            ],
        ]);
        $sheet->setCellValue('H' .$position_row,$this->total);
     } 

   
}
