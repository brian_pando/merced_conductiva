<?php

namespace App\Exports;

use Illuminate\Contracts\View\View;
use Maatwebsite\Excel\Concerns\FromView;
use Maatwebsite\Excel\Concerns\WithTitle;
use Maatwebsite\Excel\Concerns\ShouldAutoSize;
use Maatwebsite\Excel\Concerns\WithEvents;
use Maatwebsite\Excel\Concerns\RegistersEventListeners;
use Maatwebsite\Excel\Events\AfterSheet;
use Maatwebsite\Excel\Concerns\Exportable;
use PhpOffice\PhpSpreadsheet\Style\NumberFormat;
use Maatwebsite\Excel\Concerns\WithColumnFormatting;
use App\GlobalClasses\PHPExcel_Worksheet_PageSetup;
use Maatwebsite\Excel\Concerns\WithStyles;
use PhpOffice\PhpSpreadsheet\Worksheet\Worksheet;

class TransactionDetailedExcelExport implements FromView,WithTitle,WithEvents, WithColumnFormatting, ShouldAutoSize, WithStyles
{
    use RegistersEventListeners;
    use Exportable;
    
    function __construct($transactions) {
        $this->transactions = $transactions;
    }

    public function styles(Worksheet $sheet)
    {
        $count = 0;
        foreach ($this->transactions['data'] as $transaction) {
            foreach($transaction['transactiondetails'] as $detail) {
                $count++;
            }
        }
        $position_row = $count + 8;
        $row_range_total="A".$position_row.":I".$position_row;
        //$sheet->getStyle($row_range_total)->getBorders()->setBorderStyle('thin','thin','thin','thin');
        // $sheet->getStyle($row_range_total)->getFont()->setBold(true);
        return [
            $row_range_total => [
                'borders' => [
                    'top' => [
                        'borderStyle' => \PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THICK,
                        'color' => ['argb' => '00000000'],
                    ]
                ]
            ],
        ];
    }

    public function view(): View
    {   
        return view('transaction_bck.detailed-export-excel',['title' => $this->title(),'transactions'=>$this->transactions]);
    }

    /**
     * @return string
     */
    public function title(): string
    {
        return 'Pagos';
    }

    /**
     * @param \Maatwebsite\Excel\Events\AfterSheet $event
     */
    public static function afterSheet(AfterSheet $event)
    {
        //$event->sheet->setBorder('A1', 'solid');
        $event->sheet->getDelegate()->getPageSetup()
            ->setOrientation(PHPExcel_Worksheet_PageSetup::ORIENTATION_LANDSCAPE)
            ->setPaperSize(PHPExcel_Worksheet_PageSetup::PAPERSIZE_A4);
    }

    /**
     * @return array
     */
    public function columnFormats(): array
    {
        return [
            'G' => NumberFormat::FORMAT_NUMBER_00,
            'H' => NumberFormat::FORMAT_NUMBER_00,
        ];
    }

}
