<?php

namespace App\Exports;

use Illuminate\Contracts\View\View;
use Maatwebsite\Excel\Concerns\FromView;
use Maatwebsite\Excel\Concerns\WithTitle;
//use Maatwebsite\Excel\Concerns\ShouldAutoSize;
use Maatwebsite\Excel\Concerns\WithEvents;
use Maatwebsite\Excel\Concerns\RegistersEventListeners;
use Maatwebsite\Excel\Events\AfterSheet;
use Maatwebsite\Excel\Concerns\Exportable;
use App\GlobalClasses\PHPExcel_Worksheet_PageSetup;

class MarketStatusAccountByStandsPdfExport implements FromView,WithTitle,WithEvents
{
    use RegistersEventListeners;
    use Exportable;
    
    function __construct($market) {
        $this->market = $market;
    }
    
    public function view(): View
    {   //dd($this->market);
        return view('markets_bck.export-status-account-by-stands-pdf',['title' => $this->title(),'market'=>$this->market]);
    }

    /**
     * @return string
     */
    public function title(): string
    {
        return 'Puestos';
    }

    /**
     * @param \Maatwebsite\Excel\Events\AfterSheet $event
     */
    public static function afterSheet(AfterSheet $event)
    {
        $event->sheet->getDelegate()->getPageSetup()
            ->setOrientation(PHPExcel_Worksheet_PageSetup::ORIENTATION_LANDSCAPE)
            ->setPaperSize(PHPExcel_Worksheet_PageSetup::PAPERSIZE_A4);
    }

}
