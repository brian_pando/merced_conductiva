<?php

namespace App\Exports;

use Illuminate\Contracts\View\View;
use Maatwebsite\Excel\Concerns\FromView;
use Maatwebsite\Excel\Concerns\WithTitle;
use Maatwebsite\Excel\Concerns\ShouldAutoSize;
use Maatwebsite\Excel\Concerns\WithEvents;
use Maatwebsite\Excel\Concerns\RegistersEventListeners;
use App\GlobalClasses\PHPExcel_Worksheet_PageSetup;
use Maatwebsite\Excel\Events\AfterSheet;
use Maatwebsite\Excel\Concerns\Exportable;
use PhpOffice\PhpSpreadsheet\Style\NumberFormat;
use Maatwebsite\Excel\Concerns\WithColumnFormatting;
use Maatwebsite\Excel\Concerns\WithStyles;
use PhpOffice\PhpSpreadsheet\Worksheet\Worksheet;

class CashboxExcelExport implements FromView,WithTitle,ShouldAutoSize,WithEvents,WithColumnFormatting,WithStyles
{
    use RegistersEventListeners;
    use Exportable;
    
    function __construct($cashboxes) {
        $this->cashboxes = $cashboxes;
    }
    public function styles(Worksheet $sheet)
    {
        $count = 0;
        foreach ($this->cashboxes['data'] as $cashbox) { $count++;} 
        $position_row = $count + 7;
        $row_range_total="A".$position_row.":".chr(((count($this->cashboxes['concepts'])+3)-1)+65).$position_row;
        return [
            $row_range_total => [
                'borders' => [
                    'top' => [
                        'borderStyle' => \PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THICK,
                        'color' => ['argb' => '00000000'],
                    ]
                ]
            ],
        ];
    }
    public function view(): View
    {   
        return view('cashbox_bck.export-excel',['title' => $this->title(),'cashboxes'=>$this->cashboxes]);
    }

    /**
     * @return string
     */
    public function title(): string
    {
        return 'Lista de Caja';
    }

      /**
     * @param \Maatwebsite\Excel\Events\AfterSheet $event
     */
    public static function afterSheet(AfterSheet $event)
    {
        $event->sheet->getDelegate()->getPageSetup()
            ->setPaperSize(PHPExcel_Worksheet_PageSetup::PAPERSIZE_A4);     
    }
    public function columnFormats(): array
    {
        return [
            'C' => NumberFormat::FORMAT_NUMBER_00,
            'D' => NumberFormat::FORMAT_NUMBER_00,
            'E' => NumberFormat::FORMAT_NUMBER_00,
            'F' => NumberFormat::FORMAT_NUMBER_00,
            'G' => NumberFormat::FORMAT_NUMBER_00,
            'H' => NumberFormat::FORMAT_NUMBER_00,
            'I' => NumberFormat::FORMAT_NUMBER_00,
            'J' => NumberFormat::FORMAT_NUMBER_00,
            'K' => NumberFormat::FORMAT_NUMBER_00,
            'L' => NumberFormat::FORMAT_NUMBER_00,
            'M' => NumberFormat::FORMAT_NUMBER_00,
        ];
    }
}
