<?php

namespace App\Exports;

use Illuminate\Contracts\View\View;
use Maatwebsite\Excel\Concerns\FromView;
use Maatwebsite\Excel\Concerns\WithTitle;
use Maatwebsite\Excel\Concerns\ShouldAutoSize;
use Maatwebsite\Excel\Concerns\WithEvents;
use Maatwebsite\Excel\Concerns\RegistersEventListeners;
use Maatwebsite\Excel\Events\AfterSheet;
use Maatwebsite\Excel\Concerns\Exportable;
use App\GlobalClasses\PHPExcel_Worksheet_PageSetup;

class MarketStatusAccountPdfExport implements FromView,WithTitle,ShouldAutoSize,WithEvents
{
    use RegistersEventListeners;
    use Exportable;
    public $market;
    
    function __construct($market) {
        $this->market = $market;
    }
    
    public function view(): View
    {
        return view('markets_bck.export-status-account-pdf',['title' => $this->title(),'market'=>$this->market]);
    }

    /**
     * @return string
     */
    public function title(): string
    {
        return 'Mercados';
    }
    public static function afterSheet(AfterSheet $event)
    {
        $event->sheet->getDelegate()->getPageSetup()
            ->setOrientation(PHPExcel_Worksheet_PageSetup::ORIENTATION_LANDSCAPE)
            ->setPaperSize(PHPExcel_Worksheet_PageSetup::PAPERSIZE_A4);
    }

}
