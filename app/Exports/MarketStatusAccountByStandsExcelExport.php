<?php

namespace App\Exports;

use Illuminate\Contracts\View\View;
use Maatwebsite\Excel\Concerns\FromView;
use Maatwebsite\Excel\Concerns\WithTitle;
use Maatwebsite\Excel\Concerns\ShouldAutoSize;
use Maatwebsite\Excel\Concerns\WithEvents;
use Maatwebsite\Excel\Concerns\RegistersEventListeners;
use Maatwebsite\Excel\Events\AfterSheet;
use Maatwebsite\Excel\Concerns\Exportable;
use Maatwebsite\Excel\Concerns\WithStyles;
use PhpOffice\PhpSpreadsheet\Worksheet\Worksheet;
use App\GlobalClasses\PHPExcel_Worksheet_PageSetup;
use PhpOffice\PhpSpreadsheet\Cell\Coordinate;

class MarketStatusAccountByStandsExcelExport implements FromView,WithTitle,ShouldAutoSize,WithEvents,WithStyles
{
    use RegistersEventListeners;
    use Exportable;
     private $position_row; // Agregar esta línea
     public function __construct($market)
     {
        $this->market = $market;
        $this->position_row = count($this->market['datos']) + 7; // Inicializar $position_row
    }
    //para pintar linea resaltado en excel (top de los datos totales)
    public function styles(Worksheet $sheet)
    {
        $row_range_total = "A" . $this->position_row . ":" . Coordinate::stringFromColumnIndex(count($this->market['result']) + 4) . $this->position_row;
        
        return [
            $row_range_total => [
                'borders' => [
                    'top' => [
                        'borderStyle' => \PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THICK,
                        'color' => ['argb' => '00000000'],
                    ]
                ]
            ],
        ];
    }
    public function view(): View
    {   
        return view('markets_bck.export-status-account-by-stands-excel',['title' => $this->title(),'market'=>$this->market]);
    }
    /**
     * @return string
     */
    public function title(): string
    {
        return 'Puestos';
    }
    /**
     * @param \Maatwebsite\Excel\Events\AfterSheet $event
     */
    public static function afterSheet(AfterSheet $event)
    {
        $event->sheet->getDelegate()->getPageSetup()
        ->setPaperSize(PHPExcel_Worksheet_PageSetup::PAPERSIZE_A4);
    }
}
