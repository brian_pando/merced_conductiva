<?php

namespace App\Repository;

use App\Repository\UITRepositoryI;
use Illuminate\Support\Facades\Storage;

class UITRepository extends BaseRepository implements UITRepositoryI{
   public function __construct(\App\Models\UIT $model){
       parent::__construct($model);
   }
   public function all($params){
     $conditions=$params;
     $conditions['status']=1;
     return $this->model->where($conditions)->latest()->paginate(10);
   }
 
}