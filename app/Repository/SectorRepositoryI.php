<?php
namespace App\Repository;

interface SectorRepositoryI{
   public function all($params);
   public function save($params);
   public function find($id);
}