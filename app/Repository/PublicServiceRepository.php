<?php

namespace App\Repository;

use App\Repository\PublicServiceRepositoryI;
use Illuminate\Support\Facades\Storage;

class PublicServiceRepository extends BaseRepository implements PublicServiceRepositoryI{
   public function __construct(\App\Models\Publicservice $model){
       parent::__construct($model);
   }
   public function all($conditions){
    //  $conditions['status']=1;
    if(isset($conditions['no_paginate'])) {
      unset($conditions['no_paginate']);
      $q = $this->model->where($conditions)->latest()->get();
    } else {
      $q = $this->model->where($conditions)->latest()->paginate(10);
    }
     return  $q;
   }
 
}