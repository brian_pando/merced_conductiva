<?php

namespace App\Repository;

use App\Models\Market;
use App\Repository\DebtRepositoryI;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;

class DebtRepository extends BaseRepository implements DebtRepositoryI{
	private $stand_model;
	public function __construct(\App\Models\Debt $model,\App\Models\Stand $stand_model){
	   parent::__construct($model);
	   $this->stand_model = $stand_model;
   }


   public function getDebtTitular($params){
	$no_paginate=false;
	//dd($params);
	if(isset($params['no_paginate'])){
		$no_paginate=$params['no_paginate'];
		unset($params['no_paginate']);	
	}
	  if(isset($params['date_init']) and isset($params['date_end'])){
			$date_init = date_format(date_create($params['date_init']),"Y-m-d"); unset($params['date_init']);
			$date_end = date_format(date_create($params['date_end']),"Y-m-d"); unset($params['date_end']);
	 }
	 if(isset($params['coactive'])) {
		$coactive = $params['coactive'];
		unset($params['coactive']);
	 };
	 if(isset($params['debts_for_the_cashier'])) {
		$debts_for_the_cashier = $params['debts_for_the_cashier'];
		unset($params['debts_for_the_cashier']);
	 };
	 if(array_key_exists('search',$params)) {
		$search = $params['search'];
		unset($params['search']);
	 };
	 if(array_key_exists('concepts_id',$params)) {
		$concepts_id = $params['concepts_id'];
		unset($params['concepts_id']);
	 };
	 
	  $conditions=$params;
	  $conditions['debts.status']=1;

	  $q=DB::table('debts')
				->leftjoin('stands', 'stands.id', '=', 'debts.stand_id')
				->leftjoin('sectors', 'sectors.id', '=', 'stands.sector_id')
				->leftjoin('markets', 'markets.id', '=', 'stands.market_id')
				->leftjoin('concepts', 'concepts.id', '=', 'debts.concept_id')
				->leftjoin('coactives', 'coactives.id', '=', 'debts.coactive_id')
				// ->groupBy('concepts.id','concepts.code','concepts.name','markets.id','markets.name','markets.address','markets.code as market_code')
				->selectRaw('concepts.id as concept_id,
				  concepts.code as concept_code,
				  concepts.name as concept_name,
				  stands.code as stand_code,
				  sectors.name as sector_name,
				  sectors.code as sector_code,
				  markets.id as market_id,
				  stands.name as stand_name,
				  markets.name as market_name,
				  markets.address as market_address,
				  markets.code as market_code,
				  debts.date_at as debt_date_at,
				  (debts.total - debts.payment) as debt_total,
				  debts.payment as debt_payment,
				  debts.id as debt_id,
				  fractionary_id, 
				  fractionary_schedule_id, 
				  debts.type, 
				  debts.code, 
				  debts.ref_code,
				  datediff(now(),debts.date_at) as expired_days,
				  debts.coactive_id,
				  coactives.date_init coactive_date_init, 
				  coactives.description_init coactive_description_init, 
				  coactives.date_finish coactive_date_finish, 
				  coactives.description_finish coactive_description_finish, 
				  if(coactives.date_finish is null,false,true) as in_coactive')
				->where($conditions)
				->whereRaw('debts.fractionary_id is null')
				->orderBy('debts.date_at');
				// ->toSql();
		 
		
		if(isset($date_init) and isset($date_end)) {
			$q->whereBetween('debts.date_at', [$date_init." 00:00:00",$date_end." 23:59:00"]);
		}
		if(isset($coactive)){
			$q->whereRaw($coactive);
		};
		if(isset($debts_for_the_cashier)){
			$q->whereColumn('debts.total', '!=', 'debts.payment');
		}
		if(isset($concepts_id)){
			$q->whereRaw("debts.concept_id in (".implode(",",$concepts_id).")"); // Comisionista solo puede Merced, BP; SISA;DERECHO MESA
		};
		if(isset($search)) {
			$q->whereRaw("concat(concepts.code,concepts.name,concepts.number,debts.code, if(stands.code is null, '', stands.code),if(sectors.code is null, '', sectors.code),if(markets.code is null, '', markets.code)  ) like '%$search%'");
			// ->whereRaw('`modules`.`id` = `submodules`.`module_id`')
		}
		//dd($q->toSql());
		if($no_paginate) {
			//dd($q->toSql());
			$l = $q->get();
		}else {
			$l = $q->paginate(10);
		}
		return $l;
/* 		return $q->paginate(10)->toSql(); */
   } 

   // stand controller
   public function getStandByMonth($params){
	$date_init = false; $date_end = false;
	if(isset($params['date_init'])) {
		$date_init = $params['date_init'];
		unset($params['date_init']);
	}
	if(isset($params['date_end'])) {
		$date_end = $params['date_end'];
		unset($params['date_end']);
	}
	$conditions=$params;
	$conditions['debts.status']=1;

	$q = DB::table('debts')->leftJoin('reversions', 'reversions.stand_id', '=','debts.stand_id')
	->groupBy(DB::raw("year(date_at)"), DB::raw("month(date_at)"));


	if($conditions['debts.payment_status'] == 1) {
		$q->selectRaw('debts.id,debts.fractionary_id,debts.previous_total,debts.payment_status,debts.payment,debts.coactive_id,year(debts.date_at) as year, month(debts.date_at) as month, SUM(debts.payment) as total, debts.comment, reversions.id as reversion_id')
			->where($conditions);
			// ->where($conditions)->orWhere('payment','>',0);
	} else {
		$q->selectRaw('debts.id,debts.fractionary_id,debts.previous_total,debts.payment_status,debts.payment,debts.coactive_id,year(debts.date_at) as year, month(debts.date_at) as month, SUM(debts.total - debts.payment) as total, debts.comment,reversions.id as reversion_id')
		->where($conditions);
	}
	/* dd($conditions, $q->toSql());*/
	if($date_init && $date_end) {$q->whereBetween('debts.date_at', [$date_init."-01-01 00:00:00",$date_end."-12-31 23:59:59"]);}
	//return $q->toSql();
	return $q->get();
  }
  // market controller
	public function getMarketByMonth($params){
		$date_init = false; $date_end = false;
		if(isset($params['date_init'])) {
			$date_init = $params['date_init'];
			unset($params['date_init']);
		}
		if(isset($params['date_end'])) {
			$date_end = $params['date_end'];
			unset($params['date_end']);
		}
		$conditions=$params;
		$conditions['debts.status']=1;
		$selectColumns = 'markets.id as market_id,markets.name,debts.id as debts_id,year(debts.date_at) as year, month(debts.date_at) as month';
		if($conditions['debts.payment_status'] == 1) {
			$selectColumns .= ', SUM(debts.payment) as total';
		} else {
			$selectColumns .= ', SUM(debts.total - debts.payment) as total';
		}
		
		$q = DB::table('debts')
			->join('stands','stands.id','=','debts.stand_id')
			->join('markets','markets.id','=','stands.market_id');

		if (isset($params['gyres'])) {
			$selectColumns .= ',sectors.name as sectorname, gyres.name as gyrename';
			$q->leftJoin('sectors','sectors.id','=','stands.sector_id')
				->leftJoin('gyres','gyres.id','=','stands.gyre_id');
			$q->groupBy(['stands.gyre_id','stands.sector_id',DB::raw("year(debts.date_at)"), DB::raw("month(debts.date_at)")]);
		} else if (isset($conditions['stands.market_id']) || isset($params['sectors'])) {
			$selectColumns .= ',sectors.name as sectorname';
			$q->leftJoin('sectors','sectors.id','=','stands.sector_id');
			$q->groupBy(['stands.sector_id',DB::raw("year(debts.date_at)"), DB::raw("month(debts.date_at)")]);
		} else {
			$q->groupBy(['markets.id','markets.name',DB::raw("year(debts.date_at)"), DB::raw("month(debts.date_at)")]);
		}
		if(isset($conditions['sectors'])) {
			$q->whereIn('stands.sector_id', explode(",", $conditions['sectors']));
			unset($conditions['sectors']);
		}
		if(isset($conditions['gyres'])) {
			if ($conditions['gyres']!=0) $q->whereIn('stands.gyre_id', explode(",", $conditions['gyres']));
			unset($conditions['gyres']);
		}

		if($date_init && $date_end) {
			if (strlen($date_init) == 4) { // solo contiene el año
				$q->whereBetween('debts.date_at', [$date_init."-01-01 00:00:00",$date_end."-12-31 23:59:59"]);
			} else {
				$q->whereBetween('debts.date_at', [$date_init." 00:00:00",$date_end." 23:59:59"]);
			}
		}
		$q->where($conditions)->selectRaw($selectColumns);
		// return $q->toSql();
		return $q->get();
	}
	public function getNameYearsOfDebt($params){
		$date_init = false; $date_end = false;
		if(isset($params['date_init'])) {
			$date_init = $params['date_init'];
			unset($params['date_init']);
		}
		if(isset($params['date_end'])) {
			$date_end = $params['date_end'];
			unset($params['date_end']);
		}
		$conditions=$params;
	 	$conditions['debts.status']=1;  
	 	$q = DB::table('debts')
				->join('stands', 'stands.id', '=', 'debts.stand_id')
				->groupBy('year')
				->orderBy('year', 'asc')
				->selectRaw('year(debts.date_at) as year')
				->where($conditions);
				// --->get();
		if($conditions['debts.payment_status']) $q->whereRaw('debts.payment > 0');
		if($date_init && $date_end) {$q->whereBetween('debts.date_at', [$date_init."-01-01 00:00:00",$date_end."-12-31 23:59:59"]);}
		return $q->get(); 

  	}

    ////Market of year/////
	public function allMarketByYear($params){
		$conditions=$params;
		$conditions['debts.status']=1;
		$selectColumns = "markets.id as market_id,markets.name,markets.address,markets.code,year(debts.date_at) as year";
		
		$q = DB::table('debts')
				->join('stands', 'stands.id', '=', 'debts.stand_id')
				->join('markets', 'markets.id', '=', 'stands.market_id')
				->leftJoin('reversions', 'reversions.stand_id', '=','debts.stand_id');


				if ($conditions['debts.payment_status']) {
			   $selectColumns .= ',SUM(if(reversions.id is null,debts.total-debts.payment,0)) as total';
		      } else {
			     $selectColumns .= ',SUM(if(reversions.id is null,debts.total-debts.payment,0)) as total';
		       }

				//->groupBy('markets.id','markets.name','markets.address','markets.code',DB::raw("year(debts.date_at)"))
				// ->selectRaw($selectColumns);
		
		if(isset($conditions['sectors'])) {
			$q->whereIn('stands.sector_id', explode(",", $conditions['sectors']));
			unset($conditions['sectors']);
		}
		if(isset($conditions['gyres'])) {
			if ($conditions['gyres']!=0) $q->whereIn('stands.gyre_id', explode(",", $conditions['gyres']));
			unset($conditions['gyres']);
		}
		if (isset($conditions['date_init']) && $conditions['date_end']) {
			$q->whereBetween(DB::raw('YEAR(debts.date_at)'),[$params['date_init'],$params['date_end']]);
		} else if (isset($conditions['date_init'])) {
			$q->whereRaw('YEAR(debts.date_at) >= '.$params['date_init']);
		} else if (isset($conditions['date_end'])) {
			$q->whereRaw('YEAR(debts.date_at) <= '.$params['date_end']);
		}
		$conditions = array_diff_key($conditions, array_flip(['date_init','date_end']));
		
		if (isset($params['gyres'])) {
			$selectColumns .= ',sectors.name as sectorname, gyres.name as gyrename';
			$q->leftJoin('sectors','sectors.id','=','stands.sector_id')
			  ->leftJoin('gyres','gyres.id','=','stands.gyre_id');
			$q->groupBy(['stands.gyre_id','stands.sector_id',DB::raw("year(debts.date_at)")]);
		} else if (isset($conditions['stands.market_id']) || isset($params['sectors'])) {
			$selectColumns .= ',sectors.name as sectorname';
			$q->leftJoin('sectors','sectors.id','=','stands.sector_id');
			$q->groupBy(['stands.sector_id',DB::raw("year(debts.date_at)")]);
		} else {
			$q->groupBy(['markets.id','markets.name','markets.address','markets.code',DB::raw("year(debts.date_at)")]);
		}
		if($conditions['debts.payment_status']) $q->whereRaw('debts.payment > 0');
		$q->where($conditions)->selectRaw($selectColumns);

		return $q->get();
    }
   
   //////////
   // market controller
    public function allStandsByYear($params){
    	$date_init = false; $date_end = false; $stands_id = null;
    	$isYear=true;//verificar si se esta haciendo el filto por años para agurpar por años
		//dd($params);
    	if(isset($params['date_init'])) {
    		$date_init = $params['date_init'];
    		unset($params['date_init']);
    	}
    	if(isset($params['date_end'])) {
    		$date_end = $params['date_end'];
    		unset($params['date_end']);
    	}
    	if(isset($params['stands_id'])) {
    		$stands_id = explode(",",$params['stands_id']);
    		unset($params['stands_id']);
    	}
    	$conditions=$params;
    	$conditions['debts.status']=1;

	 	if($params['debts.payment_status']== -1) { // -1 = a todos

	 		unset($conditions['debts.status']);
	 		unset($conditions['debts.payment_status']);
	 		unset($conditions['debts.type']);
	 	}
	 	if ($date_init && $date_end && strlen($date_init) ===10) $isYear=false;
	 	

	 	$q = DB::table('debts')
	 	->join('stands', 'stands.id', '=', 'debts.stand_id')
	 	->join('sectors', 'sectors.id', '=', 'stands.sector_id')
	 	->join('people', 'people.id', '=', 'stands.titular_id')
	 	->leftJoin('reversions', 'reversions.stand_id', '=','debts.stand_id')

	 	->groupBy('debts.concept_id','stands.id','stands.code','stands.rate_amount','stands.rate_amount_secondary','sectors.name','people.name',
	 		'people.lastname','people.dni', $isYear ? DB::raw("year(debts.date_at)") : DB::raw("month(debts.date_at)"))

	 	->selectRaw('debts.concept_id,stands.id as stand_id,year(debts.date_at) as year,stands.code as stand_code,stands.rate_amount,stands.rate_amount_secondary,
	 		sectors.name as sector_name,people.name as titular_name, people.lastname as titular_lastname, people.dni as titular_dni,reversions.id as reversion_id')

	 	->where($conditions);

	 	if($params['debts.payment_status']== -1) {
	 		$q->selectRaw('if( debts.payment_status = 0 ,SUM(if(reversions.id is null,debts.total-debts.payment,0)) , SUM(debts.payment) ) as total,month(debts.date_at) as month ');

	 	}else{
	 		$q = $conditions['debts.payment_status'] ? $q->selectRaw('SUM(debts.payment) as total ,month(debts.date_at) as month') : $q->selectRaw('SUM(if(reversions.id is null,debts.total-debts.payment,0)) as total,month(debts.date_at) as month') ;
	 		if($conditions['debts.payment_status']) $q->whereRaw('debts.payment > 0');
	 	}
	 	if($stands_id)$q = $q->whereIn('stands.id',$stands_id);

	 	if ($date_init && $date_end) {
	 		if($isYear){ 
	 			$date_init = $date_init . (strlen($date_init) === 4 ? '-01-01' : '');
	 			$date_end = $date_end . (strlen($date_end) === 4 ? '-12-31' : '');

	 		}
	 		$q->whereBetween('debts.date_at', [$date_init,$date_end]);
	 	}


	 	return $q->get(); 
	 }
//esto
	 public function totalStandsByYear($params){
	 	$date_init = false; $date_end = false; $stands_id = null;
	 	$isYear=true;

	 	if(isset($params['date_init'])) {
	 		$date_init = $params['date_init'];
	 		unset($params['date_init']);
	 	}
	 	if(isset($params['date_end'])) {
	 		$date_end = $params['date_end'];
	 		unset($params['date_end']);
	 	}
	 	if(isset($params['stands_id'])) unset($params['stands_id']);
	 	if ($date_init && $date_end && strlen($date_init) ===10) $isYear=false;
	 	

	 	$conditions=$params;
	 	$conditions['debts.status']=1;  
	 	$q = DB::table('debts')
	 	->join('stands', 'stands.id', '=', 'debts.stand_id')
	 	->join('sectors', 'sectors.id', '=', 'stands.sector_id')
	 	->join('people', 'people.id', '=', 'stands.titular_id')
	 	->leftJoin('reversions', 'reversions.stand_id', '=','debts.stand_id')
	 	->groupBy( $isYear ?    DB::raw('year(debts.date_at)') : DB::raw('month(debts.date_at)'))
	 	->selectRaw('year(debts.date_at) as year');
	 	//->where($conditions);
				// --->get();
	 	

	 	if($params['debts.payment_status']== -1) {
	 		$q->selectRaw('if( debts.payment_status = 0 ,SUM(if(reversions.id is null,debts.total-debts.payment,0)) , SUM(debts.payment) ) as total,month(debts.date_at) as month ');
	 		unset($conditions['debts.payment_status']);
	 	}else{
	 		$q = $conditions['debts.payment_status'] ? 
	 		$q->selectRaw('SUM(debts.payment) as total ,month(debts.date_at) as month') :
	 		 $q->selectRaw('SUM(if(reversions.id is null,debts.total-debts.payment,0)) as total,month(debts.date_at) as month') ;
	 		if($conditions['debts.payment_status']) $q->whereRaw('debts.payment > 0');
	 	}


	 	if ($date_init && $date_end) {
	 		if($isYear){ 
	 			$date_init = $date_init . (strlen($date_init) === 4 ? '-01-01' : '');
	 			$date_end = $date_end . (strlen($date_end) === 4 ? '-12-31' : '');

	 		}
	 		$q->whereBetween('debts.date_at', [$date_init,$date_end]);
	 	}

	 	return $q->where($conditions)->get(); 
	 }
		
   public function all($params){
	$search = null;
	$no_paginate=$params['no_paginate'];
	unset($params['no_paginate']);
	
	if(!empty($params['search'])) {
	  $search = $params['search'];
	  unset($params['search']);
	}

	if(!isset($params['status'])) $params['debts.status']=1; 
	elseif(isset($params['status']) && $params['status'] != -1) {
		$params['debts.status']= $params['status'];
	}
	unset($params['status']);
	
	$q = $this->model->join('people', 'people.id', '=', 'debts.client_id')
		->selectRaw('debts.*,people.id as titular_id, people.name as titular_name, people.lastname as titular_lastname, people.dni as titular_dni');
	
	if($search) {
	 $q->whereRaw("concat(debts.code,if(debts.description is null,'',debts.description),if(people.name is null,'',people.name),if(people.lastname is null,'',people.lastname),if(people.dni is null,'',people.dni)) like '%$search%'");
	}
	if(isset($params['date_init']) and isset($params['date_end'])) {//para la busqueda de fechas
		$q->whereBetween('debts.date_at', [$params['date_init']." 00:00:00", $params['date_end']." 23:00:00"]);
	}
	unset($params['date_init']);
	unset($params['date_end']);
//	dd($params);
	$conditions=$params;
	$q->where($conditions);
	
	if($no_paginate) {
		$l = $q->latest('debts.created_at')->get();
	}else {
		$l = $q->latest('debts.created_at')->paginate(10);
	}
	return $l;

	//return $q->latest('debts.created_at')->paginate(10);    
	// return $q->toSql(); 
   }

   public function save($params){
	  // dd($params);
	if(array_key_exists('id',$params)){
	  $o = $this->model->find($params['id']);
	  if(isset($params['stand_id']) && $params['concept_id'] == \App\Util::$concepts['DDM_ID']){//para regenerar el codigo para mercado de carnes
		$stand = \App\Models\Stand::find($params['stand_id']);
		$concept = \App\Models\Concept::find($params['concept_id']);
		$params['code'] = $this->newCodeFine($stand,$concept,new \DateTime($params['date_at']),$params['code_manual']);
	  }
	  $o->update($params);
	}else{
		if( isset($params['concept_id']) && 
				($params['concept_id'] == \App\Util::$concepts['MTD_ID'] || $params['concept_id'] == \App\Util::$concepts['DDM_ID'])) {
			$stand = \App\Models\Stand::find($params['stand_id']);
			$concept = \App\Models\Concept::find($params['concept_id']);
			$params['code'] = $this->newCodeFine($stand,$concept,new \DateTime($params['date_at']),$params['code_manual']);
		}
		$o = $this->model->create($params);
	}
	return $o;
   }

   public function allByMarket($params){
	 
	$conditions1 = ['stands.market_id'=>$params['market_id'], 'debts.concept_id'=>$params['concept_id'], 'debts.status'=>1];
	if(isset($params['stand_id'])){
		$conditions1['debts.stand_id']=$params['stand_id'];
	}
	if(isset($params['sector_id'])){
		$conditions1['stands.sector_id']=$params['sector_id'];
	}
	if( isset($params['type']) ) $conditions1['type'] = $params['type'];
	 return DB::table('debts')
			 ->join('stands', 'stands.id', '=', 'debts.stand_id')
			->join('people', 'people.id', '=', 'debts.client_id')
			->select('debts.*')
			->where($conditions1) 
			->whereBetween('debts.date_at', [$params['date_init'],$params['date_finish']])
			->orderByDesc('debts.id')
			->get();
  }

  public function createDebtsInMarket($params) {
	$list=[];
	$conditions = ["status"=>1];
	if( array_key_exists("stand_id",$params) ) $conditions['id'] = $params['stand_id'];
	else{
		if( array_key_exists("market_id",$params) ) $conditions['market_id'] = $params['market_id'];
		if( array_key_exists("sector_id",$params) ) $conditions['sector_id'] = $params['sector_id'];
	} 
	
	$init=new \DateTime($params['date_init']);
	$end=new \DateTime($params['date_finish']);
	$interval = $end->diff($init);
	$years = $interval->format("%y");
	$months = $interval->format("%m");
	$size = $years*12+$months + 1 ;// suma 1 para asegurar el ultimo mes. sino sale 11 cuando elijes 31/dic
	
	$stands = \App\Models\Stand::where($conditions)->with("sector")->with('market')->get();

	$stands = $stands->filter(function ($stand) {return $stand->status === 1;}); //crear cuenta solo puestos activos.
	$concept = \App\Models\Concept::find($params['concept_id']);
	\App\Models\Market::find($params['market_id'])->update(['is_debt_daily'=>$params['is_debt_daily']]);

	foreach ($stands as $stand) {
		$date_at= new \DateTime($params['date_init']);
		for ($i=0;$i<$size;$i++) { 
		  	$d=$this->model->create([
				  
			'code'=>$this->newCodeMonthly($stand, $concept, $date_at),
			'concept_id'=>$params['concept_id'],
			"date_at"=> $date_at,
			"type"=>"normal",
			"total"=>  $this->add_total($stand,$params['concept_id'],cal_days_in_month(CAL_GREGORIAN, $date_at->format('n'), $date_at->format('Y')),$params['is_debt_daily']),
			"stand_id"=>$stand->id,
			"client_id"=>$stand->titular_id]);
			$list[]=$d;
			$date_at->modify("+1 month");
		}
	  }
	return array_map(function($e){ return ["id"=>$e->id,"date_at"=>$e->date_at->format("Y-m-d")];},$list);
  }

  private function add_total($stand,$concept_id, $number_days_month, $is_debt_daily){
	  $result = "";
	  if($is_debt_daily) {
		   $result = $concept_id == \App\Util::$concepts['RRS_ID'] ?  floatval($stand->rate_amount_secondary)*$number_days_month : floatval($stand->rate_amount)*$number_days_month;
	  }else {
		$result = $concept_id == \App\Util::$concepts['RRS_ID'] ?  $stand->rate_amount_secondary : $stand->rate_amount;
	  }	
	return $result;
  }
  public function newCodeMonthly($stand,$concept,$date_at,$debt_type=null){
/* dd(\Carbon\Carbon::parse($date_at)->locale('es')->isoFormat('dddd DD \d\e MMM \d\e\l YYYY')); */
	  if(isset($stand['code'])){
		return $concept['code'].($debt_type ?? "")." : ".
		($stand?("Pto:" .$stand['code']. "|".$stand['sector']->code."|".$stand['market']->code."|"):'')
		.strtoupper(\Carbon\Carbon::parse($date_at)->locale('es')->isoFormat('YYYY.MMM'));
	  }else{
		return $concept['code'].($debt_type ?? "")." : ".
		($stand?("Pto:" .$stand['name']."|".$stand->market->code."|"):'')
		.strtoupper(\Carbon\Carbon::parse($date_at)->locale('es')->isoFormat('YYYY.MMM'));
	  }
  }
  private function newCodeFine($stand,$concept,$date_at,$code_manual){
	  if(\App\Util::$concepts['MTD_ID']==$concept['id']){
		return $concept['code']."-".$code_manual ." : ". ( ($stand->code) ? (" Pto:" .$stand->code. "|".$stand->sector->code."|".$stand->market->code."|" ): ($stand->name) ).strtoupper(\Carbon\Carbon::parse($date_at)->locale('es')->isoFormat('YYYY.MMM'));
	  }else{
		return $concept['code']."|".$code_manual ."|Pto:".strtoupper(($stand->code)).':'.strtoupper(\Carbon\Carbon::parse($date_at)->locale('es')->isoFormat('dddd D')).':'.strtoupper(\Carbon\Carbon::parse($date_at)->locale('es')->isoFormat('MMM:YYYY'));
	  }
	
  }
}
