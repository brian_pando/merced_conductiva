<?php

namespace App\Repository;

use App\Repository\CobratorTicketRepositoryI;
use Illuminate\Support\Facades\Storage;

class CobratorTicketRepository extends BaseRepository implements CobratorTicketRepositoryI{
   public function __construct(\App\Models\CobratorTicket $model){
       parent::__construct($model);
   }
   public function all($params){
    $search = null;
    if(!empty($params['search'])) {
      $search = $params['search'];
      unset($params['search']);
    }
    $conditions=$params;
    $conditions['cobrator_tickets.status']=1;
  
    $q = $this->model->join('people', 'people.id', '=', 'cobrator_tickets.responsable_id')
    ->selectRaw('cobrator_tickets.*,people.name, people.lastname,people.dni')
    ->where($conditions);
    if($search) {
      $q->whereRaw("concat(cobrator_tickets.code,people.name,people.lastname,people.dni) like '%$search%'");
    }
    return $q->latest()->paginate(10);
     //return $this->model->where($conditions)->with('responsable')->latest()->paginate(10);    
   }

   public function save($params){
    if(array_key_exists('id',$params)){
      $o = $this->model->find($params['id']);
      $o->update($params);
    }else{
        $o = $this->model->create($params);
    }

    return $o;
   }
 
}