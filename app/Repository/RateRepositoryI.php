<?php
namespace App\Repository;

interface RateRepositoryI{
   public function all($params);
   public function save($params);
}