<?php

namespace App\Repository;

use App\Repository\NotificationRepositoryI;
use Illuminate\Support\Facades\Storage;
use \App\Models\Notification;

class NotificationRepository extends BaseRepository implements NotificationRepositoryI{
   public function __construct(\App\Models\Notification $model){
       parent::__construct($model);
   }
   
  public function all($params){
    $search = null;$not_paginate = null;$notifications_id = null;$notifications_stands_id=null;
    //dd($params);
    if(!empty($params['search'])) { $search = $params['search']; unset($params['search']);}
    if(!empty($params['not_paginate'])) { $not_paginate = $params['not_paginate']; unset($params['not_paginate']);}
    if(!empty($params['notifications_id'])) { $notifications_id = $params['notifications_id']; unset($params['notifications_id']);}
    if(!empty($params['notifications_stands_id'])) { $notifications_stands_id = $params['notifications_stands_id']; unset($params['notifications_stands_id']);}
     $conditions=$params;
     //$conditions['status']=1;

     $q = $this->model->with("concept")->with("to")->with('debts')->with('stand')->with('stand.sector')->with('stand.gyre')->with('stand.market') //se usan al momento de generar los pdf's
     ->join("people","people.id","=","notifications.to_id")//hacer un join para traer la data de personas
     ->leftJoin("stands","stands.id","=","notifications.stand_id")
     ->leftjoin("sectors","sectors.id","=","stands.sector_id")
     ->leftJoin("markets","markets.id","=","stands.market_id")
     ->where($conditions);

     if($notifications_id) $q->whereIn("notifications.id",$notifications_id);//if(stands.code is null, '', stands.code)
     //if($notifications_stands_id) $q->whereIn("notifications.id",$notifications_stands_id);
     if(isset($search)) $q->whereRaw("concat(if(stands.code is null,'',stands.code),if(stands.name is null,'',stands.name),if(markets.code is null,'',markets.code),notifications.code,people.name,if(people.lastname is null,'',people.lastname)) like '%$search%'");
     
     $q->selectRaw("notifications.*, stands.code as stand_code, stands.name as stand_name, sectors.code as sector_code, markets.code as market_code");
     if($not_paginate) $q = $q->orderby("notifications.date_at","asc")->get();
     else{
       //dd($q->toSql());
       $q = $q->latest()->paginate(10);}

     return $q;
   }

   public function save($params){
    $details=null;
    //dd($params);
     if(array_key_exists("datails",$params)){
       $details=$params['datails'];
       unset($params['datails']);
     }
    if(array_key_exists('id',$params)){
      $o = $this->model->find($params['id']);
      $o->update($params);
    }else{
       $params['code'] = $params['type']=='normal'?
                        $this->create_code_by_concept($params['concept_id']):
                        $this->create_code_by_fracctionary();
        $o = $this->model->create($params);
       if( isset($details) ) $o->debts()->sync($details,false);
    }
    return $o;
   }

   private function create_code_by_fracctionary(){
    //$format = "NT-FRA-{number}-{year}-SGRT";
    $year=date("Y");  
    $last_notification=$this->model->where([["type","FRA"],['code','like','NT-FRA-%-'.$year."-SGRT"]])->latest('id')->first();
    $new_code = 'NT-FRA-';
    $new_number = 1;
    if(!is_null($last_notification)){
      $code = explode("-",$last_notification->code); 
      $new_number= (int) $code[2]+1;
    }
    $new_code.=sprintf('%04d', $new_number).'-'.$year."-SGRT";
    return $new_code;
   }

   private function create_code_by_concept($concept_id){
    //$format = "NT-{concept}-{number}-{year}-SGRT";
    $year=date("Y");
    $concept=\App\Models\Concept::find($concept_id);   
    $last_notification=$this->model->where([["concept_id",$concept_id],['code','like',"NT-".$concept->code.'-%-'.$year."-SGRT"]])->latest('id')->first();
    $new_code = "NT-".$concept->code.'-';
    $new_number = 1;
    if(!is_null($last_notification)){
      $code = explode("-",$last_notification->code); 
      $new_number= (int) $code[2]+1;
    }
    $new_code.=sprintf('%04d', $new_number).'-'.$year."-SGRT";
    return $new_code;
  }

}