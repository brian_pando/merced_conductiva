<?php
namespace App\Repository;

interface FractionarytypeRepositoryI{
   public function all($params);
   public function save($params);
}