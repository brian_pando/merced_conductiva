<?php

namespace App\Repository;

use App\Repository\SectortRepositoryI;
use Illuminate\Support\Facades\Storage;
use \App\Models\Sector;

class SectorRepository extends BaseRepository implements SectorRepositoryI{
   public function __construct(\App\Models\Sector $model){
       parent::__construct($model);
   }
   
  public function all($params){
    $search = null;
    $not_paginate = false;
    if(!empty($params['search'])) {
      $search = $params['search'];
      unset($params['search']);
    }
    if(!empty($params['not_paginate'])) {
      $not_paginate = $params['not_paginate'];
      unset($params['not_paginate']);
    }

     $conditions=$params;
     $conditions['status']=1;

     $q = $this->model->where($conditions);
     if($search) {
       // id sirve para buscar los giros en el componente selectSectors
      $q->whereRaw("concat(id,code,name) like '%$search%'");
     }

     return $not_paginate ? $q->get() : $q->paginate(10);
   }

   public function all_by_market($params){
   $l= $this->model
    ->join("stands",'stands.sector_id',"=","sectors.id")
    ->join("markets",'stands.market_id',"=","markets.id")
    ->groupBy("sectors.id")
    ->where( ['market_id'=>$params['market_id'] ] )
    ->selectRaw("sectors.*")->get();
    return $l;
   }

   public function save($params){
    if(array_key_exists('id',$params)){
      $o = $this->model->find($params['id']);
      $o->update($params);
    }else{
        $o = $this->model->create($params);
    }

    return $o;
   }

}
