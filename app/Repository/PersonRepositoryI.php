<?php
namespace App\Repository;

interface PersonRepositoryI{
   public function all($params);
   public function save($params);
}