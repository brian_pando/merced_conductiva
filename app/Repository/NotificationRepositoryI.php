<?php
namespace App\Repository;

interface NotificationRepositoryI{
   public function all($params);
   public function save($params);
   public function find($id);
}