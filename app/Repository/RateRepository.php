<?php

namespace App\Repository;

use App\Repository\RatetRepositoryI;
use Illuminate\Support\Facades\Storage;

class RateRepository extends BaseRepository implements RateRepositoryI{
   public function __construct(\App\Models\Rate $model){
       parent::__construct($model);
   }
   public function all($params){
    $search = null;
    if(!empty($params['search'])) {
      $search = $params['search'];
      unset($params['search']);
    }
    $xpage = $params["xpage"];
    unset($params["xpage"]);

     $conditions=$params;
     $conditions['rates.status']=1;

     $q = $this->model->join('markets', 'markets.id', '=', 'rates.market_id')
        ->join('gyres', 'gyres.id', '=', 'rates.gyre_id')
        ->selectRaw('rates.*')
        ->where($conditions);
     if($search) {
       $q->whereRaw("concat(markets.name,gyres.name) like '%$search%'");
     }
     
     return $q->with('market')->with('gyre')->latest()->paginate( $xpage);    
   }

   public function save($params){
    if(array_key_exists('id',$params)){
      $o = $this->model->find($params['id']);
      $o->update($params);
    }else{
        $o = $this->model->create($params);
    }

    return $o;
   }
 
}