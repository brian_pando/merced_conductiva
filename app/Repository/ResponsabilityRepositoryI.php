<?php
namespace App\Repository;

interface ResponsabilityRepositoryI{
   public function all($params);
   public function save($params);
}
