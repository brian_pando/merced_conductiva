<?php
namespace App\Repository;

interface MarketRepositoryI{
   public function all($params);
   public function save($params);
}