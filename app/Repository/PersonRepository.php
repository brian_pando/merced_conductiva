<?php

namespace App\Repository;

use App\Repository\PersonRepositoryI;
use Illuminate\Support\Facades\Storage;
use App\Util;

class PersonRepository extends BaseRepository implements PersonRepositoryI{
 public function __construct(\App\Models\Person $model){
   parent::__construct($model);
 }
 public function all($params){

  $search = null;
  if(!empty($params['search'])) {
    $search = $params['search'];
    unset($params['search']);
  }
  $conditions=$params;
      //$conditions['status']=1;
     // $conditions['id']=13;
  $q = $this->model->where($conditions);
  if($search) {
        // OBSERVACIÓN: id se usa para buscar en el componente select-people-componenet
    $q->whereRaw("concat(people.id,if(people.dni is null,'',people.dni),people.name,if(people.lastname is null,'',people.lastname)) like '%$search%'");
        // ->whereRaw('`modules`.`id` = `submodules`.`module_id`')
  }
  return $q->latest()->paginate(10);
      // return $q->toSql();
}
public function listPeopleForViewPayments($params){
  $search = null;
      //dd($params);
  if(!empty($params['search'])) {
    $search = $params['search'];
    unset($params['search']);
  }
  $conditions=$params;
     // $conditions['stands.status']=1;//para que solo aparesca puestos activos
  $q = $this->model
  ->leftjoin("stands",'stands.titular_id',"=",'people.id' )
  ->leftjoin('sectors', 'sectors.id', '=', 'stands.sector_id')
  ->leftjoin('markets', 'markets.id', '=', 'stands.market_id')
  ->selectRaw('people.*, sectors.name as sector_name,stands.id as stand_id,stands.name stand_name,stands.code as stand_code,markets.code as market_code')    
  ->where($conditions);
  if($search) {
        // OBSERVACIÓN: id se usa para buscar en el componente select-people-componenet
    $q->whereRaw("concat(if(people.dni is null,'',people.dni),if(people.name is null,'',people.name),
      if(sectors.name is null,'',sectors.name),if(markets.name is null,'',markets.name),if(people.lastname is null,'',people.lastname),
      if(stands.name is null,'',stands.name),if(stands.code is null,'',stands.code),if(stands.location is null,'',stands.location)) like '%$search%'");
  }
  return $q->latest()->paginate(10);
}

public function getPeopleWithDebts($params){
  $conditions=$params;
  $selectColumns = "debts.concept_id, 
  concepts.code as concept_code,
  people.id as titular_id, 
  people.name as titular_name, 
  people.lastname as titular_lastname, 
  people.dni as titular_dni, 
  count(debts.id) as debts_number, 
  datediff(now(),debts.date_at) as expired_days, 
  GROUP_CONCAT(DATE_FORMAT(debts.date_at,'%Y-%m-%d') SEPARATOR ',') as debts_reference,
  GROUP_CONCAT(debts.id SEPARATOR ',') as debt_ids, 
  SUM(debts.total) as total";
  $groupedBy = ['people.id','debts.concept_id','debts.type'];

  if($conditions['debts.type'] !='FRA'){
     $conditions['stands.status']=1;//para que solo aparesca puestos activos
  }else {
    unset($conditions['markets.id']);
     unset($conditions['sectors.id']);
  }

    $conditions['debts.status']=1;//para que solo aparesca debts activos
    $q =$this->model->join("debts", function($join){
      $join->on( 'debts.client_id',"=",'people.id');
    })
    ->join("concepts",'concepts.id',"=",'debts.concept_id');
    
    if($conditions['debts.type'] !='FRA'){
      $selectColumns .= ",stands.id as stand_id,
      stands.code as stand_code,
      stands.name as stand_name,
      sectors.code as sector_code, 
      markets.code as market_code,
      (SELECT count( DISTINCT n.id ) from notifications n inner join debt_notification dn where n.to_id=people.id AND n.stand_id=stands.id AND dn.debt_id=debts.id and n.status>0)  as notifications";
      array_unshift($groupedBy,'stands.id');
      $q->join("stands",'stands.titular_id',"=",'people.id' )
      ->leftJoin("sectors",'sectors.id',"=",'stands.sector_id')
      ->leftJoin("markets",'markets.id',"=",'stands.market_id')
      ->orderBy('notifications','asc');
    }
    $debs_count= Util::$debts_wait_for_notify;
    if(array_key_exists("debts_count",$conditions)){
      if( $conditions['debts_count'] > 0 ) $debs_count= $conditions['debts_count'] ;
      unset($conditions['debts_count']);
    }

    $q->selectRaw($selectColumns)
    ->where($conditions)
    ->whereDate('debts.date_at', '<', now()->startOfMonth()->toDateString().' 00:00:00')
    ->havingRaw('count(debts.id) >= ?', [$debs_count])
    ->groupBy($groupedBy);

    //return $q->toSql();
    return $q->paginate(30);
  }

  public function save($params){

    $photo=\App\Util::verifyPhoto($params);
    unset($params['photo']);
    if(array_key_exists('id',$params)){
      $o = $this->model->find($params['id']);
      $o->update($params);
    }else{
      $o = $this->model->create($params);
    }
    
    if(!empty($photo) && \App\Util::is_base64image($photo) ){
      $new_photo = asset( \App\Util::storageImage("person_$o[id]",$photo) );
      $o->update(['photo'=>$new_photo]); 
    }
    return $o;
  }
  public function verifyDuplicateDNI($params){
    return \App\Models\Person::where("dni",$params['dni'])->first();
  }

}
