<?php

namespace App\Repository;

use App\Repository\ForgivenessRepositoryI;
use Illuminate\Support\Facades\DB;

class ForgivenessRepository extends BaseRepository implements ForgivenessRepositoryI{
   private $debt_repository;
   private $stand_repository;
   public function __construct(\App\Repository\DebtRepository $debt_repository,\App\Repository\StandRepository $stand_repository, \App\Models\Forgiveness $model){
       parent::__construct($model);
	   $this->debt_repository = $debt_repository;
	   $this->stand_repository = $stand_repository;
   }
   public function all($params){
    $search = null;
    //$conditions['status']=1;
    if(!empty($params['search'])) { 
      $search = $params['search'];
      unset($params['search']);
    }
    $conditions=$params;
	$q = $this->model
		->leftjoin('markets', 'markets.id', '=', 'forgivenesses.market_id')
		->leftjoin('sectors', 'sectors.id', '=', 'forgivenesses.sector_id')
		->leftjoin('people', 'people.id', '=', 'forgivenesses.person_id')
		->leftjoin('stands', 'stands.id', '=', 'forgivenesses.stand_id')
		->selectRaw('forgivenesses.*,markets.id as market_id,markets.name as market_name,sectors.id as sector_id,
			sectors.name as sector_name,people.id as titular_id, people.name as titular_name, 
			people.lastname as titular_lastname, people.dni as titular_dni,stands.code as stand_code')
        ->where($conditions);
    
    if($search) {
      $q->whereRaw("concat(forgivenesses.code,people.name,people.lastname,stands.code) like '%$search%'");
    }
    //return $this->model->with("person")->with("stand")->where($conditions)->latest()->paginate(10);
    return $q->latest()->paginate(10);
  }

   public function save($params){
	
	if(array_key_exists('id',$params)){
		
		$o = $this->model->find($params['id']);
		$o->update($params);
	}else{
		$details = $params["details"]; 
		unset($params['details']);
		//dd($details);
		$o=parent::save($params);
		
		$percent = $params['percent'];
		if($params['type'] == \App\Util::$FORGIVENE_GENERIC) {
			for($i =0; $i<count($details); $i++) {
				if($details[$i]['forgiveness_ids']==null){
					\App\Models\Stand::where('id',$details[$i]['id'])->update(['forgiveness_ids'=>$o['id']]);
				}else{
					\App\Models\Stand::where('id',$details[$i]['id'])->update(['forgiveness_ids'=>$details[$i]['forgiveness_ids']=$details[$i]['forgiveness_ids'].','.$o['id']]);
				}
				///\App\Models\Stand::update($details[$i]->id);
				$stand = $details[$i];
				$payload = [
					'debts.payment_status'=>0,
					'debts.client_id'=>$stand['titular_id'],
					'debts.stand_id'=>$stand['id'],
					'debts.previous_total'=>null,
					'debts.type'=> \App\Util::$DEBT_TYPE_NORMAL,
					'date_init'=> $params['date_init'],
					'date_end'=> $params['date_end'],
					'no_paginate'=>true,
				];
				$debts = $this->debt_repository->getDebtTitular($payload);
				$debts = json_decode(json_encode($debts), true);
				
				$debt_ids = array_map(function($e){ return $e['debt_id']; },$debts);
				$this->apply_forgiveness_in_debts($o, $debt_ids, $percent);
			}	
		}else {
			$debt_ids = array_map(function($e){ return $e['id']; },$details);
			$this->apply_forgiveness_in_debts($o, $debt_ids, $percent);
		}
	}
	
    return $o;
   }
 
   private function apply_forgiveness_in_debts($forgivene,$debt_ids, $percent){
		$forgivene->debts()->sync($debt_ids);
    	if(isset($percent)) {
    	  if($percent == 100) \App\Models\Debt::whereIn("id",$debt_ids)->update(['payment_date'=>date("Y-m-d h:m:s"),'payment_status'=>1,'previous_total'=>DB::raw('total'), 'total'=>DB::raw("total-(total*$percent/100)")]);
		 // else \App\Models\Debt::whereIn("id",$debt_ids)->update([ 'previous_total'=>DB::raw('total'), 'total'=>DB::raw("total-(total*$percent/100)")]);IF(1>3,'true','false')
		 else \App\Models\Debt::whereIn("id",$debt_ids)->update([ 'previous_total'=>DB::raw('total'), 'total'=>DB::raw("IF (payment >=(total-(total*$percent/100)), payment , total-(total*$percent/100))")]);
    	}
   		else {
    	  foreach($debt_ids as $debt_id){ 
    	    $fractionary_schedule = \App\Models\FractionarySchedule::where('debt_id',$debt_id)->first();
    	    $interest = $fractionary_schedule['interest'];
    	    \App\Models\Debt::where("id",$debt_id)->update([ 'previous_total'=>DB::raw('total'), 'total'=>DB::raw("total-$interest")]);
    	  }
    	}
   }

 
  public function delete($id){
    $o=$this->model->find($id);
    $o->update(['status'=>0]);
	if($o->type == \App\Util::$FORGIVENE_GENERIC) {
		$payload_stands = [
			'market_id'=>$o->market_id,	
			'sector_id'=>$o->sector_id,	
			'no_paginate'=>true,	
		];
		$stands = $this->stand_repository->all($payload_stands);
		$stands = $stands->toArray();
		
		if($o->status_reversion!=''){
			$ids_stands=explode(",",$o->status_reversion);

			for($a=0;$a<count($ids_stands);$a++){//recorrido de los puesto que han sido revertidos
				for($j=0;$j<count($stands);$j++){//todos los puestos del sector y mercado señalado
					if($ids_stands[$a]==$stands[$j]['id']){
						array_splice($stands, $j, 1);//elimina la posicion que quiero
						break;
					}
				}
			}	
		}
		$debts_generic = [];
		for($i =0; $i < count($stands); $i++) {
			$stand = $stands[$i];
			$payload_debts = [
				'debts.payment_status'=>0,
				'debts.client_id'=>$stand['titular_id'],
				'debts.stand_id'=>$stand['id'],
				'debts.type'=> \App\Util::$DEBT_TYPE_NORMAL,
				'date_init'=> $o['date_init'],
				'date_end'=> $o['date_end'],
				'no_paginate'=>true,
			];
			$data = $this->debt_repository->getDebtTitular($payload_debts);
			$debts_generic = array_merge($debts_generic, $data->toArray());
		}
		$debt_ids=array_map(function($e){ return $e->debt_id; },$debts_generic);
		//volver a su precio anterior y regresar al propietario
    	\App\Models\Debt::whereIn("id",$debt_ids)->where('previous_total', '!=' , null)->update(['payment_status'=>0,'payment_date'=>null,'total'=>DB::raw('previous_total'),'previous_total'=>null]);
	}else {
		$debts=$o->debts;
    	$debt_ids=array_map(function($e){ return $e['id']; },$debts->toArray());
    	//volver a su precio anterior y regresar al propietario
    	\App\Models\Debt::whereIn("id",$debt_ids)->where('previous_total', '!=' , null)->update(['payment_status'=>0,'payment_date'=>null,'total'=>DB::raw('previous_total'),'previous_total'=>null]);
	}
    return $o;
  }
 
}
