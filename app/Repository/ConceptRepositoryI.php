<?php
namespace App\Repository;

interface ConceptRepositoryI{
   public function all($params);
   public function save($params);
}