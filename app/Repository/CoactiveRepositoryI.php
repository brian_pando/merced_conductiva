<?php
namespace App\Repository;

interface CoactiveRepositoryI{
   public function all($params);
   public function save($params);
}