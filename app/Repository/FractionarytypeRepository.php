<?php

namespace App\Repository;

use App\Repository\FractionarytypeRepositoryI;
use Illuminate\Support\Facades\Storage;

class FractionarytypeRepository extends BaseRepository implements FractionarytypeRepositoryI{
   public function __construct(\App\Models\FractionaryType $model){
       parent::__construct($model);
   }
   public function all($params){
     $conditions=$params;
     $conditions['status']=1;
     return $this->model->where($conditions)->latest()->paginate(10);
   }
 
}