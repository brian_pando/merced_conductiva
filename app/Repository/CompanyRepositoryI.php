<?php
namespace App\Repository;

use App\Model\User;
use Illuminate\Support\Collection;

interface CompanyRepositoryI{
   public function all($params);
   public function save($params);
   
}