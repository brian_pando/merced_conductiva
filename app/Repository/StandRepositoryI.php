<?php
namespace App\Repository;

interface StandRepositoryI{
   public function all($params);
   public function save($params);
   public function getListStandWithTitular($params);
}