<?php

namespace App\Repository;

use App\Repository\MarketRepositoryI;
use Illuminate\Support\Facades\Storage;

class MarketRepository extends BaseRepository implements MarketRepositoryI{
   public function __construct(\App\Models\Market $model){
       parent::__construct($model);
   }
   
   public function all($params){
    $search = null;
    if(!empty($params['search'])) {
      $search = $params['search'];
      unset($params['search']);
    }
    $conditions=$params;
    $conditions['status']=1;
    
    $q = $this->model->where($conditions);
    if($search) {
      $q->whereRaw("concat(code,name,address) like '%$search%'");
      // ->whereRaw('`modules`.`id` = `submodules`.`module_id`')
    }
    return $q->latest()->paginate(15);    
    // return $q->toSql();
   }

   public function save($params){
    $photo=\App\Util::verifyPhoto($params);
    unset($params['photo']);
    if(array_key_exists('id',$params)){
      $o = $this->model->find($params['id']);
      $o->update($params);
    }else{
        $o = $this->model->create($params);
    } 

    if(!empty($photo) && \App\Util::is_base64image($photo) ){
      $new_photo = asset( \App\Util::storageImage("market_$o[id]",$photo) );
      $o->update(['photo'=>$new_photo]);
    }
    return $o;
   }

}
