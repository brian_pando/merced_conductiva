<?php

namespace App\Repository;

use App\Models\Company;
use App\Repository\CompanyRepositoryI;
use Illuminate\Support\Collection;
use Illuminate\Support\Facades\Hash; 
use Illuminate\Support\Facades\DB; 

class CompanyRepository extends BaseRepository implements CompanyRepositoryI{
   public function __construct(Company $model){
	   parent::__construct($model);
   }

   public function all($conditions){
    //  $conditions['status']=1;
    if(isset($conditions['no_paginate'])) {
      unset($conditions['no_paginate']);
      $q = $this->model->where($conditions)->latest()->get();
    } else {
      $q = $this->model->where($conditions)->latest()->paginate(10);
    }
     return  $q;
   }
}