<?php

namespace App\Repository;

use App\Models\User;
use App\Repository\UserRepositoryI;
use Illuminate\Support\Collection;
use Illuminate\Support\Facades\Hash; 
use Illuminate\Support\Facades\DB; 

class UserRepository extends BaseRepository implements UserRepositoryI{
   public function __construct(User $model){
	   parent::__construct($model);
   }
   public function all($params){
	   $conditions=['users.status'=>1];
	    $search = null;
       
        if(isset($params['search'])) {
         $search = $params['search'];
         unset($params['search']);
         }

	   $q=DB::table('users')->join('roles','roles.id','=','users.role_id')
	   ->leftjoin("people",'people.user_id',"=","users.id")->where($conditions);


	   if(array_key_exists('id',$params)){//solo traera los uno cuando es comisionista, cajero, rt_auxil, quienes no pueden cambiar la contraseña de otro
		$q->where('users.id', $params['id']);
	   }else{
		$q->whereIn('roles.name',$params['role_name']);
	   }
	   $q->selectRaw('users.*, roles.name as role_name, people.name as user_name, people.lastname as user_lastname,people.dni as user_dni');

	   if($search) {
       // id sirve para buscar los giros en el componente selectSectors
	   	
        $q->whereRaw("concat(users.id,users.code,users.name,users.username,users.email,users.status) like '%$search%'");
        }

	   return $q->latest()->paginate(10);    
   }

   public function allCommissionAgent($params) {
	   if(isset($params['roles_id'])) { $roles_id = $params['roles_id']; unset($params['roles_id']); };
	   $conditions=$params;
		$conditions['status']=1;
		return $this->model->whereIn('role_id',$roles_id)->with('role')->latest()->get();
   }
   public function cashiersAndCommissionAgent($params){
	   return $this->model->select('id','name','username','role_id')->whereIn('role_id', [4, 6, 7])->get();
   }

   public function all_roles($params){
		//$conditions=['status'=>1];
		$conditions=['users.status'=>1];
		return \App\Models\Role::all();    
   }

   public function get_role($id){
	return \App\Models\Role::find($id);    
   }

   	public function save($params){
		$photo=\App\Util::verifyPhoto($params);
		unset($params['photo']);
		if(array_key_exists('id',$params)){
			$o = $this->model->find($params['id']);
			if(array_key_exists("password",$params)){ 
				//if($params['password']!=''){ $params['password'] = Hash::make($params['password']);}
				if(!is_null($params['password'])){
					$params['password'] = Hash::make($params['password']);
				}else{
					unset($params['password']);
				}
			}
			$o->update($params);
			
		}else{
			$params['password'] = Hash::make($params['password']);
			$o = $this->model->create($params);
		}

		if(!empty($photo) && \App\Util::is_base64image($photo) ){
			$new_photo = asset( \App\Util::storageImage("user_$o[id]",$photo) );
			$o->update(['photo'=>$new_photo]);
		}
	
		return $o;
	}

	public function get($id){
		return $this->model->find($id);
	// return $this->model->with('role')->find($id);
	}

	public function validateCode($params) {
		$conditions=$params;
		$conditions['status']=1;
		return $this->model->where($conditions)->first();
	}

	public function listPermissions($user_id){
		$conditions=['user_id'=>$user_id];
		return DB::table("permissions")->where($conditions)->get();
	}

	public function savePermissions($permissions){
		$toDelete=[];
		$toSave=[];
		foreach($permissions as $permission){
			if($permission['status']==1){
			
				$markets = array_key_exists("markets",$permission) && is_array($permission['markets'])? json_encode($permission['markets']):null;
				$p= new \App\Models\Permission();
				if( array_key_exists('id',$permission)) $p = \App\Models\Permission::find($permission['id']);
				$p->user_id = $permission['user_id'];
				$p->menu = $permission['menu'];
				$p->mode = $permission['mode'];
				$p->markets =  $markets;
				if( array_key_exists('id',$permission)) $p->update();
				else  $p->save();
				
			}else{
				$toDelete[] = $permission['id'];
			}
		}
		if( count($toDelete)>0 ) $deleted = DB::delete('delete from permissions WHERE id in ('.implode(",",$toDelete).')');
		
	}

	public function delete($id){
		\App\Models\User::find($id)->update(['status'=>0,'password'=>'--']);
	}
	public function saveChangePassword($params) {
    if(isset($params['id'])) {
        $user = DB::table('users')->where('id', $params['id'])->first();
        if ($user) {
            if (Hash::check($params['currentpassword'], $user->password)) {

                if (Hash::check($params['newpassword'], $user->password)) {
                     return ['type'=>'info','msg'=>  'La nueva contraseña debe ser diferente a la contraseña actual'];
                }

                DB::table('users')->where('id', $params['id'])->update([
                    'password' => Hash::make($params['newpassword']),
                ]);

                   return ['type'=>'success','msg'=>  'Contraseña cambiada con éxito. Por favor, cierra la sesión y vuelve a iniciar sesión con tu nueva contraseña.'];
            } else {
               return ['type'=>'error','msg'=> 'La contraseña actual no es válida'];
            }
        } else {
            return ['type'=>'error','msg'=>  'Usuario no encontrado'];
        }
    }else{
    	return ['type'=>'error','msg'=> 'Usuario no autenticado'];
    }
}
}
