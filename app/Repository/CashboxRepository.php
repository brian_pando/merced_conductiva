<?php

namespace App\Repository;

use App\Repository\CashboxRepositoryI;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\DB;

class CashboxRepository extends BaseRepository implements CashboxRepositoryI{
   public function __construct(\App\Models\Cashbox $model){
	   parent::__construct($model);
   }
   
   public function all($params){
	$q = $this->model;
	if(isset($params['search'])) {
		$q = $q->whereRaw("concat(people.name,people.lastname,people.dni) like '%".$params['search']."%'");
	}

	if(isset($params['date_init']) and isset($params['date_end'])) {
		$q = $q->whereBetween('date_init', [$params['date_init']." 00:00:00", $params['date_end']." 23:59:59"]);
	}

	if(isset($params['user_id'])) {
		$q = $q->where('user_id',$params['user_id']);
	}
	if(isset($params['no_paginate'])) {
		$l = $q->with('cashboxdetails')->with('user')->latest()->get();
	}else {
		$l = $q->with('cashboxdetails')->with('user')->latest()->paginate(10);
	}
	return $l;
	// return $q->toSql();
   }

   public function save($params){
	$details=null;
	if(!empty($params['cashboxdetails'])) {
		$details = $params['cashboxdetails'];
		unset($params["cashboxdetails"]);
	  }
	if(array_key_exists('id',$params)){
	  $o = $this->model->find($params['id']);
	  $o->update($params);
	  if($details!=null){
		$this->updateCashboxDetail($o->id,$details);
	  }
	}else{
	  $o = $this->model->create($params);
	}
	//$this->updateCashboxDetail($o->id,$details);
	return $o;
   }

   private function updateCashboxDetail($cashbox_id,$cashbox_details) {
	foreach ($cashbox_details as $item) {
		if(array_key_exists('id',$item)){
		  $d = \App\Models\CashboxDetail::find($item['id']);
		  $d->update($item);
		  $this->updateTransactionDetails($item);
		} else {
		  $item["cashbox_id"] = $cashbox_id;
		  $d = \App\Models\CashboxDetail::create($item);
		}
	  }
   }
	private function updateTransactionDetails($cashbox_detail) {
		$q = DB::table('transaction_details')
		->join('transactions', 'transactions.id', '=', 'transaction_details.transaction_id')
		->join('debts', 'debts.id', '=', 'transaction_details.debt_id')
		->selectRaw('debts.concept_id ,transaction_details.id');
		
		if(is_null($cashbox_detail['concept_id'])) {
			$data = $q->where([ 'transactions.cashbox_id'=>$cashbox_detail['cashbox_id']])->get();
			foreach ($data as $detail) {
				\App\Models\TransactionDetail::where('id',$detail->id)->update(['classifier_ref'=>$cashbox_detail['code']]);
			}
		} else {
			$concept = \App\Models\Concept::where('id',$cashbox_detail['concept_id'])->first();
			$data = $q->where([ 'transactions.cashbox_id'=>$cashbox_detail['cashbox_id'], 'debts.concept_id'=>$cashbox_detail['concept_id']])->get();
			foreach ($data as $detail) {
				\App\Models\TransactionDetail::where('id',$detail->id)->update(['voucher_ref'=>$cashbox_detail['code'],'classifier_ref'=>$concept['classificator']]);
			}
		}
	}
	  
}