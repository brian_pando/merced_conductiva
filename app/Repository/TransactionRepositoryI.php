<?php
namespace App\Repository;

interface TransactionRepositoryI{
   public function all($params);
   public function save($params);
   public function delete($params);
   public function allDetailsByConcepts($params);
   public function calculateTotalAll($params);
   public function cashReceiptsReport($params);
}