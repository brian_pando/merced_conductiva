<?php
namespace App\Repository;

interface UITRepositoryI{
   public function all($params);
   public function save($params);
}