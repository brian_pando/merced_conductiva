<?php
namespace App\Repository;

interface PublicServiceRepositoryI{
   public function all($params);
   public function save($params);
}