<?php

namespace App\Repository;

use App\Repository\ConceptRepositoryI;
use Illuminate\Support\Facades\Storage;

class ConceptRepository extends BaseRepository implements ConceptRepositoryI{
   public function __construct(\App\Models\Concept $model){
       parent::__construct($model);
   }
   public function all($conditions){
    $conditions['status']=1;
    if(isset($conditions['no_paginate'])) {
      unset($conditions['no_paginate']);
      $q = $this->model->where($conditions)->latest()->get();
    } else {
      $q = $this->model->where($conditions)->latest()->paginate(20);
    }
     return  $q;
   }
 
}