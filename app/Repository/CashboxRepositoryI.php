<?php
namespace App\Repository;

interface CashboxRepositoryI{
   public function all($params);
   public function save($params);
}