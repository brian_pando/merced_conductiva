<?php  
 
namespace App\Repository;   

use Illuminate\Database\Eloquent\Model;   

class BaseRepository {     
    protected $model;       
    public function __construct(Model $model){         
        $this->model = $model;
    }

    // public function all()
    // {
    //     return $this->model->all();
    // }

    public function create(array $attributes): Model{
        return $this->model->create($attributes);
    }

    public function update(array $data, $id)
    {
        return $this->model->where('id', $id)
            ->update($data);
    }

    public function delete($id)
    {
        return $this->model->destroy($id);
    }
 
    public function find($id): ?Model{
        return $this->model->find($id);
    }

    public function save($params){
        if(array_key_exists('id',$params)){
            $o = $this->model->find($params['id']);
            $o->update($params);
        }else{
            $o = $this->model->create($params);
        }
        return $o;
    }
}