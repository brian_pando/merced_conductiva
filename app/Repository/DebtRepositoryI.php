<?php
namespace App\Repository;

interface DebtRepositoryI{
   public function all($params);
   public function save($params);
   public function allMarketByYear($params);
   public function getMarketByMonth($params);
}