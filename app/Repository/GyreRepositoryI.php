<?php
namespace App\Repository;

interface GyreRepositoryI{
   public function all($params);
   public function save($params);
   public function all_by_market($params);
}