<?php
namespace App\Repository;

interface ReversionRepositoryI{
   public function all($params);
   public function save($params);
}