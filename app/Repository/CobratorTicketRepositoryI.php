<?php
namespace App\Repository;

interface CobratorTicketRepositoryI{
   public function all($params);
   public function save($params);
}