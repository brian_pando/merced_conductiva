<?php
namespace App\Repository;

interface FractionaryRepositoryI{
   public function all($params);
   public function save($params);
}