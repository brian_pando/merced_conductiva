<?php
namespace App\Repository;

interface ForgivenessRepositoryI{
   public function all($params);
   public function save($params);
}