<?php

namespace App\Repository;

use App\Repository\GyreRepositoryI;
use Illuminate\Support\Facades\Storage;

class GyreRepository extends BaseRepository implements GyreRepositoryI{
   public function __construct(\App\Models\Gyre $model){
       parent::__construct($model);
   }
   public function all($params){
    $search = null;
    if(!empty($params['search'])) {
      $search = $params['search'];
      unset($params['search']);
    }
    $xpage = $params['xpage'];
    unset($params['xpage']);
   // dd("id",$params['id']);
     $conditions=$params;
     $conditions['status']=1;

     $q = $this->model->where($conditions);
     if($search) {
       // id sirve para buscar los giros en el componente selectGyres
      $q->whereRaw("concat(code,name) like '%$search%'");
     }

     return $q->latest()->paginate($xpage);
   }

   public function save($params){
    if(array_key_exists('id',$params)){
      $o = $this->model->find($params['id']);
      $o->update($params);
    }else{
        $o = $this->model->create($params);
    }

    return $o;
   }

	public function all_by_market($params){
		$l= $this->model
		->join("stands",'stands.gyre_id',"=","gyres.id")
		->join("markets",'stands.market_id',"=","markets.id")
		->groupBy("gyres.id")
		->where( ['market_id'=>$params['market_id'] ] )
		->selectRaw("gyres.*")->paginate(10);
		return $l;
	}
}