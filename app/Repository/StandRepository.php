<?php

namespace App\Repository;

use App\Repository\StandRepositoryI;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\DB;

class StandRepository extends BaseRepository implements StandRepositoryI{
   public function __construct(\App\Models\Stand $model){
       parent::__construct($model);
   }

   public function all($params){
    // dd($params);
    $type=null; $search = null; $number_paginate = 10; $no_paginate = false; $market_id=null; $standsAll=null;
    if(!empty($params['search'])) {
      $search = $params['search'];
      unset($params['search']);
     // $search = ($search === 'Activo') ? 1 : $search;
    }
    if(isset($params['number_paginate'])) {
      $number_paginate = $params['number_paginate'];
      unset($params['number_paginate']);
    }
    if(!empty($params['markets.type'])) {
      $type = $params['markets.type'];
      unset($params['type']);
    }
    if(isset($params['no_paginate'])) {
      $no_paginate = true;
      unset($params['no_paginate']);
    }
    if(isset($params['market_id'])) {
     $market_id = $params['market_id'];
    unset($params['market_id']);
    }
    //Listar todo los satands Activos e inactivos
    if(isset($params['standsAll'])) {
     $standsAll = $params['standsAll'];
    unset($params['standsAll']);
    }
    $conditions=$params;
    $standsAll ? '' :  $conditions['stands.status']=1;

     $q = $this->model->join('people', 'people.id', '=', 'stands.titular_id')
     ->join('markets', 'markets.id', '=', 'stands.market_id')
     ->leftjoin('sectors', 'sectors.id', '=', 'stands.sector_id')
     ->selectRaw('sectors.name as sector_name,stands.*,people.id as titular_id, people.name as titular_name, people.lastname as titular_lastname, people.dni as titular_dni')
     ->where($conditions);
      if (!empty($market_id)) {
        $q->where('stands.market_id', $market_id);
    }
     //if($params['markets.type']=='local') $q->whereRaw("concat(stands.name,stands.location,people.name,if(people.lastname is null,'',people.lastname),if(people.dni is null,'',people.dni)) like '%$search%'");
     $q->whereRaw("concat(if(markets.name is null,'',markets.name),if(sectors.name is null,'',sectors.name),if(stands.name is null,'',stands.name),if(stands.status is null,'',stands.status),if(stands.code is null,'',stands.code),if(stands.location is null,'',stands.location),people.name,if(people.lastname is null,'',people.lastname),if(people.dni is null,'',people.dni)) like '%$search%'");
   
     //dd($q->toSql());
    if($no_paginate) $q = $q->with('market')->with('sector')->with('titular')->with('gyre')->groupBy('markets.code','stands.code')->get();
    else{
      if($type=='local'){
        $q = $q->with('market')->with('sector')->with('titular')->with('gyre')->paginate($number_paginate);
      }else{
        $q = $q->with('market')->with('sector')->with('titular')->with('gyre')->paginate($number_paginate);
      } 
    }
    
    return $q;
}
   
   public function getStandsByClientID($params){
    $not_paginate = false;
    
    if(!empty($params['no_pagination'])) {
      $not_paginate = $params['no_pagination'];
      unset($params['no_pagination']);
    }
    $conditions=$params;
    $conditions['stands.status']=1;
    $q = $this->model->join('people', 'people.id', '=', 'stands.titular_id')
        ->selectRaw('stands.*')
        ->where($conditions);
    
    return $not_paginate ? $q->latest()->get() : $q->latest()->paginate(10);
     //return $q->latest()->get();    
  }

   public function save($params){
    $photo=\App\Util::verifyPhoto($params);
    $isEditSectorOrMarketOrStandName=null;
    $isEditNameLocal=null;
    //dd($params);
    unset($params['photo']);
    if(array_key_exists('id',$params)){
      if(isset($params['isEditSectorOrMarketOrStandName'])){
        $isEditSectorOrMarketOrStandName=$params['isEditSectorOrMarketOrStandName'];
        unset($params['isEditSectorOrMarketOrStandName']);
      }
      if($isEditSectorOrMarketOrStandName){//para los puesto que esta en un mercado u sector
        $sector=DB::select('SELECT code FROM sectors WHERE id = ?', [$params['sector_id']]);
        $market=DB::select('SELECT code FROM markets WHERE id = ?', [$params['market_id']]);
        $stand_id=$params['id'];
        $stand_code=$params['code'];
        $sector_code=$sector[0]->code;
        $market_code=$market[0]->code;
        DB::update(
            "UPDATE debts as d
            inner join stands s on s.id=d.stand_id
            inner join concepts c on c.id=d.concept_id
            inner join markets m2 on m2.id=s.market_id
            inner join sectors sector on sector.id=s.sector_id
            set d.code=
            CASE 
            WHEN d.concept_id=1 OR d.concept_id=2 OR d.concept_id=3 THEN
              (case
                when d.type='normal' then
                  insert(d.code,1,LENGTH(d.code)-9,CONCAT(c.code,' : Pto:','$stand_code','|','$sector_code','|','$market_code','|'))
                else 
                  CONCAT(SUBSTRING(d.code,1,17),CONCAT('$stand_code','|','$sector_code','|','$market_code'),SUBSTRING(d.code,LENGTH(d.code)-9,10))
              end)
            WHEN d.concept_id=4 THEN
              (case
                when d.type='normal' then
                  insert(d.code,1,LENGTH(d.code)-9,CONCAT(c.code,'-',d.code_manual,' : Pto:','$stand_code','|','$sector_code','|','$market_code','|'))
                ELSE 
                  CONCAT(SUBSTRING(d.code,1,17),CONCAT('$stand_code','|','$sector_code','|','$market_code'),SUBSTRING(d.code,LENGTH(d.code)-9,10))
              end)
            WHEN d.concept_id=10 THEN 
              (CASE
                when d.type='normal' then
                  CONCAT(SUBSTRING(d.code,1,LENGTH(concat(c.code,'|',d.code_manual,'|Pto:'))),'$stand_code',SUBSTRING(d.code,LENGTH(concat(c.code,'|',d.code_manual,'|Pto:',s.code))+1,LENGTH(d.code)))
                ELSE 
                CONCAT(SUBSTRING(d.code,1,17),CONCAT('$stand_code','|','$sector_code','|','$market_code'),SUBSTRING(d.code,LENGTH(d.code)-9,10))
              end)
          END
          WHERE d.payment_status = 0 and s.id = ?",[$stand_id]
        );
      }
      if(isset($params['isEditNameLocal'])){
        $isEditNameLocal=$params['isEditNameLocal'];
        unset($params['isEditNameLocal']);
      }
      if($isEditNameLocal){//actualiza el code de deudas de los locales
        $stand_id=$params['id'];
        $stand_name=$params['name'];
        DB::update(
          "UPDATE `debts` as d
          inner join stands s on s.id=d.stand_id
          inner join concepts c on c.id=d.concept_id
          set d.code=case 
            WHEN d.type='FRA' then 
              CONCAT(SUBSTRING(d.code,1,17),'$stand_name',SUBSTRING(d.code,LENGTH(d.code)-13,14))
            else
              concat(SUBSTRING(d.code,1,LENGTH(CONCAT(c.code,'-',d.code_manual,' : '))),'$stand_name',SUBSTRING(d.code,LENGTH(concat(c.code,'-',d.code_manual,' : ',s.name))+1,LENGTH(d.code)))	
          end
          WHERE d.payment_status = 0 and s.id = ?",[$stand_id]
        );
      }
      if(isset($params['is_edit_titular'])){
        unset($params['is_edit_titular']);
        DB::table('debts')->where('stand_id', $params['id'])
          ->update(['client_id'=>$params['titular_id']]);//actualizamos las deudas cambiando de titular   
      }
      
      $o = $this->model->find($params['id']);
      $o->update($params);
    }else{
        $o = $this->model->create($params);
    }

    if(!empty($photo) && \App\Util::is_base64image($photo) ){
      $new_photo = asset( \App\Util::storageImage("stand_administration_$o[id]",$photo) );
      $o->update(['photo'=>$new_photo]);
    }
    return $o;
   }

   public function get($id) {
     return $this->model->find($id);
   }
   public function getStandWithDebts($params){
    $conditions=$params;
    $conditions['status']=1;
    $q=DB::table('debts')->where($params);
    return $q->get();
   }
   public function getBringDebtsToDate($params){
    $conditions=$params;
    $conditions['status']=1;
    $q=DB::table('debts')->where($conditions)->whereDate('debts.date_at', '<',now()->lastOfMonth()->toDateString().' 23:50:59');
    return $q->get();
   }
  public function getListStandWithTitular($params){
    $noPaginate = isset($params['noPaginate']);
    unset($params['noPaginate']);
    $conditions=$params;
    $conditions['stands.status']=1;
    $q = $this->model->select('stands.id','stands.code','stands.rate_amount','people.name','people.lastname','stands.titular_id')
        ->join('people', 'people.id', '=', 'stands.titular_id')
        ->where($conditions);
    return $noPaginate ? $q->get() : $q->paginate(10);
   }
}
