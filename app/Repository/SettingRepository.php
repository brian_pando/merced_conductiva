<?php

namespace App\Repository;

use App\Repository\SettingRepositoryI;
use Illuminate\Support\Facades\Storage;

class SettingRepository extends BaseRepository implements SettingRepositoryI{
   public function __construct(\App\Models\Setting $model){
       parent::__construct($model);
   }
   public function all($params){
     $conditions=$params;
     $conditions['status']=1;
     //return $this->model->where($conditions)->latest()->paginate(20);
     return $this->model->where($conditions)->paginate(20);
   }

   public function save($params){
     $value = $params['value'];
     
     if( $is_picture = \App\Util::is_base64image($value) ){//registrando o cambiando imagen
      unset($params['value']);
     }
    if(array_key_exists('id',$params)){
      $o = $this->model->find($params['id']);
      $o->update($params);
    }else{
        $o = $this->model->create($params);
    }

    if($is_picture){
      $name_photo = \App\Util::namePhoto("setting_$o[id]");
      $new_picture = asset( \App\Util::storageImage("setting_$o[id]",$value) );
      $o->update(['value'=>$new_picture,'name_photo'=>$name_photo]);
    }
    return $o;
   }
 
}