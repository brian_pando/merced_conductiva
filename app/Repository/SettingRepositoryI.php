<?php
namespace App\Repository;

interface SettingRepositoryI{
   public function all($params);
   public function save($params);
}