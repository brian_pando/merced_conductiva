<?php

namespace App\Repository;

use App\Repository\CoactiveRepositoryI;
use Illuminate\Support\Facades\Storage;

class CoactiveRepository extends BaseRepository implements CoactiveRepositoryI{
   public function __construct(\App\Models\Coactive $model){
       parent::__construct($model);
   }
   public function all($params){
     $conditions=$params;
     $conditions['status']=1;
     return $this->model->where($conditions)->latest()->paginate(10);
   }

   public function save($params){
      $debts = $params['debts'];
      $date_init = $params['date_init'];
      $description_init = $params['description_init'];
      $date_finish = array_key_exists("date_finish",$params)?$params['date_finish']:null;
      $description_finish = array_key_exists("description_finish",$params)?$params['description_finish']:null;
      foreach($debts as $debt){
        if($debt['coactive_id']>0){
          $this->model->find($debt['coactive_id'])->update( compact(['date_init','description_init','date_finish','description_finish']) );
        }else{
          $debt_id=$debt['debt_id'];

          $c=$this->model->create(compact(['date_init','description_init','debt_id']) );
          \App\Models\Debt::find($debt_id)->update(['coactive_id'=>$c->id]);
        }

      }
   }
 
}