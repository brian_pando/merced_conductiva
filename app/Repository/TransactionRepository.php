<?php

namespace App\Repository;

use App\Repository\TransactionRepositoryI;
use Illuminate\Support\Facades\Storage;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;

class TransactionRepository extends BaseRepository implements TransactionRepositoryI{
    private $fractionaryRepository;
	public function __construct(\App\Models\Transaction $model, FractionaryRepositoryI $fractionaryRepository ){
	   parent::__construct($model);
	   $this->fractionaryRepository=$fractionaryRepository;
   }

   private function joinsAllDetailsByConcepts($q) {
	return $q->join('people', 'people.id', '=', 'transactions.client_id')
	->join('transaction_details','transaction_details.transaction_id','=','transactions.id')
	->join('debts','debts.id','=','transaction_details.debt_id')
	->join('users','users.id','=','transactions.user_id')
	->leftJoin('stands','stands.id','=','debts.stand_id');//al crear fraccionamiento de tipo (papeleta,v terreno,mauseleo, etc) no se asigna un stand_id
   }

    public function allDetailsByConcepts($params) {
		$q = $this->model;
		$q = $this->joinsAllDetailsByConcepts($q);
		$q->selectRaw('transactions.*,transaction_details.*');
		if(isset($params['user_logeado_id'])){//solo los gerentes y ssa pueden ver de todos los comisionistas y los cajeros
			//$q->where('users.role_id', '<>', 1);
			unset($params['user_logeado_id']);
		}
		$q = $this->queryWhereFilters($params,$q)
			->with('client')->with('user')
			->latest('transactions.created_at');
		//dd($q->toSql());
		if(isset($params['no_paginate'])) {
			$l = $q->get();
		}else {
			$l = $q->paginate(10);
		}		
		return $l;
    }
    public function cashReceiptsReport($params) {
		$q = $this->model;
		
		$q = $q->select('transactions.code','transactions.date_at','people.name as client_name','people.lastname as client_lastname','people.dni as client_dni',
				'users.name as user_name','users.username as user_username','transaction_details.detail','transaction_details.total','transaction_details.status','debts.type')
				->join('people','people.id','=','transactions.client_id')
				->join('users','users.id','=','transactions.user_id')
				->join('transaction_details','transaction_details.transaction_id','=','transactions.id')
				->join('debts','debts.id','=','transaction_details.debt_id');

		if(isset($params['user_logeado_id'])){//solo los gerentes pueden ver de todos los comisionistas y los cajeros
			$q->where('users.role_id', '<>', 1);
			unset($params['user_id']);
		}
		//dd($q);
		$q = $this->queryWhereFilters($params,$q);
		return $q->get();
    }
    public function calculateTotalAll($params) {
		$q = $this->model;
		$q = $this->joinsAllDetailsByConcepts($q);
		if(isset($params['user_logeado_id'])){//solo los gerentes pueden ver de todos los comisionistas y los cajeros
			$q->where('users.role_id', '<>', 1);
			unset($params['user_id']);
		}
		$status=$params['status'];
		unset($params['status']);
		if($status=='0'){
			$q->selectRaw('SUM(transaction_details.total) as total')
			->where(['transactions.status'=>$status]);
		}else{
			$q->selectRaw('SUM(transaction_details.total) as total')
			->where(['transactions.status'=>1]);
		}
		
		$q = $this->queryWhereFilters($params,$q);

		return $q->first();
    }

   public function calculateTotalAllExtorted($params) {
	$q = $this->model;
	$q = $this->joinsAllDetailsByConcepts($q);
	
	$q->selectRaw('SUM(transaction_details.total) as total')
	->where(['transactions.status'=>1]);

	$q = $this->queryWhereFilters($params,$q);

	return $q->first();
   }

   public function all($params){
	
	$q = $this->model->join('people', 'people.id', '=', 'transactions.client_id')
	  	->selectRaw('transactions.*,people.id as client_id, people.name as client_name, people.lastname as client_lastname, people.dni as client_dni');
	
	$q = $this->queryWhereFilters($params,$q);
	
	if(isset($params['user_id'])) {
		$q = $q->where('transactions.user_id',$params['user_id']);
	}

	if(isset($params['no_paginate'])) {
		$l = $q->with('client')->with('user')->with('transactiondetails')->latest()->get();
	}else {
		$l = $q->with('client')->with('user')->with('transactiondetails')->latest()->paginate(10);
	}
	 
	return $l;
	// return $q->toSql();transaction
   }
   public function allGroupByMonth($params){
	return $this->model->join("users","users.id","=","transactions.user_id")
			->groupBy(DB::raw(" user_id, year(date_at), month(date_at)"))
			->whereRaw("year(date_at)=$params[year]")
			->selectRaw("user_id, users.name, users.position, users.porcentage ,year(date_at) as year,month(date_at) as month, sum(total) as total")
			->get();
   }

   public function calculateTotalByConcept($params) {
	$q = $this->model
	->join('transaction_details','transaction_details.transaction_id','=','transactions.id')
	->join('debts','debts.id','=','transaction_details.debt_id')
	->join('concepts', 'concepts.id', '=', 'debts.concept_id')

	->selectRaw("SUM( transaction_details.total ) as total,concepts.id as concept_id, concepts.name as concept_name, concepts.code as concept_code ")
	->where(['transactions.status'=>1])
	->whereIn('concepts.id',[1,2,3])
	->groupBy('debts.concept_id');

	$q = $this->queryWhereFilters($params,$q);

	return $q->get();
   }

   public function calculateTotalBySector($params) {
	$sectors_id = [];
	if(isset($params['sectors_id'])){
	  $sectors_id = $params['sectors_id'];
	  unset($params['sectors_id']);
	}
	$q = $this->model
	->join('transaction_details','transaction_details.transaction_id','=','transactions.id')
	->join('debts','debts.id','=','transaction_details.debt_id')
	->join('stands', 'stands.id', '=', 'debts.stand_id')
	->selectRaw("SUM( transaction_details.total ) as total,stands.sector_id")
	->where(['transactions.status'=>1])
	->whereIn('stands.sector_id',$sectors_id)
	->groupBy('stands.sector_id');

	$q = $this->queryWhereFilters($params,$q);

	return $q->get();
   }

    private function queryWhereFilters($params,$q) {
		if(isset($params['search'])) {
			$q->whereRaw("concat(people.name,people.lastname,people.dni,transactions.code) like '%".$params['search']."%'");
		}
		if(isset($params['date_init']) and isset($params['date_end'])) {
			$q->whereBetween('transactions.date_at', [$params['date_init']." 00:00:00", $params['date_end']." 23:00:00"]);
		}

		if( isset($params['sector_id']) ) {
			$q->where('stands.sector_id','=',$params['sector_id']);
		}

		if( isset($params['market_id']) ) {
			$q->where('stands.market_id','=',$params['market_id']);
		}

		if(isset($params['user_id'])) {
			$q->where('transactions.user_id',$params['user_id']);
		}

		if( isset($params['concept_id']) ) {
			$q->where('debts.concept_id','=',$params['concept_id']);
		}


		if( isset($params['type']) ) {
			$q->where('debts.type','=',$params['type']);
		}
		if( isset($params['status']) ) {//para los extornados
			$q->where('transactions.status','=',$params['status']);
		}

		return $q;
    }

  public function resumenByConcept($params){
	$conditions = [ ["transactions.date_at",">=",$params['date_init'] ] ];
	if($params['date_end']) array_push($conditions,["transactions.date_at","<=",$params['date_end'] ]);
	$conditions['transactions.user_id'] = $params['transactions.user_id'];
	return DB::table('transaction_details')
				->join('transactions', 'transactions.id', '=', 'transaction_details.transaction_id')
				->join('people', 'people.id', '=', 'transactions.client_id')
				->join('users', 'users.id', '=', 'transactions.user_id')
				->join('debts', 'debts.id', '=', 'transaction_details.debt_id')
				->join('concepts', 'concepts.id', '=', 'debts.concept_id')
				->leftJoin('stands','stands.id','=','debts.stand_id')
				->leftJoin('markets','markets.id','=','stands.market_id')
				->leftJoin('sectors','sectors.id','=','stands.sector_id')
			//	->leftJoin('stands','stands.id','=','debts.stand_id');
				->selectRaw('transaction_details.*,transactions.code as transaction_code,
					transactions.status as transaction_status,transactions.date_at as transaction_date_at,
					people.name,people.lastname,people.dni,people.id as client_id,concepts.code, concepts.id as concept_id,
					markets.id as market_id,markets.name as market_name,markets.code as market_code, sectors.id as sector_id,sectors.code as sector_code,
					debts.type')
				->where($conditions)
				->get();
				// ->toSql();
  }

   public function get($id) {
	$conditions=[];
	$conditions['status']=1;
	
	return $this->model->with('client')->with('transactiondetails')->where($conditions)->latest()->paginate(10);    
   }

   	public function delete($params){
	  	$detail = $params['transactiondetails'];
		// $isTransactionDeleted =  $params['status'] == 0;
		  $this->model->where('id',$params['id'])->update(['status'=>0,'message_delete'=>$params['message_delete'],
		  'user_id_delete'=>$params['user_id_delete'],'date_delete'=>$params['date_delete']]);

		foreach ($detail as $item) {
			\App\Models\TransactionDetail::where('id',$item['id'])->update(['status'=>0]);
			$debt = \App\Models\Debt::find($item['debt_id']);
			$debt->update(['payment_status'=>0,'payment'=>$debt->payment - $item['total']]);
	   }
   }

   public function getTransactionByDebtID($debt_id) {
	return $this->model->whereHas('transactiondetails', function (Builder $query) use ($debt_id) {
	  $query->where('debt_id', $debt_id);
  	})->with('user')->with('client')->with('transactiondetails')->get();
   }
   
  public function removeDetail($id) {
	  $o = \App\Models\TransactionDetail::destroy($id);
	  return $o;
  }

   public function save($params){
	 $details = $params['transactiondetails'];
	 unset($params["transactiondetails"]);
	 if(array_key_exists('id',$params)){
	   $o = $this->model->find($params['id']);
	   $o->update($params);
	}else{
		if(!array_key_exists('code',$params)) {
			$new_code = $this->generateCode($params['user_id']);
			$params['code'] = $new_code;
		}
		
		$o = $this->model->create($params);
	}	
	$this->saveTransactionDetail($o,$details);

	return $o;
   }

   private function generateCode($user_id) {
		//$last_transaction = \App\Models\Transaction::where('code', 'like', date('Y').'%')->latest('id')->first();
		$last_transaction = \App\Models\Transaction::where('code', 'like', date('Y').'%')->where('user_id',$user_id)->latest('id')->first();
		if($last_transaction==null){
			$new_number=sprintf('%07d', 1);
		}else{
			$last_code = explode('-',$last_transaction->code);
			//si es codigo nuevo (anio-usu-number) o antiguo (anionumber).
			$new_number = sprintf('%07d', (int) ( count($last_code)>1?$last_code[2]:0) +1);
		}
		$new_code =  date('Y').'-'.sprintf('%02d', $user_id).'-'. $new_number ;
		return $new_code;
   }

   	private function saveTransactionDetail($transaction,$details) {
		//Log::debug("Transaccion guardada ".json_encode($transaction));
		foreach ($details as $detail) {
			if(array_key_exists('id',$detail)){
				$d = \App\Models\TransactionDetail::find($detail['id']);
				$d->update($detail);
			}else {
				$debt = \App\Models\Debt::find($detail['debt_id']);
				//Log::debug("DEBT ".json_encode($debt));
				if($debt['payment'] && !$debt['payment_date']){
					$detail["transaction_id"] = null; //se hace esto cuando ya hay un pago que viene de la migracion
					$total_payment_actual=$detail['total'];
					$detail['total']=$debt['payment'];
					//Log::debug("Detalle a crear Payment ".json_encode($detail));
					$d1 = \App\Models\TransactionDetail::create($detail);
					$detail['total']=$total_payment_actual;
				}
				$detail["transaction_id"] = $transaction->id;
				//Log::debug("Detalle a crear ".json_encode($detail));
				$d = \App\Models\TransactionDetail::create($detail);
			} 
			$this->updateDebts($transaction,$detail);
		}
	}

	private function updateDebts($transaction,$detail) {
		// El pago de una deuda se puede hacer por partes en multiples transacciones
		$transaction_details_debt = \App\Models\TransactionDetail::where(['debt_id'=>$detail['debt_id'],'status'=>1])->get();
		$payment_total = 0;
		foreach ($transaction_details_debt as $item) {
			$payment_total += $item->total;
		}
		
		// Si de S/50.00 el cliente solo paga 20, entonces solo se actualiza el monto pagado y la fecha pagada, pero si cancela todo, se actualiza el estado de pago.
		$params = [];
		$d = \App\Models\Debt::find($detail['debt_id']);
		if($d->total <= $payment_total) {//pago total
			$params = ['payment_date'=>$transaction->date_at,'payment'=>$payment_total,'payment_status'=> 1];
		} else {
			$params = ['payment_date'=>$transaction->date_at,'payment'=>$payment_total ];
		}
		$d->update($params);
		if( isset($d->fractionary_schedule_id )) {
			$count_pending_debts = $this->fractionaryRepository->checkByDebt( $d->fractionary_schedule_id);
			$this->fractionaryRepository->finishByDebt($count_pending_debts,$d->fractionary_schedule_id);
			
				if($count_pending_debts == 0) {
					$fractionary = \App\Models\FractionarySchedule::join("fractionaries","fractionaries.id","=","fractionary_schedules.fractionary_id")
						->select([ "fractionaries.reference_id"])
						->whereRaw("fractionary_schedules.id = $d->fractionary_schedule_id")->first();
					$fractionary_debt_references_id = explode(",",$fractionary['reference_id']);

					if($fractionary['reference_id']!=null){//los fraccionamientos no referenciadas a una deuda que proviene de puesto, multa directa, no actualizan
						foreach ($fractionary_debt_references_id as $debt_id) {
							$d = \App\Models\Debt::find($debt_id);
							$d->update(['payment_date'=>$transaction->date_at,'payment_status'=> 1,'payment'=>$d->total]);
						}
					}	
				}
			
		}
	}
 
}
