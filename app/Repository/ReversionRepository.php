<?php

namespace App\Repository;

use App\Repository\ReversionRepositoryI;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\DB;
use \App\Util;

class ReversionRepository extends BaseRepository implements ReversionRepositoryI{
   public function __construct(\App\Models\Reversion $model){
       parent::__construct($model);
   }
   public function all($params){
    $search = null;
    if(!empty($params['search'])) {
      $search = $params['search'];
      unset($params['search']);
    }
     $conditions=$params;
     $q = $this->model->join('people', 'people.id', '=', 'reversions.person_id')
     ->selectRaw('reversions.*,people.id as titular_id, people.name as titular_name, people.lastname as titular_lastname, people.dni as titular_dni')
     ->where($conditions);
     if($search) {
     $q->whereRaw("concat(reversions.code,people.name,if(people.lastname is null,'',people.lastname),if(people.dni is null,'',people.dni)) like '%$search%'");
    }
     //$conditions['status']=1;
    return $q->with("person")->with("stand")->latest()->paginate(10);
   }

   public function save($params){
     $details = $params["details"]; unset($params['details']);
     //id de todo los que se va revertir
     $debt_ids = array_map(function($e){ return $e['id']; },$details);

     $forgivenes=\App\Models\Forgiveness::where('stand_id', $params['stand_id'])->selectRaw('forgivenesses.id')->get();
     if(sizeof($forgivenes)!=0){//para cuando se generan la condonacion para un puesto
      \App\Models\Forgiveness::whereIn("id",$forgivenes)->update(['status_reversion'=>-1]);
    }

    $forgiveness_ids = $params["forgiveness_ids"]; unset($params['forgiveness_ids']);
    if($forgiveness_ids!=null){//cuando la condonacion se dio para varios puestos
      $ids=explode(",",$forgiveness_ids);
      for($i=0; $i<sizeof($ids); $i++){
        $forgivenes_generic = \App\Models\Forgiveness::find($ids[$i]);
        //dd(isset($forgivenes_generic['status_reversion']));
        if($forgivenes_generic['status_reversion']==''){
          \App\Models\Forgiveness::where('id',$forgivenes_generic['id'])->update(['status_reversion'=>$params['stand_id']]);//asignamos el id del puesto
        }else{
          $ids_stands=explode(",",$forgivenes_generic['status_reversion']);
          $is_anadir_ids_puestos=true;
          for($j=0;$j<sizeof($ids_stands);$j++){
            if($ids_stands[$j]==$params['stand_id']){
              $is_anadir_ids_puestos=false;
            }
          }
          if($is_anadir_ids_puestos==true){
            \App\Models\Forgiveness::where('id',$forgivenes_generic['id'])->update(['status_reversion'=>$forgivenes_generic['status_reversion']=$forgivenes_generic['status_reversion'].','.$params['stand_id']]);
          }
        }
        //DB::table("forgivenesses")->whereIn("id",$ids)->update(['fractionary_id'=>$fracctionary_id]);//falta ver la forma como actualizarlo
      }
    }

     $o=parent::save($params);
     $o->debts()->sync($debt_ids);
    //poner a cero y cambiar titular
    \App\Models\Debt::whereIn("id",$debt_ids)->update(['previous_total'=>DB::raw('total'), 'previous_client_id'=>DB::raw('client_id'),'client_id'=>Util::$MUNI_PERSON_ID]);
    \App\Models\Stand::find($params['stand_id'])->update(['previous_titular_id'=>DB::raw('titular_id'),'titular_id'=>Util::$MUNI_PERSON_ID]);
    return $o;
   }

   public function get($id) {
    return $this->model->find($id);
   }

   public function delete($id){
     $o=$this->model->find($id);
     $debts=$o->debts;
     $o->update(['status'=>0]);
     $debt_ids=array_map(function($e){ return $e['id']; },$debts->toArray());
     //volver a su precio anterior y regresar al propietario
     //\App\Models\Debt::whereIn("id",$debt_ids)->update(['client_id'=>DB::raw('previous_client_id'), 'total'=>DB::raw('previous_total'),'previous_total'=>null, 'previous_client_id'=>'']);
    // \App\Models\Stand::where("id",$o->stand_id)->update(['titular_id'=>DB::raw('previous_titular_id'),'previous_titular_id'=>'']);
     \App\Models\Debt::whereIn("id",$debt_ids)->update(['client_id'=>DB::raw('previous_client_id'), 'total'=>DB::raw('previous_total'),'previous_total'=>null]);
     \App\Models\Stand::where("id",$o->stand_id)->update(['titular_id'=>DB::raw('previous_titular_id')]);
     return $o;
   }
 
}