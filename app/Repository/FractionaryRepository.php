<?php

namespace App\Repository;

use App\Repository\FractionaryRepositoryI;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\DB;

class FractionaryRepository extends BaseRepository implements FractionaryRepositoryI{
  //private $fractionary_merced_sisa_kiosko; 
  private $debt_repository;
  public function __construct(\App\Models\Fractionary $model,\App\Repository\DebtRepository $debt_repository){
       parent::__construct($model);
       $this->debt_repository = $debt_repository;
      //  foreach(\App\Util::$concepts as $id=>$concept){
      //   if( preg_match("/MRCD|SISA|BAJP|MULD/",$concept) )
      //   $this->fractionary_merced_sisa_bajapolicia[]=$id;
      //  }
   }
   public function all_v2($params){

     $sql_count_expired_debts = "SELECT count(debts.id) FROM fractionary_schedules s INNER JOIN debts ON  debts.fractionary_schedule_id = s.id WHERE debts.status=1 AND s.fractionary_id = fractionaries.id AND payment_status=0 AND debts.date_at < now() ";
    $sql_count_coactive_debts = "SELECT count(debts.id) FROM debts INNER JOIN coactives co ON co.debt_id=debts.id INNER JOIN fractionary_schedules s ON debts.fractionary_schedule_id = s.id WHERE debts.status=1 AND s.fractionary_id = fractionaries.id AND payment_status=0 AND debts.coactive_id>0 AND co.description_finish ='' ";


    $columsToGet="fractionaries.* ,($sql_count_expired_debts) as expired_debts,($sql_count_coactive_debts) as coactive_debts ";

    if(isset($params['export'])){
      $columsToGet="fractionaries.*,people.name as titular1_name, people.lastname as titular1_lastname, people.dni as titular1_dni,
                    p2.name as representative_name,p2.lastname as representative_lastname,p2.dni as representative_dni,
                    stands.code as stand_code,stands.name as stand_name,($sql_count_expired_debts) as expired_debts";
        unset($params['export']);
    }

    $conditions=$params;
   
     //$conditions['status']=1;
     //$q= DB::table('fractionaries')

     $q= $this->model->with("fractionary_type")->with("concept")->with("titular")
     //->with("representative")
     ->with("titular2")
     ->join("people","people.id","=","fractionaries.titular_id")

         ->leftJoin('people as p2', 'p2.id', '=', 'fractionaries.representative_id')
        ->leftJoin('stands','stands.id','=','fractionaries.stand_id')

      // ->join("people as p2","p2.id","=","fractionaries.representative_id")
     ->selectRaw($columsToGet);//->where($conditions);
     if(isset($params['status'])) {
       $q->where("fractionaries.status",$params['status']);
     }
     if(isset($params['fractionary_type_id'])) {
      $q->where("fractionaries.fractionary_type_id",$params['fractionary_type_id']);
    }
     if(isset($params['expired_debts'])){
      //  $q->having("expired_debts",'>',0);//mejor solucion pero no funciona esta version de laravel
       $q->whereRaw("($sql_count_expired_debts) > 0");
     }
     if(isset($params['coactive_debts'])){
      //$q->havingRaw("coactive_debts > ?",[0]);
      $q->whereRaw("($sql_count_coactive_debts) > 0 ");
     }
     if(isset($params['search'])) {
      $q->whereRaw("concat(fractionaries.code, people.name,if(people.lastname is null,'',people.lastname),if(people.dni is null,'',people.dni)) like '%".$params['search']."%'");
      }
      if(isset($params['date_init']) and isset($params['date_end'])) {
        $q->whereBetween('date_start', [$params['date_init']." 00:00:00", $params['date_end']." 23:50:00"]);
      }
      if(isset($params['no_paginate'])){
         $q->orderBy("fractionaries.date_start", "desc");
       return $q->get();
      }
      //return json_encode($q->toSql());
      return $q->latest()->paginate(10);
     
   }
   public function list_print_fractionaries($params){
    //dd($params);
  
/*//status: 2 TERMINADO
//ANULADO 0
VIGENTE 1*/
  
    $q= $this->model->join('people as p1', 'p1.id', '=', 'fractionaries.titular_id')
        ->leftJoin('people as p2', 'p2.id', '=', 'fractionaries.representative_id')
        ->leftJoin('stands','stands.id','=','fractionaries.stand_id')
        ->selectRaw('fractionaries.*,p1.name as titular1_name, p1.lastname as titular1_lastname, p1.dni as titular1_dni,
                    p2.name as representative_name,p2.lastname as representative_lastname,p2.dni as representative_dni,
                    stands.code as stand_code,stands.name as stand_name');
       // ->where($params);
    if(isset($params['status'])) {
      $q->where("fractionaries.status",$params['status']);
    }
   
    if(isset($params['fractionary_type_id'])) {
     $q->where("fractionaries.fractionary_type_id",$params['fractionary_type_id']);
    }
    if(isset($params['date_init']) and isset($params['date_end'])) {
      $q->whereBetween('date_at', [$params['date_init']." 00:00:00", $params['date_end']." 23:50:00"]);
    }
    if(isset($params['search'])) {
      $q->whereRaw("concat(fractionaries.code, people.name,if(people.lastname is null,'',people.lastname),if(people.dni is null,'',people.dni)) like '%".$params['search']."%'");
    }
    return $q->get();
    //dd($q->get());
   }
   public function all($params){
     $conditions=$params;
     //$conditions['status']=1; 
     return $this->model->with("fractionary_type")->with("concept")->with("titular")->with("representative")->where($conditions)->latest()->paginate(10);
   }

   public function schedule($params){
    $conditions=$params;
    $conditions['status']=1;
    return \App\Models\FractionarySchedule::where($conditions)->with("debt")->get();
  }
  public function listSchedule($fractionary_id){
    return \App\Models\FractionarySchedule::with("debt")->where("fractionary_id",$fractionary_id)->get();
  }
  public function forgiveDebts($params){
    $percent=$params[1];
    foreach ($params[0] as $fractionary_id) {//recorre los fraccionamiento
      $debts_id=\App\Models\FractionarySchedule::select('debt_id')->where('fractionary_id','=',$fractionary_id)->get();
      foreach ($debts_id as $debt_id) {
        if($percent==100){
          \App\Models\Debt::where('payment_status', '!=' , 1)->where("id",$debt_id['debt_id'])->update(['payment_date'=>date("Y-m-d h:m:s"),'payment_status'=>1,'previous_total'=>DB::raw('total'), 'total'=>DB::raw("total-(total*$percent/100)")]);
        }else{//solo reduce la deuda en un porcentaje
          \App\Models\Debt::where('payment_status', '!=' , 1)->where("id",$debt_id['debt_id'])->update([ 'previous_total'=>DB::raw('total'), 'total'=>DB::raw("IF (payment >=(total-(total*$percent/100)), payment , total-(total*$percent/100))")]);
        }
      }
      if($percent==100){
        $this->model->where('id',$fractionary_id)->update(['status'=>2]);//asigando dos terminado 
      }
    }
  }
  public function save($params){
    try {
      DB::beginTransaction();
      if(array_key_exists("schedule",$params) ){
        $schedule = $params['schedule'];
        unset($params["schedule"]);
       }
       if( !array_key_exists('id',$params) ) $params['code'] = $this->create_code($params['fractionary_type_id']);
       $o=parent::save($params);
        //$stand = isset($params['stand_id'])? \App\Models\Stand::find($params['stand_id']):null;
        $stand = isset($params['stand_id'])? \App\Models\Stand::where("id",$params['stand_id'])->with('market')->first():null;
			  $concept = \App\Models\Concept::find($params['concept_id']);
        
       if($schedule && !array_key_exists('id',$params) ) { //nuevo frac.
        $params['id']=$o->id;
         foreach($schedule as $i=>$item){
           if( !array_key_exists("id",$item) ){
            //guardar el schedule y adjuntarle una deuda.
            $is_last_debt=false;
            if($i==count($schedule)-1) $is_last_debt =  true;
            $debt=$this->add_debt($stand,$concept,$params,$item,$is_last_debt);
            $item['debt_id']=$debt->id;
            $item['fractionary_id']=$o->id;
            $s=\App\Models\FractionarySchedule::create($item);
            $debt->update(["fractionary_schedule_id"=>$s->id]);
            if( strlen($params['reference_id']) > 0 ){
              $this->attach_fractionary_to_debt($params['reference_id'],$o->id);
            }
            // if( in_array($params['fractionary_type_id'],$this->fractionary_merced_sisa_kiosko) ){
            //   $this->attach_fractionary_to_debt($debt,$o->id);
            // }
           }
         }
       }
       DB::commit();
       return $o;
    } catch (\Exception $e) {
      DB::rollBack();
      throw $e;
    }
          
  }
  private function create_code($fractionary_type_id){
    //$format = "CF-{concept}-{number}-{year}-SGRT";
    $year=date("Y");
    $type=\App\Models\FractionaryType::find($fractionary_type_id);   
    $last_fractionary=$this->model->where([["fractionary_type_id",$fractionary_type_id],['code','like',"CF-".$type->code.'-%-'.$year."-SGRT"]])->latest('id')->first();
    $new_code = "CF-".$type->code.'-';
    $new_number = 1;
    if(!is_null($last_fractionary)){
      $code = explode("-",$last_fractionary->code); 
      $new_number= (int) $code[2]+1;
    }
    $new_number=sprintf('%04d', $new_number);
    $new_code.="$new_number-$year-SGRT";
    return $new_code;
  }
  /**
   * Crea la referencia entre la deuda y el fraccionamiento, 
   * para sabe que meses estan en fraccionamiento y tengan una bandera en reportes de Merced o Baja Policia.
   */
  private function attach_fractionary_to_debt($reference_ids, $fracctionary_id){
    //$debt->update(["fractionary_id"=>$fracctionary_id]);
    $ids=explode(",",$reference_ids);
    DB::table("debts")->whereIn("id",$ids)->update(['fractionary_id'=>$fracctionary_id]);
  }
  /**
   * Crea una deuda asignada a cada item del cronograma. asi poder agregar las deudas por fraccionamiento a caja o otras vistas.
   */
  private function add_debt($stand,$concept,$fractionary_data,$schedule, $is_last_debt){
    preg_match("/\w+-\w+-(.+)-\d+-\w+/",$fractionary_data['code'],$f_code);

    $debt = [
      'date_at'=>$schedule['date_at'],
      'concept_id'=>$fractionary_data['concept_id'],
      'client_id'=>$fractionary_data['titular_id'],
      'code'=> $this->debt_repository->newCodeMonthly($stand,$concept,new \DateTime($schedule['date_at']),"-F.".$f_code[1]),
      'stand_id'=>$fractionary_data['stand_id']??null,
      'type'=>'FRA',
      'total'=>$schedule['total'],
      'fractionary_schedule_id'=>$fractionary_data['id'],
      'user_id'=>$fractionary_data['user_id'],
      'ref_code'=>$fractionary_data['code'],
      'is_last_schedule_debt'=>$is_last_debt
    ];
    $d = \App\Models\Debt::create($debt);
    return $d;
  }

  /*public function destroy($id){
    $o=$this->model->find($id);
    $schedules = \App\Models\FractionarySchedule::where('fractionary_id',$o->id)->get()->toArray();

    if(!is_null($o->reference_id)) {
      $debts_id = explode(",",$o->reference_id);
      dd($debts_id);
      \App\Models\Debt::whereIn('id',$debts_id)->update(['fractionary_id'=>null]);
    }

    $fractionary_debts_id = array_map(function($item){return $item['debt_id'];},$schedules);
    \App\Models\Debt::whereIn('id',$fractionary_debts_id)->update(['status'=>0]);
    
    $o->update(["status"=>0]);
    
    return $o->id;
  }*/
  public function activateDeactivate($id){
    $o=$this->model->find($id);
    $schedules = \App\Models\FractionarySchedule::where('fractionary_id',$o->id)->get()->toArray();
    if($o->status===1){
      if(!is_null($o->reference_id)) {
        $debts_id = explode(",",$o->reference_id);
        \App\Models\Debt::whereIn('id',$debts_id)->update(['fractionary_id'=>null]);
      }
      
      // Actualizando deudas de fraccionamiento
      $fractionary_debts_id = array_map(function($item){return $item['debt_id'];},$schedules);
      \App\Models\Debt::whereIn('id',$fractionary_debts_id)->update(['status'=>0]);
      
      $o->update(["status"=>0]);
    }else{
      if(!is_null($o->reference_id)) {
        $debts_id = explode(",",$o->reference_id);
        \App\Models\Debt::whereIn('id',$debts_id)->update(['fractionary_id'=>$o->id]);
      }
      
      // Actualizando deudas de fraccionamiento
      $fractionary_debts_id = array_map(function($item){return $item['debt_id'];},$schedules);
      \App\Models\Debt::whereIn('id',$fractionary_debts_id)->update(['status'=>1]);//las deudas generadas al crear un fraccionamiento se activan
      
      $o->update(["status"=>1]); //activando el fraccionamiento
    }
    return $o->id;
  }

  public function checkByDebt($schedule_id) {
    $pending_debts = \App\Models\Debt::join("fractionary_schedules",'fractionary_schedules.debt_id','=','debts.id')
            ->whereRaw(" fractionary_schedules.fractionary_id = (select fs.fractionary_id from fractionary_schedules fs where fs.id=$schedule_id) ")
            ->where("debts.status",1)
            ->where("debts.payment_status",0)->get();
    return count($pending_debts);
  }

  public function finishByDebt($count_pending_debts,$schedule_id){
    if($count_pending_debts==0){ //no hay deudas anteriores pendientes en el fracc.
      $schedule = \App\Models\FractionarySchedule::find($schedule_id);
      $this->model::where("id",$schedule->fractionary_id)->update(['status'=>2]);
    }
  }  
}