<?php

namespace App\Repository;

use App\Repository\ResponsabilityRepositoryI;
use Illuminate\Support\Facades\Storage;

class ResponsabilityRepository extends BaseRepository implements ResponsabilityRepositoryI{
   public function __construct(\App\Models\Responsability $model){
       parent::__construct($model);
   }

   public function all($params){
    $conditions=$params;
    return $this->model->where($conditions)->with('sector')->get();
   }

   public function save($params){
    foreach ($params as $clave => $element) {
     if(isset($element['id'])){
      $o = $this->model->find($element['id']);
      $o->update($element);
     }else{
      $this->model->insert($element);
     }
    }
    /*if($this->multiKeyExists($params,'id')){
      foreach ($params as $item) {
        $o = $this->model->find($item['id']);
        $o->update($item);
      }
    }else{
        $o = $this->model->insert($params);
    }
  */
    return $o;
   }
   
  /* private function multiKeyExists( Array $array, $key ) {
    if (array_key_exists($key, $array)) {
        return true;
    }
    foreach ($array as $k=>$v) {
        if (!is_array($v)) {
            continue;
        }
        if (array_key_exists($key, $v)) {
            return true;
        }
    }
    return false;
  }
*/
   public function get($id) {
     return $this->model->find($id);
   }
 
}
